/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.views;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import de.tarent.kaliko.components.models.CalendarViewModel;


/**
 * 
 * Interface for an view on a calendar, e.g. month-view, week-view, day-view...
 *
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface CalendarView {
	
	public final static int DAY_VIEW = 0;
	public final static int WEEK_VIEW = 1;
	public final static int MONTH_VIEW = 2;
	public final static int LIST_VIEW = 3;
	
	public static int DAY_TYPE_DEFAULT = 0;
	public static int DAY_TYPE_OUT_OF_SCOPE = 1;
	public static int DAY_TYPE_TODAY = 2;

	public CalendarViewModel getModel();
	public int getType();
	
	/**
	 * Set the menu which will be shown when the user
	 * requests a context-action on an event 
	 * @param menu
	 */
	public void setContextMenuForEvent(ContextMenu menu);
	
	/**
	 * Set the menu which will be shown when the user
	 * requests a context-action on a place in the calendar-view
	 * without an event 
	 * @param the context-menu or null if none is defined
	 */
	public void setContextMenuForSpace(ContextMenu menu);
	
	
	/**
	 * Returns the menu which will be shown when the user
	 * requests a context-action on an event 
	 * @param 
	 */
	public ContextMenu getContextMenuForEvent();
	
	/**
	 * Returns the menu which will be shown when the user
	 * requests a context-action on a place in the calendar-view
	 * without an event.
	 * 
	 * @return the context-menu or null if none is defined
	 */
	public ContextMenu getContextMenuForSpace();
}
