/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.data.impl;

import java.util.Calendar;
import java.util.List;

import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.data.CalendarDataSource;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.CalendarEventFilter;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DefaultCalendarDataManager implements CalendarDataManager {

	protected CalendarDataSource calendarDataSource;
	
	/**
	 * Whether the data-manager should load data which is currently not needed but may be requested by the user shortly
	 * For clients with unlimited connection-traffic this may be a attractive option whereas an mobile device should keep the
	 * amount of transfered data low in order to avoid unnecessary costs 
	 */
	protected boolean precaching;
	
	public DefaultCalendarDataManager(CalendarDataSource calendarDataSource) {
		this.calendarDataSource = calendarDataSource;
	}

	public void loadEvents(Calendar begin, Calendar end, List<Category> categories) {
		calendarDataSource.getEvents(begin, end, categories);
	}

	public void addCalendarEvent(Event event, List<Category> categories) {
		calendarDataSource.addCalendarEvent(event, categories);
	}
	
	public void loadCategories() {
		calendarDataSource.getCategories();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#updateCalendarEvent(de.tarent.libraries.calendar.Event)
	 */
	public void updateCalendarEvent(Event event) {
		calendarDataSource.updateCalendarEvent(event);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#deleteEvent(de.tarent.libraries.calendar.Event)
	 */
	public void deleteEvent(Event event) {
		calendarDataSource.deleteEvent(event);	
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#doesPrecaching()
	 */
	public boolean doesPrecaching() {
		return precaching;
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#setPrecaching(boolean)
	 */
	public void setPrecaching(boolean doPrecaching) {
		precaching = doPrecaching;
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#loadEvents(de.tarent.libraries.calendar.impl.CalendarEventFilter)
	 */
	public void loadEvents(CalendarEventFilter filter) {
		calendarDataSource.getEvents(filter);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getDatabaseDescription()
	 */
	public String getDatabaseDescription() {
		return calendarDataSource.getDatabaseDescription();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getServerDescription()
	 */
	public String getServerDescription() {
		return calendarDataSource.getServerDescription();
	}
	
	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#getServerVersion()
	 */
	public String getServerVersion() {
		return calendarDataSource.getServerVersion();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getUsername()
	 */
	public String getUsername() {
		return calendarDataSource.getUsername();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#getCalendarDataSource()
	 */
	public CalendarDataSource getCalendarDataSource() {
		return calendarDataSource;
	}
}
