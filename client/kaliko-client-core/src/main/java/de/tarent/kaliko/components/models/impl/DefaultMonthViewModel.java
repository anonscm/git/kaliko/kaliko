/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.models.impl;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.models.MonthViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.utils.Messages;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultMonthViewModel implements MonthViewModel {
	
	protected Locale locale;
	protected Calendar calendar;
	protected CalendarApplicationController controller;
	
	public DefaultMonthViewModel(Locale locale, Calendar calendar, CalendarApplicationController controller) {
		this.locale = locale;
		this.calendar = calendar;
		this.controller = controller;
	}

	public void gotoNext(Calendar calendar) {
		calendar.add(Calendar.MONTH, 1);
	}

	public void gotoPrevious(Calendar calendar) {
		calendar.add(Calendar.MONTH, -1);
	}

	public String getCurrentScopeName() {
		// some more convenient names (its not so important which calendar-week is today!)
		// TODO should be configurable
		
		if(CalendarUtils.isCurrentMonth(getCalendar(), getLocale()))
			return Messages.getString("DefaultMonthViewModel_CurrentMonth");
		else if(CalendarUtils.isNextMonth(getCalendar(), getLocale()))
			return Messages.getString("DefaultMonthViewModel_NextMonth");
		else if(CalendarUtils.isLastMonth(getCalendar(), getLocale()))
			return Messages.getString("DefaultMonthViewModel_LastMonth");
		
		// This is Java6 only
		//return getCalendar().getDisplayName(Calendar.MONTH, Calendar.LONG, getLocale()) +
		
		// But we want to be compatible to Java5
		return new DateFormatSymbols(getLocale()).getMonths()[getCalendar().get(Calendar.MONTH)] +
		" " +
		getCalendar().get(Calendar.YEAR);
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public Locale getLocale() {
		return locale;
	}
	
	public CalendarApplicationController getController() {
		return controller;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeBegin()
	 */
	public Calendar getScopeBegin() {
		return firstDisplayedDay();
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeEnd()
	 */
	public Calendar getScopeEnd() {
		// A Calendar object pointing to the last displayed day. (This view shows 6 x 7 = 42 day-cells).
		Calendar end = (Calendar)getScopeBegin().clone();
		end.add(Calendar.DAY_OF_YEAR, 41);

		end.set(Calendar.HOUR_OF_DAY, 23);
		end.set(Calendar.MINUTE, 59);
		end.set(Calendar.SECOND, 60);
		end.set(Calendar.MILLISECOND, 999);
		
		return end;
	}
	
	/**
	 * 
	 * Creates a Calendar-object which specifies the first day of the current month
	 * 
	 * @return
	 */

	public Calendar firstDayOfMonth() {
		Calendar tempCalendar = (Calendar)getCalendar().clone();
		
		/*
		 * The following line is a workaround for bug #183 ("Calendar of month-view hangs when activating "goto today" or clicking on days of previous/next month",
		 * https://evolvis.org/tracker/index.php?func=detail&aid=183&group_id=31&atid=181) which actually seems to be a bug in Sun Java (not verified though).
		 * 
		 * Without the line the tempCalendar seems to 'hang' sometimes
		 * 
		 */
		tempCalendar.getTime();
		
		tempCalendar.set(Calendar.DAY_OF_MONTH, 1);
		tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
		tempCalendar.set(Calendar.MINUTE, 0);
		tempCalendar.set(Calendar.SECOND, 0);
		tempCalendar.set(Calendar.MILLISECOND, 0);
		return tempCalendar;

	}

	/**
	 * 
	 * Creates a Calendar-object which scpecifies the first day displayed in this view 
	 * 
	 * @return
	 */

	public Calendar firstDisplayedDay() {

		// create a temporary calendar-object which specifies the first day of the month
		Calendar tempCalendar = firstDayOfMonth();

		// if first day of month is a monday, add a week of the last month to the view (for convenient switches between months)
		if(tempCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			tempCalendar.add(Calendar.WEEK_OF_YEAR, -1);
		
		/* 
		 * convert the day of the week (which is relative to the first day of the week) to the actual (localized)
		 * first day of the week (which should be displayed)
		 */
		int dayOfWeek = tempCalendar.get(Calendar.DAY_OF_WEEK) - (Calendar.getInstance(getLocale()).getFirstDayOfWeek() - 1);
		// 0 is not defined
		if(dayOfWeek == 0)
			dayOfWeek = 7;

		// next, go to the monday of this week (because this will be the first day which is displayed)
		tempCalendar.add(Calendar.DAY_OF_YEAR, -(dayOfWeek-1));

		return tempCalendar;
	}
}