/**
 * 
 */
package de.tarent.kaliko.components.utils;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class StringUtils {

	/**
	 * Returns the given text trimmed to a maximum character-amount of a given length.
	 * 
	 * If the length of the given text is longer than <code>length</code>, the text is trimmed and three dots are appended.
	 * 
	 * @param text
	 * @param length
	 * @return
	 */
	public static String trimToLength(String text, int length) {
		if(text.length() <= length)
			return text;
		
		return text.substring(0, length) + "...";
	}
}
