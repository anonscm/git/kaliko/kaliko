/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.models.impl;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.models.DayViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.utils.Messages;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultDayViewModel implements DayViewModel {

	protected Calendar calendar;
	protected Locale locale;
	protected CalendarApplicationController controller;
	
	public DefaultDayViewModel(Locale locale, Calendar calendar, CalendarApplicationController controller) {
		this.locale = locale;
		this.calendar = calendar;
		
		this.controller = controller;
	}
	
	public void gotoNext(Calendar calendar) {
		calendar.add(Calendar.DAY_OF_MONTH, 1);
	}

	public void gotoPrevious(Calendar calendar) {
		calendar.add(Calendar.DAY_OF_MONTH, -1);
	}

	public String getCurrentScopeName() {
		// some more convenient names (its not so important which calendar-day is today!)
		// TODO should be configurable
		
		if(CalendarUtils.isToday(getCalendar(), getLocale()))
			return Messages.getString("DefaultDayViewModel_Today");
		else if(CalendarUtils.isTomorrow(getCalendar(), getLocale()))
			return Messages.getString("DefaultDayViewModel_Tomorrow");
		else if(CalendarUtils.isYesterday(getCalendar(), getLocale()))
			return Messages.getString("DefaultDayViewModel_Yesterday");
			
		return new DateFormatSymbols(getLocale()).getWeekdays()[calendar.get(Calendar.DAY_OF_WEEK)] + ", " + calendar.get(Calendar.DAY_OF_MONTH) +
		" " +
		// This is Java6 only
		//calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, locale) +
		// But we want to be compatible to Java5
		new DateFormatSymbols(getLocale()).getShortMonths()[getCalendar().get(Calendar.MONTH)] +
		" " +
		calendar.get(Calendar.YEAR);
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public Locale getLocale() {
		return locale;
	}

	public CalendarApplicationController getController() {
		return controller;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeBegin()
	 */
	public Calendar getScopeBegin() {
		Calendar begin = (Calendar)getCalendar().clone();
		begin.set(Calendar.HOUR_OF_DAY, 0);
		begin.set(Calendar.MINUTE, 0);
		begin.set(Calendar.SECOND, 0);
		begin.set(Calendar.MILLISECOND, 0);
		return begin;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeEnd()
	 */
	public Calendar getScopeEnd() {
		Calendar end = (Calendar)getCalendar().clone();
		end.set(Calendar.HOUR_OF_DAY, 23);
		end.set(Calendar.MINUTE, 59);
		end.set(Calendar.SECOND, 59);
		end.set(Calendar.MILLISECOND, 999);
		return end;
	}
}
