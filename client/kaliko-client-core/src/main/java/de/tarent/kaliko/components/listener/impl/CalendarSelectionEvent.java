/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.listener.impl;

import java.util.Calendar;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CalendarSelectionEvent {

	protected Calendar selectedDay;
	protected Calendar oldSelectedDay;
	
	public CalendarSelectionEvent(Calendar oldSelectedDay, Calendar selectedDay) {
		this.selectedDay = selectedDay;
		this.oldSelectedDay = oldSelectedDay;
	}
	
	public Calendar getSelectedDay() {
		return selectedDay;
	}
	
	public Calendar getOldSelectedDay() {
		return oldSelectedDay;
	}
}
