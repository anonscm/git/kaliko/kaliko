/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.models.impl;

import java.util.Calendar;
import java.util.Locale;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.models.ListViewModel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultListViewModel implements ListViewModel {

	protected Calendar calendar;
	protected Locale locale;
	protected CalendarApplicationController controller;
	
	public DefaultListViewModel(Locale locale, Calendar calendar, CalendarApplicationController controller) {
		this.locale = locale;
		this.calendar = calendar;
		
		this.controller = controller;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getCalendar()
	 */
	public Calendar getCalendar() {
		return calendar;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getController()
	 */
	public CalendarApplicationController getController() {
		return controller;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getCurrentScopeName()
	 */
	public String getCurrentScopeName() {
		return "";
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getLocale()
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#gotoNext(java.util.Calendar)
	 */
	public void gotoNext(Calendar calendar) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#gotoPrevious(java.util.Calendar)
	 */
	public void gotoPrevious(Calendar calendar) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeBegin()
	 */
	public Calendar getScopeBegin() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.kaliko.components.models.CalendarViewModel#getScopeEnd()
	 */
	public Calendar getScopeEnd() {
		// TODO Auto-generated method stub
		return null;
	}
}
