/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.utils;

import java.util.Calendar;
import java.util.Locale;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CalendarUtils {
	
	public final static int MINUTES_PER_HOUR = 60;
	public final static int MINUTES_PER_DAY = MINUTES_PER_HOUR * 24;
	public final static int MINUTES_PER_WEEK = MINUTES_PER_DAY * 7;
	public final static int MINUTES_PER_YEAR = MINUTES_PER_DAY * 365;
	
	public static boolean isToday(Calendar calendar, Locale locale) {
		return isSameDay(Calendar.getInstance(locale), calendar);
	}
	
	public static boolean isTomorrow(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.DAY_OF_YEAR, 1);
		return isSameDay(tempCalendar, calendar);
	}
	
	public static boolean isYesterday(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.DAY_OF_YEAR, -1);
		return isSameDay(tempCalendar, calendar);
	}
	
	public static boolean isCurrentWeek(Calendar calendar, Locale locale) {
		return isSameWeek(Calendar.getInstance(locale), calendar);
	}
	
	public static boolean isLastWeek(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.WEEK_OF_YEAR, -1);
		return isSameWeek(tempCalendar, calendar);
	}
	
	public static boolean isNextWeek(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.WEEK_OF_YEAR, 1);
		return isSameWeek(tempCalendar, calendar);
	}
	
	public static boolean isCurrentMonth(Calendar calendar, Locale locale) {
		return isSameMonth(Calendar.getInstance(locale), calendar);
	}
	
	public static boolean isLastMonth(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.MONTH, -1);
		return isSameMonth(tempCalendar, calendar);
	}
	
	public static boolean isNextMonth(Calendar calendar, Locale locale) {
		Calendar tempCalendar = Calendar.getInstance(locale);
		tempCalendar.add(Calendar.MONTH, 1);
		return isSameMonth(tempCalendar, calendar);
	}
	
	public static boolean isSameDay(Calendar oneCalendar, Calendar anotherCalendar) {
		return isFieldEqual(oneCalendar, anotherCalendar, Calendar.DAY_OF_YEAR);
	}
	
	public static boolean isSameWeek(Calendar oneCalendar, Calendar anotherCalendar) {
		return isFieldEqual(oneCalendar, anotherCalendar, Calendar.WEEK_OF_YEAR);
	}
	
	public static boolean isSameMonth(Calendar oneCalendar, Calendar anotherCalendar) {
		return isFieldEqual(oneCalendar, anotherCalendar, Calendar.MONTH);
	}
	
	public static boolean isFieldEqual(Calendar oneCalendar, Calendar anotherCalendar, int field) {
		return oneCalendar.get(Calendar.YEAR) == anotherCalendar.get(Calendar.YEAR) && oneCalendar.get(field) == anotherCalendar.get(field);
	}
	
	public static String getTimeAsString(int hours, int minutes) {
		return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
	}
	
	public static String getTimeAsString(Calendar calendar) {
		return getTimeAsString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
	}
	
	/**
	 * TODO support for different locales
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getDateAsString(int year, int month, int day) {
		return  (day <= 9 ? "0" : "") + day + "." +  (month <= 9 ? "0" : "") + month + "." + year;
	}
	
	public static String getDurationAsString(int minutes) {
		return minutes + " Minutes"; //$NON-NLS-1$
	}
	
	public static String getDateAsString(Calendar calendar) {
		return getDateAsString(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1 , calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	public static boolean rangeContains(Calendar rangeBegin, Calendar rangeEnd, Calendar eventBegin, Calendar eventEnd)  {
		return !(eventEnd.compareTo(rangeBegin) < 0 || eventBegin.compareTo(rangeEnd) > 0 );
	}
}
