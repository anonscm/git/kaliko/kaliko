/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.controller.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import de.tarent.kaliko.components.controller.EventSelectionModel;
import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultEventSelectionModel implements EventSelectionModel {

	protected List<Event> selectedEvents;
	protected List<EventSelectionListener> listeners;
	
	protected final static Logger logger = Logger.getLogger(DefaultEventSelectionModel.class.getName());
	
	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#addEventSelectionListener(de.tarent.kaliko.components.listener.EventSelectionListener)
	 */
	public synchronized void addEventSelectionListener(EventSelectionListener listener) {
		getEventSelectionListeners().add(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#addSelectedEvent(de.tarent.kaliko.objects.Event)
	 */
	public synchronized void addSelectedEvent(Event event) {
		getSelectedEvents().add(event);
		fireEventSelectionChanged();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#getSelectedEvents()
	 */
	public synchronized List<Event> getSelectedEvents() {
		if(selectedEvents == null)
			selectedEvents = new ArrayList<Event>();
		
		return selectedEvents;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#removeEventSelectionListener(de.tarent.kaliko.components.listener.EventSelectionListener)
	 */
	public synchronized void removeEventSelectionListener(EventSelectionListener listener) {
		getEventSelectionListeners().remove(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#removeSelectedEvent(de.tarent.kaliko.objects.Event)
	 */
	public synchronized void removeSelectedEvent(Event event) {
		getSelectedEvents().remove(event);
		fireEventSelectionChanged();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#setSelectedEvents(java.util.List)
	 */
	public synchronized void setSelectedEvents(List<Event> events) {
		this.selectedEvents = events;
		fireEventSelectionChanged();
	}	
	
	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#setSelectedEvent(de.tarent.kaliko.objects.Event)
	 */
	public synchronized void setSelectedEvent(Event event) {
		List<Event> events = new ArrayList<Event>(1);
		events.add(event);
		setSelectedEvents(events);
	}

	public synchronized List<EventSelectionListener> getEventSelectionListeners() {
		if(listeners == null)
			listeners = new ArrayList<EventSelectionListener>();
		
		return listeners;
	}

	protected synchronized void fireEventSelectionChanged() {
		logger.fine("eventSelectionChanged");
		EventSelectionEvent event = new EventSelectionEvent(getSelectedEvents());
		
		Iterator<EventSelectionListener> it = getEventSelectionListeners().iterator();
		
		while(it.hasNext())
			it.next().eventSelectionChanged(event);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#requestSelection(de.tarent.kaliko.objects.Event, boolean)
	 */
	public synchronized void requestSelection(Event event, boolean add) {
		if(!getSelectedEvents().contains(event)) {		
			if(!add)
				getSelectedEvents().clear();
			
			getSelectedEvents().add(event);
		} else {
			if(!add)
				getSelectedEvents().clear();
			else
				getSelectedEvents().remove(event);
		}
		
		fireEventSelectionChanged();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#requestSelection(de.tarent.kaliko.objects.Event)
	 */
	public synchronized void requestSelection(Event event) {
		this.requestSelection(event, false);		
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#clearSelection()
	 */
	public synchronized void clearSelection() {
		getSelectedEvents().clear();
		fireEventSelectionChanged();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.EventSelectionModel#eventsSelected()
	 */
	public boolean eventsSelected() {
		return getSelectedEvents().size() > 0;
	}
}
