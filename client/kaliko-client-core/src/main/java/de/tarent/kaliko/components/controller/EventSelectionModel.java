/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.controller;

import java.util.List;

import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.objects.Event;

/**
 * A model handling selection-states of events and notifiying registered listeners about changes
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface EventSelectionModel {

	public void addEventSelectionListener(EventSelectionListener listener);
	public void removeEventSelectionListener(EventSelectionListener listener);
	
	/**
	 * Returns a list of currently selected events
	 * 
	 * @return
	 */
	public List<Event> getSelectedEvents();
	
	public void setSelectedEvents(List<Event> events);
	
	public void setSelectedEvent(Event event);
	
	public void addSelectedEvent(Event event);
	
	public void removeSelectedEvent(Event event);
	
	public void requestSelection(Event event);
	
	public void requestSelection(Event event, boolean add);
	
	public void clearSelection();
	
	public boolean eventsSelected();
}
