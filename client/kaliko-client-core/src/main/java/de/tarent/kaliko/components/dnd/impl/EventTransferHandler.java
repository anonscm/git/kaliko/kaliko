/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.dnd.impl;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.InputEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class EventTransferHandler extends TransferHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 406143813251222936L;
	protected int action;
	
	public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
//		for(int i=0; i < transferFlavors.length; i++)
//			if(transferFlavors[i].equals(EventTransferable.DATA_FLAVOR_CALENDAR_EVENT))
//				return true;
		return false;
	}
	
	protected Transferable createTransferable(JComponent c) {
//		if(c instanceof JList) {
//			Event event = (Event)((JList)c).getSelectedValue();
//			if(action == MOVE)
//				return new EventTransferable(event);
//			else {
//				// we clone this event
//				return new EventTransferable((Event)event.clone());
//			}
//		}
//		
		return null;
	}

	public void exportAsDrag(JComponent comp, InputEvent e, int action) {
		this.action = action;
		super.exportAsDrag(comp, e, action);
	}

	protected void exportDone(JComponent source, Transferable data, int action) {
//		if(action == MOVE) {
//			if(source instanceof JList)
//				try {
//					((EventListModel)((JList)source).getModel()).remove(((EventListModel)((JList)source).getModel()).indexOf(((EventTransferable)data).getTransferData(EventTransferable.DATA_FLAVOR_CALENDAR_EVENT)));
//				} catch (UnsupportedFlavorException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			else if(source instanceof JTable)
				//((EventTableModel)((JTable)source).getModel()).removeRow(0);
				
//		}
	}

	public void exportToClipboard(JComponent comp, Clipboard clip, int action) throws IllegalStateException {
		super.exportToClipboard(comp, clip, action);
	}

	public int getSourceActions(JComponent c) {
		return COPY_OR_MOVE;
	}

	public Icon getVisualRepresentation(Transferable t) {
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/libraries/components/calendar/gfx/x-office-calendar.png")));
	}

	public boolean importData(JComponent comp, Transferable t) {
//		try {
//			Event event = (Event)t.getTransferData(EventTransferable.DATA_FLAVOR_CALENDAR_EVENT);
//			if(event != null) {
//				if(comp instanceof JList) {
//					((EventListModel)((JList)comp).getModel()).addEvent(event);
//					return true;
//				} else if(comp instanceof JTable) {
//					((EventTableModel)((JTable)comp).getModel()).addEvent(event);
//				}
//			}
//		} catch (UnsupportedFlavorException e) {
//			e.printStackTrace();
//			return false;
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
		return false;
	}	
}
