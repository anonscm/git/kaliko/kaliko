/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.utils;

import java.util.Calendar;
import java.util.Locale;

import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class EventUtils {
	
	public static boolean eventsOverlap(Event oneEvent, Event anotherEvent) {
		return(!(oneEvent.getEnd().compareTo(anotherEvent.getBegin()) <= 0 || oneEvent.getBegin().compareTo(anotherEvent.getEnd()) >= 0));
	}
	
	public static Event createEvent(String title, String location, int beginYear, int beginMonth, int beginDay, int beginHour, int beginMinute, int endYear, int endMonth, int endDay, int endHour, int endMinute, boolean allDay) {
		Calendar begin = Calendar.getInstance(Locale.getDefault());
		begin.set(Calendar.YEAR, beginYear);
		begin.set(Calendar.MONTH, beginMonth);
		begin.set(Calendar.DAY_OF_MONTH, beginDay);
		begin.set(Calendar.HOUR_OF_DAY, beginHour);
		begin.set(Calendar.MINUTE, beginMinute);
		
		
		Calendar end = Calendar.getInstance(Locale.getDefault());
		end.set(Calendar.YEAR, endYear);
		end.set(Calendar.MONTH, endMonth);
		end.set(Calendar.DAY_OF_MONTH, endDay);
		end.set(Calendar.HOUR_OF_DAY, endHour);
		end.set(Calendar.MINUTE, endMinute);
		
		return new EventImpl(title, location, begin, end, allDay);
	}
	
	
	public static boolean rangeContainsEvent(Calendar rangeBegin, Calendar rangeEnd, Event event) {
		return CalendarUtils.rangeContains(rangeBegin, rangeEnd, event.getBegin(), event.getEnd());
	}
}
