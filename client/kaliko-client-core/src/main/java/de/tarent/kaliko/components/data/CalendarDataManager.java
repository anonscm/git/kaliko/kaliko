/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.data;

import java.util.Calendar;
import java.util.List;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.CalendarEventFilter;


/**
 * 
 * A CalendarDataManager operates on a CalendarDataSource and is responsible for
 * the effective loading of data in order to allow a "liquid" user-experience.
 * 
 * Therefore it does things like (pre-)caching and pooling.
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface CalendarDataManager  {

	public void setPrecaching(boolean doPrecaching);
	public boolean doesPrecaching();
	public void loadEvents(Calendar begin, Calendar end, List<Category> categories);
	public void loadEvents(CalendarEventFilter filter);
	public void addCalendarEvent(Event event, List<Category> categories);
	public void deleteEvent(Event event);
	public void updateCalendarEvent(Event event);
	public void loadCategories();
	
	public CalendarDataSource getCalendarDataSource();

	public String getServerDescription();
	public String getServerVersion();
	public String getDatabaseDescription();
	public String getUsername();
}
