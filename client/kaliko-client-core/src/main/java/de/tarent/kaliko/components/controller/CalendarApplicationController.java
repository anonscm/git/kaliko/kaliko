/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.components.controller;

import java.awt.Frame;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.evolvis.kaliko.widgets.common.CategorySelector;

import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * 
 * This interface describes a class which operates as a central point for a calendar-application.
 * 
 * Implementing classes manage the locale, category-selection, data-sources, calendar-cursor/-selection and views.
 * It is also responsible for application-wide event-handling. 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface CalendarApplicationController {
	
	public void setCategorySelector(CategorySelector categorySelector);
	
	public CategorySelector getCategorySelector();
	
	public void setViewSwitcher(ViewSwitcher viewSwitcher);
	public ViewSwitcher getViewSwitcher();
	
	public Calendar getCurrentCalendar();
	
	public Locale getLocale();
	
	public void setLocale(Locale locale);
	
	public CalendarDataManager getCalendarDataManager();
	
	public void requestCreateOrEditEvent();
	public void requestNewEvent();
	public void requestNewEvent(Calendar begin, Calendar end);
	public void requestEditEvents(List<Event> events);
	public void requestEditEvent(Event event);
	public void requestDeleteEvents(List<Event> events);
	
	
	/**
	 * Request editing of currently selected events.
	 * Same as requestEditEvents(getSelectedEvents.getSelectedEvents());
	 *
	 */
	public void requestEditSelectedEvents();
	
	/**
	 * Request deletion of currently selected events.
	 * Same as requestDeleteEvents(getEventSelectionModel().getSelectedEvents());
	 *
	 */
	public void requestDeleteSelectedEvents();
	
	public EventSelectionModel getEventSelectionModel();
	
	public void setMainFrame(Frame frame);
	public Frame getMainFrame();
	
	public void setCalendarCursor(int field, int value);
	public void setCalendarCursor(Map<Integer, Integer> fieldsAndValues);
	public void setCalendarCursorTo(Calendar calendar);
	public void moveCalendarCursor(int field, int amount);
	public void moveCalendarCursor(Map<Integer, Integer> fieldsAndValues);
	
	public void addCalendarSelectionListener(CalendarSelectionListener listener);
	public void fireCalendarSelectionChanged(Calendar oldSelectedDay);
	
	public void addCategoryListener(CategoryListener listener);
	public void removeCategoryListener(CategoryListener listener);
	public void fireCategorySelectionChanged(List<Category> newSelection);
	public void fireCategoriesLoaded(List<Category> categories);
	
	public void addCalendarViewListener(CalendarViewListener listener);
	public void fireCalendarViewChange(CalendarView oldView, CalendarView newView);
	
	public void addCalendarDataListener(CalendarDataListener listener);
	public void fireEventsLoaded(List<Event> loadedEvents);
	public void fireEventsAdded(List<Event> events);
	public void fireEventsDeleted(List<Event> events);
	
	public ColorModel getColorModel();
	public IconModel getIconModel();
	
	// Used to store application's main SWT-Shell
	// FIXME move this to a proper location in kaliko-widgets-swt
	public void setShell(Object shell);
	public Object getShell();
}
