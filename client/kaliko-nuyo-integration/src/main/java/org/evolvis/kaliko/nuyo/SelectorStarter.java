/**
 * 
 */
package org.evolvis.kaliko.nuyo;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;

import javax.swing.JFrame;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ConnectionDefinitionWrapper;
import org.evolvis.nuyo.controller.manager.CommonDialogServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.selector.Selector;
import org.evolvis.xana.config.ConnectionDefinition;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;
import org.evolvis.xana.utils.IconFactoryException;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SelectorStarter implements CommonDialogServices {
	
	protected Selector selector;
	protected JFrame frame;
	protected OctopusDatabase database;
	
	public static void main(String[] args) {
		Addresses addresses = new SelectorStarter().showSelectorAndGetAddresses();
		
		System.out.println(addresses.getPkListAsString());
	}

	public SelectorStarter() {
		database = new OctopusDatabase();
		try {
			database.init(OctopusDatabase.createConnection(new ConnectionDefinitionWrapper(new ConnectionDefinition("contact2-bmu-postgres-tnt_demo_test", "http://localhost:8181/contact2-bmu-postgres-tnt_demo_test", "contact2-bmu-postgres-tnt_demo_test"))));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			database.setUser("userAdmin");
			database.setPassword("adminUser");
			database.login();
			database.loadCategories();
			
		} catch(Exception excp) {
			excp.printStackTrace();
		}
		
		ApplicationServices.getInstance().setCurrentDatabase(database);
		ApplicationServices.getInstance().setCommonDialogServices(this);
		try {
			SwingIconFactory.getInstance().addResourcesFolder(Selector.class, "/org/evolvis/nuyo/resources/");
		} catch (IconFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		selector = new Selector();
		frame = new JFrame("nuyo selector");
		frame.getContentPane().add(selector.getComponent());
		
		frame.pack();
	}
	
	public Addresses showSelectorAndGetAddresses() {
		frame.setVisible(true);
		
		final Object o = new Object();
		
		
		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				synchronized (o) {
					o.notify();
				}
			}
		});
		
		synchronized( o )  
		  {
		    try {
		      o.wait();
		    } catch ( InterruptedException e ) {}
		  }
		
		 return selector.getAddresses();
	}

	public int askUser(String arg0, String arg1, String[] arg2, int arg3) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int askUser(String arg0, String[] arg1, int arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int askUser(String arg0, String[] arg1, String[] arg2, int arg3) {
		// TODO Auto-generated method stub
		return 0;
	}

	public JFrame getFrame() {
		// TODO Auto-generated method stub
		return null;
	}

	public void publishError(String arg0, Object[] arg1, Throwable arg2) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, Object[] arg1) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, String arg1, Object[] arg2, Throwable arg3) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, String arg1, Object[] arg2) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, String arg1, Throwable arg2) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0, Throwable arg1) {
		// TODO Auto-generated method stub
		
	}

	public void publishError(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public String requestFilename(String arg0, String arg1, String[] arg2, String arg3, boolean arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setWaiting(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void showInfo(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	public void showInfo(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
