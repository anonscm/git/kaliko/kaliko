/**
 * 
 */
package org.evolvis.kaliko.nuyo;

import java.util.List;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AddressSearchEngine {
	
	public static void main(String[] args) {
		new AddressSearchEngine();
	}

	public AddressSearchEngine() {
		OctopusNuyoConnection conn = new OctopusNuyoConnection();
		
		List categories = conn.getCategoriesObjectList();
		
		for(Object category : categories) {
			System.out.println(category.toString());
		}
	}
}
