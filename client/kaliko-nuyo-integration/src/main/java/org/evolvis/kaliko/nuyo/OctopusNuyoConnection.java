/**
 * 
 */
package org.evolvis.kaliko.nuyo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.xana.config.ConnectionDefinition;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusNuyoConnection {

	protected static String OC_CONNECTION_IDENTIFIER = "kaliko";
	protected final static Logger logger = Logger.getLogger(OctopusNuyoConnection.class.getName());

	protected OctopusDatabase database;

	public static void main(String[] args) {

		new OctopusNuyoConnection();
	}

	public OctopusNuyoConnection() {

		//OctopusRemoteConnection conn = createConnection(new ConnectionDefinition("contact-server-war-2.1.0", "http://boink.tarent.de:8080/contact-server-war-2.1.0", "contact-server-war-2.1.0"));
//		OctopusRemoteConnection conn = OctopusDatabase.createConnection(new ConnectionDefinition("contact-server-war-2.1.0", "http://boink.tarent.de:8080/contact-server-war-2.1.0", "contact-server-war-2.1.0"));
		
//		try {
//			SwingIconFactory.getInstance().addResourcesFolder(SearchDialog.class, StartUpConstants.CONTACT_RESOURCES_PATH);
//		} catch (IconFactoryException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		database = new OctopusDatabase();
//		database.init(OctopusDatabase.createConnection(new ConnectionDefinition("contact-server-war-2.1.0", "http://localhost:8080/contact-server-war-2.1.0", "contact-server-war-2.1.0")));
//		database.init(OctopusDatabase.createConnection(new ConnectionDefinition("contact2-bmu-postgres-tnt_demo_test", "http://localhost:8181/contact2-bmu-postgres-tnt_demo_test", "contact2-bmu-postgres-tnt_demo_test")));
		try {
			database.setUser("userAdmin");
			database.setPassword("adminUser");
			database.login();
			database.loadCategories();
			
			
			
		} catch(Exception excp) {
			excp.printStackTrace();
		}
	

		//OctopusResult result = conn.callTask("getServerVersion", Collections.EMPTY_MAP);

		//System.out.println(result.getData("version").toString());

//		ApplicationServices.getInstance().setApplicationModel(new ApplicationModel());
//		BindingManager bm = new BindingManager();
//		bm.setModel(ApplicationServices.getInstance().getApplicationModel());
//		ApplicationServices.getInstance().setBindingManager(bm);
//
//		ConfigManager.getInstance().setPrefBaseNodeName("/org/evolvis/nuyo");
//		ConfigManager.getInstance().setBootstrapVariant("nuyo-client");
//		ConfigManager.getInstance().setApplicationClass(OctopusNuyoConnection.class);
//
//		ConfigManager.getInstance().init("nuyo-client");
//
//		List categories = getCategoriesObjectList();
//
//		System.out.println(categories.size());
//		for(int i=0; i < categories.size(); i++)
//			System.out.println(categories.get(i).toString());
//
//		SearchDialog searchDialog = new SearchDialog(null, ConfigManager.getPreferences(), categories);
//
//		// 	Puts the alp into the search engine to let it work on it
//		// (which means it will update it from time to time).
//		AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
//		if (alp == null) 
//			logger.warning("No AddressListParameter object in application model.");
//		searchDialog.init(alp);
//		searchDialog.getCategoryTree();
//
//		searchDialog.showSearch();
//		
//		if (searchDialog.criteriaChanged()) {
//            searchDialog.updateAddressListParameters();
//            //ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
//        }
//		
//		ContactSelectorDialog selectorDialog = new ContactSelectorDialog();
//		
//		selectorDialog.setVisible(true);

	}

	public List getCategoriesObjectList() {
		if (database != null) try {
			return database.getCategoriesList();
		} catch (ContactDBException excp) {
			excp.printStackTrace();
		}
		return new ArrayList();
	}
}
