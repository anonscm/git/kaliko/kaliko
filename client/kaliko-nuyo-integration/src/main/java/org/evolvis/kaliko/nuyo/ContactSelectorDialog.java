/**
 * 
 */
package org.evolvis.kaliko.nuyo;

import javax.swing.JDialog;

import org.evolvis.nuyo.gui.AddressTable;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ContactSelectorDialog extends JDialog {

	public ContactSelectorDialog() {
		super();
		
		AddressTable addressTable = new AddressTable();
		
		add(addressTable.getTable());
		
		pack();
	}
}
