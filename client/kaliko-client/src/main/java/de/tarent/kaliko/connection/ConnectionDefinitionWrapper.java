/**
 * 
 */
package de.tarent.kaliko.connection;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.evolvis.xana.config.ConnectionDefinition;
import org.evolvis.xana.config.ConnectionDefinition.Key;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.ConnectionParametersImpl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ConnectionDefinitionWrapper extends ConnectionParametersImpl {

	protected ConnectionDefinition connectionDefinition;
	protected String name;
	protected String address;
	protected String module;
	protected int port;
	protected boolean useTLS;
	
	public ConnectionDefinitionWrapper(ConnectionDefinition connectionDefinition) throws MalformedURLException {
		super(
				connectionDefinition.get(Key.LABEL),
				new URL(connectionDefinition.get(Key.SERVER_URL)).getHost(),
				connectionDefinition.get(Key.OCTOPUS_MODULE),
				new URL(connectionDefinition.get(Key.SERVER_URL)).getPort(),
				new URL(connectionDefinition.get(Key.SERVER_URL)).getProtocol().contains("https"));
	}
	
//	public static String getServerAddressFromURL(URL url) {
//		return url.toString();
//	}
//	
//	public static String getOctopusModuleFromURL(URL url) {
//		String path = url.getPath();
//		
//		if(path.charAt(0) == '/')
//			path = path.substring(1);
//		
//		int indexOfFirstSlash = path.indexOf('/');
//		
//		if(indexOfFirstSlash != -1)
//			path = path.substring(0, indexOfFirstSlash);
//		
//		
//		return path;
//	}
	
//	public static ConnectionDefinition connectionToConnectionDefinition(ConnectionParameters connection) {
//		try {
//			return new ConnectionDefinition(connection.getName(),
//					new URL(connection.useTLS() ? "https" : "http", connection.getAddress(), connection.getPort(), "/" + connection.getModule() + "/soap/kaliko").toString(), connection.getModule());
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
	
	public static Collection<ConnectionParameters> connectionDefinitionsToConnections(Collection<ConnectionDefinition> connectionDefinitions) {
		Iterator<ConnectionDefinition> connectionDefinitionsIterator = connectionDefinitions.iterator();
		List<ConnectionParameters> connections = new ArrayList<ConnectionParameters>(connectionDefinitions.size());
		
		while(connectionDefinitionsIterator.hasNext())
			try {
				connections.add(new ConnectionDefinitionWrapper(connectionDefinitionsIterator.next()));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return connections;
	}
}
