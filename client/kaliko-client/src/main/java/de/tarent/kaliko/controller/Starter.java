/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.controller;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.evolvis.libwallet.WalletManager;
import org.evolvis.libwallet.impl.WalletException;
import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Context;
import org.evolvis.taskmanager.TaskManager.Task;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;

import de.tarent.commons.ui.I18n;
import de.tarent.commons.ui.swing.LoginDialog;
import de.tarent.commons.utils.SystemInfo;
import de.tarent.kaliko.components.actions.impl.ActionHelper;
import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.data.impl.OctopusDataManager;
import de.tarent.kaliko.data.impl.OctopusDataSource;
import de.tarent.kaliko.ui.panels.MainPanel;
import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Starter {

	protected LoginDialog loginDialog;
	protected Image windowIcon = Toolkit.getDefaultToolkit().getImage(MainPanel.class.getResource("/de/tarent/kaliko/gfx/kaliko.png"));
	protected String userName;
	protected String password;
	protected CalendarApplicationController controller;
	protected CalendarDataManager calendarDataManager;
	protected OctopusConnection connection;

	protected final static Logger logger = Logger.getLogger(Starter.class.getName());
	
	protected final static String CONTEXT_EVENTS_DESELECTED = "events_deselected";
	
	protected I18n messages;

	public static void main(String[] args) {
		new Starter();
	}

	public Starter() {
		ConfigManager.getInstance().setPrefBaseNodeName("/de/tarent/kaliko");
		ConfigManager.getInstance().setBootstrapVariant("kaliko");
		ConfigManager.getInstance().setApplicationClass(Starter.class);
		
		this.messages = new I18n("de.tarent.kaliko.messages.messages"); 

		quirks();

		// Loads the configuration documents.
		ConfigManager.getInstance().init("kaliko-client");
		
		checkForCompatibility();
		
		initSystemTray();
		
		String lastConnection = ConfigManager.getPreferences().node("connection.last").get("name", null);
		String lastUserName = ConfigManager.getPreferences().node("user").get("name", System.getProperty("user.name"));
		String password = null;

		// try to get password from a wallet-system
		try {
			password = WalletManager.getInstance().readPassword("kaliko", "kaliko", lastUserName+"@"+lastConnection);
		} catch (WalletException e) {
			logger.warning("Could not access any wallet-system");
		}
		
		loginDialog = new LoginDialog(lastUserName, password, lastConnection, windowIcon, ConfigManager.getEnvironment().getConnectionDefinitions(), messages.getString("MainFrame_Title"));
		loginDialog.addActionListener(new LoginActionListener());
		loginDialog.setDialogVisible(true);
	}
	
	protected void initSystemTray() {
		// Java6 SystemTray
		
//		// SystemTray is supported in Java >= 1.6 
//		if (new Double(System.getProperty("java.specification.version", "0.0")).doubleValue() < 1.6)
//			return;
//		
//		// check if desktop environment supports system-tray
//		if(SystemTray.isSupported()) {
//			
//			// A context-menu for the tray-icon
//			PopupMenu trayContextMenu = new PopupMenu("kaliko.context.tray", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//			ActionRegistry.getInstance().addContainer(trayContextMenu);
//			trayContextMenu.initActions();
//
//		    TrayIcon trayIcon = new TrayIcon(windowIcon, "kaliko");
//		    trayIcon.setImageAutoSize(true);
//		    
//
//		    try {
//		        SystemTray.getSystemTray().add(trayIcon);
//		    } catch (AWTException e) {
//		        System.err.println("TrayIcon could not be added.");
//		    }
//		}
		
		// SWT
//		Display display = new Display();
//		
//		final Tray tray = display.getSystemTray ();
//		if (tray == null)
//			System.out.println ("The system tray is not available");
	}

	/**
	 * 
	 * Runs platform specific code
	 *
	 */

	private void quirks()
	{
		// Some MacOS X convenience

		// Lets the menu-bar appear where a mac-user expects it.
		System.setProperty("com.apple.macos.useScreenMenuBar", "true");

		// Sets Application-Name.
		System.setProperty("com.apple.mrj.application.apple.menu.about.name", messages.getString("MainFrame_Title"));

		// Enable font-anti-aliasing
		System.setProperty("swing.aatext", "true");
	}

	/**
	 * checks the runtime for compatibility.
	 * 
	 */
	protected void checkForCompatibility()
	{
		if (ConfigManager.getPreferences().getBoolean("complainUnsupportedJavaVersion", true))
			// kaliko is tried and tested against the Java 1.5 and 1.6 implementation from
			// Sun Microsystems only. If run with something different a warning is printed.
			ConfigManager.getPreferences().putBoolean("complainUnsupportedJavaVersion", SystemInfo.checkForCompatibility(messages.getString("MainFrame_Title"), 1.5, 1.6));
	}

	protected void tryToConnect() {
		try {
			loginDialog.setStatusText(messages.getString("LoginDialog_Progress_Authenticating"));
			loginDialog.setStatus(10);
//			connection = CalendarOctopusConnection.createConnection(loginDialog.getSelectedConnection());
			connection = null;
			connection.setUsername(userName);
			connection.setPassword(password);
			connection.login();
			loginDialog.setStatus(40);

			loginDialog.setStatusText(messages.getString("LoginDialog_Progress_SavingPreferences"));
			// remember username next time
			ConfigManager.getPreferences()
			.node("user")
			.put("name", userName);

			// remember connection next time
			ConfigManager.getPreferences().node("connection.last").put("name", loginDialog.getSelectedConnection().toString());
			
			// try to store password in a wallet
			String url = userName+"@"+loginDialog.getSelectedConnection().toString();
			
			try {
				WalletManager.getInstance().writePassword("kaliko", "kaliko", url, password);
			} catch (WalletException e) {
				logger.warning("Could not store login-data in any wallet-system");
			}

			loginDialog.setStatus(50);

			initApplication();

		} catch(OctopusCallException excp) {
			excp.printStackTrace();
			JOptionPane.showMessageDialog((LoginDialog)loginDialog, messages.getFormattedString("Starter_Login_Error_Message", getCauseMessage(excp)), messages.getString("Starter_Login_Error_Title"), JOptionPane.ERROR_MESSAGE);
			loginDialog.reset();
		}
	}

	protected String getCauseMessage(Throwable cause) {

		Throwable currentCause = cause;

		while (currentCause.getCause() != null)
			currentCause = currentCause.getCause();

		return currentCause.getLocalizedMessage();
	}

	protected void initApplication() {

		loginDialog.setStatusText(messages.getString("LoginDialog_Progress_RegisteringIcons"));
//		// Init Icon-dirs
//		try {
//			SwingIconFactory.getInstance().addResourcesFolder(getClass(), "/de/tarent/kaliko/gfx/");
//		} catch (IconFactoryException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

		loginDialog.setStatusText(messages.getString("LoginDialog_Progress_LoadingActions"));
		// Load the action definitions.
		ActionRegistry ar = ActionRegistry.getInstance();
		ar.init(ConfigManager.getAppearance().getActionDefinitions());

		loginDialog.setStatus(70);

		loginDialog.setStatusText(messages.getString("LoginDialog_Progress_StartingGUI"));
		
		final JFrame frame = new JFrame(messages.getString("MainFrame_Title"));

//		UIDefaults def = UIManager.getLookAndFeelDefaults();
//		Enumeration enume = def.keys();
//		while (enume.hasMoreElements()) {
//		Object item = enume.nextElement();
//		System.out.println(item +" " + def.get(item));
//		}

		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {

			/**
			 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
			 */
			@Override
			public void windowClosing(WindowEvent e) {
				ActionRegistry.getInstance().getAction("exit").actionPerformed(new ActionEvent(frame, -1, "exit"));
			}

		});
		frame.setIconImage(windowIcon);
		frame.getContentPane().setLayout(new BorderLayout());
		MainPanel mainPanel = new MainPanel(frame, getController());
		frame.getContentPane().add(mainPanel, BorderLayout.CENTER);

		// restore view
		String initView = ConfigManager.getPreferences().get("de.tarent.kaliko.initview", String.valueOf(CalendarView.WEEK_VIEW));
		getController().getViewSwitcher().setCurrentView(Integer.parseInt(initView));

		loginDialog.setStatus(80);


		// The Main-Window Menu
//		MenuBar menuBar = new MenuBar("kaliko.menubar", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//		ActionRegistry.getInstance().addContainer(menuBar);
//		menuBar.initActions();
//		frame.setJMenuBar(menuBar);

		// The Main-Window toolbar
//		ToolBar toolBar = new ToolBar("kaliko.toolbar", false);
//		ActionRegistry.getInstance().addContainer(toolBar);
//		try {
//			toolBar.initActions();
//		} catch (ActionContainerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		frame.getContentPane().add(toolBar, BorderLayout.NORTH);

		// A context-menu for free calendar-view-space
//		PopupMenu dayContextMenu = new PopupMenu("kaliko.context.space", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//		ActionRegistry.getInstance().addContainer(dayContextMenu);
//		dayContextMenu.initActions();
//		
//		getController().setContextMenuSpace(dayContextMenu);


		// A context-menu for calendar-events
//		PopupMenu eventContextMenu = new PopupMenu("kaliko.context.event", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//		ActionRegistry.getInstance().addContainer(eventContextMenu);
//		eventContextMenu.initActions();
		
//		getController().setContextMenuEvent(eventContextMenu);
		
		// A context-menu for the category-list containing _all_ categories
//		PopupMenu allCatsContextMenu = new PopupMenu("kaliko.context.categories.all", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//		ActionRegistry.getInstance().addContainer(allCatsContextMenu);
//		allCatsContextMenu.initActions();
		
//		((CategorySelectionPanel)getController().getCategorySelector()).setAllCategoriesContextMenu(allCatsContextMenu);
		
		// A context-menu for the category-list containing the favorite categories
//		PopupMenu favoriteCatsContextMenu = new PopupMenu("kaliko.context.categories.favorites", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()));
//		ActionRegistry.getInstance().addContainer(favoriteCatsContextMenu);
//		favoriteCatsContextMenu.initActions();
		
//		((CategorySelectionPanel)getController().getCategorySelector()).setFavoriteCategoriesContextMenu(allCatsContextMenu);
		
		// Set action-states depending on event-selection
		
		ActionRegistry.getInstance().enableContext(CONTEXT_EVENTS_DESELECTED);
		
		getController().getEventSelectionModel().addEventSelectionListener(new EventSelectionListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
			 */
			public void eventSelectionChanged(EventSelectionEvent eventSelectionEvent) {
				if(eventSelectionEvent.getSelectedEvents().size() == 0) {
					if(!ActionRegistry.getInstance().isContextEnabled(CONTEXT_EVENTS_DESELECTED))
						ActionRegistry.getInstance().enableContext(CONTEXT_EVENTS_DESELECTED);
				}
				else {
					if(ActionRegistry.getInstance().isContextEnabled(CONTEXT_EVENTS_DESELECTED))
						ActionRegistry.getInstance().disableContext(CONTEXT_EVENTS_DESELECTED);
				}
			}
		});
		
		// deselect events when view changes
		getController().addCalendarViewListener(new CalendarViewListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.CalendarViewListener#viewChanged(de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent)
			 */
			public void viewChanged(CalendarViewChangeEvent event) {
				getController().getEventSelectionModel().clearSelection();
			}
			
		});

		loginDialog.setStatus(100);

		// LoginDialog is not needed any more, free memory
		loginDialog.setDialogVisible(false);
		loginDialog = null;
		
		// restore main-frame size
		try {
			frame.setSize(Integer.parseInt(ConfigManager.getPreferences().get("org.evolvis.kaliko.mainframe_width", "780")),
					Integer.parseInt(ConfigManager.getPreferences().get("org.evolvis.kaliko.mainframe_height", "600")));
		} catch (NumberFormatException excp) {
			logger.finer("could not parse main-frame-size-values from preference org.evolvis.kaliko.mainframe_*");
			frame.setSize(780, 600);
		}

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);


		// Runs the action initializations (which depend on the UI being set up
		// first).
		ar.runDelayedInitializations();
		
		// restore category-selection when categories are loaeded
		getController().addCategoryListener(new CategoryListener() {

			boolean executed = false;
			
			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categoriesLoaded(final CategoryEvent event) {
				// ensure to only run once
				if(executed)
					return;
				
				TaskManager.getInstance().registerExclusive(new Task() {

					/**
					 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
					 */
					public void cancel() {
						// not cancelable				
					}

					/**
					 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
					 */
					public void run(Context context) {
						executed = true;
						// insert categories into list
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategories"));
						getController().getCategorySelector().setAvailableCategories(event.getCategories());
						
						// restore selection
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategorySelection"));
						getController().getCategorySelector().setSelectedCategories(ConfigManager.getPreferences().get("de.tarent.kaliko.active_categories", ";"));
						
						// restore pre-selection
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategoryPreSelection"));
						getController().getCategorySelector().setDefaultCategories(ConfigManager.getPreferences().get("de.tarent.kaliko.preselected_categories", ";"));
						
						// restore favorite categories
//						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategoryFavorites"));
//						((CategorySelectionPanel)getController().getCategorySelector()).setFavoriteCategories(ConfigManager.getPreferences().get("de.tarent.kaliko.favorite_categories", ";"));
												
					}
					
				}, messages.getString("Starter_Task_RestoringCategories"), false);
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categorySelectionChanged(CategoryEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categoryViewSelectionChanged(CategoryEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		// Register data manager to listen on changes in ui
		getController().addCategoryListener((OctopusDataManager)getCalendarDataManager());
		getController().addCalendarSelectionListener((OctopusDataManager)getCalendarDataManager());
		getController().addCalendarViewListener((OctopusDataManager)getCalendarDataManager());
		
		// Load categories
		getCalendarDataManager().loadCategories();	
	}

	protected CalendarDataManager getCalendarDataManager() {
		if(calendarDataManager == null)
			calendarDataManager = new OctopusDataManager(new OctopusDataSource(connection));

		return calendarDataManager;
	}

	public CalendarApplicationController getController() {
		if(controller == null)  {
			controller = new DefaultCalendarApplicationController(getCalendarDataManager());
			ActionHelper.setController(controller);
		}
		return controller;
	}

	protected class LoginActionListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("login")) {
				new Thread() {
					public void run() {
						userName = loginDialog.getUserName();
						password = loginDialog.getPassword();
						tryToConnect();
					}
				}.start();
			}
			else if(e.getActionCommand().equals("cancel") || e.getActionCommand().equals("quit"))
				System.exit(0);
		}
	}
}
