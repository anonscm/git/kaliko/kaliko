/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.ui.panels;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.evolvis.kaliko.widgets.swing.panels.CategorySelectionPanel;
import org.evolvis.kaliko.widgets.swing.panels.MiniMonthSelectorPanel;
import org.evolvis.kaliko.widgets.swing.panels.ScopeTitlePanel;
import org.evolvis.kaliko.widgets.swing.panels.ViewsPanel;
import org.evolvis.kaliko.widgets.swing.views.DefaultListView;

import de.tarent.commons.ui.I18n;
import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class MainPanel extends JPanel implements CalendarSelectionListener, CalendarViewListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 191929372657923401L;
	protected JSplitPane verticalSplitPane;
	protected JSplitPane viewsSplit;
	protected ViewsPanel viewsPanel;
	protected JPanel mainViewPanel;
	protected JPanel contextPanel;
	protected MiniMonthSelectorPanel miniMonthSelectorPanel;
	protected DefaultListView listView;
	protected CategorySelectionPanel categoryPanel;
//	protected StatusBar statusBar;
	protected CalendarApplicationController controller;
	
	protected I18n messages;
	
	public MainPanel(Frame frame, CalendarApplicationController controller) {
		super();
		
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
		
		this.controller = controller;
		controller.setCategorySelector(getCategoryPanel());
		controller.setViewSwitcher(getViewsPanel());
		controller.setMainFrame(frame);
		controller.addCalendarSelectionListener(this);
		controller.addCalendarViewListener(this);
		
		setLayout(new BorderLayout());
		add(getVerticalSplitPane(), BorderLayout.CENTER);
		//add(getStatusBar(), BorderLayout.SOUTH);
		
	}
	
	protected JSplitPane getVerticalSplitPane() {
		if(verticalSplitPane == null) {
			verticalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
			//verticalSplitPane.setLeftComponent(getViewsSplit());
			verticalSplitPane.setLeftComponent(getViewsPanel());
			verticalSplitPane.setRightComponent(getContextPanel());
			verticalSplitPane.setResizeWeight(1);
			verticalSplitPane.setOneTouchExpandable(true);
		}
		return verticalSplitPane;
	}
	
//	protected StatusBar getStatusBar() {
//		if(statusBar == null) {
//			statusBar = new StatusBar();
//			statusBar.setUserName(controller.getCalendarDataManager().getUsername());
//		}
//		return statusBar;
//	}
	
	protected ViewsPanel getViewsPanel() {
		if(viewsPanel == null)
			viewsPanel = new ViewsPanel(controller);		
		return viewsPanel;
	}
	
//	protected JSplitPane getViewsSplit() {
//		if(viewsSplit == null) {
//			viewsSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
//			//viewsSplit.setLeftComponent(getListView());
//			viewsSplit.setRightComponent(getViewsPanel());
//			viewsSplit.setResizeWeight(0.5);
//			viewsSplit.setOneTouchExpandable(true);
//		}
//		return viewsSplit;
//	}
	
	protected JPanel getMainViewPanel() {
		if(mainViewPanel == null) {
			mainViewPanel = new JPanel(new BorderLayout());
			mainViewPanel.add(new ScopeTitlePanel(controller), BorderLayout.NORTH);
			mainViewPanel.add(getViewsPanel(), BorderLayout.CENTER);
		}
		return mainViewPanel;
	}
	
	protected JPanel getContextPanel() {
		if(contextPanel == null) {
			contextPanel = new JPanel(new BorderLayout());
			contextPanel.add(getCategoryPanel(), BorderLayout.CENTER);
			contextPanel.add(getMiniMonthSelectorPanel(), BorderLayout.SOUTH);
		}
		return contextPanel;
	}
	
	protected JPanel getMiniMonthSelectorPanel() {
		if(miniMonthSelectorPanel == null) {
			miniMonthSelectorPanel = new MiniMonthSelectorPanel(controller);
			controller.addCalendarSelectionListener(miniMonthSelectorPanel);
		}
		return miniMonthSelectorPanel;
	}
	
	protected DefaultListView getListView() {
		if(listView == null) {
			listView = new DefaultListView(controller);
			//controller.addCalendarSelectionListener(listView);
			controller.addCalendarDataListener(listView);
		}
		return listView;
	}
	
	protected CategorySelectionPanel getCategoryPanel() {
		if(categoryPanel == null)
			categoryPanel = new CategorySelectionPanel(controller);
		return categoryPanel;
	}
	
	protected void setWindowTitle() {
		if(controller.getMainFrame() != null && controller.getViewSwitcher() != null && controller.getViewSwitcher().getCurrentView() != null)
			controller.getMainFrame().setTitle(controller.getViewSwitcher().getCurrentView().getModel().getCurrentScopeName() + " - " + messages.getString("MainFrame_Title"));
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		setWindowTitle();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarViewListener#viewChanged(de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent)
	 */
	public void viewChanged(CalendarViewChangeEvent arg0) {
		setWindowTitle();
	}
}
