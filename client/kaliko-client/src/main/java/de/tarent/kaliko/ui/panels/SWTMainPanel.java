/**
 * 
 */
package de.tarent.kaliko.ui.panels;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Calendar;

import javax.swing.AbstractAction;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.swing.panels.CategorySelectionPanel;
import org.evolvis.kaliko.widgets.swing.panels.ViewsPanel;
import org.evolvis.kaliko.widgets.swt.TabbedCategoryTables;

import de.tarent.commons.ui.I18n;
import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent;
import de.tarent.kaliko.objects.Category;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTMainPanel extends Composite implements CalendarSelectionListener, CalendarViewListener {

	protected ViewsPanel viewsPanel;
	protected CategorySelectionPanel categoryPanel;
	
	protected Frame frame;
	protected CalendarApplicationController controller;
	protected DateTime calendar;
	
	
	public java.util.List<Category> availableCategories;
	public java.util.List<Category> activatedCategories;
	public java.util.List<Category> availableFavorites;
	public java.util.List<Category> activatedFavorites;
	
	protected I18n messages;
	protected Composite parent;
	
	
	public SWTMainPanel(Composite parent, CalendarApplicationController controller) {
		super(parent, SWT.NONE);
		this.parent = parent;
		this.controller = controller;
		
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
		
		controller.addCalendarSelectionListener(this);
		controller.addCalendarViewListener(this);
		
		setLayout(new FillLayout());
		
		SashForm sashForm = new SashForm(this, SWT.HORIZONTAL);
		
		final Composite leftComposite = new Composite(sashForm, SWT.EMBEDDED);
		frame = SWT_AWT.new_Frame(leftComposite);
		
		Composite rightComposite = new Composite(sashForm, SWT.NONE);
		rightComposite.setLayout(new GridLayout(1, true));
		
		// Left composite (the calendar view) should have 3-times more space than the right one (the category-list) by default
		sashForm.setWeights(new int[]{5, 2});
		
		final TabbedCategoryTables categorySelector = new TabbedCategoryTables(rightComposite);
		categorySelector.setCategorySelectionAction(new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4244897119721584662L;

			public void actionPerformed(ActionEvent event) {			
				SWTMainPanel.this.controller.fireCategorySelectionChanged(CategoryCollectionHelper.toList(categorySelector.getSelectedCategories()));
			}
		});
		categorySelector.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		calendar = new DateTime(rightComposite, SWT.CALENDAR);
		calendar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		calendar.addListener(SWT.Selection, new Listener(){

			public void handleEvent(Event arg0) {
				int day=calendar.getDay();
				int month=calendar.getMonth();
				int year=calendar.getYear();
				
				Calendar selectedDate=  Calendar.getInstance();
				selectedDate.set(year, month, day);
				SWTMainPanel.this.controller.setCalendarCursorTo(selectedDate);
			}
			
		});
		
		controller.setViewSwitcher(getViewsPanel());
		controller.addCalendarSelectionListener(this);
		controller.addCalendarViewListener(this);
		controller.setCategorySelector(categorySelector);
		
		
		frame.setLayout(new BorderLayout());
		frame.add(getViewsPanel(), BorderLayout.CENTER);
	}
	
	protected ViewsPanel getViewsPanel() {
		if(viewsPanel == null)
			viewsPanel = new ViewsPanel(controller);		
		return viewsPanel;
	}
	
	protected void setWindowTitle() {
		parent.getDisplay().asyncExec(new Runnable() {
			public void run() {
				if(controller.getShell() != null && controller.getViewSwitcher() != null && controller.getViewSwitcher().getCurrentView() != null)
					((Shell)controller.getShell()).setText(controller.getViewSwitcher().getCurrentView().getModel().getCurrentScopeName() + " - " + messages.getString("MainFrame_Title"));
			}
		});
	}

	public void calendarSelectionChanged(CalendarSelectionEvent arg0) {
		setWindowTitle();
		
	}

	public void viewChanged(CalendarViewChangeEvent arg0) {
		setWindowTitle();
	}
	
	public void updateUI() {
		getViewsPanel().updateUI();
	}
	
	public Frame getFrame() {
		return frame;
	}
}
