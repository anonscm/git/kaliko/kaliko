/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.ui.actions;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Shell;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.ui.I18n;
import de.tarent.commons.ui.swt.AboutDialog;
import de.tarent.commons.utils.VersionTool;
import de.tarent.kaliko.components.actions.impl.ActionHelper;
import de.tarent.kaliko.controller.Starter;
import de.tarent.octopus.client.OctopusCallException;

/**
 * 
 */

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AboutAction extends AbstractGUIAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7790230946871782502L;
	
	protected static final Logger logger = Logger.getLogger(AboutAction.class.getName());

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0) {

		String serverVersion = null;
		
		try {
			serverVersion = ActionHelper.getController().getCalendarDataManager().getServerVersion();
		} catch(OctopusCallException excp) {
			logger.info("Could not determine Server-version. Probably connected to a server older than 0.6.0");
		}
		
		I18n messages = new I18n("de.tarent.kaliko.messages.messages");
		
		AboutDialog dialog = new AboutDialog((Shell)ActionHelper.getController().getShell(),
				null,
				messages.getString("MainFrame_Title"),
				messages.getString("AboutDialog_Desc"),
				VersionTool.getInfoFromClass(Starter.class).getVersion("n/a"),
				serverVersion,
				VersionTool.getInfoFromClass(Starter.class).getBuildID("development build"),
				ActionHelper.getController().getCalendarDataManager().getServerDescription(),
				ActionHelper.getController().getCalendarDataManager().getDatabaseDescription(),
				"/de/tarent/kaliko/contributions_de.html",
				"/de/tarent/kaliko/gpl.html");
		dialog.open();
	}
}
