/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.ui.actions;

import java.awt.event.ActionEvent;
import java.util.prefs.BackingStoreException;

import org.eclipse.swt.widgets.Shell;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.config.ConfigManager;

import de.tarent.kaliko.components.actions.impl.ActionHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ExitAction extends AbstractGUIAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1629443924853188819L;

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {

		/* Store preferences */

		// Store latest view (month, week, day...)
		ConfigManager.getPreferences().put("org.evolvis.kaliko.initview", String.valueOf(ActionHelper.getController().getViewSwitcher().getCurrentView().getType()));

		// Store latest categories
		ConfigManager.getPreferences().put("org.evolvis.kaliko.selected_categories", ActionHelper.getController().getCategorySelector().getSelectedCategoriesAsString());
		ConfigManager.getPreferences().put("org.evolvis.kaliko.default_categories", ActionHelper.getController().getCategorySelector().getDefaultCategoriesAsString());
		ConfigManager.getPreferences().put("org.evolvis.kaliko.favorite_categories", ActionHelper.getController().getCategorySelector().getFavoriteCategoriesAsString());

		// Store main-frame size
//		ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_width", String.valueOf(ActionHelper.getController().getMainFrame().getWidth()));
//		ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_height", String.valueOf(ActionHelper.getController().getMainFrame().getHeight()));

		// Store main-frame location
//		ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_location_x", String.valueOf(ActionHelper.getController().getMainFrame().getLocationOnScreen().getX()));
//		ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_location_y", String.valueOf(ActionHelper.getController().getMainFrame().getLocationOnScreen().getY()));
		
		Shell shell = (Shell)ActionHelper.getController().getShell();

		if(shell != null && !shell.isDisposed()) {

			ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_width", String.valueOf(shell.getSize().x));
			ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_height", String.valueOf(shell.getSize().y));

			ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_location_x", String.valueOf(shell.getLocation().x));
			ConfigManager.getPreferences().put("org.evolvis.kaliko.mainframe_location_y", String.valueOf(shell.getLocation().y));
		}
		
		try {
			ConfigManager.getPreferences().flush();
		} catch (BackingStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.exit(0);
	}
}
