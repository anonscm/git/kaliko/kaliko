/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.data.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import de.tarent.commons.ui.I18n;
import de.tarent.kaliko.components.data.CalendarDataSource;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.AccessImpl;
import de.tarent.kaliko.objects.impl.CalendarEventFilter;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.EventList;
import de.tarent.kaliko.objects.impl.Filter;
import de.tarent.kaliko.objects.impl.ResourcesList;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.remote.OctopusRemoteConnection;
import de.tarent.octopus.embedded.OctopusDirectCallConnection;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusDataSource implements CalendarDataSource {

	protected OctopusConnection connection;
	protected final static Logger logger = Logger.getLogger(CalendarDataSource.class.getName());

	protected I18n messages;
	
	public OctopusDataSource(OctopusConnection connection) {
		this.connection = connection;
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#addCalendarEvent(de.tarent.libraries.components.calendar.data.CalendarEvent, de.tarent.libraries.components.calendar.data.Category)
	 */
	@SuppressWarnings("unchecked")
	public void addCalendarEvent(Event event, List<Category> categories) {
		Map<String, Object> args = new HashMap<String, Object>();
		
		// check if at least one category is selected
		if(categories.size() < 1) {
			// TODO forward error to upper layers
			logger.warning("no category selected. not creating event.");
			return;
		}
		
		// assign categories to event-object
		event.setCategoriesList(new CategoriesList(categories));

		EventList eventList = new EventList();
		eventList.add(event);

		args.put("events", eventList);
		OctopusResult result = connection.callTask("createEvents", args);

		
		// set the event-pk
		
		List<Integer> pks = (List<Integer>)result.getData("pks");

		if(pks != null && pks.size() > 0)
			event.setPk(pks.get(0));
		else
			//  TODO GUI-Error-Message
			throw new RuntimeException("Could not create event!");
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getCategories()
	 */
	@SuppressWarnings("unchecked")
	public List<Category> getCategories() {

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("access", new AccessImpl(true, true));

		OctopusResult result = connection.callTask("getCategories", args);

		if(result.getData("categoryList") != null)
			return (List<Category>) result.getData("categoryList");

		return new ArrayList<Category>();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getWritableCategories()
	 */
	@SuppressWarnings("unchecked")
	public List<Category> getWritableCategories() {
		Map<String, Object> args = new HashMap<String, Object>();
		
		// This is a workaround. Correct access should be via AccessImpl(true, true)
		// needs to be fixed in server -- fkoester Wed, 04 Jun 2008 13:27:38 +0200
		args.put("access", new AccessImpl(false, true));

		OctopusResult result = connection.callTask("getCategories", args);

		if(result.getData("categoryList") != null)
			return (List<Category>) result.getData("categoryList");

		return new ArrayList<Category>();
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getEvents(java.util.Calendar, java.util.Calendar, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	public List<Event> getEvents(Calendar begin, Calendar end, List<Category> categories) {

		// just return an empty event-list if requested category-list is empty
		if(categories.size() == 0)
			return new ArrayList<Event>();

		// get guids for categories-
		List<String> guids = new ArrayList<String>();
		Iterator<Category> it = categories.iterator();

		while(it.hasNext())
			guids.add(it.next().getGuid());

		Filter filter = new Filter();
		filter.setWithOccurences(true);
		filter.setWithResources(true);
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("begin", begin);
		args.put("end", end);
		args.put("categories", guids);
		args.put("filter", filter);

		return ((List<Event>) connection.callTask("getEvents", args).getData("events"));
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#deleteEvent(de.tarent.libraries.components.calendar.data.CalendarEvent)
	 */
	public void deleteEvent(Event event) {
		if(event == null)  {
			// TODO forward error to upper layers
			logger.warning("event is null, returning");
			return;
		}
		
		Map<String, Object> args = new HashMap<String, Object>();

		List<Integer> pks = new ArrayList<Integer>();
		pks.add(event.getPk());

		args.put("pks", pks);

		connection.callTask("deleteEvents", args);
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#updateCalendarEvent(de.tarent.libraries.components.calendar.data.CalendarEvent)
	 */
	public void updateCalendarEvent(Event event) {
		Map<String, Object> args = new HashMap<String, Object>();

		EventList events = new EventList();
		events.add(event);

		// set update-date to current time
		// should not this be done on server-side?
		event.setUpdateDate(new Date());
		
		args.put("events", events);
		connection.callTask("updateEvents", args);
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getEvents(de.tarent.libraries.calendar.impl.CalendarEventFilter)
	 */
	public List<Event> getEvents(CalendarEventFilter arg0) {
		// TODO Auto-generated method stub
		return new ArrayList<Event>();
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getDatabaseDescription()
	 * TODO I18n
	 */
	public String getDatabaseDescription() {
		if(connection == null)
			return messages.getString("OctopusDataSource_Database_NotConnected");
		else
			return messages.getString("OctopusDataSource_Database_Unknown");
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getServerDescription()
	 */
	public String getServerDescription() {
		if(connection == null)
			return messages.getString("OctopusDataSource_Server_NotConnected");
		else if(connection instanceof OctopusRemoteConnection)
			return ((OctopusRemoteConnection)connection).getServiceURL();
		else if(connection instanceof OctopusDirectCallConnection)
			return messages.getString("OctopusDataSource_Server_Local");
		else
			return messages.getString("OctopusDataSource_Server_Unknown");
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getServerVersion()
	 */
	public String getServerVersion() {
		if(connection == null)
			return messages.getString("OctopusDataSource_Server_NotConnected");

		Map<String, Object> args = new HashMap<String, Object>(0);

		OctopusResult result = connection.callTask("getServerVersion", args);
		
		return result.getData("version").toString();
	}

	/**
	 * @see de.tarent.libraries.components.calendar.data.CalendarDataSource#getUsername()
	 */
	public String getUsername() {
		if(connection == null)
			return messages.getString("OctopusDataSource_Server_NotConnected");
		else if(connection instanceof OctopusRemoteConnection)
			return ((OctopusRemoteConnection)connection).getUsername();
		else if(connection instanceof OctopusDirectCallConnection)
			return messages.getString("OctopusDataSource_Server_Local");
		else
			return messages.getString("OctopusDataSource_Server_Unknown");
	}
}
