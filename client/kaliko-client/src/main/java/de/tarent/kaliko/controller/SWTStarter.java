/**
 * 
 */
package de.tarent.kaliko.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.evolvis.libwallet.WalletManager;
import org.evolvis.libwallet.impl.WalletException;
import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Context;
import org.evolvis.taskmanager.TaskManager.Task;
import org.evolvis.taskmanager.swt.SmallTaskManagerPanel;
import org.evolvis.xana.action.ActionContainerException;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.swt.SWTMenu;
import org.evolvis.xana.swt.SWTToolBar;
import org.evolvis.xana.swt.utils.SWTIconFactory;
import org.evolvis.xana.utils.IconFactoryException;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.I18n;
import de.tarent.commons.ui.swt.SWTLoginDialog;
import de.tarent.kaliko.components.actions.impl.ActionHelper;
import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.connection.CalendarOctopusConnection;
import de.tarent.kaliko.connection.ConnectionDefinitionWrapper;
import de.tarent.kaliko.data.impl.OctopusDataManager;
import de.tarent.kaliko.data.impl.OctopusDataSource;
import de.tarent.kaliko.ui.action.SWTContextMenu;
import de.tarent.kaliko.ui.panels.SWTMainPanel;
import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;

/**
 * 
 * <h1>Starter class for the kaliko SWT client</h1>
 * 
 * <p>The startup process of kaliko-client can be separated into the following main phases:
 * 
 * <ol>
 * 	<li>
 * 		<p><b>Initialisation</b></p>
 * 
 * 		<p>
 * 			<ul>
 * 				<li>configuration manager</li>
 *  			<li>icons</li>
 *  			<li>actions (for menus, toolbars etc)</li>
 *  			<li>collection information for connection (username, password, connection,...)</li>
 *  			<li>system tray icon (if enabled)</li>
 * 			</ul>
 * 		</p><br />
 * 	</li>
 * 
 * 	<li>
 * 		<p><b>Login-Dialog</b></p>
 * 
 * 		<p>If auto-login is not enabled a Login-Dialog is shown and the user can set username and password and select a connection to use. Furthermore he/she can manage the available connections and add new ones.</p>
 * 
 * 		<p>If auto-login is enabled this step is skipped if enough information for an auto-login is availabe (valid username, password, connection-data).</p>
 * 	</li>
 * 
 * 	<li>
 * 		<p><b>Establish Connection</b>
 * 
 * 		<p>In this phase kaliko tries to establish a connection to the given kaliko-server. If it fails the LoginDialog is shown again. (jump back to phase 2)</p>
 * 
 * 		<p>If connection in auto-login mode fails the LoginDialog is shown. (jump back to phase 2)</p>
 * 
 * 	</li>
 * 
 * 	<li>
 * 		<p><b>Start Application</b></p>
 * 
 * 		<p>In this phase the GUI is initialized and initial data for the calendar will be retrieved (available categories, events for this categories, apply user settings,...)</p>
 * 	</li>
 * </ol>
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTStarter  {

	protected Display display;
	protected SWTLoginDialog loginDialog;
	
	protected String userName;
	protected String password;
	
	protected OctopusConnection connection;
	protected Collection<ConnectionParameters> availableConnectionParameters;
	protected ConnectionParameters selectedConnectionParameters;
	protected String lastConnection;
	
	protected final static Logger logger = Logger.getLogger(SWTStarter.class.getName());
	
	protected CalendarApplicationController controller;
	protected CalendarDataManager calendarDataManager;
	
	protected final static String CONTEXT_EVENTS_DESELECTED = "events_deselected";
	
	protected I18n messages;
	
	/**
	 * Main Method for starting program
	 * 
	 * @param args parameters given on command line
	 */
	public static void main(String[] args) {
//		System.setProperty("sun.awt.noerasebackground", "true");
		new SWTStarter();
	}
	
	private SWTStarter() {		
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
		
		display = new Display();
		Display.setAppName("Kaliko");
		
		initConfiguration();
		
		initIcons();
		
		initActions();
		
		collectConnectionData();
		
		// If show_tray is set show a system tray icon
		if(ConfigManager.getPreferences().getBoolean("org.evolvis.kaliko.show_tray", false))
			initSystemTray();
		
		// If auto_login is set to true dont show a loginDialog
		if(!ConfigManager.getPreferences().getBoolean("org.evolvis.kaliko.auto_login", false)) {
			loginDialog = new SWTLoginDialog(display, getImages(), availableConnectionParameters, lastConnection, userName, password);
			
			loginDialog.addActionListener(new LoginActionListener());
			loginDialog.setDialogVisible(true);
		
			while(!loginDialog.getShell().isDisposed())
				if (!display.readAndDispatch()) display.sleep();
			
		} else {
			Iterator<ConnectionParameters> connectionParamsIt = availableConnectionParameters.iterator();
			
			while(connectionParamsIt.hasNext()) {
				ConnectionParameters connectionParams = connectionParamsIt.next();
				if(connectionParams.getName().equals(lastConnection)) {
					this.selectedConnectionParameters = connectionParams;
					break;
				}
			}
						
			tryToConnect();
		}
	}
	
	protected void initConfiguration() {
		ConfigManager.getInstance().setPrefBaseNodeName("/org/evolvis/kaliko");
		ConfigManager.getInstance().setBootstrapVariant("kaliko");
		ConfigManager.getInstance().setApplicationClass(Starter.class);
		
		ConfigManager.getInstance().init("kaliko-client");
	}
	
	protected void initIcons() {
		// Init Icon-dirs
		try {
			SWTIconFactory.getInstance().addResourcesFolder(getClass(), "/de/tarent/kaliko/gfx/");
		} catch (IconFactoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	protected void initActions() {
		// Load the action definitions.
		ActionRegistry ar = ActionRegistry.getInstance();
		ar.setIconFactory(SWTIconFactory.getInstance());
		ar.init(ConfigManager.getAppearance().getActionDefinitions());	
	}
	
	protected void collectConnectionData() {
		lastConnection = ConfigManager.getPreferences().get("last_connection", null);
		userName = ConfigManager.getPreferences().get("last_username", System.getProperty("user.name"));
		password = null;

		// try to get password from a wallet-system
		try {
			password = WalletManager.getInstance().readPassword("kaliko", "kaliko", userName+"@"+lastConnection);
		} catch (WalletException e) {
			logger.warning("Could not access any wallet-system");
		}
			
		availableConnectionParameters = ConnectionDefinitionWrapper.connectionDefinitionsToConnections(ConfigManager.getEnvironment().getConnectionDefinitions());
	}
	
	protected void initSystemTray() {
		org.eclipse.swt.graphics.Image image = null;
		
		final Tray tray = display.getSystemTray();
		ToolTip tip = null;
		if (tray == null)
			logger.warning ("The system tray is not available");
		else {			
			TrayItem item = new TrayItem(tray, SWT.NONE);
			
//			tip = new ToolTip(SWT.BALLOON | SWT.ICON_INFORMATION);
			
//			tip.setMessage("Kaliko started");
//			tip.setText("Kaliko");
			
			//image = new org.eclipse.swt.graphics.Image(display, MainPanel.class.getResourceAsStream("/de/tarent/kaliko/gfx/kaliko22.gif"));
			image = (Image)SWTIconFactory.getInstance().getIcon("kaliko22.png");
			
			final SWTMenu menu = new SWTMenu("kaliko.traymenu",PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()), new Shell(), SWT.POP_UP);
			ActionRegistry.getInstance().addContainer(menu);
			menu.initActions();

	        item.addListener (SWT.MenuDetect, new Listener () {

				/**
				 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
				 */
				public void handleEvent(Event arg0) {
					menu.getMenu().setVisible(true);
				}
	        });
	        item.addSelectionListener(new SelectionAdapter() {

				/**
				 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
				 */
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// Show / Hide main window
					Shell shell = (Shell)getController().getShell();
					
					if(shell != null)
						shell.setVisible(!shell.isVisible());
				}
	        	
	        });
	        
			item.setImage(image);
			
//			item.setToolTip(tip);
		}
		
		if(tip != null)
			tip.setVisible(true);
	}
	
	protected Image[] getImages() {
		Image[] images = new Image[3];
		
		images[0] = (Image)SWTIconFactory.getInstance().getIcon("kaliko16.png");
		images[1] = (Image)SWTIconFactory.getInstance().getIcon("kaliko22.png");
		images[2] = (Image)SWTIconFactory.getInstance().getIcon("kaliko.png");
		
		return images;
	}

	protected void initApplication() {
		
		//setStatusMessage(messages.getString("LoginDialog_Progress_RegisteringIcons"));
		
		// Init Icon-dirs
//		try {
//			SWTIconFactory.getInstance().addResourcesFolder(getClass(), "/de/tarent/kaliko/gfx/");
//		} catch (IconFactoryException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

		setStatusMessage(messages.getString("LoginDialog_Progress_LoadingActions"));
//		// Load the action definitions.
//		ActionRegistry ar = ActionRegistry.getInstance();
//		ar.setIconFactory(SWTIconFactory.getInstance());
//		ar.init(ConfigManager.getAppearance().getActionDefinitions());

		setStatus(70);

		setStatusMessage(messages.getString("LoginDialog_Progress_StartingGUI"));
		
		
		Shell shell = new Shell(display);
		getController().setShell(shell);
		shell.setText("Kaliko");
		shell.setImages(getImages());
		shell.setLayout(new GridLayout(1, true));
		
		// Create tool-bar
		SWTToolBar toolBar = new SWTToolBar("kaliko.toolbar", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()), shell);
		ActionRegistry.getInstance().addContainer(toolBar);
		try {
			toolBar.initActions();
		} catch (ActionContainerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final SWTMainPanel mainPanel = new SWTMainPanel(shell, getController());
		mainPanel.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		//SWTStatusBar statusBar = new SWTStatusBar(shell, SWT.NONE);
		SmallTaskManagerPanel taskManagerPanel = new SmallTaskManagerPanel(shell, SWT.NONE);
		taskManagerPanel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		TaskManager.getInstance().addTaskListener(taskManagerPanel);
		
		// Create menu-bar
		
		SWTMenu menuBar = new SWTMenu("kaliko.menubar",PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()), shell);
		ActionRegistry.getInstance().addContainer(menuBar);
		menuBar.initActions();	
		shell.setMenuBar(menuBar.getMenu());
		
		//	restore view
		String initView = ConfigManager.getPreferences().get("org.evolvis.kaliko.initview", String.valueOf(CalendarView.MONTH_VIEW));
		getController().getViewSwitcher().setCurrentView(Integer.parseInt(initView));
		
		
		// Create context menus
		SWTMenu contextMenuEvent = new SWTMenu("kaliko.context.event", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()), shell, SWT.POP_UP);
		ActionRegistry.getInstance().addContainer(contextMenuEvent);
		contextMenuEvent.initActions();
		SWTMenu contextMenuSpace = new SWTMenu("kaliko.context.space", PropertyResourceBundle.getBundle("de.tarent.kaliko.messages.mnemonics", Locale.getDefault()), shell, SWT.POP_UP);
		ActionRegistry.getInstance().addContainer(contextMenuSpace);
		contextMenuSpace.initActions();
		
		// Register context menus on calendar-views
		getController().getViewSwitcher().setContextMenuForEvent(new SWTContextMenu(contextMenuEvent.getMenu()));
		getController().getViewSwitcher().setContextMenuForSpace(new SWTContextMenu(contextMenuSpace.getMenu()));

		setStatus(80);
		
		ActionRegistry.getInstance().enableContext(CONTEXT_EVENTS_DESELECTED);
		
		getController().getEventSelectionModel().addEventSelectionListener(new EventSelectionListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
			 */
			public void eventSelectionChanged(EventSelectionEvent eventSelectionEvent) {
				if(eventSelectionEvent.getSelectedEvents().size() == 0) {
					if(!ActionRegistry.getInstance().isContextEnabled(CONTEXT_EVENTS_DESELECTED))
						ActionRegistry.getInstance().enableContext(CONTEXT_EVENTS_DESELECTED);
				}
				else {
					if(ActionRegistry.getInstance().isContextEnabled(CONTEXT_EVENTS_DESELECTED))
						ActionRegistry.getInstance().disableContext(CONTEXT_EVENTS_DESELECTED);
				}
			}
		});
		
		setStatus(100);

		// LoginDialog is not needed any more
		if(loginDialog != null)
			loginDialog.setDialogVisible(false);
		
		try {
			int width = Integer.parseInt(ConfigManager.getPreferences().get("org.evolvis.kaliko.mainframe_width", "780"));
			int height = Integer.parseInt(ConfigManager.getPreferences().get("org.evolvis.kaliko.mainframe_height", "600"));

			shell.setSize(width, height);
		} catch (NumberFormatException excp) {
			logger.finer("could not parse main-frame-size-values from preference org.evolvis.kaliko.mainframe_*");
		}
		
		// If start_hidden is set to true dont open kalikos main window
		if(!ConfigManager.getPreferences().getBoolean("org.evolvis.kaliko.start_hidden", false))
			shell.open();
		
		// restore category-selection when categories are loaeded
		getController().addCategoryListener(new CategoryListener() {

			boolean executed = false;
			
			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categoriesLoaded(final CategoryEvent event) {
				// ensure to only run once
				if(executed)
					return;
				
				TaskManager.getInstance().registerExclusive(new Task() {

					/**
					 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
					 */
					public void cancel() {
						// not cancelable				
					}

					/**
					 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
					 */
					public void run(Context context) {
						executed = true;
						// insert categories into list
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategories"));
						getController().getCategorySelector().setAvailableCategories(event.getCategories());
						
						// restore selection
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategorySelection"));
						getController().getCategorySelector().setSelectedCategories(ConfigManager.getPreferences().get("org.evolvis.kaliko.selected_categories", ";"));
						
						// restore default categories
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategoryPreSelection"));
						getController().getCategorySelector().setDefaultCategories(ConfigManager.getPreferences().get("org.evolvis.kaliko.default_categories", ";"));
						
						// restore favorite categories
						context.setActivityDescription(messages.getString("Starter_Task_RestoringCategoryFavorites"));
						getController().getCategorySelector().setFavoriteCategories(ConfigManager.getPreferences().get("org.evolvis.kaliko.favorite_categories", ";"));
												
					}
					
				}, messages.getString("Starter_Task_RestoringCategories"), false);
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categorySelectionChanged(CategoryEvent arg0) {
				// Nothing to do
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
			 */
			public void categoryViewSelectionChanged(CategoryEvent arg0) {
				// Nothing to do
			}
		});
		
		// Register data manager to listen on changes in ui
		getController().addCategoryListener((OctopusDataManager)getCalendarDataManager());
		getController().addCalendarSelectionListener((OctopusDataManager)getCalendarDataManager());
		getController().addCalendarViewListener((OctopusDataManager)getCalendarDataManager());
		
		getController().addCalendarDataListener(new CalendarDataListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsAdded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
			 */
			public void eventsAdded(CalendarDataEvent arg0) {
				updateGUI();
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsChanged(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
			 */
			public void eventsChanged(CalendarDataEvent arg0) {
				updateGUI();
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsDeleted(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
			 */
			public void eventsDeleted(CalendarDataEvent arg0) {
				updateGUI();
			}

			/**
			 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsLoaded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
			 */
			public void eventsLoaded(CalendarDataEvent arg0) {
				updateGUI();
			}
			
			public void updateGUI() {
				mainPanel.updateUI();
			}
		});
		
		// Load categories
		getCalendarDataManager().loadCategories();
		
		while(!shell.isDisposed())
			if (!display.readAndDispatch()) display.sleep();
		
		display.dispose();
		ActionRegistry.getInstance().getAction("exit").actionPerformed(new ActionEvent(mainPanel.getFrame(), -1, "exit"));
	}
	
	protected void tryToConnect() {
		try {
			setStatusMessage(messages.getString("LoginDialog_Progress_Authenticating"));
			setStatus(10);
			connection = CalendarOctopusConnection.createConnection(selectedConnectionParameters);
			connection.setUsername(userName);
			connection.setPassword(password);
			connection.login();
			setStatus(40);

			setStatusMessage(messages.getString("LoginDialog_Progress_SavingPreferences"));
			// remember username next time
			ConfigManager.getPreferences()
			.put("last_username", userName);

			// remember connection next time
			ConfigManager.getPreferences().put("last_connection", selectedConnectionParameters.getName());
			
			// try to store password in a wallet
			String url = userName+"@"+selectedConnectionParameters.getName();
			
			try {
				WalletManager.getInstance().writePassword("kaliko", "kaliko", url, password);
			} catch (WalletException e) {
				logger.warning("Could not store login-data in any wallet-system");
			}

			setStatus(50);

			display.asyncExec(new Runnable() {

				/**
				 * @see java.lang.Runnable#run()
				 */
				public void run() {
					initApplication();	
				}
				
			});

		} catch(final OctopusCallException excp) {
			excp.printStackTrace();
			
			Display.getCurrent().asyncExec(new Runnable() {

				/**
				 * @see java.lang.Runnable#run()
				 */
				public void run() {
					MessageBox message = new MessageBox(loginDialog != null ? loginDialog.getShell() : new Shell(), SWT.ICON_WARNING);
					message.setMessage(getCauseMessage(excp));
					message.setText("Error");
					message.open();
					if(loginDialog != null)
						loginDialog.reset();
				}
			});
		}
	}
	
	protected void setStatus(int percent) {
		if(loginDialog != null)
			loginDialog.setStatus(percent);
	}
	
	protected void setStatusMessage(String message) {
		if(loginDialog != null)
			loginDialog.setStatusText(message);
		else
			logger.info(message);
	}
	
	protected String getCauseMessage(Throwable cause) {

		Throwable currentCause = cause;

		while (currentCause.getCause() != null)
			currentCause = currentCause.getCause();

		return currentCause.getLocalizedMessage();
	}
	
	public CalendarApplicationController getController() {
		if(controller == null)  {
			controller = new DefaultCalendarApplicationController(getCalendarDataManager());
			ActionHelper.setController(controller);
		}
		return controller;
	}
	
	protected CalendarDataManager getCalendarDataManager() {
		if(calendarDataManager == null)
			calendarDataManager = new OctopusDataManager(new OctopusDataSource(connection));

		return calendarDataManager;
	}
	
	protected class LoginActionListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("login")) {
				new Thread() {
					public void run() {
						userName = loginDialog.getUserName();
						password = loginDialog.getPassword();
						selectedConnectionParameters = loginDialog.getSelectedConnection();

						tryToConnect();
					}
				}.start();
			}
			else if(e.getActionCommand().equals("cancel") || e.getActionCommand().equals("quit"))
				System.exit(0);
		}
	}
}
