/**
 * 
 */
package de.tarent.kaliko.ui.action;


import org.eclipse.swt.widgets.Menu;
import org.evolvis.kaliko.widgets.common.ContextMenu;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTContextMenu implements ContextMenu {
	
	Menu menu;
	
	public SWTContextMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.ContextMenu#showContextMenuAt(java.awt.Point)
	 */
	public void showContextMenuAt(final int x, final int y) {
		menu.getDisplay().asyncExec(new Runnable() {
			public void run() {
				menu.setLocation(x, y);
				menu.setVisible(true);
			}
		}
		);

	}
}