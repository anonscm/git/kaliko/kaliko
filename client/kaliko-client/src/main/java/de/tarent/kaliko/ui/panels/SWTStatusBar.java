/**
 * 
 */
package de.tarent.kaliko.ui.panels;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 * 
 */
public class SWTStatusBar extends Composite {

	Label user;
	String userName = "Benutzername";
	Label load;
	ProgressBar progressbar;
	Boolean loading = true;

	Button button;
	static Text text;

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display, SWT.CLOSE | SWT.RESIZE);
		shell.setLayout(new MigLayout("", "10[grow]10"));
		text = new Text(shell, SWT.NONE);
		text.setText("sfdsdfsrffgdfgsdh");
		text.setLayoutData("wrap");
		SWTStatusBar statusbar = new SWTStatusBar(shell, 0);
		statusbar.setLayoutData("south");

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

	public SWTStatusBar(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new MigLayout("", "[][200,grow][right][right]"));
		/*button = new Button(this, SWT.CHECK);
		Listener listener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == button) {
					loading = button.getSelection();
					refresh();

				}
			}
		};
		button.setLayoutData("wrap");
		button.setText("laden");
		button.addListener(SWT.Selection, listener);
		*/
		user = new Label(this, SWT.NONE);
		user.setText(userName);

		load = new Label(this, SWT.NONE);
		load.setText("Lade Ereignisse:");
		load.setLayoutData("skip");
		progressbar = new ProgressBar(this, SWT.NONE);
		progressbar.setSelection(25);
		refresh();
		// SmallTaskManagerPanel taskManagerPanel = new
		// SmallTaskManagerPanel(this, SWT.NONE);
		// TaskManager.getInstance().addTaskListener(taskManagerPanel);
	}

	public void refresh() {

		progressbar.setVisible(loading);
		load.setVisible(loading);

	}

	/**
	 * Sets a user login name as a text for the according label.
	 * 
	 * @param userName
	 *            a text to be displayed
	 */
	public void setUserName(String userName) {
		
//		 if(userName == null) logger.warning("[statusBar]: can't set empty user name");
//		 userNameLabel.setText(Messages.getFormattedString("StatusBar_Label_User",
//		 userName));
	}

	public Boolean getLoading() {
		return loading;
	}

	public void setLoading(Boolean loading) {
		this.loading = loading;
	}
}
