/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.AbstractAction;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.common.CategorySelector;
import org.evolvis.kaliko.widgets.dialogs.EventDialog;
import org.evolvis.kaliko.widgets.swing.controller.DefaultColorModel;
import org.evolvis.kaliko.widgets.swing.controller.DefaultIconModel;
import org.evolvis.kaliko.widgets.swt.dialogs.SWTEventDialog;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.controller.ColorModel;
import de.tarent.kaliko.components.controller.EventSelectionModel;
import de.tarent.kaliko.components.controller.IconModel;
import de.tarent.kaliko.components.controller.ViewSwitcher;
import de.tarent.kaliko.components.controller.impl.DefaultEventSelectionModel;
import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.utils.Messages;
import de.tarent.kaliko.components.utils.StringUtils;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * 
 * TODO divide in multiple classes with separate scopes
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultCalendarApplicationController implements CalendarApplicationController {

	protected CategorySelector categorySelector;
	protected Calendar calendar;
	protected Locale locale;
	protected ViewSwitcher viewSwitcher;
	protected Frame frame;
	protected CalendarDataManager dataManager;
	protected ColorModel colorModel;
	protected IconModel iconModel;
	protected EventSelectionModel eventSelectionModel;

	protected Object shell;

	protected List<CalendarSelectionListener> calendarSelectionListeners;
	protected List<CategoryListener> categoryListeners;
	protected List<CalendarViewListener> calendarViewListeners;
	protected List<CalendarDataListener> calendarDataListeners;

	protected final static Logger logger = Logger.getLogger(DefaultCalendarApplicationController.class.getName());

	public DefaultCalendarApplicationController(CalendarDataManager dataManager) {
		this(Locale.getDefault(), dataManager);
	}

	public DefaultCalendarApplicationController(Locale locale, CalendarDataManager dataManager) {
		this(locale, Calendar.getInstance(locale), dataManager);
	}

	public DefaultCalendarApplicationController(Locale locale, Calendar calendar, CalendarDataManager dataManager) {
		this.locale = locale;
		this.calendar = calendar;
		this.dataManager = dataManager;
	}

	public void setCategorySelector(CategorySelector categorySelector) {
		this.categorySelector = categorySelector;
	}

	public CategorySelector getCategorySelector() {
		return categorySelector;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getCurrentCalendar()
	 */
	public Calendar getCurrentCalendar() {
		return calendar;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setCurrentCalendar(java.util.Calendar)
	 */
	public void setCurrentCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getLocale()
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setLocale(java.util.Locale)
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;		
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getViewSwitcher()
	 */
	public ViewSwitcher getViewSwitcher() {
		return viewSwitcher;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setViewSwitcher(de.tarent.kaliko.components.controller.ViewSwitcher)
	 */
	public void setViewSwitcher(ViewSwitcher viewSwitcher) {
		this.viewSwitcher = viewSwitcher;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setMainFrame(javax.swing.JFrame)
	 */
	public void setMainFrame(Frame frame) {
		this.frame = frame;

	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getMainFrame()
	 */
	public Frame getMainFrame() {
		return frame;
	}



	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestCreateOrEditEvent()
	 */
	public void requestCreateOrEditEvent() {
		if(getEventSelectionModel().eventsSelected())
			requestEditSelectedEvents();
		else
			requestNewEvent();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestNewEvent()
	 */
	public void requestNewEvent() {
		requestNewEvent((Calendar)getCurrentCalendar().clone(), (Calendar)getCurrentCalendar().clone());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestNewEvent(java.util.Calendar, java.util.Calendar)
	 */
	public void requestNewEvent(final Calendar begin, final Calendar end) {
		logger.fine("requestNewEvent");

		((Shell)getShell()).getDisplay().asyncExec(new Runnable() {

			public void run() {


				final EventDialog eventDialog = new SWTEventDialog((Shell)getShell());

				eventDialog.setSelectedCategories(getCategorySelector().getDefaultCategories());
				eventDialog.setAvailableCategories(getCalendarDataManager().getCalendarDataSource().getWritableCategories());
				eventDialog.setMode(EventDialog.MODE_NEW);
				eventDialog.setBeginDay(begin);
				eventDialog.setEndDay(end);
				eventDialog.setSubmitAction(new AbstractAction() {

					/**
					 * 
					 */
					private static final long serialVersionUID = -3181216076531521974L;

					public void actionPerformed(ActionEvent e) {
						getCalendarDataManager().addCalendarEvent(eventDialog.getEvent(), CategoryCollectionHelper.toList(eventDialog.getSelectedCategories()));
					}
				});

				eventDialog.open();
			}
		});
	}


	protected void requestDeleteEvent(Event event) {
		logger.fine("requestDeleteEvent "+event);

		// If event-title is long, trim it to 30 characters
		String trimmedTitle = StringUtils.trimToLength(event.getTitle(), 30);

		MessageBox message = new MessageBox((Shell)getShell(), SWT.YES | SWT.NO | SWT.ICON_QUESTION);
		message.setMessage(Messages.getFormattedString("DefaultCalendarApplicationController_DeleteEvent_Message", trimmedTitle));
		message.setText(Messages.getString("DefaultCalendarApplicationController_DeleteEvent_Title"));
	
//		if(JOptionPane.showConfirmDialog(getMainFrame(), Messages.getFormattedString("DefaultCalendarApplicationController_DeleteEvent_Message",
//				trimmedTitle),
//				Messages.getString("DefaultCalendarApplicationController_DeleteEvent_Title"),
//				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
		if(message.open() == SWT.YES) {

			// delete event in database
			getCalendarDataManager().deleteEvent(event);

			// event-selection on deleted events has to be removed
			getEventSelectionModel().clearSelection();
		}
	}

	public void requestEditEvent(final Event event) {
		logger.fine("requestEditEvent "+event);

		((Shell)getShell()).getDisplay().asyncExec(new Runnable() {

			public void run() {

				final EventDialog eventDialog = new SWTEventDialog((Shell)getShell());

				eventDialog.setEvent(event);

				List<Category> categories = new ArrayList<Category>();

				Iterator<Category> availableCategories = getCategorySelector().getAvailableCategories().iterator();

				while(availableCategories.hasNext()) {
					Category category = availableCategories.next();
					if(event.getCategoriesList().contains(category.getGuid()))
						categories.add(category);			
				}

				eventDialog.setSelectedCategories(categories);
				eventDialog.setMode(SWTEventDialog.MODE_EDIT);
				eventDialog.setSubmitAction(new AbstractAction() {

					/**
					 * 
					 */
					private static final long serialVersionUID = -6536723567366316058L;

					/**
					 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
					 */
					public void actionPerformed(ActionEvent e) {
						getCalendarDataManager().updateCalendarEvent(eventDialog.getEvent());
					}

				});
				eventDialog.open();
			}

		});
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getCalendarDataManager()
	 */
	public CalendarDataManager getCalendarDataManager() {
		return dataManager;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#moveCalendarCursor(int, int)
	 */
	public void moveCalendarCursor(int field, int amount) {
		Calendar oldCalendar = (Calendar)getCurrentCalendar().clone();
		getCurrentCalendar().add(field, amount);
		fireCalendarSelectionChanged(oldCalendar);
	}


	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#moveCalendarCursor(java.util.Map)
	 */
	public void moveCalendarCursor(Map<Integer, Integer> fieldsAndValues) {
		Calendar oldCalendar = (Calendar)getCurrentCalendar().clone();
		Iterator<Integer> it = fieldsAndValues.keySet().iterator();
		while(it.hasNext()) {
			Integer field = it.next();
			getCurrentCalendar().add(field, fieldsAndValues.get(field));
		}
		fireCalendarSelectionChanged(oldCalendar);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setCalendarCursorTo(java.util.Calendar)
	 */
	public void setCalendarCursorTo(Calendar calendar) {
		if(calendar == null) {
			logger.warning("argument 'calendar' is null");
			return;
		}

		Map<Integer, Integer> fieldsAndValues = new HashMap<Integer, Integer>();
		fieldsAndValues.put(Calendar.YEAR, calendar.get(Calendar.YEAR));
		fieldsAndValues.put(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR));
		setCalendarCursor(fieldsAndValues);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setCalendarCursor(int, int)
	 */
	public void setCalendarCursor(int field, int value) {
		if(getCurrentCalendar().get(field) == value) {
			// nothing to do, field already has this value
			logger.finer("field already has given value, doing nothing");
			return;
		}
		Calendar oldCalendar = (Calendar)getCurrentCalendar().clone();
		getCurrentCalendar().set(field, value);
		fireCalendarSelectionChanged(oldCalendar);
	}


	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setCalendarCursor(java.util.Map)
	 */
	public void setCalendarCursor(Map<Integer, Integer> fieldsAndValues) {
		Calendar oldCalendar = (Calendar)getCurrentCalendar().clone();
		Iterator<Integer> it = fieldsAndValues.keySet().iterator();
		boolean changes = false;

		while(it.hasNext()) {
			Integer field = it.next();

			// FIXME: Commented out because of problems with Java5
//			if(getCurrentCalendar().get(field) == fieldsAndValues.get(field)) {
//			// nothing to do, field already has this value
//			logger.finer("field already has given value, doing nothing");
//			continue;
//			} else {
			changes = true; 
			getCurrentCalendar().set(field, fieldsAndValues.get(field));
//			}
		}

		if(changes)
			fireCalendarSelectionChanged(oldCalendar);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireCalendarSelectionChanged()
	 */
	public void fireCalendarSelectionChanged(Calendar oldSelectedDay) {
		logger.fine("calendarSelectionChanged");
		synchronized (getCalendarSelectionListeners()) {

			Iterator<CalendarSelectionListener> it = getCalendarSelectionListeners().iterator();

			CalendarSelectionEvent event = new CalendarSelectionEvent(oldSelectedDay, (Calendar)getCurrentCalendar().clone());

			while(it.hasNext()) 
				it.next().calendarSelectionChanged(event);
		}
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireCategorySelectionChanged(java.util.List)
	 */
	public void fireCategorySelectionChanged(List<Category> newSelection) {
		logger.fine("categorySelectionChanged");

		synchronized (getCategoryListeners()) {
			Iterator<CategoryListener> it = getCategoryListeners().iterator();

			CategoryEvent event = new CategoryEvent(newSelection);

			while(it.hasNext()) 
				it.next().categoryViewSelectionChanged(event);
		}
	}



	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireCategoriesLoaded(java.util.List)
	 */
	public void fireCategoriesLoaded(List<Category> categories) {
		logger.fine("categoriesLoaded");

		synchronized (getCategoryListeners()) {
			Iterator<CategoryListener> it = getCategoryListeners().iterator();

			CategoryEvent event = new CategoryEvent(categories);

			while(it.hasNext()) 
				it.next().categoriesLoaded(event);
		}
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireCalendarViewChange(de.tarent.kaliko.components.views.CalendarView, de.tarent.kaliko.components.views.CalendarView)
	 */
	public void fireCalendarViewChange(CalendarView oldView, CalendarView newView) {
		logger.fine("calendarViewChanged");

		synchronized (getCalendarViewListeners()) {
			Iterator<CalendarViewListener> it = getCalendarViewListeners().iterator();

			CalendarViewChangeEvent event = new CalendarViewChangeEvent(oldView, newView);

			while(it.hasNext()) 
				it.next().viewChanged(event);
		}	
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireEventsLoaded(de.tarent.kaliko.objects.Event)
	 */
	public void fireEventsLoaded(List<Event> loadedEvents) {
		logger.fine("calendarDataChanged");

		synchronized (getCalendarDataListeners()) {
			Iterator<CalendarDataListener> it = getCalendarDataListeners().iterator();

			CalendarDataEvent event = new CalendarDataEvent(loadedEvents);

			while(it.hasNext()) 
				it.next().eventsLoaded(event);
		}
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireEventsAdded(java.util.List)
	 */
	public void fireEventsAdded(List<Event> events) {
		logger.fine("eventsAdded");

		synchronized (getCalendarDataListeners()) {
			Iterator<CalendarDataListener> it = getCalendarDataListeners().iterator();

			CalendarDataEvent event = new CalendarDataEvent(events);

			while(it.hasNext())
				it.next().eventsAdded(event);
		}
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#fireEventsDeleted(java.util.List)
	 */
	public void fireEventsDeleted(List<Event> events) {
		logger.fine("eventsDeleted");

		synchronized (getCalendarDataListeners()) {
			Iterator<CalendarDataListener> it = getCalendarDataListeners().iterator();

			CalendarDataEvent event = new CalendarDataEvent(events);

			while(it.hasNext())
				it.next().eventsDeleted(event);
		}
	}

	protected List<CalendarSelectionListener> getCalendarSelectionListeners() {
		if(calendarSelectionListeners == null)
			calendarSelectionListeners = Collections.synchronizedList(new ArrayList<CalendarSelectionListener>());
		return calendarSelectionListeners; 
	}

	protected List<CategoryListener> getCategoryListeners() {
		if(categoryListeners == null)
			categoryListeners = Collections.synchronizedList(new ArrayList<CategoryListener>());
		return categoryListeners; 
	}

	protected List<CalendarViewListener> getCalendarViewListeners() {
		if(calendarViewListeners == null)
			calendarViewListeners = Collections.synchronizedList(new ArrayList<CalendarViewListener>());
		return calendarViewListeners; 
	}

	protected List<CalendarDataListener> getCalendarDataListeners() {
		if(calendarDataListeners == null)
			calendarDataListeners = Collections.synchronizedList(new ArrayList<CalendarDataListener>());
		return calendarDataListeners; 
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#addCalendarSelectionListener(de.tarent.kaliko.components.listener.CalendarSelectionListener)
	 */
	public void addCalendarSelectionListener(CalendarSelectionListener listener) {
		getCalendarSelectionListeners().add(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#addCategoryListener(de.tarent.kaliko.components.listener.CategoryListener)
	 */
	public void addCategoryListener(CategoryListener listener) {
		getCategoryListeners().add(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#removeCategoryListener(de.tarent.kaliko.components.listener.CategoryListener)
	 */
	public void removeCategoryListener(CategoryListener listener) {
		getCategoryListeners().remove(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#addCalendarViewListener(de.tarent.kaliko.components.listener.CalendarViewListener)
	 */
	public void addCalendarViewListener(CalendarViewListener listener) {
		getCalendarViewListeners().add(listener);		
	}


	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#addCalendarDataListener(de.tarent.kaliko.components.listener.CalendarDataListener)
	 */
	public void addCalendarDataListener(CalendarDataListener listener) {
		getCalendarDataListeners().add(listener);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getColorModel()
	 */
	public ColorModel getColorModel() {
		if(colorModel == null) {
			colorModel = new DefaultColorModel();
		}
		return colorModel;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getIconModel()
	 */
	public IconModel getIconModel() {
		if(iconModel == null) {
			iconModel = new DefaultIconModel();
		}
		return iconModel;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestDeleteEvents(java.util.List)
	 */
	public void requestDeleteEvents(List<Event> events) {
		Iterator<Event> it = events.iterator();

		while(it.hasNext())
			requestDeleteEvent(it.next());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestDeleteSelectedEvents()
	 */
	public void requestDeleteSelectedEvents() {
		// need to copy events in new list in order to prevent a ConcurrentModificationException
		requestDeleteEvents(new ArrayList<Event>(getEventSelectionModel().getSelectedEvents()));
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestEditEvents(java.util.List)
	 */
	public void requestEditEvents(List<Event> events) {
		Iterator<Event> it = events.iterator();

		while(it.hasNext())
			requestEditEvent(it.next());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#requestEditSelectedEvents()
	 */
	public void requestEditSelectedEvents() {
		requestEditEvents(getEventSelectionModel().getSelectedEvents());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getEventSelectionModel()
	 */
	public EventSelectionModel getEventSelectionModel() {
		if(eventSelectionModel == null)
			eventSelectionModel = new DefaultEventSelectionModel();
		return eventSelectionModel;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#getShell()
	 */
	public Object getShell() {
		return shell; 
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CalendarApplicationController#setShell(java.lang.Object)
	 */
	public void setShell(Object shell) {
		this.shell = shell;
	}
}
