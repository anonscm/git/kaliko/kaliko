/**
 * 
 */
package de.tarent.kaliko.ui.actions;

import java.awt.event.ActionEvent;

import org.eclipse.swt.widgets.Shell;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.kaliko.components.actions.impl.ActionHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ToggleMainWindowVisibility extends AbstractGUIAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6244827441974286756L;

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0) {
//		ActionHelper.getController().getMainFrame().setVisible(
//				!ActionHelper.getController().getMainFrame().isVisible());
		
		Shell shell = (Shell)ActionHelper.getController().getShell();
		shell.setVisible(!shell.isVisible());
	}

}
