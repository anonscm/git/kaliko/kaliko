/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.connection;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.remote.OctopusRemoteConnection;



/** 
 * 
 * This class holds an internal connection to the octopus.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CalendarOctopusConnection {

	protected static String OC_CONNECTION_IDENTIFIER = "kaliko";
	protected final static Logger logger = Logger.getLogger(CalendarOctopusConnection.class.getName());

	public static OctopusConnection createConnection(ConnectionParameters connection) {
		String module = connection.getModule();
		URL url;
		try {
			url = connection.toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		String pathToSSLKeyFile = null;
		
		if (System.getProperty("de.tarent.kaliko.install.dir") != null)
			pathToSSLKeyFile = System.getProperty("de.tarent.kaliko.install.dir") + "tarent_store.dat";
		else  {
			File sslKeyFileForDev = new File (System.getProperty("user.dir") + "/src/main/ssl/tarent_store.dat");
			if (sslKeyFileForDev.isFile())
				pathToSSLKeyFile = System.getProperty("user.dir") + "/src/main/ssl/tarent_store.dat";
			else {
				// for debian-packaging
				pathToSSLKeyFile= "/usr/share/config/kaliko/ssl/tarent_store.dat";
			}
		}

		if(pathToSSLKeyFile != null){
			if (url.getProtocol().contains("https") && System.setProperty("javax.net.ssl.trustStore", pathToSSLKeyFile ) == null){ //$NON-NLS-1$ //$NON-NLS-2$
				// SSL wanted
				logger.log(Level.FINER, "configuring for SSL"); //$NON-NLS-1$
				System.setProperty("javax.net.ssl.trustStore", pathToSSLKeyFile); //$NON-NLS-1$ //$NON-NLS-2$
				System.setProperty("javax.net.ssl.trustStorePassword", "tarent"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else
			logger.warning("No proper key for SSL encryption defined");
		
		// octopus connection configuration will be supplied as a map
		Map<String, String> ocConfig = new HashMap<String, String>();

		ocConfig.put(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_REMOTE);
		ocConfig.put(OctopusConnectionFactory.MODULE_KEY, module);
		ocConfig.put(OctopusRemoteConnection.PARAM_SERVICE_URL, url.toString());
		ocConfig.put(OctopusRemoteConnection.AUTH_TYPE, OctopusRemoteConnection.AUTH_TYPE_SESSION);
		ocConfig.put(OctopusRemoteConnection.AUTO_LOGIN, "true");

		Environment env = ConfigManager.getEnvironment();

		if (env.getAsBoolean(Key.USE_OCTOPUS_SESSION_COOKIE)) {
			ocConfig.put(OctopusRemoteConnection.USE_SESSION_COOKIE, "true");
		}

		if("ALL".equalsIgnoreCase(env.get(Key.DEBUG)))
			ocConfig.put(OctopusRemoteConnection.CONNECTION_TRACKING, "true");

		ocConfig.put(OctopusRemoteConnection.KEEP_SESSION_ALIVE, env.get(Key.OCTOPUS_KEEP_SESSION_ALIVE));

		// setting configuration map for this factory
		OctopusConnectionFactory.getInstance().setConfiguration(OC_CONNECTION_IDENTIFIER, ocConfig);

		// getting and saving octopus connection
		return (OctopusRemoteConnection) OctopusConnectionFactory.getInstance().getConnection(OC_CONNECTION_IDENTIFIER);
	}
}
