/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.data.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Context;
import org.evolvis.taskmanager.TaskManager.Task;

import de.tarent.commons.ui.I18n;
import de.tarent.kaliko.components.actions.impl.ActionHelper;
import de.tarent.kaliko.components.data.CalendarDataManager;
import de.tarent.kaliko.components.data.CalendarDataSource;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CalendarViewListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.components.views.DayView;
import de.tarent.kaliko.components.views.MonthView;
import de.tarent.kaliko.components.views.WeekView;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.CalendarEventFilter;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class OctopusDataManager implements CalendarDataManager, CategoryListener, CalendarSelectionListener, CalendarViewListener {

	protected OctopusDataSource dataSource;
	protected boolean precaching;
	protected final static Logger logger = Logger.getLogger(OctopusDataManager.class.getName());
	protected I18n messages;

	public OctopusDataManager(OctopusDataSource dataSource) {
		this.dataSource = dataSource;
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#doesPrecaching()
	 */
	public boolean doesPrecaching() {
		return precaching;
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#setPrecaching(boolean)
	 */
	public void setPrecaching(boolean precaching) {
		this.precaching = precaching;
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#addCalendarEvent(de.tarent.kaliko.objects.Event, java.util.List)
	 */
	public void addCalendarEvent(final Event event, final List<Category> categories) {
		TaskManager.getInstance().register(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_Add_Events_AddingEvent"));
				dataSource.addCalendarEvent(event, categories);
				List<Event> events = new ArrayList<Event>(1);
				events.add(event);
				ActionHelper.getController().fireEventsAdded(events);
			}

		}, messages.getString("Task_Add_Events_Title"), false);
	}

	/** (non-Javadoc)
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#deleteEvent(de.tarent.kaliko.objects.Event)
	 */
	public void deleteEvent(final Event event) {
		TaskManager.getInstance().register(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_Delete_Event_DeletingEvent"));
				dataSource.deleteEvent(event);
				List<Event> events = new ArrayList<Event>(1);
				events.add(event);
				ActionHelper.getController().fireEventsDeleted(events);
			}

		}, messages.getString("Task_Delete_Event_Title"), false);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getCategories()
	 */
	public void loadCategories() {

		TaskManager.getInstance().registerBlocking(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_LoadCategories_LoadingCategories"));
				List<Category> categories = dataSource.getCategories();
				// notify listeners about loaded categories
				ActionHelper.getController().fireCategoriesLoaded(categories);
			}

		}, messages.getString("Task_LoadCategories_Title"), false);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getDatabaseDescription()
	 */
	public String getDatabaseDescription() {
		return dataSource.getDatabaseDescription();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getEvents(de.tarent.kaliko.objects.impl.CalendarEventFilter)
	 */
	public void loadEvents(final CalendarEventFilter filter) {
		TaskManager.getInstance().register(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_LoadEvents_LoadingEvents"));
				List<Event> events = dataSource.getEvents(filter);
				ActionHelper.getController().fireEventsLoaded(events);
			}

		}, messages.getString("Task_LoadEvents_Title"), false);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getEvents(java.util.Calendar, java.util.Calendar, java.util.List)
	 */
	public void loadEvents(final Calendar begin, final Calendar end, final List<Category> categories) {
		TaskManager.getInstance().register(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_LoadEvents_LoadingEvents"));
				List<Event> events = dataSource.getEvents(begin, end, categories);
				ActionHelper.getController().fireEventsLoaded(events);
			}

		}, messages.getString("Task_LoadEvents_Title"), false);
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getServerDescription()
	 */
	public String getServerDescription() {
		return dataSource.getServerDescription();
	}
	
	
	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#getServerVersion()
	 */
	public String getServerVersion() {
		return dataSource.getServerVersion();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#getUsername()
	 */
	public String getUsername() {
		return dataSource.getUsername();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataSource#updateCalendarEvent(de.tarent.kaliko.objects.Event)
	 */
	public void updateCalendarEvent(final Event event) {
		TaskManager.getInstance().register(new Task() {

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#cancel()
			 */
			public void cancel() {
				// not cancelable
			}

			/**
			 * @see de.tarent.commons.utils.TaskManager.Task#run(de.tarent.commons.utils.TaskManager.Context)
			 */
			public void run(Context context) {
				context.setActivityDescription(messages.getString("Task_Update_Event_UpdatingEvent"));
				dataSource.updateCalendarEvent(event);
				// FIXME does not have the old event-data here
				//ActionHelper.getController().fireEventChanged(event, event);
				loadEventsForCurrentScope();
			}

		}, messages.getString("Task_Update_Event_Title"), false);
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoriesLoaded(CategoryEvent event) {
		// nothing to do
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categorySelectionChanged(CategoryEvent event) {

	}
	
	public void loadEventsForCurrentScope() {
		CalendarView view = ActionHelper.getController().getViewSwitcher().getCurrentView();

		loadEvents(view.getModel().getScopeBegin(), view.getModel().getScopeEnd(), new ArrayList<Category>(ActionHelper.getController().getCategorySelector().getSelectedCategories()));
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoryViewSelectionChanged(CategoryEvent event) {
		loadEventsForCurrentScope();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		// check if scope for current shown view changed and if necessary load events
		
		CalendarView view = ActionHelper.getController().getViewSwitcher().getCurrentView();
		if(view instanceof MonthView) {
			if(CalendarUtils.isSameMonth(event.getOldSelectedDay(), event.getSelectedDay()))
				return;
		} else if(view instanceof WeekView) {
			if(CalendarUtils.isSameWeek(event.getOldSelectedDay(), event.getSelectedDay()))
				return;
		} else if(view instanceof DayView) {
			if(CalendarUtils.isSameDay(event.getOldSelectedDay(), event.getSelectedDay()))
				return;
		} else
			logger.warning("unknown view-type");
		
		loadEventsForCurrentScope();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarViewListener#viewChanged(de.tarent.kaliko.components.listener.impl.CalendarViewChangeEvent)
	 */
	public void viewChanged(CalendarViewChangeEvent arg0) {
		loadEventsForCurrentScope();
	}

	/**
	 * @see de.tarent.kaliko.components.data.CalendarDataManager#getCalendarDataSource()
	 */
	public CalendarDataSource getCalendarDataSource() {
		return dataSource;
	}
}

