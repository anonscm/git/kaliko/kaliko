/*
 * Kaliko Client,
 * A Webservice Based Calendar
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.ui.panels;

import java.awt.Font;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.I18n;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class StatusBar extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3423706869609041904L;
	private static final Logger logger = Logger.getLogger(StatusBar.class.getName());
	
	protected JLabel userNameLabel;
	
	protected I18n messages;

	public StatusBar() {
		this.messages = new I18n("de.tarent.kaliko.messages.messages");
		
		FormLayout layout = new FormLayout("5dlu, pref, fill:0dlu:grow, right:max(p;100dlu), 5dlu",
        "center:min(pref;15dlu)");
        setLayout(layout);
        
        CellConstraints cc = new CellConstraints();
        
        userNameLabel = createPlainLabel(messages.getFormattedString("StatusBar_Label_User", "unknown"));
        
//        TaskManagerPanel p = new TaskManagerPanel(ActionHelper.getController().getMainFrame());
//        TaskManager.getInstance().addTaskListener(p);
        
        add(userNameLabel, cc.xy(2,1));
//        add(p, cc.xy(4,1));
	}
	
	private JLabel createPlainLabel(String text) {
    	JLabel label = new JLabel(text);
    	label.setFont(label.getFont().deriveFont(Font.PLAIN));
    	return label;
    }
	
    /**
     * Sets a user login name as a text for the according label.
     * @param userName a text to be displayed
     */
    public void setUserName( String userName ) {
        if(userName == null) logger.warning("[statusBar]: can't set empty user name");
        userNameLabel.setText(messages.getFormattedString("StatusBar_Label_User", userName));
    }
}
