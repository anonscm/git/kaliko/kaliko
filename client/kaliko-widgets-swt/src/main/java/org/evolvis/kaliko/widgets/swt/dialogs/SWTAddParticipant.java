package org.evolvis.kaliko.widgets.swt.dialogs;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * Dialog for input of Participants data
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 * 
 */ 
public class SWTAddParticipant {
	
protected static Shell shell;	
protected SWTEventDialog parent;	

protected Label firstNameLabel;
protected Text firstNameText;
protected Label lastNameLabel;
protected Text lastNameText;
protected Label emailLabel;
protected Text emailText;
protected Button submitButton;
protected Button cancelButton;	
public SWTAddParticipant(final SWTEventDialog parent) {
	this.parent=parent;
	shell = new Shell(parent.getShell(),SWT.APPLICATION_MODAL|SWT.TITLE|SWT.CLOSE|SWT.RESIZE);
	shell.setText("Teilnehmerdaten eingeben");
	shell.setLayout(new MigLayout("wrap2","[][grow,fill]"));
	firstNameLabel=new Label(shell,SWT.NONE);
	firstNameLabel.setText("Vorname:");
	firstNameText=new Text(shell,SWT.BORDER);
	firstNameText.setLayoutData("wmin 300");
	lastNameLabel=new Label(shell,SWT.NONE);
	lastNameLabel.setText("Nachname:");
	lastNameText=new Text(shell,SWT.BORDER);
	emailLabel=new Label(shell,SWT.NONE);
	emailLabel.setText("Email-Adresse:");
	emailText=new Text(shell,SWT.BORDER);
	
	Listener buttonListener=new Listener() {
	
		public void handleEvent(Event event) {
			if (event.widget==submitButton){
				parent.addParticipant(firstNameText.getText(),lastNameText.getText(),emailText.getText());
				shell.dispose();
				
			}else if (event.widget==cancelButton){
				shell.dispose();
				
			}
	
		}
	
	};
	submitButton=new Button(shell,SWT.None);
	submitButton.setText("Übernehmen");
	submitButton.setLayoutData("wmin 150");
	submitButton.addListener(SWT.Selection, buttonListener);
	cancelButton=new Button(shell,SWT.None);
	cancelButton.setText("Verwerfen");
	cancelButton.setLayoutData("wmax 150,right");
	cancelButton.addListener(SWT.Selection, buttonListener);
	
	shell.pack();
}

public static void open() {
	shell.open();
}
}
