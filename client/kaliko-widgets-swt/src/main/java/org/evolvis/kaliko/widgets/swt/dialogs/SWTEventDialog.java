package org.evolvis.kaliko.widgets.swt.dialogs;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.evolvis.kaliko.nuyo.SelectorStarter;
import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.common.ParticipantCollectionHelper;
import org.evolvis.kaliko.widgets.common.utils.Messages;
import org.evolvis.kaliko.widgets.dialogs.EventDialog;
import org.evolvis.kaliko.widgets.swt.TabbedCategoryTables;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.groupware.Address;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Participant;
import de.tarent.kaliko.objects.impl.CategoryImpl;
import de.tarent.kaliko.objects.impl.EventImpl;
import de.tarent.kaliko.objects.impl.ParticipantImpl;

/**
 * Dialog to create new event or edit existing event
 * 
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 * 
 */
public class SWTEventDialog implements EventDialog {

	protected Shell shell;
	protected Shell parent;
	protected TabFolder tabs;

	protected TabItem generalTab;

	protected Composite generalComposite;
	protected Label titleLabel;
	protected Text titleText;
	protected Label locationLabel;
	protected Text locationText;

	protected Group datetimeGroup;
	protected Label beginLabel;
	protected Label endLabel;
	protected Button allDayCheck;
	protected DateTime beginDate;
	protected DateTime beginTime;
	protected DateTime endDate;
	protected DateTime endTime;

	protected Group categoryGroup;
	protected Label categoryLabel;
	protected Button categoryButton;

	protected TabItem noticetab;
	protected Text noticeText;

	protected TabItem participantTab;
	protected Composite participantComposite;

	protected int height;
	protected List availableContacts;
	protected List participantsList;
	protected Button addNuyoButton;
	protected Button removeButton;
	protected Button adHocButton;
	protected Button selectall;

	protected Button submitButton;
	protected Button cancelButton;

	protected Label chooseCategory;
	protected Label availableLabel;
	protected Button categorySubmitButton;
	protected Button categoryCancelButton;

	protected Shell categorySelection;
	
	protected SWTCategorySelector selector;
	protected SWTParticipantSelector participantSelector;
	
	protected de.tarent.kaliko.objects.Event event;
	
	protected Action submitAction;
	
	protected Collection<Category> selectedCategories;
	protected Collection<Category> availableCategories;
	//protected Collection<Participant> availableParticipants;
	protected Collection<Participant> participants;
	
	protected final static Logger logger = Logger.getLogger(SWTEventDialog.class.getName());
/**
 * 
 * main method just for testing
 */
	public static void main(String[] args) {
		Display display=new Display();
		Shell shelly=new Shell(display);
		shelly.setLayout(new MigLayout());
		Collection<Category> av= new ArrayList<Category>();
		for (int i = 0; i < 6; i++) {
			Category item=new CategoryImpl("Kategorie"+i,"");
			av.add(item);
		}
		av.add(new CategoryImpl("AAA",""));
		TabbedCategoryTables tab = new TabbedCategoryTables(shelly);
		tab.setAvailableCategories(av);
		shelly.open();
		SWTEventDialog loginDialog=new SWTEventDialog(shelly);
		loginDialog.setAvailableCategories(av);
		loginDialog.open();
		while(!loginDialog.getShell().isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
	
	public SWTEventDialog (Shell parent) {
		this.parent = parent;
		shell = new Shell(parent,SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setText(Messages.getString("EventDialog_Title_New_Event"));

		shell.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][]"));

		tabs = new TabFolder(shell, SWT.BORDER);
		tabs.setLayoutData("span 2,wrap");

		Listener buttonListener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == submitButton) {
					submitAction();
					submitAction.actionPerformed(new ActionEvent(this, 0, ""));
					SWTEventDialog.this.getShell().close();
				}
				else if (event.widget == cancelButton)
					getShell().close();
				else if (event.widget == allDayCheck) {
					beginTime.setEnabled(!allDayCheck.getSelection());
					endTime.setEnabled(!allDayCheck.getSelection());
					
					beginTime.setVisible(!allDayCheck.getSelection());
					endTime.setVisible(!allDayCheck.getSelection());
				} else if (event.widget == categoryButton) {
					 selector = new SWTCategorySelector(SWTEventDialog.this, SWT.APPLICATION_MODAL
							| SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
					 selector.setAvailableCategories(availableCategories);
					 selector.setSelectedCategories(getSelectedCategories());
					 selector.setSubmitAction(new AbstractAction() {

						/**
						 * 
						 */
						private static final long serialVersionUID = 8606735557876624070L;

						/**
						 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
						 */
						public void actionPerformed(ActionEvent arg0) {
							selectedCategories = CategoryCollectionHelper.toList(selector.getSelectedCategories());
							refreshCategoryLabel();
						}
						 
					 });

				} else if (event.widget == addNuyoButton) {

					SelectorStarter selectorStarter = new SelectorStarter();
					Addresses addresses = selectorStarter.showSelectorAndGetAddresses();
					Collection<Participant> participants = new ArrayList<Participant>(addresses.getIndexMax()+1);
					
					for(int i=0; i <= addresses.getIndexMax(); i++) {
						Address currentAddress = addresses.get(i);
						Participant nuyoParticipant = new ParticipantImpl(currentAddress.getVorname(),
								currentAddress.getNachname(),
								currentAddress.getEmailAdresseDienstlich());
						participants.add(nuyoParticipant);
						
					}
					setParticipants(participants);
					
//					participantSelector = new SWTParticipantSelector(SWTEventDialog.this);
//					participantSelector.setSelectedParticipants(participants);
//					participantSelector.open();
					
				} else if (event.widget == removeButton) {
					if (participantsList.getSelectionCount() != 0) {
						for (int  i = participantsList.getSelectionIndices().length-1;i>=0; i--) {
							ParticipantCollectionHelper.toList(participants).remove(participantsList.getSelectionIndices()[i]);
							participantsList.remove(participantsList.getSelectionIndices()[i]);
							}

					}
				}else if (event.widget == adHocButton) {
				
					SWTAddParticipant adhocDialog= new SWTAddParticipant(SWTEventDialog.this);
					SWTAddParticipant.open();
					
				}else if (event.widget == selectall) {
				
					availableContacts.selectAll();
					
				}

			}
		};

		generalTab = new TabItem(tabs, SWT.NONE);
		generalTab.setText(Messages.getString("EventDialog_Tab_Title_General"));

		generalComposite = new Composite(tabs, SWT.NONE);
		generalComposite.setLayout(new MigLayout("", "[][grow,fill]"));

		titleLabel = new Label(generalComposite, SWT.NONE);
		titleLabel.setText(Messages
				.getString("EventDialog_Tab_General_Event_Title"));

		titleText = new Text(generalComposite, SWT.BORDER);
		titleText.setLayoutData("wrap");

		locationLabel = new Label(generalComposite, SWT.NONE);
		locationLabel.setText(Messages
				.getString("EventDialog_Tab_General_Event_Location"));

		locationText = new Text(generalComposite, SWT.BORDER);
		locationText.setLayoutData("wrap");

		datetimeGroup = new Group(generalComposite, SWT.NONE);
		datetimeGroup.setText(Messages
				.getString("EventDialog_Tab_General_Separator_DateAndTime"));
		datetimeGroup.setLayout(new MigLayout("", "[]20[][][200,grow, right]",
				"[]10[]"));
		datetimeGroup.setLayoutData("span 2,grow,wrap");

		Listener dateTimeListener = new Listener(){
			public void handleEvent(Event event) {
					if(event.widget==beginDate||event.widget==endDate){
					if (beginDate.getYear()==endDate.getYear()&&beginDate.getMonth()>endDate.getMonth())
							{endDate.setMonth(beginDate.getMonth());}
					else if	(beginDate.getYear()==endDate.getYear()&&beginDate.getMonth()==endDate.getMonth() && beginDate.getDay()>endDate.getDay()){
						endDate.setDay(beginDate.getDay());
					}else if(beginDate.getYear()>endDate.getYear()){
						endDate.setYear(beginDate.getYear());
					}
					}
					else if (event.widget ==beginTime||event.widget==endTime){
						if(beginDate.getYear()==endDate.getYear()&&beginDate.getMonth()==endDate.getMonth() && beginDate.getDay()==endDate.getDay()){
						if(beginTime.getHours()>endTime.getHours()){
							endTime.setHours(beginTime.getHours());
						}
						else if(beginTime.getHours()==endTime.getHours()&&beginTime.getMinutes()>endTime.getMinutes()){
							endTime.setMinutes(beginTime.getMinutes());
						}
						}
					}
				}};
		beginLabel = new Label(datetimeGroup, SWT.NONE);
		beginLabel.setText(Messages
				.getString("EventDialog_Tab_General_Event_Start"));
		beginDate = new DateTime(datetimeGroup, SWT.DATE | SWT.BORDER);
		beginDate.addListener(SWT.Selection, dateTimeListener);
		beginTime = new DateTime(datetimeGroup, SWT.TIME | SWT.SHORT
				| SWT.BORDER);
		beginTime.addListener(SWT.Selection, dateTimeListener);
		
		
		allDayCheck = new Button(datetimeGroup, SWT.CHECK);
		allDayCheck.setText(Messages
				.getString("EventDialog_Tab_General_Event_AllDay"));
		allDayCheck.setLayoutData("skip,wrap");
		allDayCheck.addListener(SWT.Selection, buttonListener);

		endLabel = new Label(datetimeGroup, SWT.NONE);
		endLabel.setText(Messages
				.getString("EventDialog_Tab_General_Event_End"));
		endDate = new DateTime(datetimeGroup, SWT.DATE | SWT.BORDER);
		endDate.addListener(SWT.Selection, dateTimeListener);
		endTime = new DateTime(datetimeGroup, SWT.TIME | SWT.SHORT | SWT.BORDER);
		endTime.addListener(SWT.Selection, dateTimeListener);
		
		categoryGroup = new Group(generalComposite, SWT.NONE);
		categoryGroup.setText(Messages
				.getString("EventDialog_Tab_General_Separator_Categories"));
		categoryGroup.setLayout(new MigLayout("", "[grow,left][grow,right]"));
		categoryGroup.setLayoutData("span 2,grow");

		categoryLabel = new Label(categoryGroup, SWT.NONE);
		categoryLabel.setLayoutData("grow,span5");
		refreshCategoryLabel();
		
		categoryButton = new Button(categoryGroup, SWT.NONE);
		categoryButton.setText(Messages
				.getString("EventDialog_SelectCategories_Button"));
		categoryButton.addListener(SWT.Selection, buttonListener);

		generalTab.setControl(generalComposite);

		noticetab = new TabItem(tabs, SWT.NONE);
		noticetab.setText(Messages.getString("EventDialog_Tab_Title_Note"));
		noticeText = new Text(tabs, SWT.MULTI | SWT.BORDER);
		noticeText.setLayoutData("span,hmin=" + tabs.getBounds().height);

		noticetab.setControl(noticeText);

		participantTab = new TabItem(tabs, SWT.NONE);
		participantTab.setText(Messages
				.getString("EventDialog_Tab_Title_Participants"));

		participantComposite = new Composite(tabs, SWT.NONE);
		participantComposite.setLayout(new MigLayout("",
				"[fill,grow][]", "[60,fill,grow][][][][][fill,grow]"));

		participants = new ArrayList<Participant>();
//		Participant x =new ParticipantImpl("Dennis","Schall","dschal@tarent.de");
//		Participant y = new ParticipantImpl("Fabian","Köster","fkoester@tarent.de");
//		Participant z = new ParticipantImpl("Peter","Neuhaus","pneuha@tarent.de");
//		
//		participants.add(x);
//		participants.add(y);
//		participants.add(z);
//		
		
//		availableContacts = new List(participantComposite, SWT.BORDER
//				| SWT.V_SCROLL|SWT.MULTI);
//		availableContacts.setLayoutData("top, wmin 380,height 250,spany 6,hmax 100%,shrink");


		//fillAvailableContactsList(availableParticipants);
		participantsList = new List(participantComposite, SWT.BORDER | SWT.V_SCROLL|SWT.MULTI);
		participantsList
				.setLayoutData("top, wmin 380,height 250,spany 6, wrap,hmax 100%");

		addNuyoButton = new Button(participantComposite, SWT.NONE);
		addNuyoButton.setText("Nuyo Hinzufügen");
		//addButton.setText(Messages
		//		.getString("EventDialog_Tab_Participants_Add_Button"));
		addNuyoButton.setLayoutData("cell 1 1,bottom,sg buttons,wrap");
		addNuyoButton.addListener(SWT.Selection, buttonListener);

		removeButton = new Button(participantComposite, SWT.NONE);
		removeButton.setText("Entfernen");
		//removeButton.setText(Messages
		//		.getString("EventDialog_Tab_Participants_Remove_Button"));
		removeButton.setLayoutData("sg buttons,top,wrap");
		removeButton.addListener(SWT.Selection, buttonListener);
		
		adHocButton= new Button(participantComposite,SWT.NONE);
		adHocButton.setText("Adhoc hinzufügen");
		adHocButton.setLayoutData("sg buttons,top,wrap");
		adHocButton.addListener(SWT.Selection, buttonListener);
		
		selectall=new Button(participantComposite,SWT.None);
		selectall.setText("Alle auswählen");
		selectall.setLayoutData("sg buttons,top");
		selectall.addListener(SWT.Selection, buttonListener);
		
		participantTab.setControl(participantComposite);

		submitButton = new Button(shell, SWT.NONE);
		submitButton
				.setText(Messages.getString("EventDialog_Submit_New_Event"));
		submitButton.setToolTipText(Messages
				.getString("EventDialog_Submit_New_Event_Tooltip"));
		submitButton.setLayoutData("left,wmax 200");
		submitButton.addListener(SWT.Selection, buttonListener);

		cancelButton = new Button(shell, SWT.NONE);
		cancelButton
				.setText(Messages.getString("EventDialog_Cancel_New_Event"));
		cancelButton.setToolTipText(Messages
				.getString("EventDialog_Cancel_New_Event_Tooltip"));
		cancelButton.setLayoutData("right,wmax 200");
		cancelButton.addListener(SWT.Selection, buttonListener);

		shell.pack();
		Rectangle displayBounds = shell.getDisplay().getBounds();
	       
        int nWidth = displayBounds.width  / 2;
        int nHeight = displayBounds.height / 2;

        int nLeft = nWidth - (shell.getBounds().width / 2);
        int nTop = nHeight- (shell.getBounds().height / 2);
        
        shell.setDefaultButton(submitButton);
        
        // Set shell bounds,
        shell.setLocation(nLeft, nTop);
	}

	protected void submitAction() {
		
			}

	public Shell getShell() {
		return shell;
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}

	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#open()
	 */
	public void open() {
		shell.open();
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setBeginDay(java.util.Calendar)
	 */
	public void setBeginDay(Calendar beginDay) {
		setDateTimeWidgetToDate(beginDate, beginDay);
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setBeginTime(java.util.Calendar)
	 */
	public void setBeginTime(Calendar beginTime) {
		setDateTimeWidgetToDate(this.beginTime, beginTime);
		
	}
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setEndDay(java.util.Calendar)
	 */
	public void setEndDay(Calendar endDay) {
		setDateTimeWidgetToDate(endDate, endDay);
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setEndTime(java.util.Calendar)
	 */
	public void setEndTime(Calendar endTime) {
		setDateTimeWidgetToDate(this.endTime, endTime);
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setMode(int)
	 */
	public void setMode(int mode) {
		if(mode == MODE_NEW) {
			getShell().setText(TITLE_NEW_EVENT);
			clear();
			submitButton.setText(SUBMIT_NEW_EVENT);
			submitButton.setToolTipText(SUBMIT_NEW_EVENT_TOOLTIP);
			cancelButton.setText(CANCEL_NEW_EVENT);
			cancelButton.setToolTipText(CANCEL_NEW_EVENT_TOOLTIP);
			Calendar begin = Calendar.getInstance(Locale.getDefault());
			setDateTimeWidgetsToDate(beginDate, beginTime, addOneHourAndRound(begin));
			setDateTimeWidgetsToDate(endDate, endTime, addOneHourAndRound((Calendar)begin.clone()));
			categoryButton.setEnabled(true);
			categoryButton.setToolTipText(Messages.getString("EventDialog_SelectCategories_Button_Tooltip"));
			allDayCheck.setSelection(false);
		} else if (mode == MODE_EDIT) {
			getShell().setText(TITLE_EDIT_EVENT);
			submitButton.setText(SUBMIT_EDIT_EVENT);
			submitButton.setToolTipText(SUBMIT_EDIT_EVENT_TOOLTIP);
			cancelButton.setText(CANCEL_EDIT_EVENT);
			cancelButton.setToolTipText(CANCEL_EDIT_EVENT_TOOLTIP);
			categoryButton.setEnabled(false);
			categoryButton.setToolTipText(Messages.getString("EventDialog_SelectCategories_Button_Tooltip_Disabled"));
		} else
			logger.warning("unknown dialog-mode");

		tabs.setSelection(0);
	}
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setSubmitAction(javax.swing.Action)
	 */
	public void setSubmitAction(Action submitAction) {
		this.submitAction = submitAction;
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.dialogs.EventDialog#getSelectedCategories()
	 */
	public Collection<Category> getSelectedCategories() {
		if(selectedCategories == null)
			selectedCategories = new ArrayList<Category>();
		
		return selectedCategories;
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#getEvent()
	 */
	public de.tarent.kaliko.objects.Event getEvent() {
		if(event == null || !(event instanceof EventImpl))
			event = new EventImpl();

		((EventImpl)event).setTitle(titleText.getText());
		((EventImpl)event).setLocation(locationText.getText());
		((EventImpl)event).setNote(noticeText.getText());
		((EventImpl)event).setBegin(getDateFromDateTimeWidgets(beginDate, beginTime));
		((EventImpl)event).setEnd(getDateFromDateTimeWidgets(endDate, endTime));

		((EventImpl)event).setAllDayEvent(allDayCheck.getSelection());

		return event;
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.dialogs.SWTEventDialog#setEvent(de.tarent.kaliko.objects.Event)
	 */
	public void setEvent(de.tarent.kaliko.objects.Event event) {
		this.event = event;

		if(event != null) {
			titleText.setText(event.getTitle());
			locationText.setText(event.getLocation());
			noticeText.setText(event.getNote());
			setDateTimeWidgetsToDate(beginDate, beginTime, event.getBegin());
			setDateTimeWidgetsToDate(endDate, endTime, event.getEnd());
			allDayCheck.setSelection(event.isAllDayEvent());
			beginTime.setVisible(!event.isAllDayEvent());
			endTime.setVisible(!event.isAllDayEvent());
		}
	}
	
	protected void refreshCategoryLabel() {
		java.util.List<Category> selectedCategories = CategoryCollectionHelper.toList(getSelectedCategories());
		
		StringBuffer text = new StringBuffer();
		for(int i=0; i < selectedCategories.size(); i++) {
			text.append(selectedCategories.get(i).getName());
			if(i+1 < selectedCategories.size())
				text.append(", ");
		}
		
		if(selectedCategories.size() == 0) {
			categoryLabel.setText(Messages.getString("EventDialog_No_Categories"));
			categoryLabel.setToolTipText("");
			
		} else {
			categoryLabel.setText(text.toString());
			categoryLabel.setToolTipText(text.toString());
		}
	}

	public void setAvailableCategories(Collection<Category> availableCategories) {
		this.availableCategories = availableCategories;
	}

	protected void setDateTimeWidgetsToDate(DateTime dateWidget, DateTime timeWidget, Calendar date) {
		dateWidget.setYear(date.get(Calendar.YEAR));
		dateWidget.setMonth(date.get(Calendar.MONTH));
		dateWidget.setDay(date.get(Calendar.DAY_OF_MONTH));
		
		timeWidget.setHours(date.get(Calendar.HOUR_OF_DAY));
		timeWidget.setMinutes(date.get(Calendar.MINUTE));
		
//		 Kaliko currently does not handle seconds
//		timeWidget.setSeconds(date.get(Calendar.SECOND));
	}
	
	protected void setDateTimeWidgetToDate(DateTime dateTime, Calendar date) {
		setDateTimeWidgetsToDate(dateTime, dateTime, date);
	}
	
	protected Calendar getDateFromDateTimeWidget(DateTime dateWidget) {
		return getDateFromDateTimeWidgets(dateWidget, dateWidget);
	}
	
	protected Calendar getDateFromDateTimeWidgets(DateTime dateWidget, DateTime timeWidget) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.set(Calendar.YEAR, dateWidget.getYear());
		calendar.set(Calendar.MONTH, dateWidget.getMonth());
		calendar.set(Calendar.DAY_OF_MONTH, dateWidget.getDay());
		calendar.set(Calendar.HOUR_OF_DAY, timeWidget.getHours());
		calendar.set(Calendar.MINUTE, timeWidget.getMinutes());
		
		// Kaliko currently does not handle seconds
//		calendar.set(Calendar.SECOND, dateTime.getSeconds());
		
		return calendar;
	}
	
	public void clear() {
		event = null;

		titleText.setText("");
		locationText.setText("");
		noticeText.setText("");
		setDateTimeWidgetToDate(beginDate, Calendar.getInstance(Locale.getDefault()));
		setDateTimeWidgetToDate(beginTime, Calendar.getInstance(Locale.getDefault()));
		setDateTimeWidgetToDate(endDate, Calendar.getInstance(Locale.getDefault()));
		setDateTimeWidgetToDate(endTime, Calendar.getInstance(Locale.getDefault()));
		beginTime.setVisible(true);
		endTime.setVisible(true);
	}
	
	protected Calendar addOneHourAndRound(Calendar calendar) {
		calendar.add(Calendar.HOUR, 1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	protected Calendar substractOneHourAndRound(Calendar calendar) {
		calendar.add(Calendar.HOUR, -1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	public void setSelectedCategories(Collection<Category> selectedCategories) {
		this.selectedCategories = CategoryCollectionHelper.toList(selectedCategories);
		refreshCategoryLabel();
	}
	public void addParticipant(String firstName,String lastName,String email) {
		Participant participant =new ParticipantImpl(firstName,lastName,email);
		participants.add(participant);
		refreshContactsList();
	
	}
	
	public void refreshContactsList() {
		participantsList.removeAll();
		Iterator<Participant> contactsIterator = getAvailableParticipants().iterator();
		while (contactsIterator.hasNext()) {
			Participant participant = contactsIterator.next();
			this.participantsList.add(participant.getFirstName()+ " " +participant.getLastName());
			
		}
	}

	public Collection<Participant> getAvailableParticipants() {
		return participants;
	}

	public void setParticipants(
			Collection<Participant> availableParticipants) {
		this.participants = availableParticipants;
		refreshContactsList();
	}

	
	public List getParticipants() {
		return participantsList;
	}

	public void setParticipantsList(List participants) {
		this.participantsList = participants;
	}
}
