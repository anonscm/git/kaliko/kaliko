package org.evolvis.kaliko.widgets.swt;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.evolvis.kaliko.widgets.common.AbstractCategoryTable;
import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.swt.utils.FontHelper;

import de.tarent.kaliko.objects.Category;

/**
 * A Table for displaying and choosing categories
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 *
 */

public class SWTCategoryTable extends AbstractCategoryTable {

	protected Table table;
	protected List<Category> availableCategories;
	protected List<Category> selectedCategories;
	protected List<Category> defaultCategories;
	protected Action categorySelectionAction;

	public SWTCategoryTable(Composite parent, int style){
		table = new Table(parent, SWT.CHECK | SWT.VIRTUAL | style);
		
		table.setLinesVisible (true);
		table.setHeaderVisible (true);

		TableColumn categoryColumn = new TableColumn(table, SWT.NONE);
		categoryColumn.setText("Kategorien");
		categoryColumn.setResizable(true);
		categoryColumn.setWidth(100);
		
		table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int index = table.getSelectionIndex();
				if(index < 0 || event.detail != SWT.CHECK)
					return;
				/*
				if (table.getItem(index).getChecked()) 
					selectedCategories.add(availableCategories.get(index));
				else
					selectedCategories.remove(availableCategories.get(index));
				*/
				if(categorySelectionAction != null)
					categorySelectionAction.actionPerformed(new ActionEvent(this, 0, "categorySelectionChanged"));
			}
		});
	}

	public void setLayoutData(Object arg) {
		table.setLayoutData(arg); 
	}
	
	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}
	
	public void setAvailableCategories(Collection<Category> availableCategories) {
		this.availableCategories = CategoryCollectionHelper.toList(availableCategories);
		
		refresh();
	}

	public void setDefaultCategories(Collection<Category> defaultCategories) {
		this.defaultCategories = CategoryCollectionHelper.toList(defaultCategories);
		
		refresh();
	}

	public void setSelectedCategories(Collection<Category> selectedCategories) {
		this.selectedCategories = CategoryCollectionHelper.toList(selectedCategories);
		
		refresh();
		if(categorySelectionAction != null)
			categorySelectionAction.actionPerformed(new ActionEvent(this, 0, "categorySelectionChanged"));
	}
	
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#getDefaultCategories()
	 */
	public Collection<Category> getDefaultCategories() {
		if(defaultCategories == null)
			defaultCategories = new ArrayList<Category>();
		
		return defaultCategories;
	}

	public Collection<Category> getSelectedCategories() {
		if(selectedCategories == null)
			selectedCategories = new ArrayList<Category>();
		
		return selectedCategories;
	}

	public Collection<Category> getAvailableCategories() {
		if(availableCategories == null)
			availableCategories = new ArrayList<Category>();
		
		return availableCategories;
	}

	public void addCategory(Category category) {
		getAvailableCategories().add(category);
		refresh();
	}

	public void removeCategory(Category category) {
		getAvailableCategories().remove(category);
		refresh();
	}
	
	public synchronized void refresh() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {

				table.removeAll();

				Iterator<Category> availableCategoriesIterator = getAvailableCategories().iterator();

				while(availableCategoriesIterator.hasNext()) {
					Category category = availableCategoriesIterator.next();

					TableItem item = new TableItem (table, SWT.NONE);
					item.setText(0 , category.getName());
					
					// If category is a default-category, show it's name with bold text
					if(getDefaultCategories().contains(category))
						item.setFont(FontHelper.makeFontBold(item.getFont()));


					// If category is selected, activate it's check-box
					if(getSelectedCategories().contains(category))
						item.setChecked(true);
				}
			}
		});
	}


	/**
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#setCategorySelectionAction(javax.swing.Action)
	 */
	public void setCategorySelectionAction(Action action) {
		categorySelectionAction = action;
	}
	
	public void addCurrentCategoryToDefaults() {
		int index = table.getSelectionIndex();
		if(index < 0 || index >= table.getItemCount())
			return;
			
		if(availableCategories.get(index).getAccess().isWritable()&&!defaultCategories.contains(availableCategories.get(index))) {
			defaultCategories.add(availableCategories.get(index));	
			selectedCategories.add(availableCategories.get(index));
			table.getItem(index).setFont(FontHelper.makeFontBold(table.getItem(index).getFont()));
		}
	}
	
	public void removeCurrentCategoryFromDefaults() {
		int index = table.getSelectionIndex();
		if(index < 0)
			return;
		
		defaultCategories.remove(availableCategories.get(index));	
		
		table.getItem(index).setFont(FontHelper.makeFontNotBold(table.getItem(index).getFont()));
	}

//	public void sortCategories() {
//		Table table = getTable();
//		TableItem[] items = table.getItems();
//		Collator collator = Collator.getInstance(Locale.getDefault());
//		int index=0;
//		for (int i = 1; i < items.length; i++) {
//			String value1 = items[i].getText(index);
//			for (int j = 0; j < i; j++){
//				String value2 = items[j].getText(index);
//				if (collator.compare(value1, value2) < 0) {
//					String[] values = {items[i].getText(0), items[i].getText(1)};
//					items[i].dispose();
//					TableItem item = new TableItem(table, SWT.NONE, j);
//					item.setText(values);
//					items = table.getItems();
//					break;
//				}
//			}
//		}
//	}
}
