package org.evolvis.kaliko.widgets.swt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.evolvis.kaliko.widgets.common.AbstractCategorySelector;
import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.common.CategorySelector;

import de.tarent.kaliko.objects.Category;

/**
 * Tabfolder to hold two different categoryTables(all categories,favorite
 * categories)
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 * 
 */

public class TabbedCategoryTables extends AbstractCategorySelector implements
		CategorySelector {

	protected TabFolder tabs;
	protected TabItem all;
	protected TabItem favorites;
	protected Composite allComposite;
	protected Composite favoritesComposite;
	protected SWTCategoryTable categoryTable;
	protected SWTCategoryTable favoritesTable;

	public TabbedCategoryTables(Composite parent) {

		tabs = new TabFolder(parent, SWT.NONE);
		tabs.setLayout(new GridLayout(1, true));

		favorites = new TabItem(tabs, SWT.None);
		all = new TabItem(tabs, SWT.None);

		all.setText("Alle Kategorien");
		allComposite = new Composite(tabs, SWT.NONE);
		allComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		allComposite.setLayout(new GridLayout(1, true));

		categoryTable = new SWTCategoryTable(allComposite, SWT.SINGLE
				| SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL);
		categoryTable.setLayoutData(new GridData(GridData.FILL_BOTH));

		categoryTable.getTable().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int index = categoryTable.getTable().getSelectionIndex();

				if (index < 0 || event.detail != SWT.CHECK)
					return;
				
				if (categoryTable.getTable().getItem(index).getChecked()) // Selection
					getSelectedCategories().add(CategoryCollectionHelper.toList(getAvailableCategories()).get(index));
				else // Deselection
					getSelectedCategories().remove(CategoryCollectionHelper.toList(getAvailableCategories()).get(index));
				
//				 if(categorySelectionAction != null)
//					 categorySelectionAction.actionPerformed(new ActionEvent(this, 0, "categorySelectionChanged"));
				
				favoritesTable.refresh();
			}
		});

		Menu addMenu = new Menu(parent.getShell(), SWT.POP_UP);

		MenuItem addToFavoritesItem = new MenuItem(addMenu, SWT.PUSH);
		addToFavoritesItem.setText("Zu Favoriten hinzufügen");

		/**
		 * add Item to favorites or remove Item from favorites
		 */

		addToFavoritesItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				int index = categoryTable.getTable().getSelectionIndex();
				
				if(index < 0)
					return;
				
				Category categoryToAdd =  CategoryCollectionHelper
				.toList(categoryTable.getAvailableCategories()).get(index);
				
				
				// Check if the category to add is already in favorites list
				Iterator<Category> favoriteCategoriesIt = favoritesTable.getAvailableCategories().iterator();
				
				while(favoriteCategoriesIt.hasNext())
					if(categoryToAdd.equals(favoriteCategoriesIt.next()))
						return;
				
				// If it is not, add it to favorites
				favoritesTable.addCategory(categoryToAdd);

				favoritesTable.setSelectedCategories(categoryTable.getSelectedCategories());
			}
		});

		MenuItem addToDefaultsItem = new MenuItem(addMenu, SWT.PUSH);
		addToDefaultsItem.setText("Zu Standard-Kategorien hinzufügen");
		addToDefaultsItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				categoryTable.addCurrentCategoryToDefaults();
				
				favoritesTable.refresh();
			}
		});

		MenuItem removeFromDefaultsItem = new MenuItem(addMenu, SWT.PUSH);
		removeFromDefaultsItem.setText("Von Standard-Kategorien entfernen");
		removeFromDefaultsItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				categoryTable.removeCurrentCategoryFromDefaults();
				
				favoritesTable.refresh();
			}
		});

		categoryTable.getTable().setMenu(addMenu);

		all.setControl(allComposite);

		favorites.setText("Favoriten");
		favoritesComposite = new Composite(tabs, SWT.None);
		favoritesComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		favoritesComposite.setLayout(new GridLayout(1, true));

		favoritesTable = new SWTCategoryTable(favoritesComposite, SWT.SINGLE
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.CHECK | SWT.BORDER);
		favoritesTable.setLayoutData(new GridData(GridData.FILL_BOTH));

		favoritesTable.getTable().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int index = favoritesTable.getTable().getSelectionIndex();
				List<Category> selectedFavoriteCategories = CategoryCollectionHelper
						.toList(favoritesTable.getSelectedCategories());
				List<Category> availableFavoriteCategories = CategoryCollectionHelper
						.toList(favoritesTable.getAvailableCategories());
				List<Category> availableCategories = CategoryCollectionHelper
						.toList(categoryTable.getAvailableCategories());
				List<Category> selectedCategories = CategoryCollectionHelper
						.toList(categoryTable.getSelectedCategories());

				if (index < 0 || event.detail != SWT.CHECK)
					return;

				if (favoritesTable.getTable().getItem(index).getChecked()) {
					selectedFavoriteCategories.add(availableFavoriteCategories
							.get(index));
					selectedCategories.add(availableFavoriteCategories.get(index));

				} else {
					selectedCategories.remove(availableFavoriteCategories.get(index));
					selectedFavoriteCategories.remove(availableFavoriteCategories
							.get(index));
				}
				// if(categorySelectionAction != null)
				// categorySelectionAction.actionPerformed(new ActionEvent(this,
				// 0, "categorySelectionChanged"));

				categoryTable.setSelectedCategories(selectedCategories);
				favoritesTable
						.setSelectedCategories(selectedFavoriteCategories);
			}
		});

		Menu removeMenu = new Menu(parent.getShell(), SWT.POP_UP);

		MenuItem removeItem = new MenuItem(removeMenu, SWT.PUSH);
		removeItem.setText("Aus Favoriten entfernen");
		removeItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int index = favoritesTable.getTable().getSelectionIndex();
				Category item = CategoryCollectionHelper.toList(
						favoritesTable.getAvailableCategories()).get(index);
				favoritesTable.getAvailableCategories().remove(item);
				favoritesTable.getSelectedCategories().remove(item);
				favoritesTable.refresh();
				if (index == -1)
					return;
			}
		});

		favoritesTable.getTable().setMenu(removeMenu);
		favorites.setControl(favoritesComposite);

		/**
		 * Sorts the Items if header gets clicked
		 */
		Listener sortListener = new Listener() {
			public void handleEvent(Event e) {
				List<Category> list;
				SWTCategoryTable table;
				if (tabs.getSelectionIndex() == 0) {
					list = CategoryCollectionHelper.toList(favoritesTable
							.getAvailableCategories());
					table = favoritesTable;

				} else {
					list = CategoryCollectionHelper.toList(categoryTable
							.getAvailableCategories());
					table = categoryTable;

				}
				if (table.getTable().getSortDirection() == SWT.UP) {
					table.getTable().setSortDirection(SWT.DOWN);
				} else if (table.getTable().getSortDirection() == SWT.DOWN) {
					table.getTable().setSortDirection(SWT.UP);
				}

				if (table.getTable().getSortDirection() == SWT.UP
						|| table.getTable().getSortDirection() == SWT.NONE) {
					Collections.sort(list);
					table.refresh();
					table.getTable().setSortDirection(SWT.UP);
				} else if (table.getTable().getSortDirection() == SWT.DOWN) {
					List<Category> tempList = new ArrayList<Category>();
					for (int i = (list.size() - 1); i >= 0; i--) {
						tempList.add(list.get(i));
					}
					table.setAvailableCategories(tempList);
					table.refresh();
					table.getTable().setSortDirection(SWT.DOWN);

				}
			}
		};
		favoritesTable.getTable().getColumn(0).addListener(SWT.Selection,
				sortListener);
		categoryTable.getTable().getColumn(0).addListener(SWT.Selection,
				sortListener);

		// The selection of the categories in the favorites table should be the same as in the table of all categories
		// so we sync them (by pointing the lists of the favorites-table to the lists of the all table).
		favoritesTable.setSelectedCategories(categoryTable.getSelectedCategories());
		favoritesTable.setDefaultCategories(categoryTable.getDefaultCategories());
	}

	public void setLayoutData(Object layoutData) {
		tabs.setLayoutData(layoutData);
	}

	public SWTCategoryTable getCategoryTable() {
		return categoryTable;
	}

	public void setCategoryTable(SWTCategoryTable categoryTable) {
		this.categoryTable = categoryTable;
	}

	public Composite getAllComposite() {
		return allComposite;
	}

	public void setAllComposite(Composite allComposite) {
		this.allComposite = allComposite;
	}

	public void refresh() {
		categoryTable.refresh();
		favoritesTable.refresh();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#addToFavorites(de.tarent.kaliko.objects.Category)
	 */
	public void addToFavorites(Category arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getSelectedCategories()
	 */
	public Collection<Category> getSelectedCategories() {
		return categoryTable.getSelectedCategories();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getAvailableCategories()
	 */
	public Collection<Category> getAvailableCategories() {
		return categoryTable.getAvailableCategories();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getFavoriteCategories()
	 */
	public Collection<Category> getFavoriteCategories() {
		return favoritesTable.getAvailableCategories();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getPreselectedCategories()
	 */
	public Collection<Category> getDefaultCategories() {
		return categoryTable.getDefaultCategories();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#removeFromFavorites(de.tarent.kaliko.objects.Category)
	 */
	public void removeFromFavorites(Category arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setSelectedCategories(java.util.Collection)
	 */
	public void setSelectedCategories(Collection<Category> selectedCategories) {
		categoryTable.setSelectedCategories(selectedCategories);
		
		favoritesTable.setSelectedCategories(categoryTable.getSelectedCategories());
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setAvailableCategories(java.util.Collection)
	 */
	public void setAvailableCategories(Collection<Category> availableCategories) {
		categoryTable.setAvailableCategories(availableCategories);

		refresh();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setFavoriteCategories(java.util.Collection)
	 */
	public void setFavoriteCategories(Collection<Category> favoriteCategories) {
		favoritesTable.setAvailableCategories(favoriteCategories);
		checkActiveTab();
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setDefaultCategories(java.util.Collection)
	 */
	public void setDefaultCategories(Collection<Category> defaultCategories) {
		categoryTable.setDefaultCategories(defaultCategories);
		
		favoritesTable.setDefaultCategories(categoryTable.getDefaultCategories());
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setCategorySelectionAction(javax.swing.Action)
	 */
	public void setCategorySelectionAction(Action action) {
		categoryTable.setCategorySelectionAction(action);
	}

	protected void checkActiveTab() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				
				if (favoritesTable.getAvailableCategories().size() == 0)
					tabs.setSelection(all);
				else
					tabs.setSelection(favorites);
			}
		});
	}

}
