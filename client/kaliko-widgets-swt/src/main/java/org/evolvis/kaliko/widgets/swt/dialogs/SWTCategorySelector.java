package org.evolvis.kaliko.widgets.swt.dialogs;

import java.awt.event.ActionEvent;
import java.util.Collection;

import javax.swing.Action;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.evolvis.kaliko.widgets.swt.TabbedCategoryTables;
import org.evolvis.kaliko.widgets.swt.utils.FontHelper;

import de.tarent.kaliko.objects.Category;

/**
 * 
 * Dialog to choose categories
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 * 
 */

public class SWTCategorySelector {

	protected TabbedCategoryTables tabs;
	protected TabItem all;
	protected TabItem favorites;
	protected Composite allComposite;
	protected Composite favoritesComposite;

	protected Label chooseCategory;
	protected Label availableLabel;
	protected Button categorySubmitButton;
	protected Button categoryCancelButton;

	protected Shell categorySelection;
	protected SWTEventDialog eventdialog;

	protected Action submitAction;
	
	public SWTCategorySelector(SWTEventDialog parent,int style){	
		eventdialog=parent;

		categorySelection = new Shell(parent.getShell(), style);
		categorySelection.setLayout(new MigLayout("",
				"[][fill,grow][]", "[][][][grow, fill][]"));
		categorySelection.setText("Kategorie auswählen");


		chooseCategory = new Label(categorySelection, SWT.BOLD);
		chooseCategory.setText("Bitte wählen Sie die Kategorien für das Ereignis");
		chooseCategory.setFont(FontHelper.makeFontBold(chooseCategory.getFont()));
		chooseCategory.setLayoutData("span,wrap");
//		Label separator = new Label(categorySelection,
//		SWT.SEPARATOR | SWT.VERTICAL);
//		separator.setLayoutData("wrap,hmax 40,span5");
		availableLabel = new Label(categorySelection, SWT.NONE);
		availableLabel
		.setText("Alle Kategorien auf die Sie Schreibzugriff haben:");
		availableLabel.setLayoutData("span,wrap");
		
		tabs = new TabbedCategoryTables(categorySelection);
		tabs.setLayoutData("wrap,span,grow,growy,hmin 200,shrink");
//		all = new TabItem(tabs,SWT.None);
//		all.setText("alle Kategorien");
//		allComposite= new Composite(tabs,SWT.NONE);
//		allComposite.setLayout(new MigLayout("","[grow]"));
//		categoryTable = new SWTCategoryTable(availableCategories,
//		activatedCategories, allComposite,
//		SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL
//		| SWT.FULL_SELECTION | SWT.MAX | SWT.CHECK|SWT.BORDER);
//		categoryTable
//		.setLayoutData("wrap,span,grow,growy,hmax 200,hmin 200");
//		all.setControl(allComposite);

//		favorites= new TabItem(tabs,SWT.None);
//		favorites.setText("Favoriten");
//		favoritesComposite = new Composite(tabs,SWT.None);

//		favorites.setControl(favoritesComposite);
		categorySubmitButton = new Button(categorySelection,
				SWT.None);
		categorySubmitButton.setText("Übernehmen");

		Listener listener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == categorySubmitButton) {
					submitAction.actionPerformed(new ActionEvent(this, 0, "categorySubmit"));
					categorySelection.dispose();
					
				} else if (event.widget == categoryCancelButton)
					categorySelection.dispose();
			}
		};

		categorySubmitButton.addListener(SWT.Selection, listener);
		categorySubmitButton.setLayoutData("");
		categoryCancelButton = new Button(categorySelection,
				SWT.None);
		categoryCancelButton.addListener(SWT.Selection, listener);
		categoryCancelButton.setText("Verwerfen");
		categoryCancelButton.setLayoutData("skip");
		categorySelection.pack();
		categorySelection.open();
	}
	
	public void setSubmitAction(Action submitAction) {
		this.submitAction = submitAction;
	}

	public Collection<Category> getAvailableCategories() {
		return tabs.getAvailableCategories();
	}

	public void setAvailableCategories(Collection<Category> availableCategories) {
		tabs.setAvailableCategories(availableCategories);
	}

	public Collection<Category> getSelectedCategories() {
		return tabs.getSelectedCategories();
	}

	public void setSelectedCategories(Collection<Category> selectedCategories) {
		tabs.setSelectedCategories(selectedCategories);
	}

	public TabbedCategoryTables getTabs() {
		return tabs;
	}

	public void setTabs(TabbedCategoryTables tabs) {
		this.tabs = tabs;
	}
}