package org.evolvis.kaliko.widgets.swt.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener; 
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.kaliko.widgets.common.ParticipantCollectionHelper;

import de.tarent.kaliko.objects.Participant;
import de.tarent.kaliko.objects.impl.ParticipantImpl;

/**
 * 
 * Dialog for selecting contacts as participants
 * 
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 * 
 */ 

public class SWTParticipantSelector {
	protected Shell shell;
	protected SWTEventDialog parent;
	protected Composite searchComposite;
	protected Text searchText;
	protected Button searchButton;
	protected List contactList;
	protected Button addButton;
	protected Button removeButton;
	protected List participantsList;
	
	protected Button submitButton;
	protected Button cancelButton;
	protected Listener buttonListener;
	
	protected Collection<Participant> availableParticipants;
	protected Collection<Participant> selectedParticipants;
	
	
public SWTParticipantSelector(SWTEventDialog parent){
	this.parent=parent;
	selectedParticipants = new ArrayList<Participant>();
	buttonListener = new Listener(){

		public void handleEvent(Event event) {
			if (event.widget.equals(searchButton)) {
				availableParticipants=new ArrayList<Participant>();
				Participant x =new ParticipantImpl("Dennis","Schall","dschal@tarent.de");
				Participant y = new ParticipantImpl("Fabian","Köster","fkoester@tarent.de");
				Participant z = new ParticipantImpl("Peter","Neuhaus","pneuha@tarent.de");
				
				availableParticipants.add(x);
				availableParticipants.add(y);
				availableParticipants.add(z);
				fillContactsList(availableParticipants);
				
			} else if (event.widget.equals(addButton)) {
				boolean duplicate = true;
				for (int i = 0; i < participantsList.getItemCount(); i++) {
					String selectedItem = contactList
							.getItem(contactList.getSelectionIndex());
					if (selectedItem.equals(participantsList.getItem(i))) {
						duplicate = false;
					}
				}

				if (contactList.getSelectionCount() != 0 && duplicate) {
					
					
					for (int i = 0; i < contactList.getSelectionIndices().length; i++) {
						participantsList.add(contactList
								.getItem(contactList.getSelectionIndices()[i]));
						selectedParticipants.add(ParticipantCollectionHelper.toList(availableParticipants).get(contactList.getSelectionIndices()[i]));
					}
				}
				
			} else if (event.widget.equals(removeButton)) {
				if (participantsList.getSelectionCount() != 0) {
					for (int  i = participantsList.getSelectionIndices().length-1;i>=0; i--) {
						selectedParticipants.remove(ParticipantCollectionHelper.toList(selectedParticipants).get(participantsList.getSelectionIndices()[i]));
						participantsList.remove(participantsList.getSelectionIndices()[i]);
						}

				}
				
			} else if (event.widget.equals(submitButton)) {
				SWTParticipantSelector.this.parent.setParticipants(selectedParticipants);
				shell.dispose();
			} else if (event.widget.equals(cancelButton)) {
				
				shell.dispose();
				
			}
			
		}
		
	};
	
	
	
	shell = new Shell(parent.getShell(),SWT.APPLICATION_MODAL|SWT.TITLE|SWT.CLOSE|SWT.RESIZE);
	shell.setText("Teilnehmer auswählen");
	shell.setLayout(new MigLayout(""
			,"[grow,fill][][grow,fill]"
			,"[][60,fill,grow][][][fill,grow][]"));
	searchComposite= new Composite(shell,SWT.NONE);
	searchComposite.setLayoutData("span 2,wrap");
	searchComposite.setLayout(new MigLayout("","[fill,grow][]",""));
	searchText = new Text(searchComposite,SWT.BORDER);
	searchText.setLayoutData("right");
	
	searchButton = new Button(searchComposite,SWT.NONE);
	searchButton.setText("Suchen");
	searchButton.setLayoutData("");
	searchButton.addListener(SWT.Selection, buttonListener);
	
	contactList = new List(shell,SWT.BORDER
			| SWT.V_SCROLL|SWT.MULTI);
	contactList.setLayoutData("top, wmin 380,height 250,spany 4,hmax 100%,shrink");

	participantsList = new List(shell,SWT.BORDER
			| SWT.V_SCROLL|SWT.MULTI);
	participantsList.setLayoutData("skip,top, wmin 380,height 250,spany 4,hmax 100%,shrink,wrap");

	
	addButton =new Button(shell,SWT.NONE);
	addButton.setText("Hinzufügen");
	addButton.setLayoutData("sg buttons,cell 1 2,wrap");
	addButton.addListener(SWT.Selection, buttonListener);
	
	removeButton = new Button(shell,SWT.NONE);
	removeButton.setText("Entfernen");
	removeButton.setLayoutData("sg buttons");
	removeButton.addListener(SWT.Selection, buttonListener);
	
	submitButton = new Button(shell,SWT.NONE);
	submitButton.setText("Übernehmen");
	submitButton.setLayoutData("cell 0 6,wmax 200");
	submitButton.addListener(SWT.Selection, buttonListener);
	
	cancelButton = new Button(shell,SWT.NONE);
	cancelButton.setText("Verwerfen");
	cancelButton.setLayoutData("skip,right,wmax 200");
	cancelButton.addListener(SWT.Selection, buttonListener);
	
	shell.pack();
	}

public void open() {
	shell.open();
	
}
public void fillContactsList(Collection<Participant> participants) {
	Iterator<Participant> contactsIterator = getAvailableParticipants().iterator();
	while (contactsIterator.hasNext()) {
		Participant participant = contactsIterator.next();
		contactList.add(participant.getFirstName()+ " " +participant.getLastName());
		
	}
}

public List getContactList() {
	return contactList;
}

public void setContactList(List contactList) {
	this.contactList = contactList;
}

public List getParticipantsList() {
	return participantsList;
}

public void setParticipantsList(List participantsList) {
	this.participantsList = participantsList;
}

public Collection<Participant> getAvailableParticipants() {
	return availableParticipants;
}

public void setAvailableParticipants(
		Collection<Participant> availableParticipants) {
	this.availableParticipants = availableParticipants;
}
public Collection<Participant> getSelectedParticipants() {
	return selectedParticipants;
}

public void setSelectedParticipants(
		Collection<Participant> selectedParticipants) {
	this.selectedParticipants = selectedParticipants;
	refreshParticipantsList();
}

public void refreshParticipantsList(
		) {
	Iterator<Participant> contactsIterator = getSelectedParticipants().iterator();
	while (contactsIterator.hasNext()) {
		Participant participant = contactsIterator.next();
		participantsList.add(participant.getFirstName()+ " " +participant.getLastName());
		
	}
	
}
}
