/**
 * 
 */
package org.evolvis.kaliko.widgets.swt.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class FontHelper {

	public static Font makeFontBold(Font font) {
		
		return new Font(font.getDevice(), font.getFontData()[0].name, (int)font.getFontData()[0].height, SWT.BOLD);
	}
	
	public static Font makeFontNotBold(Font font) {
		
		return new Font(font.getDevice(), font.getFontData()[0].name, (int)font.getFontData()[0].height, SWT.NONE);
	}
}
