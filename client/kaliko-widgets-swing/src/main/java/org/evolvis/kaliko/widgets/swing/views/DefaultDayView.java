/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.OverlayLayout;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.SimpleEventList;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.models.DayViewModel;
import de.tarent.kaliko.components.models.impl.DefaultDayViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.utils.EventUtils;
import de.tarent.kaliko.components.views.DayViewPanel;
import de.tarent.kaliko.components.views.WeekView;
import de.tarent.kaliko.objects.Event;

/**
 * A calendar-view implementation which shows one day
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultDayView extends DayViewPanel implements CalendarSelectionListener, EventSelectionListener, CalendarDataListener, CategoryListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2266671890635406000L;
	protected DefaultDayViewEventPane eventPane;
	protected JPanel dailyEventPanel;

	protected SimpleEventList dailyEventList;
	protected JScrollPane dailyEventScrollPane;
	protected JScrollPane eventScrollPane;

	protected int offset;
	protected float oneHour;

	protected List<DayViewEvent> dayViewEventList;

	protected DayViewModel model;

	protected boolean isThisViewActive;

	protected boolean drawTimes;

	protected WeekView weekView;

	protected int type = DAY_TYPE_DEFAULT;
	protected boolean isSelected = false;

	/**
	 * minimal movement when dragging events (in minutes)
	 */
	protected int minimalMovement = 15;

	public DefaultDayView(Calendar calendar, CalendarApplicationController controller, boolean drawTimes) {
		this(calendar, controller, drawTimes, null);
	}

	public DefaultDayView(Calendar calendar, CalendarApplicationController controller, boolean drawTimes, WeekView weekView) {
		this(Locale.getDefault(), calendar, controller, drawTimes, weekView);
	}

	public DefaultDayView(Locale locale, Calendar calendar, CalendarApplicationController controller, boolean drawTimes, WeekView weekView) {
		this(new DefaultDayViewModel(locale, calendar, controller), drawTimes, weekView);
	}

	public DefaultDayView(DayViewModel model, WeekView weekView) {
		this(model, true, weekView);
	}

	public DefaultDayView(DayViewModel model, boolean drawTimes, WeekView weekView) {
		super();

		this.model = model;
		this.drawTimes = drawTimes;
		this.weekView = weekView;

		FormLayout layout = new FormLayout("fill:pref:grow", // columns
		"fill:pref:grow"); // rows

		setLayout(layout);

		CellConstraints cc = new CellConstraints();

		if(weekView == null) {
			layout.insertRow(1, new RowSpec("pref"));
			layout.insertRow(2, new RowSpec("2dlu"));
			add(getDailyEventScrollPane(), cc.xy(1, 1));
		}

		add(getEventScrollPane(), cc.xy(1, (weekView == null ? 3 : 1)));
	}

	protected JScrollPane getEventScrollPane() {
		if(eventScrollPane == null)
			eventScrollPane = new JScrollPane(getEventPane());

		return eventScrollPane;
	}

	protected DefaultDayViewEventPane getEventPane() {
		if(eventPane == null) {
			eventPane = new DefaultDayViewEventPane(this);
			eventPane.setLayout(new OverlayLayout(eventPane));
			//eventPane.add(getGridPanel(), JLayeredPane.DEFAULT_LAYER);
			//eventPane.add(getEventsPanel(), JLayeredPane.DRAG_LAYER);
			//eventPane.setPreferredSize(new Dimension((int)eventPane.getPreferredSize().getWidth(), 1000));
		}
		return eventPane;
	}

	protected SimpleEventList getDailyEventList() {
		if(dailyEventList == null) {
			dailyEventList = new SimpleEventList(getModel());
			dailyEventList.setVisibleRowCount(2);
		}
		return dailyEventList;
	}

	protected JScrollPane getDailyEventScrollPane() {
		if(dailyEventScrollPane == null)
			dailyEventScrollPane = new JScrollPane(getDailyEventList());

		return dailyEventScrollPane;
	}

	protected int locationToTime(int locationY) {

		if(locationY - offset <= 0)
			return 0;

		int distance = locationY - offset;

		return (int) ((float)((float) distance / oneHour) * 60);	
	}

	protected int timeToLocation(int minutes) {	
		return offset + (int) ((float) ((float)minutes / 60) * oneHour);
	}

	protected void calcGridTimeMapping() {
		JSeparator firstSeparator = getEventPane().grids[0];
		JSeparator lastSeparator = getEventPane().grids[23];

		offset = firstSeparator.getY();
		if(offset <= 0)
			offset = 0;

		oneHour = ((float)((lastSeparator.getY() + (lastSeparator.getHeight() / 2)) - offset) / 23);
	}

	protected void arrangeEvents(boolean arrangeY) {
		calcGridTimeMapping();	

		/* Check for events that overlap and therefore need to be arranged side by side
		 * 
		 * If such concurrent events are found their concurrencyLevel in the dayViewEventList will be raised
		 * 
		 */

		synchronized (getDayViewEventList()) {

			Iterator<DayViewEvent> it1 = getDayViewEventList().iterator();
			while(it1.hasNext())
				it1.next().resetLevels();


			if(getDayViewEventList().size() > 1) {
				for(int i=0; i < getDayViewEventList().size(); i++) {
					for(int z=i+1; z < getDayViewEventList().size(); z++) {
						if(EventUtils.eventsOverlap(getDayViewEventList().get(i).getEvent(), getDayViewEventList().get(z).getEvent())) {
							//System.out.println("events overlap, "+getDayViewEventList().get(i).getEvent().getBegin().get(Calendar.DAY_OF_MONTH)+"/"+
							//	getDayViewEventList().get(i).getEvent().getEnd().get(Calendar.DAY_OF_MONTH)+", "+
							//	getDayViewEventList().get(z).getEvent().getBegin().get(Calendar.DAY_OF_MONTH)+"/"+
							//	getDayViewEventList().get(z).getEvent().getEnd().get(Calendar.DAY_OF_MONTH));	
							getDayViewEventList().get(i).setConcurrencyLevel(getDayViewEventList().get(i).getConcurrencyLevel()+1);
							getDayViewEventList().get(z).setConcurrencyLevel(getDayViewEventList().get(z).getConcurrencyLevel()+1);
							getDayViewEventList().get(z).setDisplacementLevel((getDayViewEventList().get(z).getDisplacementLevel()+1));
						}
					}
				}		
			}

			/* Now set the size and location of the events */

			Iterator<DayViewEvent> it = getDayViewEventList().iterator();

			while(it.hasNext()) {
				DayViewEvent dayViewEvent = it.next();

				int concurrencyLevel = dayViewEvent.getConcurrencyLevel();
				int displacementLevel = dayViewEvent.getDisplacementLevel();

				int startTime;
				if(CalendarUtils.isSameDay(dayViewEvent.getEvent().getBegin(), getModel().getCalendar()))
					startTime = dayViewEvent.getEvent().getBegin().get(Calendar.HOUR_OF_DAY) * 60 + dayViewEvent.getEvent().getBegin().get(Calendar.MINUTE);
				else
					startTime = 0;

				int endTime;
				if(CalendarUtils.isSameDay(dayViewEvent.getEvent().getEnd(), getModel().getCalendar()))
					endTime = dayViewEvent.getEvent().getEnd().get(Calendar.HOUR_OF_DAY) * 60 + dayViewEvent.getEvent().getEnd().get(Calendar.MINUTE);
				else
					endTime = 23 * 60 + 59;

				dayViewEvent.setBounds((drawTimes ? 40 : 0) + ((getEventPane().getEventsPanel().getWidth()- (drawTimes ? 50 : 0)) / concurrencyLevel) * displacementLevel,
						arrangeY ? timeToLocation(startTime) : dayViewEvent.getY(),
								(getEventPane().getEventsPanel().getWidth() - (drawTimes ? 50 : 0)) / concurrencyLevel,
								arrangeY ? (timeToLocation(endTime) - timeToLocation(startTime)) : dayViewEvent.getHeight());
			}
		}
	}

	public List<DayViewEvent> getDayViewEventList() {
		if(dayViewEventList == null)
			dayViewEventList = Collections.synchronizedList(new ArrayList<DayViewEvent>());

		return dayViewEventList;
	}

	public CalendarViewModel getModel() {
		return model;
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		clearEvents();

		if(weekView != null) {
			//weekView.setDailyEventSize(getDailyEventList().getModel().getSize());
			if(CalendarUtils.isSameDay(getModel().getCalendar(), event.getSelectedDay()))
				setSelected(true);
			else
				setSelected(false);
		}
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
		setType(type);
	}

	public void setType(int type) {
		this.type = type;

		if(weekView != null && isSelected) {
			getEventPane().getGridPanel().setBackground(getModel().getController().getColorModel().getColorFor("day.selection"));
			getDailyEventList().setBackground(getModel().getController().getColorModel().getColorFor("day.selection"));
		} else if(type == DAY_TYPE_DEFAULT) {
			getEventPane().getGridPanel().setBackground(getModel().getController().getColorModel().getColorFor("day.default"));
			getDailyEventList().setBackground(getModel().getController().getColorModel().getColorFor("day.default"));
		} else if(type == DAY_TYPE_TODAY) {
			getEventPane().getGridPanel().setBackground(getModel().getController().getColorModel().getColorFor("day.today"));
			getDailyEventList().setBackground(getModel().getController().getColorModel().getColorFor("day.today"));
		}
	}

	/**
	 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
	 */
	public void eventSelectionChanged(EventSelectionEvent event) {

	}

	protected void clearEvents() {
		synchronized (getDayViewEventList()) {

			//visualized event should not listen on selection-state-changes anymore
			Iterator<DayViewEvent> it = getDayViewEventList().iterator();

			// remove listener on of this event
			while(it.hasNext()) {
				DayViewEvent dayViewEvent = it.next();

				// deselect event (if it is selected)
				getModel().getController().getEventSelectionModel().removeSelectedEvent(dayViewEvent.getEvent());

				getModel().getController().getEventSelectionModel().removeEventSelectionListener(dayViewEvent);
			}

			getEventPane().getEventsPanel().removeAll();
			getDayViewEventList().clear();
			getDailyEventList().clear();
		}
	}

	protected void addEvents(List<Event> events) {
		Iterator<Event> it = events.iterator();

		synchronized (getDayViewEventList()) {


			// whether dayViewEvents changed and view has to be repainted
			boolean dayViewsChanged = false;

			while(it.hasNext()) {
				Event currentEvent = it.next();

				// if event is not today, do nothing
				if(!EventUtils.rangeContainsEvent(getModel().getScopeBegin(), getModel().getScopeEnd(), currentEvent))
					continue;

				// check if event is in activated categories, otherwise do not insert event into view
				if(!getModel().getController().getCategorySelector().hasEventSelectedCategories(currentEvent))
					continue;

				if(!currentEvent.isAllDayEvent()) {
					dayViewsChanged = true;

					DayViewEvent dayViewEvent = new DayViewEvent(currentEvent, DefaultDayView.this);

					// visualized event should listen on selection-state-changes
					getModel().getController().getEventSelectionModel().addEventSelectionListener(dayViewEvent);

					getEventPane().getEventsPanel().add(dayViewEvent);
					getDayViewEventList().add(dayViewEvent);
				} else
					getDailyEventList().addEvent(currentEvent);
			}

			if(dayViewsChanged) {
				arrangeEvents(true);
				getEventPane().getEventsPanel().repaint();
			}
		}
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsLoaded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsLoaded(CalendarDataEvent event) {
		clearEvents();

		addEvents(event.getEvents());
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoriesLoaded(CategoryEvent event) {
		// nothing to do		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categorySelectionChanged(CategoryEvent event) {
		// nothing to do
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoryViewSelectionChanged(CategoryEvent event) {
		// TODO check if a category has been added to (not removed from) selection and therefore the current events do not have to be cleared
		clearEvents();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsAdded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsAdded(CalendarDataEvent event) {
		addEvents(event.getEvents());
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsChanged(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsChanged(CalendarDataEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsDeleted(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsDeleted(CalendarDataEvent event) {
		Iterator<Event> it = event.getEvents().iterator();

		synchronized (getDayViewEventList()) {

			// whether dayViewEvents changed and view has to be repainted
			boolean dayViewsChanged = false;

			while(it.hasNext()) {
				Event currentEvent = it.next();

				if(!currentEvent.isAllDayEvent()) {

					for(int i=0; i < getDayViewEventList().size(); i++) {
						DayViewEvent dayViewEvent = getDayViewEventList().get(i);
						if(dayViewEvent.getEvent().equals(currentEvent)) {
							// visualisation for the event found and will be removed

							dayViewsChanged = true;

							// visualized event should not listen anymore on selection-state-changes
							getModel().getController().getEventSelectionModel().removeEventSelectionListener(dayViewEvent);

							// remove the visualisation of this event 
							getEventPane().getEventsPanel().remove(dayViewEvent);
							getDayViewEventList().remove(dayViewEvent);			
						}
					}		
				} else
					getDailyEventList().removeEvent(currentEvent);
			}

			if(dayViewsChanged) {
				arrangeEvents(true);
				getEventPane().getEventsPanel().repaint();
			}
		}
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}
}