/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.widgets.EventList;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SimpleEventList extends JList implements EventList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6831737614110456620L;
	protected CalendarViewModel parentModel;
	protected ContextMenu contextMenuEvent;
	protected ContextMenu contextMenuSpace;

	public SimpleEventList(CalendarViewModel parentModel) {
		super();
		setModel(new SortedEventListModel());
		this.parentModel = parentModel;

		setVisibleRowCount(0);
		setPrototypeCellValue(new EventImpl("I", "" , Calendar.getInstance(), Calendar.getInstance(), false));
		setCellRenderer(new DefaultListCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 8098097832124625856L;

			/**
			 * @see javax.swing.DefaultListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
			 */
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {				
				Event event = (Event)value;
				JLabel label = (JLabel) super.getListCellRendererComponent(list, event.isAllDayEvent() ? "<html><font size=\"-2\"><b>" + event.getTitle() + "</b></font></html>" : "<html><font size=\"-2\">" + CalendarUtils.getTimeAsString(event.getBegin()) + " <b>" + event.getTitle() + "</b></font></html>", index, isSelected, cellHasFocus);
				label.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
				label.setFont(label.getFont().deriveFont(Font.PLAIN));
				label.setHorizontalAlignment(SwingConstants.LEFT);
				label.setOpaque(true);
				label.setBackground(isSelected ? getController().getCategorySelector().getColorForCategory("").darker() : getController().getCategorySelector().getColorForCategory(""));
				label.setToolTipText(event.isAllDayEvent() ? "<html><h2>"+event.getTitle()+"</h2><h3>"+event.getLocation()+"</h3>Begin: "+ CalendarUtils.getDateAsString(event.getBegin()) + "<br />End :" + CalendarUtils.getDateAsString(event.getEnd()) +"</html>" : "<html><h2>"+event.getTitle()+"</h2><h3>"+event.getLocation()+"</h3>Begin: " + CalendarUtils.getTimeAsString(event.getBegin()) + "<br />End: " + CalendarUtils.getTimeAsString(event.getEnd()) + "</html>");

				return label;
			}

		});
		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if(e.isPopupTrigger()) {
					// if defined, show a context menu

					ContextMenu menu = null;

					// get the index of the list item closest to the mouse-click
					int index = locationToIndex(e.getPoint());
					// set index to -1 in order to only get an element if
					// the mouse-click actually was on an event, not just close to it
					if(getCellBounds(index, index) == null || !getCellBounds(index, index).contains(e.getPoint()))
						index = -1;

					if(index != -1) {
						getParentModel().getController().getEventSelectionModel().setSelectedEvent((Event)getModel().getElementAt(index));
						menu = getContextMenuForEvent();
					}
					else
						menu = getContextMenuForSpace();

					if(menu != null)
						menu.showContextMenuAt(e.getX(), e.getY());
				}
			}
		});

		// listen on changed event-selection-states
		getController().getEventSelectionModel().addEventSelectionListener(new EventSelectionListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
			 */
			public void eventSelectionChanged(EventSelectionEvent selectionEvent) {
				for(int i=0; i < getModel().getSize(); i++) {
					Event currentEvent = (Event)getModel().getElementAt(i);
					if(selectionEvent.getSelectedEvents().contains(currentEvent))
						getSelectionModel().addSelectionInterval(i, i);
					else
						getSelectionModel().removeSelectionInterval(i, i);
				}

			}

		});
	}
	
	public synchronized void addEvent(Event event) {
		((SortedEventListModel)getModel()).addEvent(event);
	}

	public synchronized void removeEvent(Event event) {
		((SortedEventListModel)getModel()).removeEvent(event);
	}
	
	public synchronized void clear() {
		((SortedEventListModel)getModel()).clear();
	}
	
	public synchronized void setEvents(List<Event> events) {
		((SortedEventListModel)getModel()).setEvents(events);
	}
	
	protected CalendarApplicationController getController() {
		return getParentModel().getController();
	}

	protected CalendarViewModel getParentModel() {
		return parentModel;
	}
	
	protected class SortedEventListModel extends AbstractListModel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1562952465317935134L;
		
		private List<Event> events;

		public SortedEventListModel() {
			events = Collections.synchronizedList(new ArrayList<Event>());
		}
		
		public void addEvent(Event event) {
			synchronized (events) {
				events.add(event);
				Collections.sort(events);
			}
		}
		
		public void removeEvent(Event event) {
			synchronized (events) {
				events.remove(event);
			}
		}
		
		public void clear() {
			synchronized (events) {
				events.clear();
			}
		}
		
		public void setEvents(List<Event> events) {
			synchronized (events) {
				this.events.clear();
				this.events.addAll(events);
				Collections.sort(this.events);
			}
		}

		public Object getElementAt(int i) {
			/* This is a workaround for a bug in Sun Java's javax.swing.plaf.basic.BasicListUI
			 * which does not check if the index is existant anymore when the Swing-Thread executes
			 * the paint() method at a later point when the model has changed again.
			 * 
			 * If this code is removed (undangerous) IndexOutOfBoundsExceptions may be thrown
			 */
			if(i >= getSize())
				return new EventImpl();
			
			return events.get(i);
		}

		public int getSize() {
			return events.size();
		}
		
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		return contextMenuEvent;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		return contextMenuSpace;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		this.contextMenuEvent = contextMenuEvent;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		this.contextMenuSpace = contextMenuSpace;
	}
}
