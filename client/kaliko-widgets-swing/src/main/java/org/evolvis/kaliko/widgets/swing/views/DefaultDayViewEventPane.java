/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.OverlayLayout;
import javax.swing.Scrollable;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultDayViewEventPane extends JLayeredPane implements Scrollable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5108077952535855043L;
	
	protected DefaultDayView parent;
	
	protected JPanel gridPanel;;
	protected JSeparator grids[];
	protected JPanel eventsPanel;
	protected ContextMenu contextMenuSpace;
	
	public DefaultDayViewEventPane(DefaultDayView parent) {
		super();
		this.parent = parent;
		setLayout(new OverlayLayout(this));
		setPreferredSize(new Dimension((int)getPreferredSize().getWidth(), 1000));
		add(getGridPanel(), JLayeredPane.DEFAULT_LAYER);
		add(getEventsPanel(), JLayeredPane.DRAG_LAYER);
	}
	
	protected JPanel getEventsPanel() {
		if(eventsPanel == null) {
			eventsPanel = new JPanel();
			eventsPanel.setOpaque(false);
			eventsPanel.setLayout(null);

			eventsPanel.addComponentListener(new ComponentAdapter() {

				public void componentResized(ComponentEvent e) {
					parent.arrangeEvents(true);
				}
			});
		}
		return eventsPanel;
	}

	protected JPanel getGridPanel() {
		if(gridPanel == null) {

			FormLayout layout = new FormLayout("", // columns
			""); // rows

			CellConstraints cc = new CellConstraints();

			PanelBuilder builder = new PanelBuilder(layout);

			if(parent.drawTimes) {
				builder.appendColumn(new ColumnSpec("pref"));
				builder.appendColumn(new ColumnSpec("1dlu"));
			}
			builder.appendColumn(new ColumnSpec("fill:pref:grow"));

			RowSpec spec = new RowSpec("pref");
			RowSpec spaceSpec = new RowSpec("2dlu:grow");

			int hour = 0;

			grids = new JSeparator[24];

			// create the grid
			for(int i=0; i < 24; i++) {
				builder.appendRow(spec);

				if(parent.drawTimes)
					builder.addLabel((hour < 10 ? "0" : "") + hour + "⁰⁰", cc.xy(1, i*2 + 1));

				builder.add(grids[i] = new JSeparator(), cc.xy((parent.drawTimes ? 3 : 1), i*2 + 1));

				builder.appendRow(spaceSpec);
				hour++;
			}
			// the first line should not be visible in week view for design reasons
			if(parent.weekView != null)
				grids[0].setVisible(false);

			gridPanel = builder.getPanel();
			gridPanel.setAlignmentX(0.5f);
			gridPanel.setOpaque(true);
			gridPanel.setBackground(parent.getModel().getController().getColorModel().getColorFor("day.default"));
			gridPanel.addMouseListener(new MouseAdapter() {
				
				/**
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
				@Override
				public void mouseClicked(MouseEvent e) {
					if(parent.weekView != null && e.getClickCount() == 2)
						parent.getModel().getController().requestNewEvent();
				}
				

				@Override
				public void mousePressed(MouseEvent e) {
					parent.getModel().getController().setCalendarCursorTo(parent.getModel().getCalendar());
					maybeShowPopup(e);
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					maybeShowPopup(e);
				}

				private void maybeShowPopup(MouseEvent e) {
					if(e.isPopupTrigger()) {
						//parent.getModel().getController().setCalendarCursorTo(parent.getModel().getCalendar());
						
						if(contextMenuSpace != null)
							contextMenuSpace.showContextMenuAt(e.getX(), e.getY());
					}
				}
			});
		}
		return gridPanel;
	}
	
	/**
	 * @see javax.swing.Scrollable#getPreferredScrollableViewportSize()
	 */
	public Dimension getPreferredScrollableViewportSize() {
		return new Dimension((int)getPreferredSize().getWidth(), 50);
	}

	/**
	 * @see javax.swing.Scrollable#getScrollableBlockIncrement(java.awt.Rectangle, int, int)
	 */
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 50;
	}

	/**
	 * @see javax.swing.Scrollable#getScrollableTracksViewportHeight()
	 */
	public boolean getScrollableTracksViewportHeight() {
		return (parent.weekView != null);
	}

	/**
	 * @see javax.swing.Scrollable#getScrollableTracksViewportWidth()
	 */
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	/**
	 * @see javax.swing.Scrollable#getScrollableUnitIncrement(java.awt.Rectangle, int, int)
	 */
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 50;
	}

}
