/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.event.ActionListener;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Context;
import org.evolvis.taskmanager.TaskManager.Task;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.models.MonthViewModel;
import de.tarent.kaliko.components.models.impl.DefaultMonthViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.utils.EventUtils;
import de.tarent.kaliko.components.utils.Messages;
import de.tarent.kaliko.components.views.MonthViewPanel;
import de.tarent.kaliko.objects.Event;

/**
 * 
 * A Calendar-View which shows one month at once
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultMonthView extends MonthViewPanel implements CalendarSelectionListener, CategoryListener, CalendarDataListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 537791407265059723L;
	protected JPanel monthChooserPanel;
	protected JPanel calendarPanel;

	protected JLabel[] dayLabels;
	protected JLabel[] weekLabels;

	protected JLabel monthLabel;
	protected JButton previousMonthButton;
	protected JButton nextMonthButton;

	protected MonthViewDayCell[][] dayCells;
	
	protected ContextMenu contextMenuEvent;
	protected ContextMenu contextMenuSpace;

	protected boolean drawDayNames;
	protected boolean drawWeekNumbers;
	protected boolean useShortWeekDays;

	protected int dayNameAlignment = SwingConstants.CENTER;

	protected ActionListener actionListener;

	protected MonthViewModel model;

	protected boolean isThisViewActive;

	/**
	 * specifies the index of the cell representing the first day of the month
	 */
	protected int firstDayOfMonthCellIndex;

	public DefaultMonthView(CalendarApplicationController controller) {
		this(Locale.getDefault(), controller);
	}

	public DefaultMonthView(Locale locale, CalendarApplicationController controller) {
		this(locale, Calendar.getInstance(), controller);
	}

	public DefaultMonthView(Calendar calendar, CalendarApplicationController controller) {
		this(Locale.getDefault(), calendar, controller);
	}

	public DefaultMonthView(Locale locale, Calendar calendar, CalendarApplicationController controller) {
		this(new DefaultMonthViewModel(locale, calendar, controller));
	}

	public DefaultMonthView(MonthViewModel model) {
		super();

		this.model = model;

		dayCells = new MonthViewDayCell[7][6];

		setLayout(new FormLayout("fill:pref:grow", // columns
		"fill:pref:grow")); // rows

		CellConstraints cc = new CellConstraints();

		drawDayNames = true;
		drawWeekNumbers = true;

		add(getCalendarPanel(), cc.xy(1, 1));

		numerateFields(false);
	}

	protected JPanel getCalendarPanel() {
		if(calendarPanel == null) {
			FormLayout layout = new FormLayout(new ColumnSpec[0], new RowSpec[0]);

			calendarPanel = new JPanel(layout);

			CellConstraints cc = new CellConstraints();

			RowSpec extendingRowSpec = new RowSpec("fill:pref:grow");
			ColumnSpec extendingColumnSpec = new ColumnSpec("fill:pref:grow");
			RowSpec fixedRowSpec = new RowSpec("pref");
			ColumnSpec fixedColumnSpec = new ColumnSpec("pref");

			int firstDayOfWeek = Calendar.getInstance(getModel().getLocale()).getFirstDayOfWeek(); 

			String[] weekDays;
			if(useShortWeekDays)
				weekDays = new DateFormatSymbols(getModel().getLocale()).getShortWeekdays();
			else
				weekDays = new DateFormatSymbols(getModel().getLocale()).getWeekdays();

			// setup column layout
			if(drawWeekNumbers) {
				layout.appendColumn(fixedColumnSpec);
				weekLabels = new JLabel[6];
			}

			for(int x=0; x < 7; x++)
				layout.appendColumn(extendingColumnSpec);

			// Insert Day-Name Labels ("Monday", "Tuesday", ...)

			if(drawDayNames) {
				layout.appendRow(fixedRowSpec);

				dayLabels = new JLabel[weekDays.length-1];

				for(int i=0; i < weekDays.length-1; i++) {

					int index = (firstDayOfWeek+i) % weekDays.length;

					// 0 is not defined
					if(index == 0)
						index = 1;
					calendarPanel.add(dayLabels[i] = new JLabel(weekDays[index], dayNameAlignment), cc.xy((drawWeekNumbers ? 2 : 1) + i, 1));

					if(index == Calendar.SUNDAY)
						dayLabels[i].setForeground(getModel().getController().getColorModel().getColorFor("day_label.red_letter_day"));
				}
			}

			// Insert Day-Cells

			for(int y= 0; y < 6; y++) {
				layout.appendRow(extendingRowSpec);

				if(drawWeekNumbers)
					calendarPanel.add(weekLabels[y] = new JLabel(), cc.xy(1, (drawDayNames ? 2 : 1) + y));

				for(int x=0; x < weekDays.length-1; x++)
					calendarPanel.add(dayCells[x][y] = new MonthViewDayCell(this), cc.xy((drawWeekNumbers ? 2 : 1) + x, (drawDayNames ? 2 : 1) + y));
			}

			// we want a symmetric / balanced layout under all circumstances so we group all columns and all rows

			int firstColumn = drawWeekNumbers ? 1 : 0;

			int[] columns = new int[layout.getColumnCount() - (drawWeekNumbers ? 1 : 0)];
			for(int i=0; i < columns.length; i++)
				columns[i] = firstColumn + i + 1;

			int firstRow = drawDayNames ? 1 : 0;

			int[] rows = new int[layout.getRowCount() - (drawDayNames ? 1 : 0)];
			for(int i=0; i < rows.length; i++)
				rows[i] = firstRow + i + 1;

			layout.setColumnGroups(new int[][] { columns });
			layout.setRowGroups(new int[][] { rows });
		}
		return calendarPanel;
	}

	protected void numerateFields(boolean clearFields) {	

		// create a temporary calendar-object which specifies the first displayed day in this view
		Calendar tempCalendar = getModel().getScopeBegin();

		for(int y= 0; y < 6; y++) {
			for(int x=0; x < 7; x++) {

				if(clearFields)
					dayCells[x][y].clear();

				dayCells[x][y].setCalendar((Calendar)tempCalendar.clone());

				// TODO probably there is a more elegant solution to get this value in dayToCell()
				if(tempCalendar.get(Calendar.DAY_OF_MONTH) == 1 && tempCalendar.get(Calendar.MONTH) == getModel().getCalendar().get(Calendar.MONTH))
					firstDayOfMonthCellIndex = y * 6 + x;

				// if the current day is today, highlight it
				if(CalendarUtils.isToday(tempCalendar, getModel().getLocale()))
					dayCells[x][y].setType(DAY_TYPE_TODAY);
				// if it is not a day of this month, display it gray
				else if(tempCalendar.get(Calendar.MONTH) == getModel().getCalendar().get(Calendar.MONTH))
					dayCells[x][y].setType(DAY_TYPE_DEFAULT);
				else
					dayCells[x][y].setType(DAY_TYPE_OUT_OF_SCOPE);

				// if the current day is a holiday (red-letter-day) mark it red
				dayCells[x][y].setRedLetterDay(tempCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);

				// set the selection-state of the current day
				dayCells[x][y].setSelected(CalendarUtils.isSameDay(getModel().getController().getCurrentCalendar(), tempCalendar));

				tempCalendar.add(Calendar.DAY_OF_YEAR, 1);
			}
		}

		// create a temporary calendar-object which specifies the first day of the month
		tempCalendar = ((DefaultMonthViewModel)getModel()).firstDayOfMonth();

		if(drawWeekNumbers) {
			for(int i=0; i < weekLabels.length; i++) {
				weekLabels[i].setText(String.valueOf(tempCalendar.get(Calendar.WEEK_OF_YEAR)));
				tempCalendar.add(Calendar.WEEK_OF_YEAR, 1);
			}
		}					
	}

	protected void clearEvents() {
		for(int y= 0; y < 6; y++) {
			for(int x=0; x < 7; x++) {
				dayCells[x][y].clear();
			}
		}
	}

	protected void insertEventsIntoFields() {

		TaskManager.getInstance().register(new Task() {

			public void cancel() {
				// TODO Auto-generated method stub

			}

			public void run(Context context) {
				Calendar calendarCursor = getModel().getScopeBegin();
				calendarCursor.set(Calendar.HOUR_OF_DAY, 0);
				calendarCursor.set(Calendar.MINUTE, 0);
				calendarCursor.set(Calendar.SECOND, 0);
				calendarCursor.set(Calendar.MILLISECOND, 0);


				Calendar end = (Calendar)calendarCursor.clone();
				calendarCursor.set(Calendar.HOUR_OF_DAY, 23);
				calendarCursor.set(Calendar.MINUTE, 59);
				calendarCursor.set(Calendar.SECOND, 59);
				calendarCursor.set(Calendar.MILLISECOND, 999);

				context.setGoal(42);

				for(int y= 0; y < 6; y++) {
					for(int x=0; x < 7; x++) {

						context.setActivityDescription(Messages.getFormattedString("Task_Load_Events_Activity_Desc", calendarCursor.get(Calendar.DAY_OF_MONTH), new DateFormatSymbols(Locale.getDefault()).getShortMonths()[calendarCursor.get(Calendar.MONTH)]));

						if(context.isCancelled())
							break;

//						dayCells[x][y].setEvents(getModel().getController().getCalendarDataManager().getEvents(calendarCursor, end, getModel().getController().getCategorySelector().getActiveCategories()));

						calendarCursor.add(Calendar.DAY_OF_MONTH, 1);
						end.add(Calendar.DAY_OF_MONTH, 1);

						context.setCurrent(y*7+(x+1));
					}
					if(context.isCancelled())
						break;
				}
			}

		}, Messages.getString("Task_Load_Events_Title"), true);
	}

	public CalendarViewModel getModel() {
		return model;
	}

	protected MonthViewDayCell dayToCell(Calendar day) {

		// A Calendar object pointing on the first displayed day
		Calendar tempCalendar = getModel().getScopeBegin();

		// A Calendar object pointing to the last displayed day. (This view shows 6 x 7 = 42 day-cells).
		Calendar lastDisplayedDay = (Calendar)tempCalendar.clone();
		lastDisplayedDay.add(Calendar.DAY_OF_YEAR, 41);

		// Check if the day is in the range of the first and the last displayed day. if not return null.
		if(day.compareTo(tempCalendar) < 0 || day.compareTo(lastDisplayedDay) > 0)
			return null;

		// FIXME does not work correctly for days which are in the "grey zone" (do not belong to current month but are displayed) 
		int cellIndex = day.get(Calendar.DAY_OF_MONTH) - 1 + firstDayOfMonthCellIndex;

		return dayCells[(cellIndex % 7)][cellIndex / 7];
	}

	protected List<MonthViewDayCell> cellsBetween(Calendar begin, Calendar end) {
		List<MonthViewDayCell> cells = new ArrayList<MonthViewDayCell>();

		// A Calendar object pointing on the first displayed day
		Calendar tempCalendar = getModel().getScopeBegin();

		// A Calendar object pointing to the last displayed day. (This view shows 6 x 7 = 42 day-cells).
		Calendar lastDisplayedDay = getModel().getScopeEnd();

		end.set(Calendar.HOUR_OF_DAY, 23);
		end.set(Calendar.MINUTE, 59);
		end.set(Calendar.SECOND, 60);
		end.set(Calendar.MILLISECOND, 999);

		// Check if the begin or end is in the range of the first and the last displayed day. if not return an empty list
		if((begin.compareTo(tempCalendar) < 0 && end.compareTo(tempCalendar) < 0) ||
				(begin.compareTo(lastDisplayedDay) > 0 || end.compareTo(lastDisplayedDay) > 0))
			return cells;

		// FIXME does not work correctly for days which are in the "grey zone" (do not belong to current month but are displayed) 
		int beginIndex = begin.get(Calendar.DAY_OF_MONTH) - 1 + firstDayOfMonthCellIndex;
		int endIndex = end.get(Calendar.DAY_OF_MONTH) - 1 + firstDayOfMonthCellIndex;

		for(int i=beginIndex; i <= endIndex; i++)
			cells.add(dayCells[(i % 7)][i / 7]);

		return cells;
	}
	
	protected void addEvents(List<Event> events) {
		
		Iterator<Event> eventsIt = events.iterator();
		while(eventsIt.hasNext()) {
			Event currentEvent = eventsIt.next();
			
			// check if event is in activated categories, otherwise do not insert event into view
			if(!getModel().getController().getCategorySelector().hasEventSelectedCategories(currentEvent))
				continue;
			
			for(int x=0; x < dayCells.length; x++) {
				for(int y=0; y < dayCells[x].length; y++) {
					// need to set the begin-time to 0:00:00:000 in order to get events for the whole day
					Calendar dayCellBegin = (Calendar)dayCells[x][y].getCalendar().clone();
					dayCellBegin.set(Calendar.HOUR_OF_DAY, 0);
					dayCellBegin.set(Calendar.MINUTE, 0);
					dayCellBegin.set(Calendar.SECOND, 0);
					dayCellBegin.set(Calendar.MILLISECOND, 0);

					// need to set the end-time to 23:59:59:999 in order to get events for the whole day
					Calendar dayCellEnd = (Calendar)dayCells[x][y].getCalendar().clone();
					dayCellEnd.set(Calendar.HOUR_OF_DAY, 23);
					dayCellEnd.set(Calendar.MINUTE, 59);
					dayCellEnd.set(Calendar.SECOND, 59);
					dayCellEnd.set(Calendar.MILLISECOND, 999);
					if(EventUtils.rangeContainsEvent(dayCellBegin, dayCellEnd, currentEvent))
						dayCells[x][y].addEvent(currentEvent);
				}
			}
		}
	}

//	public void eventAdded(CalendarChangeEvent event) {
//		// FIXME does not consider multiple occurences
//
//		// FIXME does not work correctly
////		Iterator<MonthViewDayCell> it = cellsBetween(event.getNewEvent().getBegin(), event.getNewEvent().getEnd()).iterator();
//
////		while(it.hasNext()) {
////		MonthViewDayCell dayCell = it.next();
////		if(dayCell != null)
////		dayCell.addEvent(event.getNewEvent());
////		}
//
//		for(int x=0; x < dayCells.length; x++) {
//			for(int y=0; y < dayCells[x].length; y++) {
//
////				// need to set the begin-time to 0:00:00:000 in order to get events for the whole day
////				Calendar dayCellBegin = (Calendar)dayCells[x][y].getCalendar().clone();
////				dayCellBegin.set(Calendar.HOUR_OF_DAY, 0);
////				dayCellBegin.set(Calendar.MINUTE, 0);
////				dayCellBegin.set(Calendar.SECOND, 0);
////				dayCellBegin.set(Calendar.MILLISECOND, 0);
//
////				// need to set the end-time to 23:59:59:999 in order to get events for the whole day
////				Calendar dayCellEnd = (Calendar)dayCells[x][y].getCalendar().clone();
////				dayCellEnd.set(Calendar.HOUR_OF_DAY, 23);
////				dayCellEnd.set(Calendar.MINUTE, 59);
////				dayCellEnd.set(Calendar.SECOND, 59);
////				dayCellEnd.set(Calendar.MILLISECOND, 999);
//
//				if(dayCells[x][y].getCalendar().get(Calendar.MONTH) >= event.getNewEvent().getBegin().get(Calendar.MONTH) &&
//						dayCells[x][y].getCalendar().get(Calendar.MONTH) <= event.getNewEvent().getEnd().get(Calendar.MONTH) &&
//						dayCells[x][y].getCalendar().get(Calendar.DAY_OF_MONTH) >= event.getNewEvent().getBegin().get(Calendar.DAY_OF_MONTH) &&
//						dayCells[x][y].getCalendar().get(Calendar.DAY_OF_MONTH) <= event.getNewEvent().getEnd().get(Calendar.DAY_OF_MONTH))
//					dayCells[x][y].addEvent(event.getNewEvent());
//			}
//		}
//	}
//
//	protected boolean dayContainsEvent(Event event, Calendar day) {
//		int year = day.get(Calendar.YEAR);
//		int dayOfYear = day.get(Calendar.DAY_OF_YEAR);
//
//		return !((year < event.getBegin().get(Calendar.YEAR) || dayOfYear < event.getBegin().get(Calendar.DAY_OF_YEAR)) ||
//				(year > event.getEnd().get(Calendar.YEAR) || dayOfYear > event.getEnd().get(Calendar.DAY_OF_YEAR)));
//
//	}
//
//	public void eventChanged(CalendarChangeEvent event) {
//		// FIXME does not consider multiple occurences
//
//		// TODO ensure to not update cells twice
//
//		Iterator<MonthViewDayCell> it = cellsBetween(event.getOldEvent().getBegin(), event.getOldEvent().getEnd()).iterator();
//
//		while(it.hasNext()) {
//			MonthViewDayCell dayCell = it.next();
//			if(dayCell != null) {
//				if(dayContainsEvent(event.getOldEvent(), dayCell.getCalendar()) && !dayContainsEvent(event.getNewEvent(), dayCell.getCalendar()))
//					dayCell.removeEvent(event.getOldEvent());
//				else
//					dayCell.repaint();
//			}
//		}
//
//		it = cellsBetween(event.getNewEvent().getBegin(), event.getNewEvent().getEnd()).iterator();
//
//		while(it.hasNext()) {
//			MonthViewDayCell dayCell = it.next();
//			if(dayCell != null) {
//				if(!dayContainsEvent(event.getOldEvent(), dayCell.getCalendar()) && dayContainsEvent(event.getNewEvent(), dayCell.getCalendar()))
//					dayCell.addEvent(event.getNewEvent());
//				else
//					dayCell.repaint();
//			}
//		}
//	}
//
//	public void eventRemoved(CalendarChangeEvent event) {
//		// FIXME does not consider multiple occurences
//
//		// FIXME does not work yet
////		Iterator<MonthViewDayCell> it = cellsBetween(event.getOldEvent().getBegin(), event.getOldEvent().getEnd()).iterator();
//
////		while(it.hasNext()) {
////		MonthViewDayCell dayCell = it.next();
////		if(dayCell != null)
////		dayCell.removeEvent(event.getOldEvent());
////		}
//
//		for(int x=0; x < dayCells.length; x++) {
//			for(int y=0; y < dayCells[x].length; y++)
//				dayCells[x][y].removeEvent(event.getOldEvent());
//		}
//	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoryViewSelectionChanged(CategoryEvent event) {
		clearEvents();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		// if month has changed, clear and numerate fields
		if(!CalendarUtils.isSameMonth(event.getOldSelectedDay(), event.getSelectedDay()))
			numerateFields(true);

		// update selection-states
		for(int y= 0; y < 6; y++)
			for(int x=0; x < 7; x++)
				dayCells[x][y].setSelected(CalendarUtils.isSameDay(event.getSelectedDay(), dayCells[x][y].getCalendar()));
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categorySelectionChanged(CategoryEvent event) {
		clearEvents();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoriesLoaded(CategoryEvent event) {
		// nothing to do
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsLoaded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsLoaded(CalendarDataEvent event) {
		clearEvents();
		
		addEvents(event.getEvents());
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsAdded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsAdded(CalendarDataEvent event) {
		addEvents(event.getEvents());
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsChanged(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsChanged(CalendarDataEvent event) {
		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsDeleted(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsDeleted(CalendarDataEvent event) {
		// TODO could be more effecient
		
		for(int x=0; x < dayCells.length; x++)
			for(int y=0; y < dayCells[x].length; y++)
				for(int i=0; i < event.getEvents().size(); i++)
					dayCells[x][y].removeEvent(event.getEvents().get(i));
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		return contextMenuEvent; 
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		return contextMenuSpace;
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		this.contextMenuEvent = contextMenuEvent;
		
		// We need to update the childs, too
		for(int x=0; x < dayCells.length; x++)
			for(int y=0; y < dayCells[x].length; y++)
				dayCells[x][y].setContextMenuForEvent(contextMenuEvent);
	}

	/**
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		this.contextMenuSpace = contextMenuSpace;
		
		// We need to update the childs, too
		for(int x=0; x < dayCells.length; x++)
			for(int y=0; y < dayCells[x].length; y++)
				dayCells[x][y].setContextMenuForSpace(contextMenuSpace);
	}
}