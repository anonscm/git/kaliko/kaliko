/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.evolvis.kaliko.widgets.common.utils.Messages;
import org.evolvis.kaliko.widgets.dialogs.EventDialog;
import org.evolvis.kaliko.widgets.swing.EnhancedJSpinnerDateEditor;
import org.evolvis.kaliko.widgets.swing.SwingCategoryTable;
import org.evolvis.kaliko.widgets.swing.TimeChooser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.commons.ui.EscapeDialog;
import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SwingEventDialog extends EscapeDialog implements EventDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5182526425323292943L;
	protected JPanel mainPanel;
	protected JTabbedPane tabbedPane;

	protected JPanel generalPanel;
	protected JPanel notePanel;
	protected JPanel notificationPanel;
	protected JPanel participantsPanel;
	protected JPanel recurrencyPanel;

	protected JTextField titleField;
	protected JTextField locationField;
	protected EnhancedJSpinnerDateEditor beginDateChooser;
	protected EnhancedJSpinnerDateEditor endDateChooser;
	protected TimeChooser beginTimeChooser;
	protected TimeChooser endTimeChooser;
	protected JCheckBox allDayCheckBox;
	protected JTextArea noteField;
	protected JTextArea participantsField;
	protected JLabel durationLabel;
	protected JLabel categoriesLabel;
	protected JButton changeCategoriesButton;

	protected CommonDialogButtons dialogButtons;

	protected ActionListener actionListener;
	protected TimeListener timeListener;

	protected Event event;
	protected List<Category> categories;

	protected Action submitAction;
	protected List<Category> availableCategories;

	protected final static Logger logger = Logger.getLogger(SwingEventDialog.class.getName());

	public SwingEventDialog(Frame parent) {
		this(parent, null);
	}

	public SwingEventDialog(Frame parent, Action submitAction) {
		this(parent, null, submitAction, MODE_NEW);
	}

	public SwingEventDialog(Frame parent, Event event, Action submitAction, int mode) {
		super(parent, "Event", true);

		setEvent(event);
		setSubmitAction(submitAction);

		setContentPane(getMainPanel());

		setSize(600, 350);
		setLocationRelativeTo(parent);

		setMode(mode);

		addWindowListener(new EventDialogWindowListener());
	}

	protected JPanel getMainPanel() {
		if(mainPanel == null) {

			PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", "fill:pref:grow, 10dlu, pref"));

			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();

			builder.add(getTabbedPane(), cc.xy(1, 1));

			builder.add(getDialogButtons(), cc.xy(1, 3));

			mainPanel = builder.getPanel();
		}
		return mainPanel;
	}

	protected CommonDialogButtons getDialogButtons() {
		if(dialogButtons == null)
			dialogButtons = CommonDialogButtons.getSubmitCancelAndActionButtons(SUBMIT_NEW_EVENT, CommonDialogButtons.CANCEL, null, null, getActionListener(), null, getRootPane());

		return dialogButtons;
	}

	protected JTabbedPane getTabbedPane() {
		if(tabbedPane == null) {
			tabbedPane = new JTabbedPane();

			tabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(tabbedPane));

			tabbedPane.addTab(Messages.getString("EventDialog_Tab_Title_General"), getGeneralPanel());
			//tabbedPane.addTab(Messages.getString("EventDialog_Tab_Title_Recurrency"), getRecurrencyPanel());
			//tabbedPane.addTab(Messages.getString("EventDialog_Tab_Title_Notification"), getNotificationPanel());
			tabbedPane.addTab(Messages.getString("EventDialog_Tab_Title_Note"), getNotePanel());
			//tabbedPane.addTab(Messages.getString("EventDialog_Tab_Title_Participants"), getParticipantsPanel());
		}
		return tabbedPane;
	}

//	private JPanel getRecurrencyPanel() {
//	if(recurrencyPanel == null) {
//	recurrencyPanel = new JPanel();

//	}
//	return recurrencyPanel;
//	}

//	private JPanel getParticipantsPanel() {
//	if(participantsPanel == null) {
//	PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", // colums
//	"fill:pref:grow")); // rows

//	builder.setDefaultDialogBorder();

//	CellConstraints cc = new CellConstraints();

//	builder.add(getParticipantsField(), cc.xy(1, 1));

//	participantsPanel = builder.getPanel();
//	}
//	return participantsPanel;
//	}

//	private JPanel getNotificationPanel() {
//	if(notificationPanel == null) {
//	notificationPanel = new JPanel();
//	}
//	return notificationPanel;
//	}

	private JPanel getNotePanel() {
		if(notePanel == null) {

			PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", // colums
			"fill:pref:grow")); // rows

			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();

			builder.add(getNoteField(), cc.xy(1, 1));

			notePanel = builder.getPanel();
		}
		return notePanel;
	}

	private JPanel getGeneralPanel() {
		if(generalPanel == null) {
			generalPanel = new JPanel();

			PanelBuilder builder = new PanelBuilder(new FormLayout("pref, 3dlu, pref, 3dlu, pref, 3dlu:grow, pref", // colums
			"pref, 3dlu, pref, 10dlu, pref, 3dlu, fill:pref, 3dlu, fill:pref, 10dlu, pref, 3dlu, pref")); // rows

			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();

			builder.addLabel(Messages.getString("EventDialog_Tab_General_Event_Title"), cc.xy(1, 1));
			builder.add(getTitleField(), cc.xyw(3, 1, 5));

			builder.add(createPlainLabel(Messages.getString("EventDialog_Tab_General_Event_Location")), cc.xy(1, 3));
			builder.add(getLocationField(), cc.xyw(3, 3, 5));

			builder.addSeparator(Messages.getString("EventDialog_Tab_General_Separator_DateAndTime"), cc.xyw(1, 5, 7));

			builder.add(createPlainLabel(Messages.getString("EventDialog_Tab_General_Event_Start")), cc.xy(1, 7));
			builder.add(getBeginDateChooser(), cc.xy(3, 7));
			builder.add(getBeginTimeChooser(), cc.xy(5, 7));
			builder.add(getAllDayCheckBox(), cc.xy(7, 7));

			builder.add(createPlainLabel(Messages.getString("EventDialog_Tab_General_Event_End")), cc.xy(1, 9));
			builder.add(getEndDateChooser(), cc.xy(3, 9));
			builder.add(getEndTimeChooser(), cc.xy(5, 9));
			builder.add(getDurationLabel(), cc.xy(7, 9));

			builder.addSeparator(Messages.getString("EventDialog_Tab_General_Separator_Categories"), cc.xyw(1, 11, 7));

			builder.add(getCategoriesLabel(), cc.xyw(1, 13, 6));
			builder.add(getChangeCategoriesButton(), cc.xy(7, 13));

			generalPanel = builder.getPanel();
		}
		return generalPanel;
	}

	protected JLabel getCategoriesLabel() {
		if(categoriesLabel == null) {
			categoriesLabel = createPlainLabel("");
		}
		return categoriesLabel;
	}

	protected JButton getChangeCategoriesButton() {
		if(changeCategoriesButton == null) {
			changeCategoriesButton = new JButton(Messages.getString("EventDialog_SelectCategories_Button"));
			changeCategoriesButton.addActionListener(new ActionListener() {

				/**
				 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
				 */
				public void actionPerformed(ActionEvent e) {
					final EscapeDialog dialog = new EscapeDialog(SwingEventDialog.this, Messages.getString("EventDialog_SelectCategories_DialogTitle"), true);

					final SwingCategoryTable categoryTable = new SwingCategoryTable();
					// TODO this class should be fully passive. therefore the categories must be set externally
					categoryTable.setAvailableCategories(getAvailableCategories());
					categoryTable.setSelectedCategories(getSelectedCategories());
					categoryTable.setEnabled(true);

					JScrollPane scrollPane = new JScrollPane(categoryTable);
					scrollPane.getViewport().setOpaque(true);
					scrollPane.getViewport().setBackground(Color.WHITE);
					scrollPane.setPreferredSize(new Dimension(1, 1));

					PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", // columns
					"pref, 3dlu, fill:pref:grow, 10dlu, pref")); // rows

					builder.setDefaultDialogBorder();

					CellConstraints cc = new CellConstraints();

					builder.add(createPlainLabel(Messages.getString("EventDialog_SelectCategories_Message")), cc.xy(1, 1));
					builder.add(scrollPane, cc.xy(1, 3));
					builder.add(CommonDialogButtons.getSubmitCancelButtons(new ActionListener() {

						/**
						 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
						 */
						public void actionPerformed(ActionEvent e) {
							if(e.getActionCommand().equals("cancel"))
								dialog.setVisible(false);
							else if(e.getActionCommand().equals("submit")) {
								setSelectedCategories(categoryTable.getSelectedCategories());
								dialog.setVisible(false);
								dialog.dispose();
							}
						}

					}, dialog.getRootPane()), cc.xy(1, 5));

					dialog.setContentPane(builder.getPanel());

					dialog.setSize(500, 320);
					dialog.setLocationRelativeTo(SwingEventDialog.this);
					dialog.setVisible(true);
				}

			});
		}
		return changeCategoriesButton;
	}

	public void setAvailableCategories(Collection<Category> categories) {
		this.availableCategories = new ArrayList<Category>(categories);
	}

	public List<Category> getAvailableCategories() {
		return availableCategories;
	}

	public void setSelectedCategories(Collection<Category> categories) {
		this.categories = new ArrayList<Category>(categories);

		StringBuffer text = new StringBuffer();
		for(int i=0; i < categories.size(); i++) {
			text.append(this.categories.get(i).getName());
			if(i+1 < categories.size())
				text.append(", ");
		}
		if(categories.size() == 0)
			getCategoriesLabel().setText(Messages.getString("EventDialog_No_Categories"));
		else
			getCategoriesLabel().setText(text.toString());
		getCategoriesLabel().setToolTipText(text.toString());
	}

	public Collection<Category> getSelectedCategories() {
		return categories;
	}

	protected JCheckBox getAllDayCheckBox() {
		if(allDayCheckBox == null) {
			allDayCheckBox = createPlainCheckBox(Messages.getString("EventDialog_Tab_General_Event_AllDay"));
			allDayCheckBox.addActionListener(new ActionListener() {

				/**
				 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
				 */
				public void actionPerformed(ActionEvent e) {
//					getBeginTimeChooser().setEnabled(!allDayCheckBox.isSelected());
//					getEndTimeChooser().setEnabled(!allDayCheckBox.isSelected());
				}

			});
		}
		return allDayCheckBox;
	}

	protected ActionListener getActionListener() {
		if(actionListener == null) {
			actionListener = new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					if(event.getActionCommand().equals("submit")) {
						closeWindow();
						submitAction.actionPerformed(event);
					}
					else if(event.getActionCommand().equals("cancel"))
						closeWindow();
				}
			};
		}
		return actionListener;
	}

	protected JTextField getTitleField() {
		if(titleField == null) {
			titleField = new JTextField();
			titleField.setFont(titleField.getFont().deriveFont(Font.BOLD));
		}
		return titleField;
	}

	protected JTextField getLocationField() {
		if(locationField == null) {
			locationField = new JTextField();
		}
		return locationField;
	}

	protected EnhancedJSpinnerDateEditor getBeginDateChooser() {
		if(beginDateChooser == null) {
			beginDateChooser = new EnhancedJSpinnerDateEditor();
			beginDateChooser.setCalendar(Calendar.getInstance(Locale.getDefault()));
			beginDateChooser.addChangeListener(getTimeListener());

		}
		return beginDateChooser;
	}

	protected EnhancedJSpinnerDateEditor getEndDateChooser() {
		if(endDateChooser == null) {
			endDateChooser = new EnhancedJSpinnerDateEditor();
			endDateChooser.setCalendar(Calendar.getInstance(Locale.getDefault()));
			endDateChooser.addChangeListener(getTimeListener());

		}
		return endDateChooser;
	}


	protected TimeChooser getBeginTimeChooser() {
		if(beginTimeChooser == null) {
			beginTimeChooser = new TimeChooser();
			beginTimeChooser.addItemListener(getTimeListener());
		}
		return beginTimeChooser;
	}

	protected TimeChooser getEndTimeChooser() {
		if(endTimeChooser == null) {
			endTimeChooser = new TimeChooser();
			endTimeChooser.addItemListener(getTimeListener());
		}
		return endTimeChooser;
	}

	protected JTextArea getNoteField() {
		if(noteField == null)
			noteField = new JTextArea();
		return noteField;
	}

	protected JTextArea getParticipantsField() {
		if(participantsField == null)
			participantsField = new JTextArea();
		return participantsField;
	}

	protected JLabel getDurationLabel() {
		if(durationLabel == null) {
			durationLabel = createPlainLabel(Messages.getFormattedString("EventDialog_Tab_General_Event_Duration", ""));
			durationLabel.setVisible(false);
			updateDuration();
		}
		return durationLabel;
	}

	protected void updateDuration() {
		// TODO calculate duration
		int duration = 0;
		getDurationLabel().setText((Messages.getFormattedString("EventDialog_Tab_General_Event_Duration", CalendarUtils.getDurationAsString(duration))));
	}

	private JLabel createPlainLabel(String text) {
		JLabel label = new JLabel(text);
		label.setFont(label.getFont().deriveFont(Font.PLAIN));
		return label;
	}

	private JCheckBox createPlainCheckBox(String text) {
		JCheckBox checkBox = new JCheckBox(text);
		checkBox.setFont(checkBox.getFont().deriveFont(Font.PLAIN));
		return checkBox;
	}

	public void clear()  {
		event = null;

		getTitleField().setText("");
		getLocationField().setText("");
		getNoteField().setText("");
		getBeginDateChooser().setCalendar(Calendar.getInstance(Locale.getDefault()));
		getEndDateChooser().setCalendar(Calendar.getInstance(Locale.getDefault()));
	}

	public Event getEvent() {
		if(event == null || !(event instanceof EventImpl))
			event = new EventImpl("", "", Calendar.getInstance(), Calendar.getInstance(), allDayCheckBox.isSelected());

		((EventImpl)event).setTitle(getTitleField().getText());
		((EventImpl)event).setLocation(getLocationField().getText());
		((EventImpl)event).setNote(getNoteField().getText());
		((EventImpl)event).setBegin(getBeginDateChooser().getCalendar());
		((EventImpl)event).setEnd(getEndDateChooser().getCalendar());

		if(getAllDayCheckBox().isSelected())
			((EventImpl)event).setAllDayEvent(true);
//		else {
//		((EventImpl)event).setAllDayEvent(false);
//		Calendar begin = event.getBegin();
//		begin.set(Calendar.HOUR_OF_DAY, getBeginTimeChooser().getTime().getHours());
//		begin.set(Calendar.MINUTE, getBeginTimeChooser().getTime().getMinutes());
//		((EventImpl)event).setBegin(begin);

//		Calendar end = event.getBegin();
//		end.set(Calendar.HOUR_OF_DAY, getEndTimeChooser().getTime().getHours());
//		end.set(Calendar.MINUTE, getEndTimeChooser().getTime().getMinutes());
//		((EventImpl)event).setEnd(end);
//		}

		return event;
	}

	public void setBeginDay(Calendar begin) {
		setBeginTime(addOneHourAndRound(begin));
	}

	public void setEndDay(Calendar end) {
		setEndTime(addOneHourAndRound(end));
	}

	public void setBeginTime(Calendar begin) {
		getBeginDateChooser().setCalendar(begin);
	}

	public void setEndTime(Calendar end) {
		getEndDateChooser().setCalendar(end);
	}

	public void setEvent(Event event) {
		this.event = event;

		if(event != null) {
			getTitleField().setText(event.getTitle());
			getLocationField().setText(event.getLocation());
			getNoteField().setText(event.getNote());
			getBeginDateChooser().setCalendar(event.getBegin());
			getEndDateChooser().setCalendar(event.getEnd());
			getAllDayCheckBox().setSelected(event.isAllDayEvent());
		}
	}

	public void setSubmitAction(Action submitAction) {
		this.submitAction = submitAction;
	}

	public void setMode(int mode) {
		if(mode == EventDialog.MODE_NEW) {
			setTitle(TITLE_NEW_EVENT);
			clear();
			dialogButtons.getSubmitButton().setText(SUBMIT_NEW_EVENT);
			dialogButtons.getSubmitButton().setToolTipText(SUBMIT_NEW_EVENT_TOOLTIP);
			dialogButtons.getCancelButton().setText(CANCEL_NEW_EVENT);
			dialogButtons.getCancelButton().setToolTipText(CANCEL_NEW_EVENT_TOOLTIP);
			Calendar begin = Calendar.getInstance(Locale.getDefault());
			getBeginDateChooser().setCalendar(addOneHourAndRound(begin));
			getEndDateChooser().setCalendar(addOneHourAndRound((Calendar)begin.clone()));
			getChangeCategoriesButton().setEnabled(true);
			getChangeCategoriesButton().setToolTipText(Messages.getString("EventDialog_SelectCategories_Button_Tooltip"));
			getAllDayCheckBox().setSelected(false);
		} else if (mode == EventDialog.MODE_EDIT) {
			setTitle(TITLE_EDIT_EVENT);
			dialogButtons.getSubmitButton().setText(SUBMIT_EDIT_EVENT);
			dialogButtons.getSubmitButton().setToolTipText(SUBMIT_EDIT_EVENT_TOOLTIP);
			dialogButtons.getCancelButton().setText(CANCEL_EDIT_EVENT);
			dialogButtons.getCancelButton().setToolTipText(CANCEL_EDIT_EVENT_TOOLTIP);
			getChangeCategoriesButton().setEnabled(false);
			getChangeCategoriesButton().setToolTipText(Messages.getString("EventDialog_SelectCategories_Button_Tooltip_Disabled"));
		} else
			logger.warning("unknown dialog-mode");

		getTabbedPane().setSelectedIndex(0);
	}

	protected Calendar addOneHourAndRound(Calendar calendar) {
		calendar.add(Calendar.HOUR, 1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	protected Calendar substractOneHourAndRound(Calendar calendar) {
		calendar.add(Calendar.HOUR, -1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	protected TimeListener getTimeListener() {
		if(timeListener == null)
			timeListener = new TimeListener();

		return timeListener;
	}
	
	public void open() {
		setVisible(true);
	}

	protected class TimeListener implements ItemListener, ChangeListener {
		/**
		 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
		 */
		public void itemStateChanged(ItemEvent e) {
			// update duration-label
			updateDuration();
		}

		/**
		 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
		 */
		public void stateChanged(ChangeEvent e) {
			// if a time changed, check if other time needs to be updated to have a valid range
			if(getBeginDateChooser().getCalendar().compareTo(getEndDateChooser().getCalendar()) >= 0) {
				if(e.getSource().equals(getBeginDateChooser()))
					getEndDateChooser().setCalendar(addOneHourAndRound((Calendar)getBeginDateChooser().getCalendar().clone()));
				else if(e.getSource().equals(getEndDateChooser()))
					getBeginDateChooser().setCalendar(substractOneHourAndRound((Calendar)getEndDateChooser().getCalendar().clone()));

			}
			updateDuration();
		}
	}

	protected class EventDialogWindowListener extends WindowAdapter
	{		
		/*
		 * The Title-Field should get the focus at start
		 * 
		 */
		public void windowActivated(WindowEvent e) {
			getTitleField().requestFocusInWindow();
		}
	}
}
