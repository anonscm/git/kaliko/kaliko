/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.controller;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.UIManager;

import de.tarent.kaliko.components.controller.ColorModel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultColorModel implements ColorModel {

	protected Map<String, Color> colors;
	
	public DefaultColorModel() {
		colors = new HashMap<String, Color>();
		colors.put("day.default", Color.WHITE);
		colors.put("day.selection", UIManager.getLookAndFeelDefaults().getColor("List.selectionBackground"));
		colors.put("day.today", new Color(169, 201, 139));
		colors.put("day.out_of_scope", UIManager.getLookAndFeelDefaults().getColor("TextField.inactiveBackground"));
		colors.put("day_line.default", Color.BLACK);
		colors.put("day_line.selection", Color.BLACK);
		colors.put("day_line.out_of_scope", Color.GRAY);
		colors.put("day_line.today", Color.BLACK);
		colors.put("day_label.default", Color.BLACK);
		colors.put("day_label.red_letter_day", new Color(164, 0, 0));
	}
	
	/**
	 * @see de.tarent.kaliko.components.controller.ColorModel#getColorFor(java.lang.String)
	 */
	public Color getColorFor(String id) {
		return colors.get(id);
	}

}
