/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.EventTable;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.CategoryListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.listener.impl.CategoryEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.models.ListViewModel;
import de.tarent.kaliko.components.models.impl.DefaultListViewModel;
import de.tarent.kaliko.components.views.ListViewPanel;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultListView extends ListViewPanel implements CategoryListener, CalendarSelectionListener, CalendarDataListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2720379536774351597L;
	protected EventTable eventTable;
	protected JScrollPane eventScrollPane;
	protected JScrollPane appointmentListScrollPane;
//	protected FilterPanel filterPanel;
	protected ListViewModel model;
	
	public DefaultListView(CalendarApplicationController controller) {
		this(new DefaultListViewModel(controller.getLocale(), controller.getCurrentCalendar(), controller));
	}
	
	public DefaultListView(ListViewModel model) {
		super();
		
		this.model = model;
		
		setLayout(new FormLayout("fill:pref:grow", // columns
				"pref, 2dlu, fill:pref:grow")); //rows	
		
		CellConstraints cc = new CellConstraints();
		
//		add(getFilterPanel(), cc.xy(1, 1));
		add(getEventScrollPane(), cc.xy(1, 3));
		
		reinit();
	}
	
	protected JScrollPane getAppointmentListScrollPane() {
		if(appointmentListScrollPane == null) {
			appointmentListScrollPane = new JScrollPane(getEventTable());
		}
		return appointmentListScrollPane;
	}
	
	protected JScrollPane getEventScrollPane() {
		if(eventScrollPane == null) {
			eventScrollPane = new JScrollPane(getEventTable());
			eventScrollPane.setPreferredSize(new Dimension((int)eventScrollPane.getPreferredSize().getWidth(), 1));
		}
		return eventScrollPane;
	}
	
	protected EventTable getEventTable() {
		if(eventTable == null) {
//			eventList = new JTable(new EventTableModel());
//			eventList.setDragEnabled(true);
//			eventList.setTransferHandler(new EventTransferHandler());
//			eventList.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
//
//				/**
//				 * 
//				 */
//				private static final long serialVersionUID = -2591254102819670620L;
//
//				/**
//				 * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
//				 */
//				@Override
//				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//					Calendar calendar = (Calendar)value;
//					return super.getTableCellRendererComponent(table, CalendarUtils.getDateAsString(calendar) + " " + CalendarUtils.getTimeAsString(calendar), isSelected, hasFocus, row, column);
//				}
//				
//			});
			eventTable = new EventTable(getModel());
		}
		return eventTable;
	}
	
//	protected FilterPanel getFilterPanel() {
//		if(filterPanel == null) {
//			List<ListFilter> availableFilters = new ArrayList<ListFilter>();
//			
//			// TODO this should be configurable
//			
//			CalendarEventFilter filter2 = new CalendarEventFilter();
//			filter2.setFilterName("All");
//			availableFilters.add(filter2);
//			
//			filterPanel = new FilterPanel(availableFilters);
//			filterPanel.addFilterChangeListener(new FilterChangeListener() {
//
//				/**
//				 * @see de.tarent.commons.ui.FilterPanel.FilterChangeListener#filterChanged(de.tarent.commons.datahandling.ListFilter)
//				 */
//				public void filterChanged(ListFilter arg0) {
//					reinit();
//				}
//				
//			});
//		}
//		return filterPanel;
//	}

	public String getCurrentDisplayedView() {
		return "";
	}
	
	protected void reinit() {
//		((EventTableModel)getEventList().getModel()).setData(getModel().getController().getCalendarDataManager().getEvents((CalendarEventFilter)getFilterPanel().getActiveFilter()));
	}

	public class EventTableModel extends AbstractTableModel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -640355203004195029L;
		protected String[] columnNames = new String[] { "Title", "Location", "Begin" };
		protected List<Event> events;

		public EventTableModel() {
			events = new ArrayList<Event>();
		}
		
		public void addEvent(Event event) {
			events.add(event);
			fireTableRowsInserted(events.size()-1, events.size()-1);
		}
		
		public void setData(List<Event> events) {
			this.events = events;
			fireTableDataChanged();
		}
		
		public void clear() {
			events.clear();
		}

		public int getColumnCount() {
			return columnNames.length;
		}

		public String getColumnName(int column) {
			return columnNames[column];
		}

		public Class<?> getColumnClass(int columnIndex) {
			if(columnIndex == 0)
				return String.class;
			else if(columnIndex == 1)
				return String.class;
			else if(columnIndex == 2)
				return Calendar.class;
			return null;
		}

		public int getRowCount() {
			return events.size();
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			if(columnIndex == 0)
				return events.get(rowIndex).getTitle();
			else if(columnIndex == 1)
				return events.get(rowIndex).getLocation();
			else if(columnIndex == 2)
				return events.get(rowIndex).getBegin();
			return null;
		}

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return false;
		}
		
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			// TODO Auto-generated method stub
			
		}		
	}

	public CalendarViewModel getModel() {
		return model;
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		reinit();	
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoryViewSelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoryViewSelectionChanged(CategoryEvent event) {
		reinit();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categorySelectionChanged(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categorySelectionChanged(CategoryEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * @see de.tarent.kaliko.components.listener.CategoryListener#categoriesLoaded(de.tarent.kaliko.components.listener.impl.CategoryEvent)
	 */
	public void categoriesLoaded(CategoryEvent event) {	
		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsAdded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsAdded(CalendarDataEvent event) {
		Iterator<Event> it = event.getEvents().iterator();
		
		while(it.hasNext())
			getEventTable().addEvent(it.next());
		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsChanged(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsChanged(CalendarDataEvent event) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsDeleted(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsDeleted(CalendarDataEvent event) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsLoaded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsLoaded(CalendarDataEvent event) {
		getEventTable().setEvents(event.getEvents());
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}
}
