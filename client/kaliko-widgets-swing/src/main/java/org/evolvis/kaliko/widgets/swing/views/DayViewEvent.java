/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.event.MouseInputAdapter;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DayViewEvent extends JLabel implements EventSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1629791726062544387L;
	protected Event event;
	protected DefaultDayView parent;
	protected int concurrencyLevel = 1;
	protected int displacementLevel = 0;
	protected static int minWrapSize = 40;
	protected ContextMenu contextMenuEdit;

	public DayViewEvent(Event event, DefaultDayView parent) {

		this.event = event;
		this.parent = parent;

		setFont(getFont().deriveFont(Font.PLAIN));
		updateText();
		setBackground(parent.getModel().getController().getCategorySelector().getColorForCategory(""));
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.CENTER);
		//setBackground(new Color(157, 199, 255));
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		addComponentListener(new ComponentAdapter() {

			/**
			 * @see java.awt.event.ComponentAdapter#componentResized(java.awt.event.ComponentEvent)
			 */
			@Override
			public void componentResized(ComponentEvent e) {
				updateText();
				super.componentResized(e);
			}
			
		});

		MouseInputAdapter mia = new MouseInputAdapter() {
			int y;
			boolean resize = false;
			Event oldEvent;

			/**
			 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				
				//if(SwingUtilities.isLeftMouseButton(e))
				DayViewEvent.this.parent.getModel().getController().getEventSelectionModel().requestSelection(getEvent(), e.getModifiersEx() == MouseEvent.CTRL_DOWN_MASK);
				
				if(e.getClickCount() >= 2)
					DayViewEvent.this.parent.getModel().getController().requestEditEvent(getEvent());
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				//if(DayViewEvent.this.getHeight()-e.getY() < 10)
					// DnD-actions are currently not activated. disable cursors
					//DayViewEvent.this.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				//else
					// DnD-actions are currently not activated. disable cursors
					//DayViewEvent.this.setCursor(Cursor.getDefaultCursor());
			}

			@Override
			public void mousePressed(MouseEvent me) {
				if(SwingUtilities.isLeftMouseButton(me)) {
					y = me.getY();
					if(DayViewEvent.this.getHeight()-y < 10) {
						resize = true;
						// DnD-actions are currently not activated. disable cursors
						//DayViewEvent.this.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
					}
					else {
						resize = false;
						// DnD-actions are currently not activated. disable cursors
						//DayViewEvent.this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
					}
				} else if (me.isPopupTrigger()) {
					DayViewEvent.this.parent.getModel().getController().getEventSelectionModel().setSelectedEvent(getEvent());
					showEditPopup(me);
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {

//				if(SwingUtilities.isLeftMouseButton(e)) {
//					if(resize) { // Changes the length of a calendar-event without changing its start-time
//						//DayViewEvent.this.setSize(DayViewEvent.this.getWidth(), e.getY());
//						int endTime = DayViewEvent.this.parent.locationToTime(DayViewEvent.this.getHeight() + DayViewEvent.this.getY() + e.getY() - y);
//						if(endTime % DayViewEvent.this.parent.minimalMovement < 3) {
//							
//							endTime = endTime - (endTime % DayViewEvent.this.parent.minimalMovement);
//							
//							DayViewEvent.this.setSize(DayViewEvent.this.getWidth(), DayViewEvent.this.parent.timeToLocation(endTime) - DayViewEvent.this.getY());
//							
//							// copy the original event before changing in order to fire correct event in mouseReleased()
//							if(oldEvent == null)
//								oldEvent = (Event)getEvent().clone();
//							
//							getEvent().getEnd().set(Calendar.HOUR_OF_DAY, endTime / 60);
//							getEvent().getEnd().set(Calendar.MINUTE, endTime % 60);
//							
//							DayViewEvent.this.parent.arrangeEvents(false);
//						}
//					} else { // changes the start end end time of a calendar-event without changing its length
//						int startTime = DayViewEvent.this.parent.locationToTime(DayViewEvent.this.getY() + e.getY() - y);
//						int endTime = DayViewEvent.this.parent.locationToTime(DayViewEvent.this.getHeight() + DayViewEvent.this.getY() + e.getY() - y);
//						
//						if(startTime % DayViewEvent.this.parent.minimalMovement < 3) {
//							
//							startTime = startTime - (startTime % DayViewEvent.this.parent.minimalMovement);
//							
//							DayViewEvent.this.setLocation(DayViewEvent.this.getX(),
//									DayViewEvent.this.parent.timeToLocation(startTime));
//							
//							// copy the original event before changing in order to fire correct event in mouseReleased()
//							if(oldEvent == null)
//								oldEvent = (Event)getEvent().clone();
//							
//							getEvent().getBegin().set(Calendar.HOUR_OF_DAY, startTime / 60);
//							getEvent().getBegin().set(Calendar.MINUTE, startTime % 60);
//							getEvent().getEnd().set(Calendar.HOUR_OF_DAY, endTime / 60);
//							getEvent().getEnd().set(Calendar.MINUTE, endTime % 60);
//							DayViewEvent.this.parent.arrangeEvents(false);
//							updateText();
//						}
//
//					}
//				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				
				if(DayViewEvent.this.getHeight()-y < 10)
					DayViewEvent.this.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				else
					DayViewEvent.this.setCursor(Cursor.getDefaultCursor());
				if(oldEvent != null) {
//					DayViewEvent.this.parent.getModel().getController().fireEventChanged(oldEvent, getEvent());
//					oldEvent = null;
				}
			}
			
			private void showEditPopup(final MouseEvent e) {				
				if(contextMenuEdit != null)
					contextMenuEdit.showContextMenuAt(e.getX(), e.getY());
			}
		};
		addMouseMotionListener(mia);
		addMouseListener(mia);
	}

	public Event getEvent() {
		return event;
	}
	
	public int getConcurrencyLevel() {
		return concurrencyLevel;
	}
	
	public void setConcurrencyLevel(int level) {
		this.concurrencyLevel = level;
	}
	
	public int getDisplacementLevel() {
		return displacementLevel;
	}
	
	public void setDisplacementLevel(int level) {
		this.displacementLevel = level;
	}
	
	public void resetLevels() {
		this.displacementLevel = 0;
		this.concurrencyLevel = 1;
	}
	
	protected void updateText() {
		// TODO I18n
		setText("<html><center>" + CalendarUtils.getTimeAsString(event.getBegin()) + (getHeight() > minWrapSize ? "<br>" : " ") +
				"<b>" + event.getTitle() + "</b>"+ (getHeight() > minWrapSize ? "<br>" : " ") + 
				CalendarUtils.getTimeAsString(event.getEnd()) +  "</center></html>");
		setToolTipText("<html><h2>"+event.getTitle()+"</h2><h3>"+event.getLocation()+"</h3>Begin: " + CalendarUtils.getDateAsString(event.getBegin()) + " " + CalendarUtils.getTimeAsString(event.getBegin()) + "<br />End: " + CalendarUtils.getDateAsString(event.getEnd()) + " " + CalendarUtils.getTimeAsString(event.getEnd()) + "</html>");
	}
	
	public void setSelected(boolean selected) {
		if(selected)
			setBackground(parent.getModel().getController().getCategorySelector().getColorForCategory("").darker());
		else
			setBackground(parent.getModel().getController().getCategorySelector().getColorForCategory(""));
	}

	public void setMultiDayBefore(boolean multiDayBefore) {
		setIcon(multiDayBefore ? parent.getModel().getController().getIconModel().getIconFor("go-up.png") : null);
	}
	
	public void setMultiDayAfter(boolean multiDayAfter) {
		setIcon(multiDayAfter ? parent.getModel().getController().getIconModel().getIconFor("go-down.png") : null);
	}

	/**
	 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
	 */
	public void eventSelectionChanged(EventSelectionEvent event) {
		setSelected(event.getSelectedEvents().contains(getEvent()));
	}
}
