/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.controller;

import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import de.tarent.kaliko.components.controller.IconModel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultIconModel implements IconModel {

	Map<String, Icon> iconMap;
	
	public DefaultIconModel() {
		iconMap = new HashMap<String, Icon>();
	}
	
	/**
	 * @see de.tarent.kaliko.components.controller.IconModel#getIconFor(java.lang.String)
	 */
	public Icon getIconFor(String iconID) {
		Icon icon = iconMap.get(iconID);
		if(icon == null) {
			icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage("/de/tarent/libraries/components/calendar/gfx/"+iconID));
			iconMap.put(iconID, icon);
		}
		return icon;
	}

}
