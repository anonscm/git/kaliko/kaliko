/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import javax.swing.JPanel;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultWeekViewTimeBar extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3265647057288287777L;
	
	protected JPanel gridPanel;
	
	public DefaultWeekViewTimeBar() {
		super();

		setLayout(new FormLayout("pref", // columns
		"pref, 2dlu, fill:pref:grow")); // rows

		CellConstraints cc = new CellConstraints();
		
		add(getGridPanel(), cc.xy(1, 3));
	}
	
	protected JPanel getGridPanel() {
		if(gridPanel == null) {

			FormLayout layout = new FormLayout("", // columns
			""); // rows

			CellConstraints cc = new CellConstraints();

			PanelBuilder builder = new PanelBuilder(layout);

			builder.appendColumn(new ColumnSpec("pref"));
			builder.appendColumn(new ColumnSpec("1dlu"));
			
			builder.appendColumn(new ColumnSpec("fill:pref:grow"));

			RowSpec spec = new RowSpec("pref");
			RowSpec spaceSpec = new RowSpec("2dlu:grow");

			int hour = 0;

			// create the grid
			for(int i=0; i < 24; i++) {
				builder.appendRow(spec);

				builder.addLabel((hour < 10 ? "0" : "") + hour + "⁰⁰", cc.xy(1, i*2 + 1));

				builder.appendRow(spaceSpec);
				hour++;
			}

			gridPanel = builder.getPanel();
		}
		return gridPanel;
	}
}
