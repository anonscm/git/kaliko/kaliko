/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

import java.util.Calendar;
import java.util.Locale;

import com.toedter.calendar.JSpinnerDateEditor;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class EnhancedJSpinnerDateEditor extends JSpinnerDateEditor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 700129250580799170L;

	public EnhancedJSpinnerDateEditor() {
		super();
	}
	
	public void setCalendar(Calendar calendar) {
		this.setDate(calendar.getTime());
	}
	
	public Calendar getCalendar() {
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		calendar.setTime(getDate());
		return calendar;
	}
}
