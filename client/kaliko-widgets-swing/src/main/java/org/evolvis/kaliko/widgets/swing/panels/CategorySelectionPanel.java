/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.evolvis.kaliko.widgets.common.CategoryCollectionHelper;
import org.evolvis.kaliko.widgets.common.CategorySelector;
import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.SwingCategoryTable;
import org.evolvis.kaliko.widgets.swing.SwingCategoryTable.CategorySelectionListener;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;
import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.utils.Messages;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class CategorySelectionPanel extends JPanel implements CategorySelector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4428820961692810853L;

	protected SwingCategoryTable allCategoriesList;
	protected SwingCategoryTable favoriteCategoriesList;
	protected Map<Category, Color> categoryColors;
	protected CalendarApplicationController controller;
	protected JScrollPane allCategoriesScrollPane;
	protected JScrollPane favoriteCategoriesScrollPane;
	protected JTabbedPane tabbedPane;
	protected ContextMenu favoriteCategoriesContextMenu;
	protected ContextMenu allCategoriesContextMenu;
	protected boolean selectionInitialized = false;

	public CategorySelectionPanel(CalendarApplicationController controller) {

		this.controller = controller;

		categoryColors = new HashMap<Category, Color>();

		FormLayout layout = new FormLayout("fill:pref:grow", // columns
		"fill:pref:grow"); // rows

		setLayout(layout);

		CellConstraints cc = new CellConstraints();

		add(getTabbedPane(), cc.xy(1, 1));
		//getTabbedPane().setSelectedIndex(1);
	}

	protected JTabbedPane getTabbedPane() {
		if(tabbedPane == null) {
			tabbedPane = new JTabbedPane();
			tabbedPane.addTab(Messages.getString("CategorySelectionPanel_Tab_Favorites_Title"), getFavoriteCategoriesScrollPane());
			tabbedPane.addTab(Messages.getString("CategorySelectionPanel_Tab_All_Title"), getAllCategoriesScrollPane());
			tabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(tabbedPane));
		}
		return tabbedPane;
	}

	protected JScrollPane getFavoriteCategoriesScrollPane() {
		if(favoriteCategoriesScrollPane == null) {
			favoriteCategoriesScrollPane = new JScrollPane(getFavoriteCategoriesList());
			favoriteCategoriesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			favoriteCategoriesScrollPane.setPreferredSize(new Dimension(1, 1));
		}
		return favoriteCategoriesScrollPane;
	}

	protected JScrollPane getAllCategoriesScrollPane() {
		if(allCategoriesScrollPane == null) {
			allCategoriesScrollPane = new JScrollPane(getAllCategoriesList());
			allCategoriesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			allCategoriesScrollPane.setPreferredSize(new Dimension(1, 1));
		}
		return allCategoriesScrollPane;
	}

	protected SwingCategoryTable getFavoriteCategoriesList() {
		if(favoriteCategoriesList == null) {
			favoriteCategoriesList = new SwingCategoryTable();
//			favoriteCategoriesList = new JTable(new CategoryListModel());
//			favoriteCategoriesList.setOpaque(true);
//			favoriteCategoriesList.setBackground(Color.WHITE);
//			favoriteCategoriesList.setEnabled(false);
//			((CategoryListModel)favoriteCategoriesList.getModel()).setAvailableCategories(new ArrayList<Category>());
			favoriteCategoriesList.addMouseListener(new MouseAdapter() {

				/**
				 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
				 */
				@Override
				public void mousePressed(MouseEvent e) {
					maybeShowPopup(e);
				}

				/**
				 * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
				 */
				@Override
				public void mouseReleased(MouseEvent e) {
					maybeShowPopup(e);
				}
				
				protected void maybeShowPopup(MouseEvent e) {
					// if the user did a mouse-click which should be interpreted as a popup-call (system-dependend), show a popup-menu
			    	if(favoriteCategoriesContextMenu != null && getFavoriteCategoriesList().isEnabled() && e.isPopupTrigger())
//						favoriteCategoriesContextMenu.show(getFavoriteCategoriesList(), e.getX(), e.getY());
			    		favoriteCategoriesContextMenu.showContextMenuAt(e.getX(), e.getY());
				}
				
			});
		}
		return favoriteCategoriesList;
	}

	protected SwingCategoryTable getAllCategoriesList() {
		if(allCategoriesList == null) {
			allCategoriesList = new SwingCategoryTable();
			allCategoriesList.addCategorySelectionListener(new CategorySelectionListener() {

				/**
				 * @see de.tarent.kaliko.components.widgets.impl.SwingCategoryTable.CategorySelectionListener#categorySelectionChanged()
				 */
				public void categorySelectionChanged() {
					if(selectionInitialized) {
						getController().fireCategorySelectionChanged(CategoryCollectionHelper.toList(getSelectedCategories()));
					}
				}
				
			});
//			allCategoriesList = new JTable(new CategoryListModel());
//			allCategoriesList.setOpaque(true);
//			allCategoriesList.setBackground(Color.WHITE);
//			allCategoriesList.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer() {
//
//				/**
//				 * 
//				 */
//				private static final long serialVersionUID = 3531531909121895282L;
//
//				@Override
//				public Component getTableCellRendererComponent(JTable arg0, Object value, boolean isSelected, boolean arg3, int arg4, int arg5) {
//					Category category = ((Category)value);
//					JLabel comp = (JLabel)super.getTableCellRendererComponent(arg0, category.getName(), isSelected, false, arg4, arg5);
//					comp.setBackground(isSelected ? CategorySelectionPanel.this.getColorForCategory(category).darker() : CategorySelectionPanel.this.getColorForCategory(category));
//					comp.setToolTipText(category.getName());
//					return comp;
//				}
//
//			});
//			// TODO set column 0 not editable
//			allCategoriesList.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JCheckBox()) {
//
//				/**
//				 * 
//				 */
//				private static final long serialVersionUID = -7116990150079408530L;
//
//				@Override
//				public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
//					JCheckBox comp = (JCheckBox)super.getTableCellEditorComponent(table, value, isSelected, row, column);
//					comp.setHorizontalAlignment(SwingConstants.RIGHT);
//					comp.setOpaque(true);
//					Category category = (Category) getAllCategoriesList().getModel().getValueAt(row, 0);
//					comp.setBackground(CategorySelectionPanel.this.getColorForCategory(category));
//					return comp;
//				}
//
//			});	
//			CheckBoxCellRenderer renderer = new CheckBoxCellRenderer();
//			allCategoriesList.getColumnModel().getColumn(1).setCellRenderer(renderer);
//			allCategoriesList.setShowGrid(false);
//			allCategoriesList.setCellSelectionEnabled(false);
//			allCategoriesList.setRowSelectionAllowed(true);
//			allCategoriesList.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//			allCategoriesList.getColumnModel().getColumn(1).setPreferredWidth((int)renderer.getPreferredSize().getWidth());
//			allCategoriesList.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
//			allCategoriesList.setIntercellSpacing(new Dimension(0, (int)allCategoriesList.getIntercellSpacing().getHeight()));
//			allCategoriesList.setEnabled(false);
			allCategoriesList.addMouseListener(new MouseAdapter() {

				/**
				 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
				 */
				@Override
				public void mousePressed(MouseEvent e) {
					maybeShowPopup(e);
				}

				/**
				 * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
				 */
				@Override
				public void mouseReleased(MouseEvent e) {
					maybeShowPopup(e);
				}
				
				protected void maybeShowPopup(MouseEvent e) {
					// if the user did a mouse-click which should be interpreted as a popup-call (system-dependend), show a popup-menu
			    	if(allCategoriesContextMenu != null && getAllCategoriesList().isEnabled() && e.isPopupTrigger())
//						allCategoriesContextMenu.show(getAllCategoriesList(), e.getX(), e.getY());
			    		allCategoriesContextMenu.showContextMenuAt(e.getX(), e.getY());
				}
				
			});
		}
		return allCategoriesList;
	}

	protected CalendarApplicationController getController() {
		return controller;
	}

	protected class CheckBoxCellRenderer extends JCheckBox implements TableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7921133983172470413L;

		public CheckBoxCellRenderer() {
			super();
			this.setOpaque(true);
			this.setBackground(Color.WHITE);
			this.setHorizontalAlignment(SwingConstants.RIGHT);
		}

		/**
		 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean cellHasFocus, int column, int row) {
			this.setSelected((Boolean)value);
			Category category = (Category) getAllCategoriesList().getModel().getValueAt(row, 0);
			this.setBackground(isSelected ? CategorySelectionPanel.this.getColorForCategory(category).darker() : CategorySelectionPanel.this.getColorForCategory(category));
			return this;
		}
	}

	public void setAvailableCategories(Collection<Category> categories) {
		getAllCategoriesList().setAvailableCategories(categories);
		
		getAllCategoriesList().setEnabled(true);
	}

//	public List<Category> getSelectedCategories() {
//		return getAllCategoriesList().getSelectedCategories();
//	}


	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getSelectedCategory()
	 */
	public List<Category> getSelectedCategories() {
		
		//JTable currentTable = getTabbedPane().getSelectedIndex() == 0 ? getFavoriteCategoriesList() : getAllCategoriesList();
		JTable currentTable = getAllCategoriesList();
		
		int[] indices = currentTable.getSelectedRows();

		List<Category> selectedCategories = new ArrayList<Category>();

		for(int i=0; i < indices.length; i++)
			selectedCategories.add((Category)currentTable.getModel().getValueAt(indices[i], 0));

		return selectedCategories;
	}
	
	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getSelectedWritableCategories()
	 */
	public List<Category> getSelectedWritableCategories() {
		List<Category> activatedWritableCategories = new ArrayList<Category>();
		
		Iterator<Category> it = getSelectedCategories().iterator();
		while(it.hasNext()) {
			Category category = it.next();
			if(category.getAccess().isWritable())
				activatedWritableCategories.add(category);
		}
		
		return activatedWritableCategories;
	}

	protected void fireCategorySelectionChanged() {
		getController().fireCategorySelectionChanged(getSelectedCategories());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getAvailableCategories()
	 */
	public List<Category> getAvailableCategories() {
		return getAllCategoriesList().getAvailableCategories();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getColorForCategory(de.tarent.kaliko.components.data.Category)
	 */
	public Color getColorForCategory(Category category) {
		Color color = categoryColors.get(category);

		if(color == null)
			return Color.PINK;

		return color;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getColorForCategory(java.lang.String)
	 */
	public Color getColorForCategory(String guid) {
		Iterator it = categoryColors.keySet().iterator();
		while(it.hasNext()) {
			Category category = (Category)it.next();
			if(category.getGuid().equals(guid))
				return getColorForCategory(category); 
		}
		return getColorForCategory((Category)null);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getSelectedCategoriesAsString()
	 */
	public String getSelectedCategoriesAsString() {
		Iterator<Category> categoriesIt = getSelectedCategories().iterator();
		StringBuffer categoriesAsString = new StringBuffer(";");

		while(categoriesIt.hasNext())
			categoriesAsString.append(categoriesIt.next().getGuid()+";");

		return categoriesAsString.toString();
	}
	
	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getPreselectedCategoriesAsString()
	 */
	public String getPreselectedCategoriesAsString() {
		Iterator<Category> categoriesIt = getSelectedCategories().iterator();
		StringBuffer categoriesAsString = new StringBuffer(";");

		while(categoriesIt.hasNext())
			categoriesAsString.append(categoriesIt.next().getGuid()+";");

		return categoriesAsString.toString();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#setSelectedCategories(java.lang.String)
	 */
	public void setSelectedCategories(String selectedCategories) {
		getAllCategoriesList().setSelectedCategories(selectedCategories);
		selectionInitialized = true;
		getController().fireCategorySelectionChanged(getSelectedCategories());
	}
	
	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#setPreselectedCategories(java.lang.String)
	 */
	public void setDefaultCategories(String categories) {
		getAllCategoriesList().setDefaultCategories(categories);
	}
	
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setDefaultCategories(java.util.Collection)
	 */
	public void setDefaultCategories(Collection<Category> defaultCategories) {
		getAllCategoriesList().setDefaultCategories(defaultCategories);
	}

	public void setFavoriteCategories(String categories) {
		List<Category> availableCategories = getAvailableCategories();

		List<Category> favoriteCategories = new ArrayList<Category>();

		for(int i=0; i < availableCategories.size(); i++) {
			if(categories.contains(availableCategories.get(i).getGuid()))
				favoriteCategories.add(availableCategories.get(i));
		}

		getFavoriteCategoriesList().setAvailableCategories(favoriteCategories);

	}
	
	public String getFavoriteCategoriesAsString() {
		Iterator<Category> categoriesIt = getFavoriteCategoriesList().getAvailableCategories().iterator();
		StringBuffer categoriesAsString = new StringBuffer(";");

		while(categoriesIt.hasNext())
			categoriesAsString.append(categoriesIt.next().getGuid()+";");

		return categoriesAsString.toString();
	}
	
	public void setFavoriteCategoriesContextMenu(ContextMenu menu) {
		this.favoriteCategoriesContextMenu = menu;
	}
	
	public void setAllCategoriesContextMenu(ContextMenu menu) {
		this.allCategoriesContextMenu = menu;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#addToFavorites(de.tarent.kaliko.objects.Category)
	 */
	public void addToFavorites(Category category) {
		getFavoriteCategoriesList().addCategory(category);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#removeFromFavorites(de.tarent.kaliko.objects.Category)
	 */
	public void removeFromFavorites(Category category) {
		getFavoriteCategoriesList().removeCategory(category);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#setColorForCategory(de.tarent.kaliko.objects.Category, java.awt.Color)
	 */
	public void setColorForCategory(Category category, Color color) {
		CategorySelectionPanel.this.categoryColors.put(category, color);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.CategorySelector#getWritableCategories()
	 */
	public List<Category> getWritableCategories() {
		ArrayList<Category> writableCategories = new ArrayList<Category>(); 
		Iterator<Category> it = getAvailableCategories().iterator();
		
		while(it.hasNext()) {
			Category category = it.next();
			if(category.getAccess().isWritable())
				writableCategories.add(category);
		}
		return writableCategories;
	}

	/**
	 * 
	 * @see de.tarent.kaliko.components.controller.CategorySelector#hasEventSelectedCategories(de.tarent.kaliko.objects.Event)
	 */
	public boolean hasEventSelectedCategories(Event event) {
		// check if event is in activated categories, otherwise do not insert event into view
		Iterator<Category> it = event.getCategoriesList().iterator();
		
		while(it.hasNext()) {
			if(getSelectedCategoriesAsString().contains(it.next().getGuid()))
				return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getFavoriteCategories()
	 */
	public Collection<Category> getFavoriteCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setActivatedCategories(java.util.Collection)
	 */
	public void setSelectedCategories(Collection<Category> arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setFavoriteCategories(java.util.Collection)
	 */
	public void setFavoriteCategories(Collection<Category> arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getDefaultCategories()
	 */
	public Collection<Category> getDefaultCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getDefaultCategoriesAsString()
	 */
	public String getDefaultCategoriesAsString() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setCategoryActivationAction(javax.swing.Action)
	 */
	public void setCategorySelectionAction(Action action) {
		// TODO Auto-generated method stub
		
	}
	
	
}
