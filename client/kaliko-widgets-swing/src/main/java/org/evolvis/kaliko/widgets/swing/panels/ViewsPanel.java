/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import java.awt.BorderLayout;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JPanel;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.views.DefaultDayView;
import org.evolvis.kaliko.widgets.swing.views.DefaultMonthView;
import org.evolvis.kaliko.widgets.swing.views.DefaultWeekView;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.controller.ViewSwitcher;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.components.views.CalendarViewPanel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ViewsPanel extends JPanel implements ViewSwitcher {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8605031013924665217L;
	protected CalendarApplicationController controller;
	protected DefaultDayView dayView;
	protected DefaultWeekView weekView;
	protected DefaultMonthView monthView;
	
	protected CalendarView currentView;
	protected CalendarView previousView;
	
	protected ContextMenu contextMenuEvent;
	protected ContextMenu contextMenuSpace;
	
	protected final static Logger logger = Logger.getLogger(ViewsPanel.class.getName());

	public ViewsPanel(CalendarApplicationController controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		
	}
	
	protected DefaultMonthView getMonthView() {
		if(monthView == null) {
			monthView = new DefaultMonthView(getController().getCurrentCalendar(), getController());
			
			getController().addCalendarSelectionListener(monthView);
			getController().addCalendarDataListener(monthView);
			//getController().addCalendarChangeListener(monthView);
		}
		return monthView;
	}

	protected DefaultWeekView getWeekView() {
		if(weekView == null) {
			weekView = new DefaultWeekView(getController().getCurrentCalendar(), getController());

			getController().addCalendarSelectionListener(weekView);
			//getController().addCalendarChangeListener(weekView);
			getController().addCalendarDataListener(weekView);
		}
		return weekView;
	}

	protected DefaultDayView getDayView() {
		if(dayView == null) {
			dayView = new DefaultDayView(getController().getCurrentCalendar(), getController(), true);
			
			getController().addCalendarSelectionListener(dayView);
			//getController().addCalendarChangeListener(dayView);
			getController().addCalendarDataListener(dayView);
		}
		return dayView;
	}
	
	protected void switchView(CalendarViewPanel view) {
		// do not do anything when view did not change
		if(currentView == view)
			return;
		
		previousView = currentView;
		currentView = view;

		removeAll();
		add(view, BorderLayout.CENTER);
		validate();
		repaint();
		
		getController().fireCalendarViewChange(previousView, view);
	}
	
	protected CalendarApplicationController getController() {
		return controller;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#gotoNext()
	 */
	public void gotoNext() {
		Calendar oldCalendar = (Calendar)getController().getCurrentCalendar().clone();
		currentView.getModel().gotoNext(getController().getCurrentCalendar());
		getController().fireCalendarSelectionChanged(oldCalendar);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#gotoPrevious()
	 */
	public void gotoPrevious() {
		Calendar oldCalendar = (Calendar)getController().getCurrentCalendar().clone();
		currentView.getModel().gotoPrevious(getController().getCurrentCalendar());
		getController().fireCalendarSelectionChanged(oldCalendar);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#gotoToday()
	 */
	public void gotoToday() {
		// if current calendar equals today, do nothing
		if(isToday())
			return;
		
		Calendar today = Calendar.getInstance(getController().getLocale());
		
		Map<Integer, Integer> fieldsAndValues = new HashMap<Integer, Integer>();
		fieldsAndValues.put(Calendar.YEAR, today.get(Calendar.YEAR));
		fieldsAndValues.put(Calendar.DAY_OF_YEAR, today.get(Calendar.DAY_OF_YEAR));
		
		getController().setCalendarCursor(fieldsAndValues);
	}
	
	/**
	 * Whether the current calendar equals today
	 * @return
	 */
	protected boolean isToday() {
		Calendar today = Calendar.getInstance(getController().getLocale());
		
		return (getController().getCurrentCalendar().get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
				getController().getCurrentCalendar().get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
				getController().getCurrentCalendar().get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH));
	}
	

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setCurrentView(int)
	 */
	public void setCurrentView(int view) {
		if(view == CalendarView.DAY_VIEW)
			switchView(getDayView());
		else if(view == CalendarView.WEEK_VIEW)
			switchView(getWeekView());
		else if(view == CalendarView.MONTH_VIEW)
			switchView(getMonthView());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getCurrentView()
	 */
	public CalendarView getCurrentView() {
		return currentView;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		return contextMenuEvent;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		return contextMenuSpace;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		this.contextMenuEvent = contextMenuEvent;
		
		// Update all calendar-views
		getDayView().setContextMenuForEvent(contextMenuEvent);
		getWeekView().setContextMenuForEvent(contextMenuEvent);
		getMonthView().setContextMenuForEvent(contextMenuEvent);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		this.contextMenuSpace = contextMenuSpace;
		
		// Update all calendar-views
		getDayView().setContextMenuForSpace(contextMenuSpace);
		getWeekView().setContextMenuForSpace(contextMenuSpace);
		getMonthView().setContextMenuForSpace(contextMenuSpace);
	}
}
