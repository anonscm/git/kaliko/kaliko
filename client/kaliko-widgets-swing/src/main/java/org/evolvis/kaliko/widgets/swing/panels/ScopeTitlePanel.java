/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.kaliko.components.controller.CalendarApplicationController;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ScopeTitlePanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5732273565430027276L;
	protected JLabel currentDisplayedScopeLabel;
	
	public ScopeTitlePanel(CalendarApplicationController controller) {
		super();

		setLayout(new FormLayout("0dlu:grow, max(120dlu;pref), 0dlu:grow", // columns
		"pref")); // rows

		CellConstraints cc = new CellConstraints();

		add(getCurrentDisplayedScopeLabel(), cc.xy(2, 1));
	}

	protected JLabel getCurrentDisplayedScopeLabel() {
		if(currentDisplayedScopeLabel == null) {
			currentDisplayedScopeLabel = new JLabel("", SwingConstants.CENTER);
		}
		return currentDisplayedScopeLabel;
	}
	
	public void setCurrentDisplayedScopeText(String text) {
		getCurrentDisplayedScopeLabel().setText("<html><h2>" + text + "</h2></html>");
	}
}
