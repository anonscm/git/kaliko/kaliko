/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.Color;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarDataListener;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarDataEvent;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.models.WeekViewModel;
import de.tarent.kaliko.components.models.impl.DefaultWeekViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.views.WeekViewPanel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultWeekView extends WeekViewPanel implements CalendarSelectionListener, CalendarDataListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6777587510723133121L;

	protected Locale locale;

	protected WeekViewModel model;
	protected DefaultDayView[] dayViews;
	protected JLabel[] dayLabels;

	protected boolean drawDayNames;
	protected boolean useShortWeekDays = true;

	protected int dayNameAlignment = SwingConstants.CENTER;
	protected int maxDailyEvents = 0;

	protected boolean drawTimes = true;

	protected final static Color COLOR_SUNDAY = new Color(164, 0, 0);

	public DefaultWeekView(CalendarApplicationController controller) {
		this(Locale.getDefault(), controller);

	}

	public DefaultWeekView(Locale locale, CalendarApplicationController controller) {
		this(locale, Calendar.getInstance(locale), controller);
	}

	public DefaultWeekView(Calendar calendar, CalendarApplicationController controller) {
		this(Locale.getDefault(), calendar, controller);
	}

	public DefaultWeekView(Locale locale, Calendar calendar, CalendarApplicationController controller) {
		this(new DefaultWeekViewModel(locale, calendar, controller));
	}

	public DefaultWeekView(WeekViewModel model) {
		super();

		this.model = model;

		drawDayNames = true;

		FormLayout layout = new FormLayout("fill:pref:grow, fill:pref:grow, fill:pref:grow, fill:pref:grow, fill:pref:grow, fill:pref:grow, fill:pref:grow", // columns
		""); // rows

		setLayout(layout);

		CellConstraints cc = new CellConstraints();

		if(drawTimes)
			layout.insertColumn(1, new ColumnSpec("pref"));

		int firstDayOfWeek = Calendar.getInstance(getModel().getLocale()).getFirstDayOfWeek();

		// Insert Day-Name Labels ("Monday", "Tuesday", ...)

		if(drawDayNames) {
			layout.appendRow(new RowSpec("pref"));

			dayLabels = new JLabel[7];

			for(int i=0; i < 7; i++)
				add(dayLabels[i] = new JLabel(new String(), dayNameAlignment), cc.xy((drawTimes ? 2 : 1) + i, 1));

			updateDayLabels();
		}

		dayViews = new DefaultDayView[7];

		Calendar calendarCursor = (Calendar)getModel().getCalendar().clone();
		calendarCursor.set(Calendar.DAY_OF_WEEK, firstDayOfWeek);

		layout.appendRow(new RowSpec("pref"));
		layout.appendRow(new RowSpec("fill:pref:grow"));

		for(int i=0; i < dayViews.length; i++) {

			dayViews[i] = new DefaultDayView((Calendar)calendarCursor.clone(), getModel().getController(), false, this);
			
			dayViews[i].setType(CalendarUtils.isToday(calendarCursor, getModel().getLocale()) ? DAY_TYPE_TODAY : DAY_TYPE_DEFAULT);
			
			// set initial selection-state
			dayViews[i].setSelected(CalendarUtils.isSameDay(getModel().getController().getCurrentCalendar(), calendarCursor));
			
			add(dayViews[i].getDailyEventScrollPane(), cc.xy((drawTimes ? 2 : 1) + i,  (drawDayNames ? 2 : 1)));
			add(dayViews[i], cc.xy((drawTimes ? 2 : 1) + i, (drawDayNames ? 3 : 2)));
			calendarCursor.add(Calendar.DAY_OF_WEEK, 1);
		}

		// we want a symmetric / balanced layout under all circumstances so we group all columns

		int[] columns = new int[layout.getColumnCount() - (drawTimes ? 1 : 0)];
		for(int i=0; i < columns.length; i++)
			columns[i] = i + (drawTimes ? 2 : 1);

		layout.setColumnGroups(new int[][] { columns });

		add(new DefaultWeekViewTimeBar(), cc.xy(1, (drawDayNames ? 3 : 2)));
	}

	protected void updateDayLabels() {
		String[] weekDays;
		if(useShortWeekDays)
			weekDays = new DateFormatSymbols(getModel().getLocale()).getShortWeekdays();
		else
			weekDays = new DateFormatSymbols(getModel().getLocale()).getWeekdays();

		int firstDayOfWeek = Calendar.getInstance(getModel().getLocale()).getFirstDayOfWeek();

		Calendar tempCalendar = (Calendar)getModel().getCalendar().clone();
		tempCalendar.set(Calendar.DAY_OF_WEEK, firstDayOfWeek);

		for(int i=0; i < dayLabels.length; i++) {

			int index = (firstDayOfWeek+i) % weekDays.length;

			// 0 is not defined
			if(index == 0)
				index = 1;

			dayLabels[i].setText(weekDays[index] + ", " + tempCalendar.get(Calendar.DAY_OF_MONTH) +"."+(tempCalendar.get(Calendar.MONTH) + 1) +".");

			tempCalendar.add(Calendar.DAY_OF_WEEK, 1);

			if(index == Calendar.SUNDAY)
				dayLabels[i].setForeground(COLOR_SUNDAY);
		}
	}

	public CalendarViewModel getModel() {
		return model;
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {

		if(!CalendarUtils.isSameWeek(event.getOldSelectedDay(), event.getSelectedDay())) {
			maxDailyEvents = -1;

			Calendar calendarCursor = (Calendar)getModel().getCalendar().clone();
			calendarCursor.set(Calendar.DAY_OF_WEEK, Calendar.getInstance(getModel().getLocale()).getFirstDayOfWeek());

			for(int i=0; i <7; i++) {
				dayViews[i].getModel().getCalendar().set(Calendar.YEAR, calendarCursor.get(Calendar.YEAR));
				dayViews[i].getModel().getCalendar().set(Calendar.MONTH, calendarCursor.get(Calendar.MONTH));
				dayViews[i].getModel().getCalendar().set(Calendar.DAY_OF_MONTH, calendarCursor.get(Calendar.DAY_OF_MONTH));
				dayViews[i].getModel().getCalendar().set(Calendar.HOUR_OF_DAY, calendarCursor.get(Calendar.HOUR_OF_DAY));
				dayViews[i].getModel().getCalendar().set(Calendar.MINUTE, calendarCursor.get(Calendar.MINUTE));
				dayViews[i].getModel().getCalendar().set(Calendar.SECOND, calendarCursor.get(Calendar.SECOND));
				dayViews[i].getModel().getCalendar().set(Calendar.MILLISECOND, calendarCursor.get(Calendar.MILLISECOND));
				dayViews[i].setType(CalendarUtils.isToday(calendarCursor, getModel().getLocale()) ? DAY_TYPE_TODAY : DAY_TYPE_DEFAULT);
				dayViews[i].calendarSelectionChanged(event);
				calendarCursor.add(Calendar.DAY_OF_WEEK, 1);
			}

			if(drawDayNames)
				updateDayLabels();
		} else {
		
		// if week is the same, only update selection-states
		
			for(int i=0; i < 7; i++)
				dayViews[i].setSelected(CalendarUtils.isSameDay(dayViews[i].getModel().getCalendar(), event.getSelectedDay()));
		}
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsLoaded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsLoaded(CalendarDataEvent event) {
		for(int i=0; i <7; i++)
			dayViews[i].eventsLoaded(event);
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsAdded(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsAdded(CalendarDataEvent event) {
		for(int i=0; i <7; i++)
			dayViews[i].eventsAdded(event);	
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsChanged(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsChanged(CalendarDataEvent event) {
		for(int i=0; i <7; i++)
			dayViews[i].eventsChanged(event);
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarDataListener#eventsDeleted(de.tarent.kaliko.components.listener.impl.CalendarDataEvent)
	 */
	public void eventsDeleted(CalendarDataEvent event) {
		for(int i=0; i <7; i++)
			dayViews[i].eventsDeleted(event);
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.tarent.kaliko.components.views.CalendarView#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu arg0) {
		// TODO Auto-generated method stub
		
	}
}