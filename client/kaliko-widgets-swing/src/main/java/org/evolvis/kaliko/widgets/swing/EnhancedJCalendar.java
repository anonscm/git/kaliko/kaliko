/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

import java.util.Calendar;

import javax.swing.JComboBox;

import com.toedter.calendar.JCalendar;

import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class EnhancedJCalendar extends JCalendar {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5097173445273307884L;
	
	public EnhancedJCalendar(Calendar calendar) {
		super(calendar.getTime());
		((JComboBox)this.monthChooser.getComboBox()).addMouseWheelListener(new ComboBoxMouseWheelNavigator((JComboBox)this.monthChooser.getComboBox()));
		//((JSpinField)this.yearChooser.getSpinner()).addMouseWheelListener(new SpinFieldMouseWheelNavigator((JSpinField)this.yearChooser.getSpinner()));
	}
}
