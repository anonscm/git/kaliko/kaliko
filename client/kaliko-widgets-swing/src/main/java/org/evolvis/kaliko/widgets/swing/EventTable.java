/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.evolvis.kaliko.widgets.common.ContextMenu;

import de.tarent.kaliko.components.listener.EventSelectionListener;
import de.tarent.kaliko.components.listener.impl.EventSelectionEvent;
import de.tarent.kaliko.components.models.CalendarViewModel;
import de.tarent.kaliko.components.utils.CalendarUtils;
import de.tarent.kaliko.components.utils.TableSorter;
import de.tarent.kaliko.components.widgets.EventList;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class EventTable extends JTable implements EventList {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7046119335305252333L;
	protected CalendarViewModel parentModel;
	protected ContextMenu contextMenuSpace;
	protected ContextMenu contextMenuEvent;
	
	public EventTable(CalendarViewModel parentModel) {
		super();
		this.parentModel = parentModel;
		this.setModel(new TableSorter(new EventTableModel()));
		
		TableCellRenderer calendarRenderer = new DefaultTableCellRenderer() {
		
			/**
			 * 
			 */
			private static final long serialVersionUID = -2591254102819670620L;

			/**
			 * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
			 */
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Calendar calendar = (Calendar)value;
				return super.getTableCellRendererComponent(table, CalendarUtils.getDateAsString(calendar) + " " + CalendarUtils.getTimeAsString(calendar), isSelected, hasFocus, row, column);
			}
			
		};
		
		getColumnModel().getColumn(1).setCellRenderer(calendarRenderer);
		getColumnModel().getColumn(2).setCellRenderer(calendarRenderer);
		
		// Uses Java6-API
		//setAutoCreateRowSorter(true);
		
		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if(e.isPopupTrigger()) {
					// if defined, show a context menu

					ContextMenu menu = null;

					// get the index of the list item closest to the mouse-click
					int index = EventTable.this.rowAtPoint(e.getPoint());
					// set index to -1 in order to only get an element if
					// the mouse-click actually was on an event, not just close to it
//					if(getCellBounds(index, index) == null || !getCellBounds(index, index).contains(e.getPoint()))
//						index = -1;

					if(index != -1) {
						getParentModel().getController().getEventSelectionModel().setSelectedEvent((Event)((EventTableModel)getModel()).getEventAt(index));
						menu = contextMenuEvent;
					}
					else
						menu = contextMenuSpace;

					if(menu != null)
						menu.showContextMenuAt(e.getX(), e.getY());
				}
			}
		});
		
		// listen on changed event-selection-states
		getParentModel().getController().getEventSelectionModel().addEventSelectionListener(new EventSelectionListener() {

			/**
			 * @see de.tarent.kaliko.components.listener.EventSelectionListener#eventSelectionChanged(de.tarent.kaliko.components.listener.impl.EventSelectionEvent)
			 */
			public void eventSelectionChanged(EventSelectionEvent event) {
				for(int i=0; i < getModel().getRowCount(); i++) {
					Event currentEvent = (Event)((EventTableModel)getModel()).getEventAt(i);
					if(event.getSelectedEvents().contains(currentEvent))
						getSelectionModel().addSelectionInterval(i, i);
					else
						getSelectionModel().removeSelectionInterval(i, i);
				}

			}

		});
	}
	
	protected CalendarViewModel getParentModel() {
		return parentModel;
	}
	
	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#addEvent(de.tarent.kaliko.objects.Event)
	 */
	public void addEvent(Event event) {
		((EventTableModel)getModel()).addEvent(event);
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#clear()
	 */
	public void clear() {
		((EventTableModel)getModel()).clear();
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#removeEvent(de.tarent.kaliko.objects.Event)
	 */
	public void removeEvent(Event event) {
		((EventTableModel)getModel()).removeEvent(event);
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#setEvents(java.util.List)
	 */
	public void setEvents(List<Event> events) {
		((EventTableModel)getModel()).setEvents(events);
	}
	
	protected class EventTableModel extends AbstractTableModel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -836197101597150929L;
		protected String[] columnNames = { "Title", "Begin", "End", "Categories" };
		protected List<Event> events;
		
		public EventTableModel() {
			events = new ArrayList<Event>();
		}
		
		public Event getEventAt(int index) {
			return events.get(index);
		}

		public void addEvent(Event event) {
			events.add(event);
			Collections.sort(events);
			int rowIndex = events.indexOf(event);
			fireTableRowsInserted(rowIndex, rowIndex);
		}
		
		public void clear() {
			events.clear();
			fireTableDataChanged();
		}
		
		public void removeEvent(Event event) {
			int index = events.indexOf(event);
			events.remove(index);
			fireTableRowsDeleted(index, index);
		}
		
		public void setEvents(List<Event> events) {
			Collections.sort(events);
			this.events = events;
			fireTableDataChanged();
		}
		
		/**
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		public int getColumnCount() {
			return columnNames.length;
		}

		/**
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		public int getRowCount() {
			return events.size();
		}

		/**
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		public Object getValueAt(int rowIndex, int columnIndex) {
			if(rowIndex+1 > getRowCount())
				return null;
			if(columnIndex+1 > columnNames.length)
				return null;
			
			if(columnIndex == 0)
				return events.get(rowIndex).getTitle();
			else if(columnIndex == 1)
				return events.get(rowIndex).getBegin();
			else if(columnIndex == 2)
				return events.get(rowIndex).getEnd();
			else if(columnIndex == 3)
				return events.get(rowIndex).getCategoriesList();
			
			// does not happen
			return null;
		}

		/**
		 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
		 */
		@Override
		public String getColumnName(int columnIndex) {
			return columnNames[columnIndex];
		}
		
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		return contextMenuEvent;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		return contextMenuSpace;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		this.contextMenuEvent = contextMenuEvent;
	}

	/**
	 * @see de.tarent.kaliko.components.widgets.EventList#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		this.contextMenuSpace = contextMenuSpace;
	}
}
