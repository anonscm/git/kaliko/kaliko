/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.dialogs.SwingEventDialog;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.controller.ViewSwitcher;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;
import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.components.views.CalendarViewPanel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ViewSwitchPanel extends JPanel implements ViewSwitcher, CalendarSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2143730796649251298L;
	protected JPanel buttonPanel;
	protected CalendarNavigationPanel calendarNavigationPanel;
	protected ViewsPanel viewsPanel;

	protected JButton jumpToTodayButton;

	protected JToggleButton dayViewButton;
	protected JToggleButton weekViewButton;
	protected JToggleButton monthViewButton;

	protected JButton previousButton;
	protected JButton nextButton;
	
	protected CalendarView currentView;

	protected ActionListener actionListener;

	protected SwingEventDialog newEventDialog;
	
	protected CalendarApplicationController controller;

	public ViewSwitchPanel(CalendarApplicationController controller) {
		super();
		
		this.controller = controller;

		setLayout(new FormLayout("fill:pref:grow", // columns
		"pref, 5dlu, pref, 5dlu fill:pref:grow")); // rows

		CellConstraints cc = new CellConstraints();

		add(getButtonPanel(), cc.xy(1, 1));
		add(getCalendarNavigationPanel(), cc.xy(1, 3));
		add(getViewsPanel(), cc.xy(1, 5));
		
		getController().addCalendarSelectionListener(this);

		getMonthViewButton().doClick();
	}

	protected JPanel getButtonPanel() {
		if(buttonPanel == null) {

			FormLayout layout = new FormLayout("0dlu:grow, pref, 5dlu, fill:2dlu, 5dlu, pref, 5dlu, pref, 5dlu, pref, 0dlu:grow", // columns
			"pref"); // rows

			PanelBuilder builder = new PanelBuilder(layout);

			CellConstraints cc = new CellConstraints();

			builder.add(getGotoTodayButton(), cc.xy(2, 1));
			builder.add(new JSeparator(JSeparator.VERTICAL), cc.xy(4, 1));

			builder.add(getDayViewButton(), cc.xy(6, 1));
			builder.add(getWeekViewButton(), cc.xy(8, 1));
			builder.add(getMonthViewButton(), cc.xy(10, 1));

			ButtonGroup buttonGroup = new ButtonGroup();
			buttonGroup.add(getDayViewButton());
			buttonGroup.add(getWeekViewButton());
			buttonGroup.add(getMonthViewButton());

			buttonPanel = builder.getPanel();
		}
		return buttonPanel;
	}

	protected CalendarNavigationPanel getCalendarNavigationPanel() {
		if(calendarNavigationPanel == null) {
			calendarNavigationPanel = new CalendarNavigationPanel(getController());
		}
		return calendarNavigationPanel;
	}

	protected JButton getGotoTodayButton() {
		if(jumpToTodayButton == null) {
			jumpToTodayButton = new JButton("Jump To Today");
			jumpToTodayButton.addActionListener(getActionListener());
			jumpToTodayButton.setEnabled(!isToday());
		}
		return jumpToTodayButton;
	}

	protected JToggleButton getDayViewButton() {
		if(dayViewButton == null) {
			dayViewButton = new JToggleButton("Day View");
			dayViewButton.addActionListener(getActionListener());
		}
		return dayViewButton;
	}

	protected JToggleButton getWeekViewButton() {
		if(weekViewButton == null) {
			weekViewButton = new JToggleButton("Week View");			
			weekViewButton.addActionListener(getActionListener());
		}
		return weekViewButton;
	}

	protected JToggleButton getMonthViewButton() {
		if(monthViewButton == null) {
			monthViewButton = new JToggleButton("Month View");	
			monthViewButton.addActionListener(getActionListener());
		}
		return monthViewButton;
	}

	protected ViewsPanel getViewsPanel() {
		if(viewsPanel == null)
			viewsPanel = new ViewsPanel(getController());
		
		return viewsPanel;
	}
	
	protected CalendarApplicationController getController() {
		return controller;
	}
	
	protected void switchView(CalendarViewPanel view) {
		// do not do anything when view did not change
		if(currentView == view)
			return;
		
		currentView = view;
		
		getViewsPanel().switchView(view);

		getCalendarNavigationPanel().setCurrentDisplayedScopeText(currentView.getModel().getCurrentScopeName());
	}

	public void gotoPrevious() {
		Calendar oldCalendar = (Calendar)getController().getCurrentCalendar().clone();
		currentView.getModel().gotoPrevious(getController().getCurrentCalendar());
		getCalendarNavigationPanel().setCurrentDisplayedScopeText(currentView.getModel().getCurrentScopeName());
		getController().fireCalendarSelectionChanged(oldCalendar);
	}

	public void gotoNext() {
		Calendar oldCalendar = (Calendar)getController().getCurrentCalendar().clone();
		currentView.getModel().gotoNext(getController().getCurrentCalendar());
		getCalendarNavigationPanel().setCurrentDisplayedScopeText(currentView.getModel().getCurrentScopeName());
		getController().fireCalendarSelectionChanged(oldCalendar);
	}

	/**
	 * Whether the current calendar equals today
	 * @return
	 */
	public boolean isToday() {
		Calendar today = Calendar.getInstance(getController().getLocale());
		
		return (getController().getCurrentCalendar().get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
				getController().getCurrentCalendar().get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
				getController().getCurrentCalendar().get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH));
	}
	
	public void gotoToday() {
		
		// if current calendar equals today, do nothing
		if(isToday())
			return;
		
		getController().setCalendarCursorTo(Calendar.getInstance(getController().getLocale()));	
		getCalendarNavigationPanel().setCurrentDisplayedScopeText(currentView.getModel().getCurrentScopeName());
	}

	protected ActionListener getActionListener() {
		if(actionListener == null) {
			actionListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(e.getSource().equals(ViewSwitchPanel.this.getDayViewButton())) 
						ViewSwitchPanel.this.switchView(ViewSwitchPanel.this.getViewsPanel().getDayView());
					else if(e.getSource().equals(ViewSwitchPanel.this.getWeekViewButton()))
						ViewSwitchPanel.this.switchView(ViewSwitchPanel.this.getViewsPanel().getWeekView());
					else if(e.getSource().equals(ViewSwitchPanel.this.getMonthViewButton()))
						ViewSwitchPanel.this.switchView(ViewSwitchPanel.this.getViewsPanel().getMonthView());
					else if(e.getSource().equals(getGotoTodayButton()))
						ViewSwitchPanel.this.gotoToday();
				}

			};
		}
		return actionListener;
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setCurrentView(int)
	 */
	public void setCurrentView(int view) {
		if(view == CalendarView.DAY_VIEW)
			getDayViewButton().doClick();
		else if(view == CalendarView.WEEK_VIEW)
			getWeekViewButton().doClick();
		else if(view == CalendarView.MONTH_VIEW)
			getMonthViewButton().doClick();
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		// check if we need to dis-/enable today-button
		getGotoTodayButton().setEnabled(!isToday());
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getCurrentView()
	 */
	public CalendarView getCurrentView() {
		return getViewsPanel().getCurrentView();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getContextMenuForEvent()
	 */
	public ContextMenu getContextMenuForEvent() {
		return getViewsPanel().getContextMenuForEvent();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#getContextMenuForSpace()
	 */
	public ContextMenu getContextMenuForSpace() {
		return getViewsPanel().getContextMenuForSpace();
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setContextMenuForEvent(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		getViewsPanel().setContextMenuForEvent(contextMenuEvent);
	}

	/**
	 * @see de.tarent.kaliko.components.controller.ViewSwitcher#setContextMenuForSpace(org.evolvis.kaliko.widgets.common.ContextMenu)
	 */
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		getViewsPanel().setContextMenuForSpace(contextMenuSpace);
	}
}
