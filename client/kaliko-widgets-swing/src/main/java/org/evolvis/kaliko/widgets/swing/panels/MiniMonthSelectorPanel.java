/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import org.evolvis.kaliko.widgets.swing.EnhancedJCalendar;

import com.toedter.calendar.JCalendar;

import de.tarent.kaliko.components.controller.CalendarApplicationController;
import de.tarent.kaliko.components.listener.CalendarSelectionListener;
import de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class MiniMonthSelectorPanel extends JPanel implements CalendarSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1543007818951990796L;
	protected JCalendar miniMonthSelector;
	protected CalendarApplicationController controller;
	protected PropertyChangeListener listener;
	protected boolean external = false;

	public MiniMonthSelectorPanel(CalendarApplicationController controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		add(getMiniMonthSelector(), BorderLayout.CENTER);
		getMiniMonthSelector().addPropertyChangeListener(getPropertyChangeListener());
	}

	protected JCalendar getMiniMonthSelector() {
		if(miniMonthSelector == null)
			miniMonthSelector = new EnhancedJCalendar(controller.getCurrentCalendar());

		return miniMonthSelector;
	}

	protected PropertyChangeListener getPropertyChangeListener() {
		if(listener == null) {
			listener = new PropertyChangeListener() {

				/**
				 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
				 */
				public void propertyChange(PropertyChangeEvent evt) {
					// if the property-change was triggered externally, do not fire an event
					if(!external) {
						Map<Integer, Integer> fieldsAndValues = new HashMap<Integer, Integer>();
						fieldsAndValues.put(Calendar.YEAR, getMiniMonthSelector().getCalendar().get(Calendar.YEAR));
						fieldsAndValues.put(Calendar.MONTH, getMiniMonthSelector().getCalendar().get(Calendar.MONTH));
						fieldsAndValues.put(Calendar.DAY_OF_MONTH, getMiniMonthSelector().getCalendar().get(Calendar.DAY_OF_MONTH));
						controller.setCalendarCursor(fieldsAndValues);
					}
					external = false;
				}

			};
		}
		return listener;
	}

	/**
	 * @see de.tarent.kaliko.components.listener.CalendarSelectionListener#calendarSelectionChanged(de.tarent.kaliko.components.listener.impl.CalendarSelectionEvent)
	 */
	public void calendarSelectionChanged(CalendarSelectionEvent event) {
		external = true;
		getMiniMonthSelector().setCalendar(controller.getCurrentCalendar());
	}
}
