/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

/**
 * This is a widget for kaliko to choose the time for new events
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 * 
 */

import java.awt.Component;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;
import de.tarent.kaliko.components.data.Time;
import de.tarent.kaliko.components.utils.CalendarUtils;

public class TimeChooser extends JComboBox {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6482621575992275487L;

	TreeMap<Integer, String> timeTreemap = new TreeMap<Integer, String>();

	private int error = 0;
	
	public TimeChooser() {
		this(0, 0);
	}

	public TimeChooser(int hours, int minutes) {

		super(new DefaultComboBoxModel());

		setEditor(new TimeComboBoxEditor());

		setEditable(true);

		addMouseWheelListener(new ComboBoxMouseWheelNavigator(this));

		for (int i = 0; i < 96; i++) {
			timeTreemap.put(i, CalendarUtils.getTimeAsString((i * 15) / 60,
					(i * 15) % 60));
			((DefaultComboBoxModel) getModel()).addElement(timeTreemap.get(i));
		}
	}

	public Time getTime() {
		return (Time) getSelectedItem();
	}

	/**
	 * The TimeComboBoxEditor use the methods getEditorComponent() and getItem()
	 * to search user input data in the DefaultComoBoxModel. Also decreed this
	 * class an autocomplete method. 
	 * 
	 * @author Viktor Hamm tarent GmbH Bonn
	 * 
	 */
	protected class TimeComboBoxEditor extends BasicComboBoxEditor {

		private String search;
		private String search2;
		
		/**
		 * @see javax.swing.plaf.basic.BasicComboBoxEditor#getEditorComponent()
		 */
		public Component getEditorComponent() {
			
			editor.setDocument(new TimeVerifyDocument());
			editor.selectAll();

			return editor;
		}

		/**
		 * @see javax.swing.plaf.basic.BasicComboBoxEditor#getItem()
		 */
		public Object getItem() {

			search = editor.getText();

			for (int i = 0; i < getModel().getSize(); i++) {
				try {
					
					/**
					 * Autocomplete: 12 -> 12:00 , 8 -> 08:00
					 */
					if (getModel().getElementAt(i).toString().startsWith(
							"0" + search)
							|| getModel().getElementAt(i).toString()
									.startsWith(search)) {

						editor.setText(getModel().getElementAt(i).toString());
						editor.selectAll();

						return search;

					} 
					/**
					 * Autocomplete: If a subvalue are in the DefaultComoBoxModel
					 * e.g: 123 -> 12:30
					 */
					else if (getModel().getElementAt(i).toString()
							.startsWith(
									search.substring(0, 2) + ":"
											+ search.substring(2, 3))) {

						editor.setText(getModel().getElementAt(i).toString());
						editor.selectAll();

						return search;
						
					} 
					/**
					 * Autocomplete: If a subvalue are not in the DefaultComboBoxModel,
					 * then append an 0 behind the last Digit. e.g 125 -> 12:50 
					 */
					else if (editor.getText().length() == 3 ) {

						search2 = search.substring(0, 2) + ":"
								+ search.substring(2, 3) + "0";
						
						if (Integer.parseInt(search.substring(0, 2)) > 23
								|| Integer.parseInt(search.substring(2, 3)) > 6)
							error = 1;
							
						editor.setText(search2);
						return search;
					}

				} catch (Exception e) {
				}
			}
			
			error = 0;
			editor.selectAll();
			return search;

		}

		/**
		 * 
		 * @author Viktor Hamm tarent GmbH Bonn
		 *
		 * This document validates the user input data with overwritting
		 * the insertString(offs, str, a) mehtod.
		 * 
		 * Allowed inputs are numbers between 0 and 9 and doublepoints
		 * in Format ##:##
		 * 
		 * errors would be red marked
		 * 
		 */
		protected class TimeVerifyDocument extends PlainDocument {
			/**
			 * 
			 */
			private static final long serialVersionUID = 7855614923899168243L;

			private static final String isValid = ":0123456789";

			/**
			 * @see javax.swing.text.PlainDocument#insertString(int,
			 *      java.lang.String, javax.swing.text.AttributeSet)
			 */
			@Override
			public void insertString(int offs, String str, AttributeSet a)
					throws BadLocationException {
				
				String result = null;

				for (int i = 0; i < str.length(); i++) {

					/**
					 * when str == null, then do nothing and give 
					 * the input out.
					 */
					if (str == null) {
						
						super.insertString(offs, str, null);
						return;

					}
					
					/**
					 * Check the inputs & length of valid data. If is not so
					 * then return an error.
					 */
					else if (isValid.indexOf(str.charAt(i)) == -1
							|| editor.getText().length() > 4) {

						error = 1;
						editor.selectAll();
					} 
					
					/**
					 * When no doublepoint are in the inputs then insert one.
					 */ 
					else if (editor.getText().indexOf(":", 1) == -1) {

							Pattern regex = Pattern.compile("[0-9]{2}");
							Matcher match = regex.matcher(editor.getText()
											.toString());
							
							if (match.find() != false){

								result = match.group();
								
								try {
									
									/**
									 * check the size of the first and second numbers.
									 * If it over 23, then return an error. 
									 */
									if (Integer.parseInt(result) > 23)
										error = 1;
										
									String subvalue1 = result + ":";
									String subvalue2 = editor.getText().toString()
													   .charAt(2) + str;
									
									/**
									 * check the size of the two last numbers
									 * If it over 60, then return an error.
									 */
									if (Integer.parseInt(subvalue2) > 60)
										error = 1;
									
									String resultString = subvalue1 + subvalue2;
									
									editor.setText(resultString);
									editor.selectAll();
									str = "";
									
								} catch (Exception e) {
								}
							}
					}
					
					else {
						try {
							/**
							 * check the size of the two last numbers, when
							 * the user types a doublepoint. If it over 60
							 * then return an error.
							 */ 
							if (Integer.parseInt(editor.getText().toString()
									   .charAt(3) + str) > 60) {
								error = 1;
								editor.selectAll();
							}
						}catch(Exception e) {
						}			
					}
				}

				switch (error) {
				case 0:
					editor.setBackground(java.awt.Color.WHITE);
					super.insertString(offs, str, null);
					break;
				case 1:
					editor.setBackground(java.awt.Color.RED);
					super.insertString(offs, str, null);
					break;
				}
			}
		}
	}
}
