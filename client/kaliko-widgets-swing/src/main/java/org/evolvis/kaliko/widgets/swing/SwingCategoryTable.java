/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.evolvis.kaliko.widgets.common.CategoryTable;

import de.tarent.kaliko.components.utils.Messages;
import de.tarent.kaliko.components.utils.TableSorter;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.impl.CategoryImpl;

/**
 * 
 * A JTable specialized for displaying categories and allowing a selection
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SwingCategoryTable extends JTable implements CategoryTable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2298233258004131779L;
	
	protected List<CategorySelectionListener> categorySelectionListeners;

	public SwingCategoryTable() {
		setModel(new TableSorter(new CategoryTableModel()));
		setOpaque(true);
		setBackground(Color.WHITE);

		// Uses Java6-API
//		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(getModel());
//		Comparator<Category> comparator = new Comparator<Category>() {
//		    public int compare(Category cat1, Category cat2) {
//		    	return cat1.getName().compareTo(cat2.getName());
//		    }
//		};
//		sorter.setComparator(0, comparator);
//		setRowSorter(sorter);
//		((TableSorter)getModel()).setColumnComparator(Category.class, new Comparator<Category> () {
//			public int compare(Category cat1, Category cat2) {
//		    	return cat1.getName().compareTo(cat2.getName());
//		    }
//		});
//		((TableSorter)getModel()).setSortingStatus(1, TableSorter.ASCENDING);
		
		
		getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 3531531909121895282L;

			@Override
			public Component getTableCellRendererComponent(JTable arg0, Object value, boolean isSelected, boolean arg3, int arg4, int arg5) {
				Category category = ((Category)value);
				JLabel comp = (JLabel)super.getTableCellRendererComponent(arg0, category.getName(), isSelected, false, arg4, arg5);
				//comp.setBackground(isSelected ? CategorySelectionPanel.this.getColorForCategory(category).darker() : CategorySelectionPanel.this.getColorForCategory(category));
				comp.setToolTipText(category.getName());
				return comp;
			}

		});
		// TODO set column 0 not editable
		getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JCheckBox()) {

			/**
			 * 
			 */
			private static final long serialVersionUID = -7116990150079408530L;

			@Override
			public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
				JCheckBox comp = (JCheckBox)super.getTableCellEditorComponent(table, value, isSelected, row, column);
				comp.setHorizontalAlignment(SwingConstants.RIGHT);
				comp.setOpaque(true);
				//Category category = (Category) table.getModel().getValueAt(row, 0);
				//comp.setBackground(CategorySelectionPanel.this.getColorForCategory(category));
				comp.setBackground(UIManager.getLookAndFeelDefaults().getColor("Table.selectionBackground"));
				return comp;
			}

		});	
		CheckBoxCellRenderer renderer = new CheckBoxCellRenderer();
		getColumnModel().getColumn(1).setCellRenderer(renderer);
		setShowGrid(false);
		setCellSelectionEnabled(false);
		setRowSelectionAllowed(true);
		getColumnModel().getColumn(1).setMinWidth((int)renderer.getPreferredSize().getWidth());
		getColumnModel().getColumn(1).setMaxWidth((int)renderer.getPreferredSize().getWidth());
		getColumnModel().getColumn(1).setResizable(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		setIntercellSpacing(new Dimension(0, (int)getIntercellSpacing().getHeight()));
		setEnabled(false);
		getTableHeader().setReorderingAllowed(false);
		addMouseListener(new MouseAdapter() {
			
			/**
			 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				// select categories by double-click
				if(e.getClickCount() == 2) {
					int rowAtPoint = rowAtPoint(e.getPoint());
					if(rowAtPoint < 0)
						return;
					
					getModel().setValueAt(!((Boolean)getModel().getValueAt(rowAtPoint, 1)), rowAtPoint, 1);
				}
			}

			/**
			 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				maybeShowPopup(e);
			}

			/**
			 * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
			 */
			@Override
			public void mouseReleased(MouseEvent e) {
				maybeShowPopup(e);
			}
			
			protected void maybeShowPopup(MouseEvent e) {
				// if the user did a mouse-click which should be interpreted as a popup-call (system-dependend), show a popup-menu
//		    	if(allCategoriesContextMenu != null && getAllCategoriesList().isEnabled() && e.isPopupTrigger())
//					allCategoriesContextMenu.show(getAllCategoriesList(), e.getX(), e.getY());
			}
			
		});
		((TableSorter)getModel()).getTableModel().addTableModelListener(new TableModelListener() {

			/**
			 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
			 */
			public void tableChanged(TableModelEvent e) {
				if(e.getType() == TableModelEvent.UPDATE && e.getColumn() == 1)
					SwingCategoryTable.this.fireCategorySelectionChanged();
			}
			
		});
	}
	
	public void setAvailableCategories(Collection<Category> categories) {
		((CategoryTableModel)((TableSorter)getModel()).getTableModel()).setCategories(new ArrayList<Category>(categories));
	}
	
	public void setSelectedCategories(Collection<Category> selectedCategories) {
		List<Category> availableCategories = getAvailableCategories();

		for(int i=0; i < availableCategories.size(); i++)
			((CategoryTableModel)((TableSorter)getModel()).getTableModel()).setSelected(i, selectedCategories.contains(availableCategories.get(i)));
	}
	
	public void setSelectedCategories(String selectedCategories) {
		List<Category> availableCategories = getAvailableCategories();

		for(int i=0; i < availableCategories.size(); i++)
			((CategoryTableModel)((TableSorter)getModel()).getTableModel()).setSelected(i, selectedCategories.contains(availableCategories.get(i).getGuid()));
	}
	
	public void setDefaultCategories(String preselectedCategories) {
		List<Category> availableCategories = getAvailableCategories();
		
		for(int i=0; i < availableCategories.size(); i++)
			if(preselectedCategories.contains(availableCategories.get(i).getGuid()))
				getSelectionModel().addSelectionInterval(i, i);
	}
	
	public void setDefaultCategories(Collection<Category> activatedCategories) {
		Iterator<Category> categoriesIt = activatedCategories.iterator();
		StringBuffer categoriesAsString = new StringBuffer(";");

		while(categoriesIt.hasNext())
			categoriesAsString.append(categoriesIt.next().getGuid()+";");

		setSelectedCategories(categoriesAsString.toString());
	}
	
	public List<Category> getSelectedCategories() {
		return ((CategoryTableModel)((TableSorter)getModel()).getTableModel()).getActivatedCategories(); 
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#getDefaultCategories()
	 */
	public Collection<Category> getDefaultCategories() {
		// TODO Auto-generated method stub
		return new ArrayList<Category>();
	}

	public List<Category> getAvailableCategories() {
		return ((CategoryTableModel)((TableSorter)getModel()).getTableModel()).getCategories();
	}
	
	public void addCategory(Category category) {
		((CategoryTableModel)getModel()).addCategory(category);
	}
	
	public void removeCategory(Category category) {
		((CategoryTableModel)getModel()).removeCategory(category);
	}
	
	public void fireCategorySelectionChanged() {
		Iterator<CategorySelectionListener> it = getCategorySelectionListeners().iterator();
	
		while(it.hasNext())
			it.next().categorySelectionChanged();
	}
	
	public void addCategorySelectionListener(CategorySelectionListener listener) {
		getCategorySelectionListeners().add(listener);
	}
	
	public void removeCategorySelectionListener(CategorySelectionListener listener) {
		getCategorySelectionListeners().remove(listener);
	}
	
	protected List<CategorySelectionListener> getCategorySelectionListeners() {
		if(categorySelectionListeners == null)
			categorySelectionListeners = new ArrayList<CategorySelectionListener>();
		return categorySelectionListeners;
	}
	
	protected class CategoryTableModel extends AbstractTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4572111391606645903L;

		protected List<Category> categories;
		protected List<Boolean> selection;
		protected boolean initialized;

		public CategoryTableModel() {
			initialized = false;
			categories = new ArrayList<Category>();
			selection = new ArrayList<Boolean>();
		}

		public List<Category> getCategories() {
			return this.categories;
		}

		public void setCategories(List<Category> categories) {
			Collections.sort(categories);
			this.categories = categories;
			this.selection = new ArrayList<Boolean>(categories.size());
			for(int i=0; i < categories.size(); i++)
				this.selection.add(i, Boolean.FALSE);

			initialized = true;
			fireTableDataChanged();
		}

		public void setCategoriesAndSelection(List<Category> categories, List<Boolean> selection) {
			if(categories == null || selection == null || selection.size() != categories.size())
				throw new RuntimeException("category- and selection-list have no proper state");

			this.categories = categories;
			this.selection = selection;

			initialized = true;
			fireTableDataChanged();
		}

		public List<Category> getActivatedCategories() {
			List<Category> selectedCategories = new ArrayList<Category>();

			for(int i=0; i < categories.size(); i++) {
				if(selection.get(i))
					selectedCategories.add(categories.get(i));
			}

			return selectedCategories;
		}

		public Class<?> getColumnClass(int columnIndex) {
			if(columnIndex == 0)
				return String.class;
			else if(columnIndex == 1)
				return Boolean.class;
			return null;
		}

		public int getColumnCount() {
			return 2;
		}

		public String getColumnName(int columnIndex) {
			if(columnIndex == 0)
				return Messages.getString("CategorySelectionPanel_CategoryTable_Column_Name");
			else if(columnIndex == 1)
				return "";
			return null;
		}

		public int getRowCount() {
			if(!initialized)
				return 1;

			return categories.size();
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			if(!initialized) {
				if(columnIndex == 0)
					return new CategoryImpl(Messages.getString("CategorySelectionPanel_CategoryTable_DummyEntry_Loading"), null, null);
				else if(columnIndex == 1)
					return Boolean.FALSE;

				return null;
			}

			if(columnIndex == 0)
				return categories.get(rowIndex);
			else if(columnIndex == 1)
				return selection.get(rowIndex);
			return null;
		}

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			if(!initialized)
				return false;

			return columnIndex == 1;
		}

		public void setValueAt(Object value, int rowIndex, int columnIndex) {
			if(!initialized)
				return;

			if(columnIndex == 0)
				categories.set(rowIndex, (Category)value);
			else if(columnIndex == 1)
				selection.set(rowIndex, (Boolean)value);

			fireTableCellUpdated(rowIndex, columnIndex);
		}		

		public void setSelected(int index, boolean selected) {
			if(!initialized)
				return;

			setValueAt(Boolean.valueOf(selected), index, 1);
		}
		
		public void addCategory(Category category) {
			getCategories().add(category);
			int index = getCategories().size();
			fireTableRowsInserted(index, index);
		}
		
		public void removeCategory(Category category) {
			int index = getCategories().indexOf(category);
			getCategories().remove(category);
			fireTableRowsDeleted(index, index);
		}
	}
	
	protected class CheckBoxCellRenderer extends JCheckBox implements TableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7921133983172470413L;

		public CheckBoxCellRenderer() {
			super();
			this.setOpaque(true);
			this.setBackground(Color.WHITE);
			this.setHorizontalAlignment(SwingConstants.RIGHT);
		}

		/**
		 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean cellHasFocus, int column, int row) {
			this.setSelected((Boolean)value);
			//Category category = (Category) table.getModel().getValueAt(row, 0);
			//this.setBackground(isSelected ? CategorySelectionPanel.this.getColorForCategory(category).darker() : CategorySelectionPanel.this.getColorForCategory(category));
			setBackground(isSelected ? UIManager.getLookAndFeelDefaults().getColor("Table.selectionBackground"): Color.WHITE);
			return this;
		}
	}
	
	public interface CategorySelectionListener {
		public void categorySelectionChanged();
	}

	/* (non-Javadoc)
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#setCategoryActivationAction(javax.swing.Action)
	 */
	public void setCategorySelectionAction(Action action) {
		// TODO Auto-generated method stub
		
	}
}
