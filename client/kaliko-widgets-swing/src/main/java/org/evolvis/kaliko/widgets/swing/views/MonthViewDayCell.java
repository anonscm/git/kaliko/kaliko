/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.views;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import org.evolvis.kaliko.widgets.common.ContextMenu;
import org.evolvis.kaliko.widgets.swing.SimpleEventList;

import de.tarent.kaliko.components.views.CalendarView;
import de.tarent.kaliko.components.views.MonthView;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class MonthViewDayCell extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1179056596119725684L;
	protected JScrollPane scrollPane;
	protected SimpleEventList eventList;
	protected JLabel titleLabel;

	protected FocusListener focusListener;

	protected Calendar calendar;
	protected String dayName = "";
	protected int type;

	protected final MonthView parent;

	protected boolean isSelected;

	public MonthViewDayCell(final MonthView parent) {
		super();
		this.parent = parent;
		setLayout(new BorderLayout());
		add(getTitleLabel(), BorderLayout.NORTH);
		add(getScrollPane(), BorderLayout.CENTER);
		setBorder(BorderFactory.createLineBorder(parent.getModel().getController().getColorModel().getColorFor("day_line.default")));

		getEventList().addMouseListener(new MouseAdapter() {

			/**
			 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				MonthViewDayCell.this.getSurroundingView().getModel().getController().setCalendarCursorTo(getCalendar());
			}

			/**
			 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				//MonthViewDayCell.this.getSurroundingView().getModel().getController().setCalendarCursorTo(getCalendar());

				// get the index of the list item closest to the mouse-click
				int index = getEventList().locationToIndex(e.getPoint());
				// set index to -1 in order to only get an element if
				// the mouse-click actually was on an event, not just close to it
				if(getEventList().getCellBounds(index, index) == null || !getEventList().getCellBounds(index, index).contains(e.getPoint()))
					index = -1;

				if(index != -1)
					MonthViewDayCell.this.getSurroundingView().getModel().getController().getEventSelectionModel().requestSelection((Event)getEventList().getModel().getElementAt(index), e.getModifiersEx() == MouseEvent.CTRL_DOWN_MASK);
				else
					MonthViewDayCell.this.getSurroundingView().getModel().getController().getEventSelectionModel().clearSelection();

				// create or edit events by double-click
				if(e.getClickCount() == 2) {
					if(index != -1)
						MonthViewDayCell.this.getSurroundingView().getModel().getController().requestEditEvent((Event)getEventList().getModel().getElementAt(index));
					else
						MonthViewDayCell.this.getSurroundingView().getModel().getController().requestNewEvent();
				}
			}

		});
	}

	protected MonthView getSurroundingView() {
		return parent;
	}

	protected JScrollPane getScrollPane() {
		if(scrollPane == null) {
			scrollPane = new JScrollPane(getEventList());
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setBorder(BorderFactory.createEmptyBorder());
		}
		return scrollPane;
	}

	protected SimpleEventList getEventList() {
		if(eventList == null) 
			eventList = new SimpleEventList(parent.getModel());
		
		return eventList;
	}

	protected JLabel getTitleLabel() {
		if(titleLabel == null) {
			titleLabel = new JLabel();
			titleLabel.setFont(titleLabel.getFont().deriveFont(Font.PLAIN));
			titleLabel.setOpaque(true);
			titleLabel.setBackground(UIManager.getLookAndFeelDefaults().getColor("MenuBar.highlight"));
			titleLabel.setFocusable(true);
			titleLabel.addFocusListener(getFocusListener());
		}
		return titleLabel;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
		setType(type);
	}

	protected FocusListener getFocusListener() {
		if(focusListener == null) {
			focusListener = new FocusListener() {

				public void focusGained(FocusEvent e) {
					setSelected(true);
				}

				public void focusLost(FocusEvent e) {
					setSelected(false);
				}			
			};
		}
		return focusListener;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
		repaintTitleLabel();
	}

	public void setDayName(String dayName) {
		this.dayName = dayName;
		repaintTitleLabel();
	}

	protected void repaintTitleLabel() {
		SwingUtilities.invokeLater(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				getTitleLabel().setText("<html><b>" + (getCalendar().get(Calendar.DAY_OF_MONTH)) + "</b> <font size=\"-2\">" + MonthViewDayCell.this.dayName+"</font>");
			}
			
		});
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void setType(int type) {
		this.type = type;

		if(type == CalendarView.DAY_TYPE_OUT_OF_SCOPE) {
			getTitleLabel().setFont(getTitleLabel().getFont().deriveFont(Font.ITALIC));
			setBorder(BorderFactory.createLineBorder(parent.getModel().getController().getColorModel().getColorFor("day_line.out_of_scope")));
			getEventList().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.out_of_scope"));
			getTitleLabel().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.out_of_scope"));
		}
		else if(type == CalendarView.DAY_TYPE_DEFAULT) {
			getTitleLabel().setFont(getTitleLabel().getFont().deriveFont(Font.PLAIN));
			setBorder(BorderFactory.createLineBorder(parent.getModel().getController().getColorModel().getColorFor("day_line.default")));
			getEventList().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.default"));
			getTitleLabel().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.default"));
		}
		else if(type == CalendarView.DAY_TYPE_TODAY) {
			getTitleLabel().setFont(getTitleLabel().getFont().deriveFont(Font.PLAIN));
			Border border = new LineBorder(parent.getModel().getController().getColorModel().getColorFor("day_line.today"), 2);
			setBorder(border);
			getEventList().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.today"));
			getTitleLabel().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.today"));
		}

		if(isSelected) {
			getEventList().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.selection"));
			getTitleLabel().setBackground(parent.getModel().getController().getColorModel().getColorFor("day.selection"));
		}
	}

	public void setRedLetterDay(boolean isRedLetterDay) {
		if(isRedLetterDay)
			getTitleLabel().setForeground(parent.getModel().getController().getColorModel().getColorFor("day_label.red_letter_day"));
		else
			getTitleLabel().setForeground(parent.getModel().getController().getColorModel().getColorFor("day_label.default"));
	}

	public Event getEventAtIndex(int index) {
		return (Event) getEventList().getModel().getElementAt(index);
	}

	public void addEvent(Event event) {
		getEventList().addEvent(event);
	}

	public void removeEvent(Event event) {
		parent.getModel().getController().getEventSelectionModel().removeSelectedEvent(event);
		getEventList().removeEvent(event);
	}

	public void setEvents(List<Event> events) {
		getEventList().setEvents(events);
	}

	public void clear() {
		for(int i=0; i < getEventList().getModel().getSize(); i++)
			parent.getModel().getController().getEventSelectionModel().removeSelectedEvent((Event)getEventList().getModel().getElementAt(i));

		getEventList().clear();
	}
	
	public void setContextMenuForEvent(ContextMenu contextMenuEvent) {
		getEventList().setContextMenuForEvent(contextMenuEvent);
	}
	
	public void setContextMenuForSpace(ContextMenu contextMenuSpace) {
		getEventList().setContextMenuForSpace(contextMenuSpace);
	}
}