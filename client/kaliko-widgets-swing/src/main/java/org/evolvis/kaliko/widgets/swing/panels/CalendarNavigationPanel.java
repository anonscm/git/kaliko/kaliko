/*
 * Kaliko Swing GUI Components,
 * Swing GUI Components for Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Swing GUI Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.swing.panels;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.kaliko.components.controller.CalendarApplicationController;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CalendarNavigationPanel extends ScopeTitlePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4508032807397634991L;
	protected JButton previousButton;
	protected JButton nextButton;

	protected ActionListener actionListener;
	
	protected CalendarApplicationController controller;

	public CalendarNavigationPanel(CalendarApplicationController controller) {
		super(controller);

		setLayout(new FormLayout("0dlu:grow, pref, 5dlu, max(120dlu;pref), 5dlu, pref, 0dlu:grow", // columns
		"pref")); // rows

		CellConstraints cc = new CellConstraints();

		add(getPreviousButton(), cc.xy(2, 1));
		add(getCurrentDisplayedScopeLabel(), cc.xy(4, 1));
		add(getNextButton(), cc.xy(6, 1));
	}

	protected JButton getPreviousButton() {
		if(previousButton == null) {
			previousButton = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/libraries/components/calendar/gfx/go-previous.png"))));
			previousButton.addActionListener(getActionListener());
		}
		return previousButton;
	}

	protected JButton getNextButton() {
		if(nextButton == null) {
			nextButton = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/libraries/components/calendar/gfx/go-next.png"))));
			nextButton.addActionListener(getActionListener());
		}
		return nextButton;
	}

	protected ActionListener getActionListener() {
		if(actionListener == null) {
			actionListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(e.getSource().equals(getPreviousButton()))
						CalendarNavigationPanel.this.controller.getViewSwitcher().gotoPrevious();
					else if(e.getSource().equals(getNextButton()))
						CalendarNavigationPanel.this.controller.getViewSwitcher().gotoNext();
				}				
			};
		}
		return actionListener;
	}
}
