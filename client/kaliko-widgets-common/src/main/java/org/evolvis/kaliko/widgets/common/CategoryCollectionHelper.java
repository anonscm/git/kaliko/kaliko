/**
 * 
 */
package org.evolvis.kaliko.widgets.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import de.tarent.kaliko.objects.Category;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CategoryCollectionHelper {
	
	private CategoryCollectionHelper() {
		// Do not instantiate
	}

	public static String getCategoriesAsString(Collection<Category> categoryList) {
		Iterator<Category> categoriesIt = categoryList.iterator();
		StringBuffer categoriesAsString = new StringBuffer(";");

		while(categoriesIt.hasNext())
			categoriesAsString.append(categoriesIt.next().getGuid()+";");

		return categoriesAsString.toString();
	}

	public static Collection<Category> getSubsetFromCollectionByGuidString(String stringWithGuids, Collection<Category> allCategories) {
		Iterator<Category> availableCategories = allCategories.iterator();

		Collection<Category> subSet = new ArrayList<Category>();

		while(availableCategories.hasNext()) {
			Category category = availableCategories.next();
			if(stringWithGuids.contains(category.getGuid()))
				subSet.add(category);
		}
		
		return subSet;
	}
	
	/**
	 * Just returns the the given Collection if it is an implementation of java.util.List.
	 * Otherwise it creates an ArrayList with the elements of the collection.
	 * 
	 * @param collection
	 * @return
	 */
	public static List<Category> toList(Collection<Category> collection) {
		if(collection instanceof List)
			return (List<Category>)collection;
		
		return new ArrayList<Category>(collection);
	}
}
