/**
 * 
 */
package org.evolvis.kaliko.widgets.dialogs;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.swing.Action;

import org.evolvis.kaliko.widgets.common.utils.Messages;

import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface EventDialog {

	/**
	 * Mode for creating a new event
	 */
	public final static int MODE_NEW = 0;
	
	/**
	 * Mode for editing an existant event
	 */
	public final static int MODE_EDIT = 1;
	
	public final static String TITLE_NEW_EVENT = Messages.getString("EventDialog_Title_New_Event");
	public final static String TITLE_EDIT_EVENT = Messages.getString("EventDialog_Title_Edit_Event");

	public final static String SUBMIT_NEW_EVENT = Messages.getString("EventDialog_Submit_New_Event");
	public final static String SUBMIT_EDIT_EVENT = Messages.getString("EventDialog_Submit_Edit_Event");

	public final static String CANCEL_NEW_EVENT = Messages.getString("EventDialog_Cancel_New_Event");
	public final static String CANCEL_EDIT_EVENT = CommonDialogButtons.CANCEL;

	public final static String SUBMIT_NEW_EVENT_TOOLTIP = Messages.getString("EventDialog_Submit_New_Event_Tooltip");
	public final static String SUBMIT_EDIT_EVENT_TOOLTIP = Messages.getString("EventDialog_Submit_Edit_Event_Tooltip");

	public final static String CANCEL_NEW_EVENT_TOOLTIP = Messages.getString("EventDialog_Cancel_New_Event_Tooltip");
	public final static String CANCEL_EDIT_EVENT_TOOLTIP = CommonDialogButtons.CANCEL_TOOLTIP;
	
	public void open();
	
	public void setEvent(Event event);
	
	public void setSelectedCategories(Collection<Category> categories);
	
	public void setAvailableCategories(Collection<Category> categories);
	
	public void setMode(int mode);
	
	public void setBeginDay(Calendar begin);
	
	public void setEndDay(Calendar end);
	
	public void setBeginTime(Calendar begin);
	
	public void setEndTime(Calendar end);
	
	public void setSubmitAction(Action action);
	
	public Event getEvent();
	
	public Collection<Category> getSelectedCategories();
} 