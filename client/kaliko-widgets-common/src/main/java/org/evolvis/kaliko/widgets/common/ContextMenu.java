/**
 * 
 */
package org.evolvis.kaliko.widgets.common;

import java.awt.Point;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface ContextMenu {

	public void showContextMenuAt(int x, int y);
}
