/**
 * 
 */
package org.evolvis.kaliko.widgets.common;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractCategoryTable implements CategoryTable {

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#setSelectedCategories(java.lang.String)
	 */
	public void setSelectedCategories(String selectedCategories) {
		setSelectedCategories(CategoryCollectionHelper.getSubsetFromCollectionByGuidString(selectedCategories, getAvailableCategories()));
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategoryTable#setDefaultCategories(java.lang.String)
	 */
	public void setDefaultCategories(String defaultCategories) {
		setDefaultCategories(CategoryCollectionHelper.getSubsetFromCollectionByGuidString(defaultCategories, getAvailableCategories()));
	}

}
