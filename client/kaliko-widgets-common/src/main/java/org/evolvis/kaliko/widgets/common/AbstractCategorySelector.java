/**
 * 
 */
package org.evolvis.kaliko.widgets.common;

import java.awt.Color;
import java.util.Iterator;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractCategorySelector implements CategorySelector {

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getColorForCategory(de.tarent.kaliko.objects.Category)
	 */
	public Color getColorForCategory(Category category) {
		// Colors for categories are currently not supported. So just return a default color.
		return Color.PINK;
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getColorForCategory(java.lang.String)
	 */
	public Color getColorForCategory(String guid) {
		// Colors for categories are currently not supported. So just return a default color.
		return Color.PINK;
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getSelectedCategoriesAsString()
	 */
	public String getSelectedCategoriesAsString() {
		return CategoryCollectionHelper.getCategoriesAsString(getSelectedCategories());
	}


	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getFavoriteCategoriesAsString()
	 */
	public String getFavoriteCategoriesAsString() {
		return CategoryCollectionHelper.getCategoriesAsString(getFavoriteCategories());
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getDefaultCategoriesAsString()
	 */
	public String getDefaultCategoriesAsString() {
		return CategoryCollectionHelper.getCategoriesAsString(getDefaultCategories());
	}

//	/**
//	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getSelectedWritableCategories()
//	 */
//	public Collection<Category> getSelectedWritableCategories() {
//		Collection<Category> selectedWritableCategories = new ArrayList<Category>();
//		
//		Iterator<Category> it = getSelectedCategories().iterator();
//		while(it.hasNext()) {
//			Category category = it.next();
//			if(category.getAccess().isWritable())
//				selectedWritableCategories.add(category);
//		}
//		
//		return selectedWritableCategories;
//	}

//	/**
//	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#getWritableCategories()
//	 */
//	public Collection<Category> getWritableCategories() {
//		ArrayList<Category> writableCategories = new ArrayList<Category>(); 
//		Iterator<Category> it = getAvailableCategories().iterator();
//		
//		while(it.hasNext()) {
//			Category category = it.next();
//			if(category.getAccess().isWritable())
//				writableCategories.add(category);
//		}
//		
//		return writableCategories;
//	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#hasEventSelectedCategories(de.tarent.kaliko.objects.Event)
	 */
	public boolean hasEventSelectedCategories(Event event) {
		// check if event is in selected categories, otherwise do not insert event into view
		Iterator<Category> it = event.getCategoriesList().iterator();
		
		while(it.hasNext()) {
			if(getSelectedCategoriesAsString().contains(it.next().getGuid()))
				return true;
		}
		return false;
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setColorForCategory(de.tarent.kaliko.objects.Category, java.awt.Color)
	 */
	public void setColorForCategory(Category category, Color color) {
		// Currently colors are not supported. So do nothing.
	}
	
	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setSelectedCategories(java.lang.String)
	 */
	public void setSelectedCategories(String stringWithGuids) {
		setSelectedCategories(CategoryCollectionHelper.getSubsetFromCollectionByGuidString(stringWithGuids, getAvailableCategories()));
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setFavoriteCategories(java.lang.String)
	 */
	public void setFavoriteCategories(String stringWithGuids) {
		setFavoriteCategories(CategoryCollectionHelper.getSubsetFromCollectionByGuidString(stringWithGuids, getAvailableCategories()));
	}

	/**
	 * @see org.evolvis.kaliko.widgets.common.CategorySelector#setDefaultCategories(java.lang.String)
	 */
	public void setDefaultCategories(String stringWithGuids) {
		setDefaultCategories(CategoryCollectionHelper.getSubsetFromCollectionByGuidString(stringWithGuids, getAvailableCategories()));		
	}
}
