/**
 * 
 */
package org.evolvis.kaliko.widgets.common;

import java.util.Collection;

import javax.swing.Action;

import de.tarent.kaliko.objects.Category;

/**
 * This interface describes the methods a category-table should provide.
 * 
 * For explanation of the different category-sets see interface CategorySelector.
 * 
 * @see CategorySelector
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface CategoryTable {

	/* Methods for available categories */
	public Collection<Category> getAvailableCategories();
	public void setAvailableCategories(Collection<Category> categories);
	
	/* Methods for selected categories */
	public Collection<Category> getSelectedCategories();
	public void setSelectedCategories(Collection<Category> selectedCategories);
	public void setSelectedCategories(String selectedCategories);
	
	/* Methods for default categories */
	public Collection<Category> getDefaultCategories();
	public void setDefaultCategories(Collection<Category> defaultCategories);
	public void setDefaultCategories(String defaultCategories);
	
	
	public void addCategory(Category category);
	public void removeCategory(Category category);
	
	public void setCategorySelectionAction(Action action);
}
