package org.evolvis.kaliko.widgets.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Participant;

public class ParticipantCollectionHelper {
	private ParticipantCollectionHelper() {
		// Do not instantiate
	}
//	public static String getParticipantsAsString(Collection<Participant> categoryList) {
//		Iterator<Participant> categoriesIt = categoryList.iterator();
//		StringBuffer participantsAsString = new StringBuffer(";");
//
//		while(categoriesIt.hasNext())
//			participantsAsString.append(categoriesIt.next().getGuid()+";");
//
//		return participantsAsString.toString();
//	}
//
//	public static Collection<Participant> getSubsetFromCollectionByGuidString(String stringWithGuids, Collection<Participant> allCategories) {
//		Iterator<Participant> availableParticipants = allParticipants.iterator();
//
//		Collection<Participant> subSet = new ArrayList<Participant>();
//
//		while(availableParticipants.hasNext()) {
//			Participant participant = availableParticipants.next();
//			if(stringWithGuids.contains(participant.getGuid()))
//				subSet.add(participant);
//		}
//		
//		return subSet;
//	}
	/**
	 * Just returns the the given Collection if it is an implementation of java.util.List.
	 * Otherwise it creates an ArrayList with the elements of the collection.
	 * 
	 * @param collection
	 * @return
	 */
	public static List<Participant> toList(Collection<Participant> collection) {
		if(collection instanceof List)
			return (List<Participant>)collection;
		
		return new ArrayList<Participant>(collection);
	}
}
