/*
 * Kaliko Client Core Components,
 * Core Components for kaliko Calendar-Applications
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Client Core Components'
 * Signature of Elmar Geese, 4 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.kaliko.widgets.common;

import java.awt.Color;
import java.util.Collection;

import javax.swing.Action;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;

/**
 * A interface for all classes which allow category-selection-management
 * 
 * There are three different types of category-sets:
 * 
 * <ul>
 * 	<li>Available Categories - The largest set. All other sets are subsets of this one. It contains all categories for which the current user has read and/or write access</li>
 * 	<li>Favorite Categories - A user-definable subset of categories which are most important for the user.</li>
 *  <li>Selected Categories - The set of categories which are selected to be displayed in all main calendar-views.</li>
 *  <li>Default Categories - The set of categories to which newly created events are assigned by default.</li>
 * </ul>
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface CategorySelector {

	/* Methods for available categories */
	public Collection<Category> getAvailableCategories();
	public void setAvailableCategories(Collection<Category> categories);
	
	/* Methods for selected categories */
	public Collection<Category> getSelectedCategories();
	public void setSelectedCategories(Collection<Category> selectedCategories);
	public String getSelectedCategoriesAsString();
	public void setSelectedCategories(String selectedCategories);
	
	/* Methods for default categories */
	public Collection<Category> getDefaultCategories();
	public void setDefaultCategories(Collection<Category> defaultCategories);
	public String getDefaultCategoriesAsString();
	public void setDefaultCategories(String defaultCategories);
	
//	/* Methods for writable categories */
//	public Collection<Category> getWritableCategories();
//	public Collection<Category> getSelectedWritableCategories();
	
	/* Methods for favorite categories */
	public Collection<Category> getFavoriteCategories();
	public void setFavoriteCategories(Collection<Category> categories);
	public String getFavoriteCategoriesAsString();
	public void setFavoriteCategories(String categories);
	
	public void addToFavorites(Category category);
	public void removeFromFavorites(Category category);
	
	/* Methods for category-colors */
	public Color getColorForCategory(Category category);
	public Color getColorForCategory(String guid);
	public void setColorForCategory(Category category, Color color);
	
	
	/**
	 * Check if event is in selected categories
	 * 
	 * @param event
	 * @return
	 */
	public boolean hasEventSelectedCategories(Event event);
	
	public void setCategorySelectionAction(Action action);
}
