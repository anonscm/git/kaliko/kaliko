package de.tarent.kaliko.holidays;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.component.VEvent;

/**
 * An HolidayCalendar which parses an iCalendar file.
 * 
 * <p>
 * An example for a minimal iCal file with one holiday event with 
 * a yearly frequency:
 * <pre>
 *  BEGIN:VCALENDAR
*  BEGIN:VEVENT
*  RRULE:FREQ=YEARLY;INTERVAL=1;BYMONTH=01
*  SUMMARY:Neujahr
*  DTSTART;VALUE=DATE:20000101
*  END:VEVENT
*  END:VCALENDAR
*  </pre>
*  
*   <p>
 * An example for a minimal iCal file with one holiday event with 
 * a yearly frequency:
 * <pre>
 *  BEGIN:VCALENDAR
*  BEGIN:VEVENT
*  SUMMARY:Fronleichnam
*  DTSTART;VALUE=DATE:20080522
*  END:VEVENT
*  BEGIN:VEVENT
*  SUMMARY:Fronleichnam
*  DTSTART;VALUE=DATE:20090611
*  END:VEVENT
*  BEGIN:VEVENT
*  SUMMARY:Fronleichnam
*  DTSTART;VALUE=DATE:20100603
*  END:VEVENT
*  END:VCALENDAR
*  </pre>
*  
 * @author Thomas Schmitz, tarent GmbH
 */
public class ICalHolidayCalendar extends HolidayCalender {

	/**
	 * Creates a new ICalHolidayCalendar which parses the given
	 * InputStream.
	 */
	public ICalHolidayCalendar(int year, InputStream in) throws IOException, ParserException {
		parseICalStream(year, in);
	}
	
	/**
	 * Creates a new ICalHolidayCalendar which parses the given
	 * File.
	 */
	public ICalHolidayCalendar(int year, File icalFile) throws IOException, ParserException{
		parseICalStream(year, new FileInputStream(icalFile));
	}
	
	private void parseICalStream(int year, InputStream in) throws IOException, ParserException {
		setHolidays(new ArrayList<Holiday>());
		CalendarBuilder calBuilder = new CalendarBuilder();
		Calendar calendar = calBuilder.build(in);
		Iterator<Component> iter = calendar.getComponents().iterator();
		while(iter.hasNext()){
			Component component = iter.next();
			if(component instanceof VEvent){
				GregorianCalendar gregorian = new GregorianCalendar();
				gregorian.setTime(((VEvent) component).getStartDate().getDate());
				Holiday holiday = new Holiday();
				holiday.setName(((VEvent) component).getSummary().getValue());
				if(((VEvent)component).getProperty("RRULE") != null){					
					gregorian.set(java.util.Calendar.YEAR, year);					
					holiday.setDate(gregorian.getTime());
					addHoliday(holiday);
				}
				else if (gregorian.get(java.util.Calendar.YEAR) == year){
					holiday.setDate(gregorian.getTime());
					addHoliday(holiday);
				}
			}
		}
	}
}
