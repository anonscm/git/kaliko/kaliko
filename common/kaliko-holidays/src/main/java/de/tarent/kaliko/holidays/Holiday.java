package de.tarent.kaliko.holidays;

import java.util.Date;

/**
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class Holiday {

	private String name;
	
	private Date date;
	
	private boolean isNationalWide;

	/**
	 * Returns the name of this holiday.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this holiday.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the date of this holiday.
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date of this holiday.
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Returns whether this holiday is national wide or not.
	 */
	public boolean isNationalWide() {
		return isNationalWide;
	}

	/**
	 * Sets whether this holiday is national wide or not.
	 */
	public void setNationalWide(boolean isNationalWide) {
		this.isNationalWide = isNationalWide;
	}

}
