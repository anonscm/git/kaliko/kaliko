package de.tarent.kaliko.holidays;

public interface Holidays {

	public static final String NEW_YEAR = "Neujahr";																// 1. Januar
	
	public static final String EPIPHANY = "Heilige drei Könige";												// 6. Januar
	
	public static final String HOLY_THURSDAY = "Gründonnerstag";										// Ostersonntag - 3 Tage
	
	public static final String GOOD_FRIDAY = "Karfreitag";														// Ostersonntag - 2Tage
	
	public static final String EASTERN = "Ostersonntag";														// muss berechnet werden
	
	public static final String EASTER_MONDAY = "Ostermontag";											// Ostersonntag + 1Tag	
	
	public static final String MAYDAY = "Tag der Arbeit";															// 1. Mai
	
	public static final String ASCENSIONDAY = "Christ Himmelfahrt";										// Ostersonntag + 39Tage
	
	public static final String WHITSUN_SUNDAY = "Pfingstsonntag";										// Ostersonntag + 49Tage	
	
	public static final String WHITSUN_MONDAY = "Pfingstmontag";										// Ostersonntag + 50Tage
	
	public static final String CORPUS_CHRISTI = "Fronleichnam";												// Ostersonntag + 60Tage
	
	public static final String ASSUMPTION_DAY = "Maria Himmelfahrt";									// 15. August
	
	public static final String GERMAN_UNIFICATION_DAY = "Tag der deutschen Einheit";			// 3. Oktober
	
	public static final String REFORMATION_DAY = "Reformationstag";										// 31. Oktober
	
	public static final String HALLOWEEN = "Allerheiligen";														// 1.November
	
	public static final String PENANCE_DAY = "Buß- und Bettag";											// Mittwoch vor dem 23. November
	
	public static final String FIRST_CHRISTMAS_DAY = "1. Weihnachtstag";								// 25.Dezember
	
	public static final String SECOND_CHRISTMAS_DAY = "2. Weihnachtstag";							// 26.Dezember
}
