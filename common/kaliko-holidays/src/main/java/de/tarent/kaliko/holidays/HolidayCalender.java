package de.tarent.kaliko.holidays;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * A simple holiday calendar without any holidays. The holidays have to be added
 * manually.  
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class HolidayCalender {

	private List<Holiday> holidays;
	
	/**
	 * Creates a new instance of an empty HolidayCalendar.
	 */
	public HolidayCalender() {
		setHolidays(new ArrayList<Holiday>());
	}
	
	/**
	 * Returns the holiday list.
	 */
	public List<Holiday> getHolidays() {
		return holidays;
	}

	/**
	 * Sets the holiday list.
	 */
	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}
	
	/**
	 * Adds a new holiday.
	 */
	public void addHoliday(Holiday holiday){
		this.holidays.add(holiday);
	}
	
	/**
	 * Returns whether the given date range contains one or more holidays.
	 */
	public boolean isHolidayInDateRange(Date from, Date to) {
		return getHolidaysInDateRange(from, to).size() > 0;
	}
	
	/**
	 * Returns a list of holidays which are enclosed in the given date range.
	 */
	public List<Holiday> getHolidaysInDateRange(Date from, Date to){
		List<Holiday> holidaysInRange = new ArrayList<Holiday>();
		GregorianCalendar fromCal = new GregorianCalendar();
		fromCal.setTime(from);
		for(int i = 0; i < getHolidays().size(); i++) {
			Date holidayDate = getHolidays().get(i).getDate();
			if(holidayDate.compareTo(from) >= 0 && holidayDate.compareTo(to) <= 0)
				holidaysInRange.add(getHolidays().get(i));
		}		
		GregorianCalendar toCal = new GregorianCalendar();
		toCal.setTime(to);
		if(fromCal.get(Calendar.YEAR) != toCal.get(Calendar.YEAR)) {
			for(int i = 0; i < getHolidays().size(); i++) {
				Date holidayDate = getHolidays().get(i).getDate();
				if(holidayDate.compareTo(from) >= 0 && holidayDate.compareTo(to) <= 0)
					holidaysInRange.add(getHolidays().get(i));
			}
		}		
		return holidaysInRange;
	}
	
	/**
	 * Returns whether a given date is an holiday.
	 */
	public boolean isDateAnHoliday(Date date) {
		if(getHolidaysInDateRange(date, date).size() > 0)
			return true;
		return false;
	}

}