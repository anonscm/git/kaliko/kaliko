package de.tarent.kaliko.holidays;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * GermanHolidayCalendar is a HolidayCalendar which handles the German holidays.
 * In Germany a holiday does not always applies cross-national.
 *  
 * @author Thomas Schmitz, tarent GmbH
 */
public class GermanHolidayCalendar extends HolidayCalender implements Holidays {	
	
	private int year;
	
	/**
	 * Creates a new instance of a GermanHolidayCalendar. The calendar contains
	 * a list of all German holidays in the given year.
	 */
	public GermanHolidayCalendar(int year) {
		this.year = year;
		setHolidays(new ArrayList<Holiday>());
		getHolidays().add(getNewYear());
		getHolidays().add(getEpiphany());
		getHolidays().add(getHolyThursday());
		getHolidays().add(getGoodFriday());
		getHolidays().add(getEastern());
		getHolidays().add(getEasterMonday());
		getHolidays().add(getMayDay());
		getHolidays().add(getAscensionDay());
		getHolidays().add(getWhitSunday());
		getHolidays().add(getWhitMonday());
		getHolidays().add(getCorpusChristi());
		getHolidays().add(getAssumptionDay());
		getHolidays().add(getGermanUnificationDay());
		getHolidays().add(getReformationDay());
		getHolidays().add(getHalloween());
		getHolidays().add(getPenanceDay());
		getHolidays().add(getFirstChristmasDay());
		getHolidays().add(getSecondChristmasDay());
	}
	
	/**
	 * Calculates the Easter Date and returns it as GregorianCalendar.
	 */
	private GregorianCalendar getEasterCalendar() {
		int y = year/100;
    	int n = year - 19 * (year / 19);
    	int k = (y - 17) / 25;
    	
    	int a = y - y/4 - (y - k) / 3 + 19 * n + 15;
    	int b = a - 30 * (a / 30);
    	int c = b - (b / 28) * (1 - (12 / 28) * (29 / (b + 1)) * ((21 - n) / 11));
    	
    	int d = year + year/4 + c + 2 - y + y / 4;
    	int e = d - 7 * (d / 7);
    	int f = c - e;
    	
    	int month = 3 + (f + 40) / 44;
    	int day = f + 28 - 31 * (month / 4);
    	
    	return new GregorianCalendar(year, month-1, day);
	}
	
	/**
	 * Returns the New Year holiday. New Year is always 1. January.
	 */
	public Holiday getNewYear() {
		Holiday newYear  = new Holiday();
		newYear.setName(Holidays.NEW_YEAR);
		newYear.setDate(new GregorianCalendar(year, 0, 1).getTime());
		newYear.setNationalWide(true);
		return newYear;
	}
	
	/**
	 * Returns the Epiphany holiday (Heilige drei Könige). Epiphany is always 6. January.
	 */
	public Holiday getEpiphany() {
		Holiday epiphany = new Holiday();
		epiphany.setName(Holidays.EPIPHANY);
		epiphany.setDate(new GregorianCalendar(year, 0, 6).getTime());
		epiphany.setNationalWide(false);
		return epiphany;
	}
	
	/**
	 * Returns the Holy Thursday (Gründonnerstag). Holy Thursday is
	 * three days before Eastern. 
	 */
	public Holiday getHolyThursday() {
		GregorianCalendar easter = getEasterCalendar();
		easter.add(Calendar.DAY_OF_YEAR, -3);
		Holiday holyThursday = new Holiday();
		holyThursday.setName(Holidays.HOLY_THURSDAY);
		holyThursday.setDate(easter.getTime());
		holyThursday.setNationalWide(false);
		return holyThursday;
	}
	
	/**
	 * Returns the Good Friday (Karfreitag). Good Friday is two days
	 * before Eastern.
	 */
	public Holiday getGoodFriday() {
		GregorianCalendar eastern = getEasterCalendar();
		eastern.add(Calendar.DAY_OF_YEAR, -2);
		Holiday goodFriday = new Holiday();
		goodFriday.setName(Holidays.GOOD_FRIDAY);
		goodFriday.setDate(eastern.getTime());
		goodFriday.setNationalWide(true);
		return goodFriday;
	}
	
	/**
	 * Returns the Easter Sunday. 
	 */
	public Holiday getEastern() {    	
    	Holiday eastern = new Holiday();
    	eastern.setName(Holidays.EASTERN);
    	eastern.setDate(getEasterCalendar().getTime());
    	eastern.setNationalWide(true);
    	return eastern;
    }
	
	/**
	 * Returns the Easter Monday.
	 */
	public Holiday getEasterMonday() {
		GregorianCalendar easter = getEasterCalendar();
		easter.add(Calendar.DAY_OF_YEAR, 1);
		Holiday easterMonday = new Holiday();
		easterMonday.setName(Holidays.EASTER_MONDAY);
		easterMonday.setDate(easter.getTime());
		easterMonday.setNationalWide(true);
		return easterMonday;
	}	
	
	/**
	 * Returns the May Day. May Day is always 1. May.
	 */
	public Holiday getMayDay() {
		Holiday mayDay = new Holiday();
		mayDay.setName(Holidays.MAYDAY);
		mayDay.setDate(new GregorianCalendar(year, 4, 1).getTime());
		mayDay.setNationalWide(true);
		return mayDay;
	}
	
	/**
	 * Returns the Ascension Day (Christi Himmelfahrt). Ascension Day
	 * is 39 days after Eastern.
	 */
	public Holiday getAscensionDay() {
		GregorianCalendar easterCal =getEasterCalendar();
		easterCal.add(Calendar.DAY_OF_YEAR, 39);
		Holiday ascensionday = new Holiday();
		ascensionday.setName(Holidays.ASCENSIONDAY);
		ascensionday.setDate(easterCal.getTime());
		ascensionday.setNationalWide(true);
		return ascensionday;
	}
	
	/**
	 * Returns the Whit Sunday (Pfingstsonntag). Whit Sunday is
	 * 49 days after Eastern.
	 */
	public Holiday getWhitSunday() {
		GregorianCalendar easterCal =getEasterCalendar();
		easterCal.add(Calendar.DAY_OF_YEAR, 49);
		Holiday whitSunday = new Holiday();
		whitSunday.setName(Holidays.WHITSUN_SUNDAY);
		whitSunday.setDate(easterCal.getTime());
		whitSunday.setNationalWide(true);
		return whitSunday;
	}
	
	/**
	 * Returns the Whit Monday (Pfingstmontag). Whit Monday is
	 * 50 days after Eastern.
	 */
	public Holiday getWhitMonday() {
		GregorianCalendar easterCal =getEasterCalendar();
		easterCal.add(Calendar.DAY_OF_YEAR, 50);
		Holiday whitMonday = new Holiday();
		whitMonday.setName(Holidays.WHITSUN_MONDAY);
		whitMonday.setDate(easterCal.getTime());
		whitMonday.setNationalWide(true);
		return whitMonday;
	}
	
	/**
	 * Returns the Corpus Christi (Fronleichnam). Corpus Christi is 60
	 * days after Eastern.
	 */
	public Holiday getCorpusChristi() {
		GregorianCalendar easterCal =getEasterCalendar();
		easterCal.add(Calendar.DAY_OF_YEAR, 60);
		Holiday corpus = new Holiday();
		corpus.setName(Holidays.CORPUS_CHRISTI);
		corpus.setDate(easterCal.getTime());
		corpus.setNationalWide(false);
		return corpus;
	}
	
	/**
	 * Returns the Assumption Day (Maria Himmelfahrt). Assumption Day is
	 * always the 15. August.
	 */
	public Holiday getAssumptionDay() {
		Holiday assumptionDay = new Holiday();
		assumptionDay.setName(Holidays.ASSUMPTION_DAY);
		assumptionDay.setDate(new GregorianCalendar(year, 7, 15).getTime());
		assumptionDay.setNationalWide(false);
		return assumptionDay;
	}
	
	/**
	 * Returns the German Unification Day (Tag der deutschen Einheit). German
	 * Unification Day is always the 3. October.
	 */
	public Holiday getGermanUnificationDay() {
		Holiday germanUnificationDay = new Holiday();
		germanUnificationDay.setName(Holidays.GERMAN_UNIFICATION_DAY);
		germanUnificationDay.setDate(new GregorianCalendar(year, 9, 3).getTime());
		germanUnificationDay.setNationalWide(true);
		return germanUnificationDay;
	}
	
	/**
	 * Returns the Reformation Day. Reformation Day is always the
	 * 31. October.
	 */
	public Holiday getReformationDay() {
		Holiday reformationDay = new Holiday();
		reformationDay.setName(Holidays.REFORMATION_DAY);
		reformationDay.setDate(new GregorianCalendar(year, 9, 31).getTime());
		reformationDay.setNationalWide(false);
		return reformationDay;
	}
	
	/**
	 * Returns the Halloween Day. Halloween is always 1. November.
	 */
	public Holiday getHalloween() {
		Holiday halloween = new Holiday();
		halloween.setDate(new GregorianCalendar(year, 10, 1).getTime());
		halloween.setName(Holidays.HALLOWEEN);
		halloween.setNationalWide(false);
		return halloween;
	}
	
	/**
	 * Returns the Penance Day (Buß- und Bettag). Penance Day is always on
	 * Wednesday before 23. November.
	 */
	public Holiday getPenanceDay() {
		Holiday penanceDay = new Holiday();
		penanceDay.setName(Holidays.PENANCE_DAY);
		GregorianCalendar penanceCalendar = new GregorianCalendar(year, 10, 22);
		while(penanceCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.WEDNESDAY) {
			penanceCalendar.add(Calendar.DAY_OF_WEEK, -1);
		}
		penanceDay.setDate(penanceCalendar.getTime());
		penanceDay.setNationalWide(false);
		return penanceDay;
	}
	
	/**
	 * Returns the first Christmas Day.
	 */
	public Holiday getFirstChristmasDay() {
		Holiday firstChrismas = new Holiday();
		firstChrismas.setName(Holidays.FIRST_CHRISTMAS_DAY);
		firstChrismas.setDate(new GregorianCalendar(year, 11, 25).getTime());
		firstChrismas.setNationalWide(true);
		return firstChrismas;
	}
	
	/**
	 * Returns the second Christmas Day.
	 */
	public Holiday getSecondChristmasDay() {
		Holiday secondChrismas = new Holiday();
		secondChrismas.setName(Holidays.SECOND_CHRISTMAS_DAY);
		secondChrismas.setDate(new GregorianCalendar(year, 11, 26).getTime());
		secondChrismas.setNationalWide(true);
		return secondChrismas;
	}
}
