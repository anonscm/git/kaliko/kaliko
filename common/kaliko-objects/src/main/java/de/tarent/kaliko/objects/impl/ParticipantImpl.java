package de.tarent.kaliko.objects.impl;

import de.tarent.kaliko.objects.Participant;

public class ParticipantImpl implements Participant {

	private String firstName;
	private String lastName;
	private String email;
	
	public ParticipantImpl(String firstName,String lastName,String email){
		this.firstName=firstName;
		this.lastName=lastName;
		this.email=email;
	}
	
	public String getEmail() {
		return this.email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setEmail(String email) {
		
	}

	public void setFirstName(String firstName) {
		
	}

	public void setLastName(String lastName) {
		
	}

}
