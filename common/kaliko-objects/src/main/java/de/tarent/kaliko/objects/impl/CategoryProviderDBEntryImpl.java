/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import de.tarent.kaliko.objects.CategoryProviderDBEntry;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CategoryProviderDBEntryImpl implements CategoryProviderDBEntry {

	private int pk;
	private String classname;
	private String storageInformation;
	
	public String getClassname() {
		return this.classname;
	}
	public int getPk() {
		return this.pk;
	}
	public String getStorageInformation() {
		return this.storageInformation;
	}
	public void setClassname(String classname) {
		this.classname = classname;
		
	}
	public void setPk(int pk) {
		this.pk = pk; 
	}
	public void setStorageInformation(String storageInformation) {
		this.storageInformation = storageInformation ;
	}
	
}	
