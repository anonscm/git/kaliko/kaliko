/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects;

import java.util.Date;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public interface Occurence {

	public static final String PROPERTY_PK = "pk";
	public static final String PROPERTY_FK_EVENT = "fkEvent";
	public static final String PROPERTY_INTERVAL_TYPE = "intervalType";
	public static final String PROPERTY_FREQUENCY = "frequency";
	public static final String PROPERTY_TYPE = "type";
	public static final String PROPERTY_UPDOWN = "upDown";
	public static final String PROPERTY_STARTDATE = "startDate";
	public static final String PROPERTY_ENDDATE = "endDate";
	public static final String PROPERTY_STARTTIME = "startTime";
	public static final String PROPERTY_ENDTIME = "endTime";
	public static final String PROPERTY_DELETED = "deleted";
	public static final String PROPERTY_UPDATEDATE = "updateDate";
	
	public static final String [] PROPERTIES = {
		PROPERTY_PK,
		PROPERTY_FK_EVENT,
		PROPERTY_INTERVAL_TYPE,
		PROPERTY_FREQUENCY,
		PROPERTY_TYPE,
		PROPERTY_UPDOWN,
		PROPERTY_STARTDATE,
		PROPERTY_ENDDATE,
		PROPERTY_STARTTIME,
		PROPERTY_ENDTIME,
		PROPERTY_DELETED,
		PROPERTY_UPDATEDATE
	};
	
	public Date getUpdateDate();
	public void setUpdateDate(Date date);
	
	public boolean getDeleted();
	
	public void setDeleted(boolean deleted);
	
	public int getPk();
	
	public void setPk(int pk);
	
	public int getFkEvent();
	
	public void setFkEvent(int fkEvent);
	
	public int getIntervalType();
	
	public void setIntervalType(int intervalType);
	
	public int getFrequency();
	
	public void setFrequency(int frequency);
	
	public int getType();
	
	public void setType(int type);
	
	public int getUpDown();
	
	public void setUpDown(int upDown);
	
	public Date getStartDate();
	
	public void setStartDate(Date startDate);
	
	public Date getEndDate();
	
	public void setEndDate(Date endDate);
	
	public Date getStartTime();
	
	public void setStartTime(Date startTime);
	
	public Date getEndTime();
	
	public void setEndTime(Date endTime);
	
	public boolean isAllDay();
	public void setAllDay(boolean allDay);
}
