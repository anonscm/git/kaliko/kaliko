/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import java.util.Date;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Type;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class TypeImpl implements Type {

	private int pk;
	private String urn;
	private Date updatedate;
	
	public int getPk() {
		return pk;
	}

	public String getUrn() {
		return urn;
	}

	public void setPk(int pk) {
		this.pk = pk;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public Date getUpdateDate() {
		return this.updatedate;
	}

	public void setUpdateDate(Date date) {
		this.updatedate = date ; 
		
	}
	
	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;
		
		if(! (anObject instanceof Type))
			return false;
		
		if(this.getPk() > 0)
			return ((Type)anObject).getPk() == this.getPk();
		
		return false;
	}
}
