/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;

/**
 * 
 * @author Andre´ Biegel, tarent GmbH
 *
 * TODO choose proper method names
 */

public class EventImpl implements Event {

	private int pk;
	private String type;
	private String title;
	private OccurencesList occurences;
	private ResourcesList resources;
	private CategoriesList categories;
	private boolean deleted;
	private Date updatedate;

	public EventImpl() {
		this(-1, null, null, null, null, new OccurencesList(), new ResourcesList(), new CategoriesList());
	}

	public EventImpl(String title, String location, Calendar begin, Calendar end, boolean allDay) {
		this(title, location, new OccurencesList());
		getOccurencesList().add(new OccurenceImpl(begin.getTime(), end.getTime(), allDay ? null : begin.getTime(), allDay ? null : end.getTime()));
	}

	public EventImpl(String title, String location, OccurencesList occurences) {
		this(-1, null, title, location, null, occurences, new ResourcesList(), new CategoriesList());
	}

	public EventImpl(int pk, String type, String title, String location, String note, OccurencesList occurences, ResourcesList resources, CategoriesList categories) {
		this.pk = pk;
		this.type = type;
		this.title = title;
		// TODO add location and note as resources
		
		this.occurences = occurences;
		this.resources = resources;
		this.categories = categories;
	}

	public int getPk() {
		return this.pk;
	}
	
	public void setPk(int pk) {
		this.pk= pk;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title= title;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type=type;
	}

	public String getLocation() {
		return getPlainTextResourceText(RESOURCE_LOCATION_0_PREFIX);
	}
	
	public void setLocation(String location) {
		setPlainTextResource(RESOURCE_LOCATION_0_PREFIX, location);
	}
	
	private void setPlainTextResource(String guidPrefix, String text) {
		// check if the plain-text-resource already exists
		
		if(resources != null) {
			Resource resource = getPlainTextResource(guidPrefix);
			
			if(resource != null) {
				// found the plain text resource. Now set the new text.
				resource.setText(text);
				return;
			}
		} else
			resources = new ResourcesList();
		
		// no plain text resource exists. Create it!
		ResourceImpl plainTextResource = new ResourceImpl();
		plainTextResource.setGuid(guidPrefix);
		plainTextResource.setText(text);
		
		resources.add(plainTextResource);
	}
	
	private String getPlainTextResourceText(String guidPrefix) {
		Resource plainTextResource = getPlainTextResource(guidPrefix);
		
		if(plainTextResource != null)
			return plainTextResource.getText();
		
		return "";
	}
	
	private Resource getPlainTextResource(String guidPrefix) {
		if(resources == null)
			return null;
		
		for(Resource resource : resources) {
			if(resource.getGuid().startsWith(guidPrefix)) {
				
				// found the plain text resource, return it
				return resource;
			}
		}

		// plain text resource not found, return null
		return null;
	}

	public String getNote() {
		return getPlainTextResourceText(RESOURCE_NOTE_0_PREFIX);
	}

	public void setNote(String note) {
		setPlainTextResource(RESOURCE_NOTE_0_PREFIX, note);
	}

	public Occurence getOccurences(int i) {
		if (i >= this.occurences.size())
			return null;
		return (Occurence) this.occurences.get(i);
	}

	public OccurencesList getOccurencesList() {
		return this.occurences;
	}
	
	public void addOccurences(Occurence occ) {
		this.occurences.add(occ);
	}
	
	public void addOccurences(OccurenceImpl occ) {
		this.occurences.add(occ);
	}

	public void setOccurencesList(OccurencesList occ) {
		this.occurences = occ;
	}

//	public void addOccurences(OccurenceImpl occurence) {
//		boolean contains = false;
//		Iterator<Occurence> iter = this.occurences.iterator();
//		while (iter.hasNext()) {
//			if (iter.next().getPk() == occurence.getPk() && occurence.getPk() != 0)
//				contains = true;
//		}
//
//		if (!contains)
//			this.occurences.add(occurence);
//	}

	public Resource getResources(int i) {
		if (i > this.resources.size())
			return null;
		return (Resource) this.resources.get(i);
	}

	public ResourcesList getResourcesList() {
		return this.resources;
	}

	public void setResourcesList(ResourcesList resources) {
		this.resources = resources;
	}
	
	public void addResources(Resource resource) {
		this.resources.add(resource);
	}
	
	public void addResources(ResourceImpl resource) {
		this.resources.add(resource);
	}

//	public void addResources(ResourceImpl resource) {
//		boolean contains = false;
//		Iterator<Resource> iter = this.resources.iterator();
//		while (iter.hasNext()) {
//			if (iter.next().getPk() == resource.getPk())
//				contains = true;
//		}
//
//		if (!contains)
//			this.resources.add(resource);
//	}

	public void addCategories(Category category) {
		this.categories.add(category);
	}
	
	public void addCategories(CategoryImpl category) {
		this.categories.add(category);
	}
	
	public CategoriesList getCategoriesList() {
		return this.categories;
	}

	public void setCategoriesList(CategoriesList categories) {
		this.categories = categories;
	}
	
//	public void addCategories(EventCategory entry) {
//		if (!this.categories.contains(entry.getGuid()))
//			this.categories.add(entry.getGuid());		
//	}


//	public void addCategories(EventCategoryImpl entry) {
//		if (!this.categories.contains(entry.getGuid()))
//			this.categories.add(entry.getGuid());
//	}


	public boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted  = deleted;
	}

	public Date getUpdateDate() {
		return this.updatedate;
	}

	public void setUpdateDate(Date date) {
		this.updatedate = date;

	}

	/**
	 * @see de.tarent.kaliko.objects.Event#getBegin()
	 */
	public Calendar getBegin() {
		if (getOccurences(0) == null) {
			return Calendar.getInstance();
		}
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		calendar.setTime(getOccurences(0).getStartDate());

		if(getOccurences(0).getStartTime() != null) {
			Calendar timeCalendar = Calendar.getInstance(Locale.getDefault());
			
			//if(timeCalendar.getTimeZone().inDaylightTime(getOccurences(0).getStartTime()))
				timeCalendar.setTime(getOccurences(0).getStartTime());
			//else
				//timeCalendar.setTimeInMillis(getOccurences(0).getStartTime().getTime()+timeCalendar.getTimeZone().getDSTSavings());


			calendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
			calendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
			calendar.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
			calendar.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
		}

		return calendar;
	}

	/**
	 * @see de.tarent.kaliko.objects.Event#getEnd()
	 */
	public Calendar getEnd() {
		if (getOccurences(0) == null) {
			return Calendar.getInstance();
		}
		// get latest occurence
		Occurence latest = null;
		for(Occurence occurence : getOccurencesList()) {
			if(latest == null)
				latest = occurence;
			if(occurence.getEndDate().compareTo(latest.getEndDate()) > 0) {
				latest = occurence;
			}
		}
		
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		calendar.setTime(latest.getEndDate());
		
		return calendar;
	}

	public void setBegin(Calendar begin) {
		if(getOccurences(0) == null) {
			getOccurencesList().add(new OccurenceImpl());
		}

		getOccurences(0).setStartDate(begin.getTime());
		getOccurences(0).setStartTime(begin.getTime());
	}

	public void setEnd(Calendar end) {
		if(getOccurences(0) == null) {
			getOccurencesList().add(new OccurenceImpl());
		}

		getOccurences(0).setEndDate(end.getTime());
		getOccurences(0).setEndTime(end.getTime());
	}

	/**
	 * @see de.tarent.kaliko.objects.Event#isAllDayEvent()
	 */
	public boolean isAllDayEvent() {
		if(getOccurences(0) == null)
			return false;

		return getOccurences(0).isAllDay();
	}


	/**
	 * @see de.tarent.kaliko.objects.Event#setAllDayEvent(boolean)
	 */
	public void setAllDayEvent(boolean allDay) {
		if(getOccurences(0) == null)
			return;
		
		getOccurences(0).setAllDay(allDay);
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Event event) {
		return getBegin().compareTo(event.getBegin());
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		return new EventImpl(pk, type, title, getLocation(), getNote(), occurences, resources, categories);
	}
	
	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;

		if(! (anObject instanceof Event))
			return false;
		
		if(this.getPk() > 0)
			return ((Event)anObject).getPk() == this.getPk();	
		
		return false;
	}
}
