package de.tarent.kaliko.objects;

public interface Participant {
	public static final String PROPERTY_FIRST_NAME = "firstName";
	public static final String PROPERTY_LAST_NAME = "lastName";
	public static final String PROPERTY_EMAIL = "email";

	public static final String[] PROPERTIES = { PROPERTY_FIRST_NAME,
			PROPERTY_LAST_NAME, PROPERTY_EMAIL };

	public String getFirstName();

	public void setFirstName(String firstName);

	public String getLastName();

	public void setLastName(String lastName);

	public String getEmail();

	public void setEmail(String email);

}
