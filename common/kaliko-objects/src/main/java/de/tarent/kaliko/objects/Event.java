/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects;

import java.util.Calendar;
import java.util.Date;

import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.CategoryImpl;
import de.tarent.kaliko.objects.impl.OccurenceImpl;
import de.tarent.kaliko.objects.impl.OccurencesList;
import de.tarent.kaliko.objects.impl.ResourceImpl;
import de.tarent.kaliko.objects.impl.ResourcesList;

public interface Event extends Cloneable, Comparable<Event> {

	public static final String PROPERTY_PK = "pk";
	public static final String PROPERTY_TYPE = "type";
	public static final String PROPERTY_TITLE = "title";
	public static final String PROPERTY_OCCURENCES = "occurences";
	public static final String PROPERTY_RESOURCES = "resources";
	public static final String PROPERTY_CATEGORIES = "categories";
	public static final String PROPERTY_DELETED = "deleted";
	public static final String PROPERTY_UPDATEDATE = "updateDate";
	
	public static final String RESOURCE_LOCATION_0_PREFIX = "location0";
	public static final String RESOURCE_NOTE_0_PREFIX = "note0";
	
	public static final String[] PROPERTIES = {
		PROPERTY_PK,
		PROPERTY_TYPE,
		PROPERTY_TITLE,
		PROPERTY_OCCURENCES,
		PROPERTY_RESOURCES,
		PROPERTY_CATEGORIES,
		PROPERTY_DELETED,
		PROPERTY_UPDATEDATE
	};
	
	public Date getUpdateDate();
	public void setUpdateDate(Date date);
		
	public boolean getDeleted();
	public void setDeleted(boolean deleted);

	
	public CategoriesList getCategoriesList();
	
	public void setCategoriesList(CategoriesList categories);
	
	public void addCategories(Category category);
	
	public void addCategories(CategoryImpl category);
	
//	public void addCategories(EventCategory entry);
//	public void addCategories(String guid);
	
	
	public void addResources(Resource res);
	public void addResources(ResourceImpl res);
	
	public Resource getResources(int i);
	
	public ResourcesList getResourcesList();
	
	public void setResourcesList(ResourcesList resources);
	
//	public void addResources(ResourceImpl resource);
	
	
	public void addOccurences(Occurence occ);
	public void addOccurences(OccurenceImpl occ);
	
	public Occurence getOccurences(int i);
	
	public OccurencesList getOccurencesList();
	
	public void setOccurencesList(OccurencesList occurences);
	
//	public void addOccurences(OccurenceImpl occurence);

	
	public int getPk();
	public void setPk(int pk);

	public String getType();
	public void setType(String type);

	public String getTitle();
	public void setTitle(String title);

	public String getLocation();
	
	public void setLocation(String location);

	public String getNote();
	
	public void setNote(String note);
	
	public Calendar getBegin();
	public Calendar getEnd();
	
	public boolean isAllDayEvent();
	public void setAllDayEvent(boolean allDay);
	
	public Object clone();
}
