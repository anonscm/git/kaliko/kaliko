/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import java.util.Date;

import de.tarent.kaliko.objects.Occurence;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class OccurenceImpl implements Occurence {

	private int pk;
	private int fkEvent;
	private int intervalType;
	private int frequency;
	private int type;
	private int upDown;
	private Date startDate;
	private Date endDate;
	private Date startTime;
	private Date endTime;
	private boolean deleted;
	private Date updatedate;
	
	public OccurenceImpl() {
		this(null, null, null, null);
	}
	
	public OccurenceImpl(Date startDate, Date endDate, Date startTime, Date endTime) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getFkEvent() {
		return fkEvent;
	}
	public void setFkEvent(int fkEvent) {
		this.fkEvent = fkEvent;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int getIntervalType() {
		return intervalType;
	}
	public void setIntervalType(int intervalType) {
		this.intervalType = intervalType;
	}
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getUpDown() {
		return upDown;
	}
	public void setUpDown(int upDown) {
		this.upDown = upDown;
	}
	public boolean getDeleted() {
		return this.deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public Date getUpdateDate() {
		return this.updatedate;
	}
	public void setUpdateDate(Date date) {
		this.updatedate = date ;
		
	}

	/**
	 * @see de.tarent.kaliko.objects.Occurence#isAllDay()
	 */
	public boolean isAllDay() {
		return (getStartTime() == null && getEndTime() == null);
	}

	/**
	 * @see de.tarent.kaliko.objects.Occurence#setAllDay(boolean)
	 */
	public void setAllDay(boolean allDay) {
		if(allDay) {
			setStartTime(null);
			setEndTime(null);
		}
	}
	
	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;

		if(! (anObject instanceof Occurence))
			return false;
		
		if(this.getPk() > 0)
			return ((Occurence)anObject).getPk() == this.getPk();
		
		return false;
	}
}
