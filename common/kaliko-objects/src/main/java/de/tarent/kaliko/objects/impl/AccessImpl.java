/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;

/** This class contains access rights for a category.
 * Each category has an Access object included.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class AccessImpl implements Access {

	private boolean readable;
	private boolean writable;
	private int pk;
	
	public AccessImpl() {
		
	}
	
	public AccessImpl(boolean readable, boolean writable) {
		this.readable = readable;
		this.writable = writable;
	}
	
	public boolean isReadable() {
		return readable;
	}

	public boolean isWritable() {
		return writable;
	}

	public void setReadable(boolean readable) {
		this.readable = readable;
	}

	public void setWritable(boolean writable) {
		this.writable = writable;
	}

	public int getPk() {
		return this.pk;
	}

	public void setPk(int pk) {
		this.pk = pk;
		
	}

	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;
	
		if(! (anObject instanceof Access))
			return false;
		
		if(this.getPk() > 0)
			return ((Access)anObject).getPk() == this.getPk();
		
		return false;
	}
}
