/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.CategoryProvider;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CategoryProviderImpl implements CategoryProvider {

	private int pk;
	private String name;
	private String url;
	
	
	public String getName() {
		return name;
	}

	public int getPk() {
		return pk;
	}

	public String getUrl() {
		return url;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPk(int pk) {
		this.pk = pk;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public CategoriesList getCategories(String username, Access rights) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;
		
		if(! (anObject instanceof CategoryProvider))
			return false;
		
//		if(this.getPk() > 0)
//			return ((CategoryProvider)anObject).getPk() == this.getPk(); 
		
		
		return false;
	}
}
