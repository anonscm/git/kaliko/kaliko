/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import java.util.Date;

import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;

/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/

public class ResourceImpl implements Resource {

	private int pk;
	private int fkEvent;
	private int fkTypes;
	private String guid;
	private String text;
	private String infoUrl;
	private Date notificationTime;
	private String notificationMessage;
	private int participationStatus;
	private String participationNote;
	private boolean deleted;
	private Date updatedate;
	
	public int getFkEvent() {

		return this.fkEvent;
	}

	public String getGuid() {

		return this.guid;
	}

	public String getNotificationMessage() {

		return this.notificationMessage;
	}

	public Date getNotificationTime() {

		return this.notificationTime;
	}

	public String getParticipationNote() {

		return this.participationNote;
	}

	public int getParticipationStatus() {

		return this.participationStatus;
	}

	public int getPk() {

		return this.pk;
	}

	public String getText() {

		return this.text;
	}

	public int getFkTypes() {

		return this.fkTypes;
	}

	public String getInfoUrl() {

		return this.infoUrl;
	}

	public void setFkEvent(int fkEvent) {

		this.fkEvent = fkEvent;
	}

	public void setGuid(String guid) {

		this.guid = guid;
	}

	public void setNotificationMessage(String message) {

		this.notificationMessage = message;
	}

	public void setNotificationTime(Date time) {

		this.notificationTime = time;
	}

	public void setParticipationNote(String note) {

		this.participationNote = note;
	}

	public void setParticipationStatus(int status) {

		this.participationStatus = status;
	}

	public void setPk(int pk) {

		this.pk = pk;
	}

	public void setText(String text) {

		this.text = text;
	}

	public void setFkTypes(int type) {

		this.fkTypes = type;
	}

	public void setInfoUrl(String url) {

		this.infoUrl = url;
	}

	public boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getUpdateDate() {
		return this.updatedate;
	}

	public void setUpdateDate(Date date) {
		this.updatedate = date; 
		
	}

	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;

		if(! (anObject instanceof Resource))
			return false;
		
		if(this.getPk() > 0)
			return ((Resource)anObject).getPk() == this.getPk();
		
		return false;
	}
}
