/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects;



/**
 * 
 * @author Martin Pelzer, tarent GmbH
 */

public interface Category extends Comparable<Category> {

	public static final String PROPERTY_PK = "pk";
	public static final String PROPERTY_FK_EVENT ="fkEvent";
	public static final String PROPERTY_NAME = "name";
	public static final String PROPERTY_GUID = "guid";
	public static final String PROPERTY_ACCESS = "access";
	
	public static final String[] PROPERTIES = {
		PROPERTY_PK,
		PROPERTY_FK_EVENT,
		PROPERTY_NAME,
		PROPERTY_GUID,
		PROPERTY_ACCESS
	};
	
	public int getPk();
	public void setPk(int i );
	
	public int getFkEvent();
	public void setFkEvent(int fk);
	
	public String getName();
	public void setName(String name);
	
	public String getGuid();
	public void setGuid(String guid);
	
	public Access getAccess();
	public void setAccess(Access access);
}
