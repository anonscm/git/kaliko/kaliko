/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.common;

//import java.util.Map;

/**
 * The static methods of this class define how GUIDs are being assembled.
 * 
 * @author pneuha
 *
 */
public class ResourceGuidGenerator {
	
	// separator for guid schema
	private static final String SEPARATOR = ";";
	
	/**
	 * 
	 * @param type
	 * @param specifiers
	 * @return
	 */
	private static String getGenericGuid(String type, String... specifiers) {
		
		StringBuilder guid = new StringBuilder();
		guid.append(type);
		
		for (String s : specifiers)
			guid.append(SEPARATOR).append(s);
		
		return guid.toString();
	}
	
	/**
	 * The method takes a template in the form of "label[0]:label[1]:...:label[n-1]"
	 * where ':' is the separator as defined by <code>SEPARATOR</code> and replaces
	 * the label with the corresponding values from <code>mapping</code>
	 * 
	 * @param type
	 * @param template
	 * @param mapping
	 * @return
	 */
//	public static String getTemplateGuid(String type, String template, Map<String, String> mapping) {
//		
//		String [] labels = template.split(SEPARATOR);
//		
//		StringBuilder guid = new StringBuilder();
//		guid.append(type);
//		
//		for (String l : labels) {
//			if (! mapping.containsKey(l))
//				return null; // XXX throw exception?
//			guid.append(SEPARATOR).append(l);
//		}
//		
//		return guid.toString();
//	}

	/**
	 * 
	 * @param host
	 * @param port
	 * @param userID
	 * @return
	 */
	public static String getNuyoParticipantGuid(String host, String port, String userID) {
		return getGenericGuid("participant", host, port, userID);
	}

	/**
	 * 
	 * @param name
	 * @param email
	 * @return
	 */
	public static String getAdHocParicipantGuid(String name, String email) {
		return getGenericGuid("participant", name, email);
	}

	/**
	 * 
	 * @param locationID
	 * @return
	 */
	public static String getLocationGuid(String locationID) {
		return getGenericGuid("location", locationID);
	}

	/**
	 * 
	 * @return
	 */
	public static String getNoteGuid(int eventPk) {
		return getGenericGuid("note", Integer.toString(eventPk));
	}

	/**
	 * 
	 * @param hostUrl
	 * @param ldapSearchPath
	 * @param ldapUser
	 * @return
	 */
	public static String getOwnerGuid(String hostUrl, String ldapSearchPath, String ldapUser) {
		return getGenericGuid("owner", hostUrl, ldapSearchPath, ldapUser);
	}
}
