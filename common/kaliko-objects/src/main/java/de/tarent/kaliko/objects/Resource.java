/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects;

import java.util.Date;

/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/

public interface Resource {

	public static final String PROPERTY_PK = "pk";
	public static final String PROPERTY_FK_TYPES = "fkTypes";
	public static final String PROPERTY_FK_EVENT ="fkEvent";  
	public static final String PROPERTY_GUID = "guid";
	public static final String PROPERTY_TEXT = "text";
	public static final String PROPERTY_INFOURL = "infoUrl";
	public static final String PROPERTY_NOTIFICATIONTIME = "notificationTime";
	public static final String PROPERTY_NOTIFICATIONMESSAGE = "notificationMessage";
	public static final String PROPERTY_PARTICIPATIONSTATUS = "participationStatus";
	public static final String PROPERTY_PARTICIPATIONNOTE = "participationNote";
	public static final String PROPERTY_DELETED = "deleted";
	public static final String PROPERTY_UPDATEDATE = "updateDate";
	
	public static final String[] PROPERTIES = {
		PROPERTY_PK,
		PROPERTY_FK_TYPES,
		PROPERTY_FK_EVENT,
		PROPERTY_GUID,
		PROPERTY_TEXT,
		PROPERTY_INFOURL,
		PROPERTY_NOTIFICATIONTIME,
		PROPERTY_NOTIFICATIONMESSAGE,
		PROPERTY_PARTICIPATIONSTATUS,
		PROPERTY_PARTICIPATIONNOTE,
		PROPERTY_DELETED,
		PROPERTY_UPDATEDATE
		};
	
	public Date getUpdateDate();
	public void setUpdateDate(Date date);
	public boolean getDeleted();
	public void setDeleted(boolean deleted);
	public int getPk();
	public void setPk(int pk);
	
	public int getFkTypes();
	public void setFkTypes(int type);
	
	public int getFkEvent();
	public void setFkEvent(int fkEvent);
	
	public String getGuid();
	public void setGuid(String guid);

	public String getText();
	public void setText(String text);

	public String getInfoUrl();
	public void setInfoUrl(String infourl);

	public Date getNotificationTime();
	public void setNotificationTime(Date time);

	
	public String  getNotificationMessage();
	public void setNotificationMessage(String message);

	public int getParticipationStatus();
	public void setParticipationStatus(int status);
	
	public String getParticipationNote();
	public void setParticipationNote(String note);
	
}
