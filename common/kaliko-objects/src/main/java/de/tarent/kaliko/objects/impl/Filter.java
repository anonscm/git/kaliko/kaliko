/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import java.util.Date;

/** 
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class Filter {

	private boolean withOccurences;
	private boolean withResources;
	private boolean onlyUpdated;
	private Date updatedSince;
	private String location;
	private String type;
	private String title;
	private String category;
	private int start;
	private int limit;
	
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean getOnlyUpdated() {
		return onlyUpdated;
	}
	public void setOnlyUpdated(boolean onlyUpdated) {
		this.onlyUpdated = onlyUpdated;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getUpdatedSince() {
		return updatedSince;
	}
	public void setUpdatedSince(Date updatedSince) {
		this.updatedSince = updatedSince;
	}
	public boolean getWithOccurences() {
		return withOccurences;
	}
	public void setWithOccurences(boolean withOccurences) {
		this.withOccurences = withOccurences;
	}
	public boolean getWithResources() {
		return withResources;
	}
	public void setWithResources(boolean withResources) {
		this.withResources = withResources;
	}	
}
