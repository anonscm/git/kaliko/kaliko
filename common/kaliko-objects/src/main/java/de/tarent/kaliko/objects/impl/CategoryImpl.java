/*
 * Kaliko Common Objects,
 * Common Objects for the Kaliko Clients and Servers
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Common Objects'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.objects.impl;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;

/** This class represents a category. A category is not stored
 * in the server database. It is fetched from category providers
 * by the server. In the database there is only stored the relation
 * between events and categories (represented by class EventCategory).
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CategoryImpl implements Category {

	private int pk;
	private int fkEvent;
	private String name;
	private String guid;
	private Access access;
	
	public CategoryImpl() {
		this(null, null);
	}
	
	public CategoryImpl(String name, String guid) {
		this(name, guid, new AccessImpl());
	}
	
	public CategoryImpl(String name, String guid, Access access) {
		this.name = name;
		this.guid = guid;
		this.access = access;
	}
	
	public int getPk() {
		return this.pk;
	}

	public void setPk(int i) {
		this.pk = i;
	}

	public int getFkEvent() {
		return this.fkEvent;
	}
	
	public void setFkEvent(int fk) {
		this.fkEvent = fk;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Access getAccess() {
		return this.access;
	}

	public void setAccess(Access access) {
		this.access = access;
	}


	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Category anotherCat) {
		return getName().compareTo(anotherCat.getName());
	}
	
	/**
	 * 
	 * @param anObject the object to compare
	 * @return true if anObject is semantically equal to this
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object anObject) {
		if(this == anObject)
			return true;
		
		if(! (anObject instanceof Category))
			return false;
		
		if(this.getGuid() != null)
			return this.getGuid().equals(((Category)anObject).getGuid());
		
		return false;
	}
	
	
}