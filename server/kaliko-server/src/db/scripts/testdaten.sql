--Test Daten Script von Andre´ Biegel
--
-- deletes all data an inserts Testdata for the testing...

DELETE FROM categoryprovider;
DELETE FROM event_resource;
DELETE FROM event_category;
DELETE FROM occurence;
DELETE FROM types;
DELETE FROM event;

ALTER SEQUENCE categoryprovider_pk_SEQ RESTART 1; 
ALTER SEQUENCE event_resource_pk_resource_SEQ RESTART 1; 
ALTER SEQUENCE event_category_pk_category_SEQ RESTART 1; 
ALTER SEQUENCE occurence_pk_occurence_SEQ RESTART 1; 
ALTER SEQUENCE types_pk_types_SEQ RESTART 1;
ALTER SEQUENCE event_pk_event_SEQ RESTART 1;


--INSERT INTO occurence ()values();
--test data for the event table
INSERT INTO event (pk_event ,"type" , title , "category" , location , note ,deleted, creationdate ,updatedate ) values (DEFAULT,'test','test','test','test','test',false,DEFAULT,DEFAULT);
INSERT INTO event (pk_event ,"type" , title , "category" , location , note ,deleted, creationdate ,updatedate ) values (DEFAULT,'1test','1test','1test','1test','1test',false,DEFAULT,DEFAULT);
INSERT INTO event (pk_event ,"type" , title , "category" , location , note ,deleted, creationdate ,updatedate ) values (DEFAULT,'2test','2test','2test','2test','2test',false,DEFAULT,DEFAULT);
INSERT INTO event (pk_event ,"type" , title , "category" , location , note ,deleted, creationdate ,updatedate ) values (DEFAULT,'3test','3test','3test','3test','3test',false,DEFAULT,DEFAULT);
INSERT INTO event (pk_event ,type , title , "category" , location , note ,deleted, creationdate ,updatedate ) values (DEFAULT,'4test','4test','4test','4test','4test',false,DEFAULT,DEFAULT);
--testdata for the occurence table

INSERT INTO occurence (pk_occurence,fk_event,intervaltype,frequency , "type", updown,startdate ,enddate ,starttime ,endtime ,deleted, creationdate ,updatedate) values (DEFAULT,1,1,1,1,1,DEFAULT,DEFAULT,DEFAULT,DEFAULT,false,DEFAULT,DEFAULT);
INSERT INTO occurence (pk_occurence,fk_event,intervaltype,frequency , "type", updown,startdate ,enddate ,starttime ,endtime ,deleted, creationdate ,updatedate) values (DEFAULT,1,2,2,2,2,DEFAULT,DEFAULT,DEFAULT,DEFAULT,false,DEFAULT,DEFAULT);
INSERT INTO occurence (pk_occurence,fk_event,intervaltype,frequency , "type", updown,startdate ,enddate ,starttime ,endtime ,deleted, creationdate ,updatedate) values (DEFAULT,2,3,3,3,3,DEFAULT,DEFAULT,DEFAULT,DEFAULT,false,DEFAULT,DEFAULT);
INSERT INTO occurence (pk_occurence,fk_event,intervaltype,frequency , "type", updown,startdate ,enddate ,starttime ,endtime ,deleted, creationdate ,updatedate) values (DEFAULT,3,4,4,4,4,DEFAULT,DEFAULT,DEFAULT,DEFAULT,false,DEFAULT,DEFAULT);
INSERT INTO occurence (pk_occurence,fk_event,intervaltype,frequency , "type", updown,startdate ,enddate ,starttime ,endtime ,deleted, creationdate ,updatedate) values (DEFAULT,4,5,5,5,5,DEFAULT,DEFAULT,DEFAULT,DEFAULT,false,DEFAULT,DEFAULT);


--testdata for the types table
INSERT INTO types (pk_types ,urn,name,creationdate,updatedate ) values (DEFAULT,'urn1','test',DEFAULT,DEFAULT);
INSERT INTO types (pk_types ,urn,name,creationdate,updatedate ) values (DEFAULT,'urn2','1test',DEFAULT,DEFAULT);
INSERT INTO types (pk_types ,urn,name,creationdate,updatedate ) values (DEFAULT,'urn3','2test',DEFAULT,DEFAULT);
INSERT INTO types (pk_types ,urn,name,creationdate,updatedate ) values (DEFAULT,'urn4','3test',DEFAULT,DEFAULT);
INSERT INTO types (pk_types ,urn,name,creationdate,updatedate ) values (DEFAULT,'urn5','4test',DEFAULT,DEFAULT);

--testdata for the resource table
INSERT INTO "event_resource" (pk_resource,"type" ,fk_event ,guid , text,url,notificationtime, notificationmessage,participationstatus,participationnote,deleted,creationdate,updatedate) values (DEFAULT,1,1,'test','test','test',DEFAULT,'test',1,'test',false,DEFAULT,DEFAULT);
INSERT INTO "event_resource" (pk_resource,"type" ,fk_event ,guid , text,url,notificationtime, notificationmessage,participationstatus,participationnote,deleted,creationdate,updatedate) values (DEFAULT,1,1,'test','test','test',DEFAULT,'1test',1,'test',false,DEFAULT,DEFAULT);
INSERT INTO "event_resource" (pk_resource,"type" ,fk_event ,guid , text,url,notificationtime, notificationmessage,participationstatus,participationnote,deleted,creationdate,updatedate) values (DEFAULT,1,1,'test','test','test',DEFAULT,'2test',1,'test',false,DEFAULT,DEFAULT);
INSERT INTO "event_resource" (pk_resource,"type" ,fk_event ,guid , text,url,notificationtime, notificationmessage,participationstatus,participationnote,deleted,creationdate,updatedate) values (DEFAULT,1,1,'test','test','test',DEFAULT,'3test',1,'test',false,DEFAULT,DEFAULT);
INSERT INTO "event_resource" (pk_resource,"type" ,fk_event ,guid , text,url,notificationtime, notificationmessage,participationstatus,participationnote,deleted,creationdate,updatedate) values (DEFAULT,1,1,'test','test','test',DEFAULT,'4test',1,'test',false,DEFAULT,DEFAULT);


--testdata for the event_catagory table

INSERT INTO event_category (pk_category,fk_event ,guid ,name)values(DEFAULT,1,'guid0','name0');
INSERT INTO event_category (pk_category,fk_event ,guid ,name)values(DEFAULT,1,'guid1','name1');
INSERT INTO event_category (pk_category,fk_event ,guid ,name)values(DEFAULT,1,'guid2','name2');
INSERT INTO event_category (pk_category,fk_event ,guid ,name)values(DEFAULT,1,'guid3','name3');
INSERT INTO event_category (pk_category,fk_event ,guid ,name)values(DEFAULT,1,'guid4','name4');

--testdata for the categoryprovider table
INSERT INTO categoryprovider (pk, storageinformation, classname)values(DEFAULT,'Kategorie 1,Kategorie 2,Kategorie 3,Kategorie 4,Kategorie 5','de.tarent.kaliko.categories.provider.MockupCategoryProvider');