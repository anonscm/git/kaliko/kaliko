/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v4.1.1                     */
/* Target DBMS:           PostgreSQL 8                                    */
/* Project file:          calendar02.dez                                  */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database creation script                        */
/* Created on:            2008-06-27 13:53                                */
/* ---------------------------------------------------------------------- */


CREATE LANGUAGE plpgsql;

/* ---------------------------------------------------------------------- */
/* Tables                                                                 */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add table "event"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE event (
    pk_event SERIAL CONSTRAINT NN_pk_event NOT NULL,
    type CHARACTER VARYING(40),
    title CHARACTER VARYING(200),
    deleted BOOLEAN DEFAULT false,
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_event PRIMARY KEY (pk_event)
);

/* ---------------------------------------------------------------------- */
/* Add table "event_resource"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE event_resource (
    pk_resource SERIAL CONSTRAINT NN_pk_resource NOT NULL,
    fk_types INTEGER CONSTRAINT NN_fk_types NOT NULL,
    fk_event INTEGER CONSTRAINT NN_fk_event NOT NULL,
    guid CHARACTER VARYING(250) CONSTRAINT NN_guid NOT NULL,
    text CHARACTER VARYING(250),
    infourl CHARACTER VARYING(250),
    notificationtime TIMESTAMP,
    notificationmessage CHARACTER VARYING(200),
    participationstatus INTEGER,
    participationnote CHARACTER VARYING(200),
    deleted BOOLEAN DEFAULT false,
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_event_resource PRIMARY KEY (pk_resource)
);

/* ---------------------------------------------------------------------- */
/* Add table "types"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE types (
    pk_types SERIAL CONSTRAINT NN_pk_types NOT NULL,
    urn CHARACTER VARYING(60) CONSTRAINT NN_urn NOT NULL,
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_types PRIMARY KEY (pk_types),
    CONSTRAINT TUC_types_1 UNIQUE (urn)
);

/* ---------------------------------------------------------------------- */
/* Add table "occurence"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE occurence (
    pk_occurence SERIAL CONSTRAINT NN_pk_occurence NOT NULL,
    fk_event INTEGER CONSTRAINT NN_fk_event NOT NULL,
    intervalType INTEGER,
    frequency INTEGER,
    type INTEGER,
    updown INTEGER,
    startdate DATE,
    enddate DATE,
    starttime TIME WITH TIME ZONE,
    endtime TIME WITH TIME ZONE,
    deleted BOOLEAN DEFAULT false,
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_occurence PRIMARY KEY (pk_occurence)
);

/* ---------------------------------------------------------------------- */
/* Add table "event_category"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE event_category (
    pk_category SERIAL CONSTRAINT NN_pk_category NOT NULL,
    fk_event INTEGER CONSTRAINT NN_fk_event NOT NULL,
    guid CHARACTER VARYING(40),
    name CHARACTER VARYING(200),
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_event_category PRIMARY KEY (pk_category)
);

/* ---------------------------------------------------------------------- */
/* Add table "categoryprovider"                                           */
/* ---------------------------------------------------------------------- */

CREATE TABLE categoryprovider (
    pk_categoryprovider SERIAL CONSTRAINT NN_pk_categoryprovider NOT NULL,
    storageInformation CHARACTER VARYING(250),
    classname CHARACTER VARYING(250),
    creationdate TIMESTAMP,
    updatedate TIMESTAMP,
    CONSTRAINT PK_categoryprovider PRIMARY KEY (pk_categoryprovider)
);

/* ---------------------------------------------------------------------- */
/* Foreign key constraints                                                */
/* ---------------------------------------------------------------------- */

ALTER TABLE event_resource ADD CONSTRAINT types_event_resource 
    FOREIGN KEY (fk_types) REFERENCES types (pk_types);

ALTER TABLE event_resource ADD CONSTRAINT event_event_resource 
    FOREIGN KEY (fk_event) REFERENCES event (pk_event);

ALTER TABLE occurence ADD CONSTRAINT event_occurence 
    FOREIGN KEY (fk_event) REFERENCES event (pk_event);

ALTER TABLE event_category ADD CONSTRAINT event_event_category 
    FOREIGN KEY (fk_event) REFERENCES event (pk_event);

/* ---------------------------------------------------------------------- */
/* Procedures                                                             */
/* ---------------------------------------------------------------------- */

CREATE FUNCTION stampCreate() RETURNS TRIGGER AS $stamp$
BEGIN
NEW.creationdate := current_timestamp;
NEW.updatedate := current_timestamp;
RETURN NEW;
END;
$stamp$ LANGUAGE plpgsql;;

CREATE FUNCTION stampUpdate() RETURNS TRIGGER AS $stamp$
BEGIN
NEW.updatedate := current_timestamp;
RETURN NEW;
END;
$stamp$ LANGUAGE plpgsql;;

/* ---------------------------------------------------------------------- */
/* Triggers                                                               */
/* ---------------------------------------------------------------------- */

CREATE TRIGGER trg_stampBeforeUpdate_event
BEFORE UPDATE ON event FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_event
BEFORE INSERT ON event FOR EACH ROW EXECUTE PROCEDURE stampCreate();;

CREATE TRIGGER trg_stampBeforeUpdate_event_resource
BEFORE UPDATE ON event_resource FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_event_resource
BEFORE INSERT ON event_resource FOR EACH ROW EXECUTE PROCEDURE stampCreate();;

CREATE TRIGGER trg_stampBeforeUpdate_types
BEFORE UPDATE ON types FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_types
BEFORE INSERT ON types FOR EACH ROW EXECUTE PROCEDURE stampCreate();;

CREATE TRIGGER trg_stampBeforeUpdate_occurence
BEFORE UPDATE ON occurence FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_occurence
BEFORE INSERT ON occurence FOR EACH ROW EXECUTE PROCEDURE stampCreate();;

CREATE TRIGGER trg_stampBeforeUpdate_event
BEFORE UPDATE ON event_category FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_occurence
BEFORE INSERT ON event_category FOR EACH ROW EXECUTE PROCEDURE stampCreate();;

CREATE TRIGGER trg_stampBeforeUpdate_event
BEFORE UPDATE ON categoryprovider FOR EACH ROW EXECUTE PROCEDURE stampUpdate();;

CREATE TRIGGER trg_stampBeforeCreate_occurence
BEFORE INSERT ON categoryprovider FOR EACH ROW EXECUTE PROCEDURE stampCreate();;
