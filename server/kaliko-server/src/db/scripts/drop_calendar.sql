/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v4.1.1                     */
/* Target DBMS:           PostgreSQL 8                                    */
/* Project file:          calendar02.dez                                  */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database drop script                            */
/* Created on:            2008-06-27 13:53                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop triggers                                                          */
/* ---------------------------------------------------------------------- */

DROP TRIGGER trg_stampBeforeUpdate_event;

DROP TRIGGER trg_stampBeforeCreate_event;

DROP TRIGGER trg_stampBeforeUpdate_event_resource;

DROP TRIGGER trg_stampBeforeCreate_event_resource;

DROP TRIGGER trg_stampBeforeUpdate_types;

DROP TRIGGER trg_stampBeforeCreate_types;

DROP TRIGGER trg_stampBeforeUpdate_occurence;

DROP TRIGGER trg_stampBeforeCreate_occurence;

DROP TRIGGER trg_stampBeforeUpdate_event;

DROP TRIGGER trg_stampBeforeCreate_occurence;

DROP TRIGGER trg_stampBeforeUpdate_event;

DROP TRIGGER trg_stampBeforeCreate_occurence;

/* ---------------------------------------------------------------------- */
/* Drop procedures                                                        */
/* ---------------------------------------------------------------------- */

DROP PROCEDURE stampCreate();

DROP PROCEDURE stampUpdate();

/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */

ALTER TABLE event_resource DROP CONSTRAINT types_event_resource;

ALTER TABLE event_resource DROP CONSTRAINT event_event_resource;

ALTER TABLE occurence DROP CONSTRAINT event_occurence;

ALTER TABLE event_category DROP CONSTRAINT event_event_category;

/* ---------------------------------------------------------------------- */
/* Drop table "event"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE event DROP CONSTRAINT PK_event;

/* Drop table */

DROP TABLE event;

/* ---------------------------------------------------------------------- */
/* Drop table "event_resource"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE event_resource DROP CONSTRAINT PK_event_resource;

/* Drop table */

DROP TABLE event_resource;

/* ---------------------------------------------------------------------- */
/* Drop table "types"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE types DROP CONSTRAINT PK_types;

ALTER TABLE types DROP CONSTRAINT TUC_types_1;

/* Drop table */

DROP TABLE types;

/* ---------------------------------------------------------------------- */
/* Drop table "occurence"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE occurence DROP CONSTRAINT PK_occurence;

/* Drop table */

DROP TABLE occurence;

/* ---------------------------------------------------------------------- */
/* Drop table "event_category"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE event_category DROP CONSTRAINT PK_event_category;

/* Drop table */

DROP TABLE event_category;

/* ---------------------------------------------------------------------- */
/* Drop table "categoryprovider"                                          */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE categoryprovider DROP CONSTRAINT PK_categoryprovider;

/* Drop table */

DROP TABLE categoryprovider;
