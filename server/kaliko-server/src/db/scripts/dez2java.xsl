<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java"
exclude-result-prefixes="java" xmlns:redirect="org.apache.xalan.xslt.extensions.Redirect" extension-element-prefixes="redirect">
	<!-- load properties.xml as variable prop -->
	<xsl:variable name="prop" select="document('dez2java_properties.xml')"/>
	<!-- set output format and encoding -->
	<xsl:output indent="no" method="text" encoding="UTF-8"/>	
	
	<xsl:template match="dezign">
		<!-- select the last created version -->
		<xsl:apply-templates select="VERSION[last()]/DATADICT"/>
	</xsl:template>
	
	<xsl:template match="DATADICT">
		<xsl:apply-templates select="ENTITIES"></xsl:apply-templates>
	</xsl:template>

	<xsl:template match="ENTITIES">		
		<xsl:apply-templates select="ENT"/>		
	</xsl:template>
	
	<xsl:template match="ENT">
		<xsl:variable name="firstChar" select ="substring(NAME,1,1)"/>
		<xsl:variable name="restChars" select ="substring(NAME,2,string-length(NAME))"/>
		<xsl:variable name="interfaceName"><xsl:value-of select="translate($firstChar,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/><xsl:value-of select="$restChars"/></xsl:variable>
 		<xsl:variable name="outputFileName"><xsl:value-of select="concat($prop/properties/file_location,$prop/properties/file_name_prefix,$interfaceName,'.java')"/></xsl:variable>
    	<redirect:write select="$outputFileName">
/*
 * Created on <xsl:value-of select="java:format(java:java.text.SimpleDateFormat.new('dd.MM.yyyy'), java:java.util.Date.new())"/>
 */
package <xsl:value-of select="$prop/properties/package_name"/>;

<xsl:apply-templates select="$prop/properties/imports"/>

/**
 * <xsl:value-of select="$prop/properties/description"/>
 * @author <xsl:value-of select="$prop/properties/author"/>
 */
<xsl:value-of select="$prop/properties/class_prefix"/><xsl:text disable-output-escaping="yes"> </xsl:text><xsl:value-of select="$prop/properties/file_name_prefix"/><xsl:value-of select="$interfaceName"/> {
	/**
	 * <xsl:apply-templates select="DESC"/>
	 */
	<xsl:value-of select="$prop/properties/variable_prefix"/><xsl:text disable-output-escaping="yes"> </xsl:text>TABLENAME="<xsl:value-of select="NAME"/>";
	<xsl:apply-templates select="ATTRIBUTES"/>
}
		</redirect:write>
	</xsl:template>
	
	<xsl:template match="ATTRIBUTES">
		<xsl:apply-templates select="ATTR"/>
	</xsl:template>
	
	<xsl:template match="ATTR">
	/** 
	 * <xsl:apply-templates select="DESC"/>
	 * DATA TYPE <xsl:apply-templates select="DT"/>
	 */
	<xsl:value-of select="$prop/properties/variable_prefix"/><xsl:text disable-output-escaping="yes"> </xsl:text><xsl:value-of select="$prop/properties/column_prefix"/><xsl:value-of select="translate(NAME,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>="<xsl:value-of select="NAME"/>";
	<xsl:value-of select="$prop/properties/variable_prefix"/><xsl:text disable-output-escaping="yes"> </xsl:text><xsl:value-of select="translate(NAME,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>="<xsl:value-of select="concat(../../NAME,'.',NAME)"/>";
	</xsl:template>
	
	<xsl:template match="DT">
		<!-- Data type name -->
		<xsl:value-of select="DTLISTNAME"></xsl:value-of>
		<!-- the dyta type length will be added only if specified -->
		<xsl:if test="LE !=''">
		<xsl:text>(</xsl:text>
		<xsl:value-of select="LE"></xsl:value-of>
		<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="NAME">
		<xsl:value-of select="."></xsl:value-of>
	</xsl:template>
	
	<xsl:template match="DESC">
		<xsl:value-of select="."></xsl:value-of>
	</xsl:template>

	<xsl:template match="imports">
		<xsl:for-each select="import">
			<xsl:if test=".!=''">
import <xsl:value-of select="."/>;
			</xsl:if>
		</xsl:for-each>
	</xsl:template>	
</xsl:stylesheet>
