///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.List;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Type;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
///**
// * @author abiege
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class TypeWorkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//
//	
//	@Test
//	public void testGetTypes(){
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getTypes").invoke();
//		
//		List<Type> types = (List<Type>) res.getData("resourceTypes");
//		
//		assertTrue(types.size() == 5);
//		assertTrue(types.get(0).getName().equals("test"));
//		assertTrue(types.get(0).getUrn().equals("urn1"));
//		assertTrue(types.get(4).getName().equals("4test"));
//		assertTrue(types.get(4).getUrn().equals("urn5"));
//	}
//	
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
