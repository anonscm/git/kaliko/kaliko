/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * Everybody has 2 Categories one privat and an public.
 * request will get his and the publics off others 
 * 
 */
package de.tarent.kaliko.categories.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.CategoryProvider;
import de.tarent.kaliko.objects.impl.AccessImpl;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.CategoryImpl;
import de.tarent.ldap.LdapProxy;
import de.tarent.ldap.query.LdapQueryString;
import de.tarent.ldap.query.QueryObject;
import de.tarent.ldap.user.LdapUser;


/**
 * @author Andre ´ Biegel,tarent GmbH, Fabian K&ouml;ster (f.koester@tarent.de), Peter Neuhaus tarent GmbH Bonn
 *
 */
public class LDAPCategoryProvider implements CategoryProvider {

	private String storageInformation;
//	private String uniqueCategoryId ="LDAPCatProv_";
	private static final String uniqueCategoryId = "LDAPCategoryProvider";
	
	protected final static Logger logger = Logger.getLogger(LDAPCategoryProvider.class);

	public CategoriesList getCategories(String username, Access rights) {
		
		CategoriesList result = new CategoriesList();
		
		// XXX
		if (rights.isReadable() && rights.isWritable())
			return getCateoriesForReadAndWriteRequest(username,result,rights);
		if (rights.isReadable())
			return getCateggoriesForReadableRequest(username,result,rights);
		if (rights.isWritable())
			return getCategoriesForWriteableRequest(username,result,rights);
	
		return result;
	}
	
	private CategoriesList getCateoriesForReadAndWriteRequest(String username, CategoriesList result, Access rights) {
		logger.debug("getCateoriesForReadAndWriteRequest()");
		result = addPublicCategories(result,username);
		result = addPublicOwnCategory(result,rights,username);
		result = addPersonalCategories(result,rights,username);
		return result;
	}
	
	private CategoriesList getCategoriesForWriteableRequest(String username, CategoriesList result, Access rights) {
		logger.debug("getCategoriesForWriteableRequest()");
		//result = addPublicCategories(result);
		result = addPersonalCategories(result,rights,username);
		result = addPublicOwnCategory(result,rights,username);
		
//		logger.debug("getCategoriesForWriteableRequest() user: " + username);
//		
//		for (Category category : result)
//			logger.debug(category.getName());
		
		return result;
	}

	private CategoriesList getCateggoriesForReadableRequest(String username, CategoriesList result, Access rights) {
		logger.debug("getCateggoriesForReadableRequest()");
		result = addPublicCategories(result,username);
		result = addPublicOwnCategory(result,rights,username);
		result = addPersonalCategories(result,rights,username);
		
//		logger.debug("getCateggoriesForReadableRequest() user: " + username);
//		
//		for (Category category : result)
//			logger.debug(category.getName());
		
		return result;
	}
	
	private CategoriesList addPublicOwnCategory(CategoriesList result, Access rights, String username ) {
		
		String ldapServerURL = getLdapServerURL();
		LdapProxy proxy = LdapProxy.getInstance(ldapServerURL, getLdapSearchBases());
		String userCn = (String) proxy.getAttributes((String)proxy.getDn(username, "uid").get(0)).get("cn");
		if (userCn == null) // fallback, if cn not defined
			userCn = username;
				
		rights.setReadable(true);
		rights.setWritable(true);
		Category personalPrivat = new CategoryImpl();
		personalPrivat.setName(userCn + " PUBLIC");
		personalPrivat.setAccess(rights);
		personalPrivat.setGuid(getBasicGuid(username)+"PUBLIC");
		result.add(personalPrivat);
		
//		logger.debug("addPublicOwnCategory(): category name: " + personalPrivat.getName());

		return result;
	}
	
	private CategoriesList addPersonalCategories(CategoriesList result, Access rights, String username) {
		
		String ldapServerURL = getLdapServerURL();
		LdapProxy proxy = LdapProxy.getInstance(ldapServerURL, getLdapSearchBases());
		String userCn = (String) proxy.getAttributes((String)proxy.getDn(username, "uid").get(0)).get("cn");
		if (userCn == null) // fallback, if cn not defined
			userCn = username;
				
		Category personalPrivat = new CategoryImpl();
		personalPrivat.setName(userCn + " PRIVAT");
		personalPrivat.setAccess(rights);
		personalPrivat.setGuid(getBasicGuid(username)+"PRIVAT");
		result.add(personalPrivat);
		
//		logger.debug("addPersonalCategories(): category name: " + personalPrivat.getName());
		
		return result;
	}

	/**
	 * generates public category for each LDAP-user
	 * 
	 * @param result
	 * @param username
	 * @return
	 */
	private CategoriesList addPublicCategories(CategoriesList result,String username) {
		
		Iterator<LdapUser> user = getUserListFromLDAP().iterator();

		while (user.hasNext()) {
			LdapUser element = (LdapUser) user.next();
			
			if (element.getAttribute("uid") == null)
				continue;
			
			if (element.getAttribute("uid").equals(username))
				continue;
			
			Category publicCat = new CategoryImpl(
					element.getAttribute("cn") + " PUBLIC",
					getBasicGuid(element.getAttribute("uid")) + "PUBLIC",
					new AccessImpl(true, element.getAttribute("uid").equals(username)));
			
//			logger.debug("addPublicCategories(): category name: " + publicCat.getName());
			
			result.add(publicCat);
		}
		return result;
	}

	private List<LdapUser> getUserListFromLDAP() {
		String ldapServerURL = getLdapServerURL();
		
		logger.debug("Trying to retrieve categories from LDAP with address " + ldapServerURL);
		
		LdapProxy proxy = LdapProxy.getInstance(ldapServerURL, getLdapSearchBases());
		
		Iterator<String> userDns = proxy.getDn(getLDAPQuery()).iterator();

		List<LdapUser> userList = new ArrayList<LdapUser>();
		
		while(userDns.hasNext())
			userList.add(new LdapUser(proxy, userDns.next()));
		
		logger.info("Retrieved " + userList.size() + " users from LDAP " + ldapServerURL + ".");
		
		return userList;
	}
	
	private String getLdapServerURL() {
		return getFieldFromStorageInformation(0);
	}
	
	private List<String> getLdapSearchBases() {
	
		String [] bases = getFieldFromStorageInformation(1).split(";");
		
//		for (String base : bases)
//			logger.debug("getLdapSearchBases() base: " + base);
		
		return Arrays.asList(bases);
	}
	
	private QueryObject getLDAPQuery() {
		String ldapQueryString = getFieldFromStorageInformation(2);
		
		if(ldapQueryString != null)
			return new LdapQueryString(ldapQueryString);
		else
			return new LdapQueryString("(objectClass=*)");
//			return new LdapQuery();
	}
	
	/**
	 * LDAPCategoryProvider is configured via an database-entry in table 'categoryprovider'. In the column 'storageinformation'
	 * all configuration-details are stored. This string has the following format:
	 * eventCategory
	 * URL;;searchBases;;filterExpressions
	 * 
	 * This methods returns the content of the field 
	 * 
	 * 'URL' if 0 is given as fieldIndex,
	 * 'searchBases' if 1 is given as fieldIndex or
	 * 'filterExpressions' if 2 is given as fieldIndex.
	 *  
	 * 
	 * @param fieldIndex
	 * @return
	 */
	private String getFieldFromStorageInformation(int fieldIndex) {
		if(storageInformation == null) 
			throw new RuntimeException("LDAP Category Provider is not configured properly, storageInformation is not set.");
		
		try {
			return storageInformation.split(";;")[fieldIndex];
		} catch(IndexOutOfBoundsException excp) {
			
			// If fieldIndex is greater than 1 the field is not mandatory so just return null.
			// Otherwise throw an exception because not all needed configuration-data is present.
			if(fieldIndex > 1)
				return null;
			else
				throw new RuntimeException("LDAP Category Provider is not configured properly. "+storageInformation);
		}
	}
	
	public void setStorageInformation(String information) {
		this.storageInformation = information ; 
	}
	
	// TODO: put to common.GuidGenerator
	private String getBasicGuid(String uid){
		return  uniqueCategoryId + ":" + uid +":";
	}
}
