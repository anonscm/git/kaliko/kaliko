/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.persistence.ParamList;
import de.tarent.dblayer.sql.SQL;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.factory.ResourceEntityFactory;
import de.tarent.kaliko.mapping.ResourceDBMapping;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;
import de.tarent.kaliko.objects.Type;
import de.tarent.kaliko.objects.impl.ResourceImpl;
import de.tarent.kaliko.objects.impl.ResourcesList;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;
/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/

public class ResourceDAO extends AbstractDAO {

	private static ResourceDAO instance = null;
	
	ResourceDBMapping mapping = new ResourceDBMapping(CalendarDBAccess.getContextWithoutConnection());
	ResourceEntityFactory entityFactory = ResourceEntityFactory.getInstance();
	
	protected final static Logger logger = Logger.getLogger(ResourceDAO.class);
	
	private ResourceDAO(){
		super();
		setDbMapping(mapping);
		setEntityFactory(entityFactory); 
	}
	public static synchronized ResourceDAO getInstance() {
				
		if ( instance == null )
			 instance = new ResourceDAO();

		return instance;
	}

	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((ResourceImpl)entity).setPk(keys.getPk());
	}
	
	/** method returns a list of resources of an predefined event key(Primary key)
	 *  
	 * @author Andre´ Biegel ,tarent GmbH
	 * @param dbc DBContext
	 * @param begin start of the interval
	 * @param end end of the interval
	 * @return  List   list of resources
	 */

	public List<Resource> getResourcesByEventPk(DBContext dbc, int pkEvent) throws SQLException {
		return (List<Resource>) getEntityList(dbc,ResourceDBMapping.STMT_SELECT_RESOURCES_BY_EVENT_PK,Event.PROPERTY_PK,pkEvent);
	}

	/**These Method updates an Resource item in the DB by firstly checking which detail is really updated. After that the newentry is then updated in the DB
	 * 
	 * @author Andre´ Biegel
	 * @param  newentry an new Occurence Object with maybe new details
	 * @param  old  old Occurence entry 
	 * @throws SQLException 
	 * @throws ObjectSynchronizationError updatedate null or hole object not synchronized
	 * @throws IllegalObjectCreationException indiates an missing FK for event
	 */
	public void updateEntity(DBContext dbc, Resource newentry) throws SQLException, ObjectSynchronizationException{
		//x is the Not-Updated-Flag	
		int x = -1;
		
		Resource old = ResourceDAO.getInstance().getResourceByPk(dbc,newentry.getPk());
		//if (newentry.getFkEvent() == x)throw new IllegalObjectCreationException("the fk event in this Occurence´is not set");
		if (old.getUpdateDate().after(newentry.getUpdateDate())) {
			throw new ObjectSynchronizationException(
					"Occurence is not synchronized");
		}
		if (newentry.getPk() != old.getPk()) {
			update(dbc, old);
		} else {
			if (newentry.getGuid() == null) {
				newentry.setGuid(old.getGuid());
			}
			if (newentry.getText() == null) {
				newentry.setText(old.getText());
			}
			if (newentry.getInfoUrl() == null) {
				newentry.setInfoUrl(old.getInfoUrl());
			}
			if (newentry.getNotificationTime() == null) {
				newentry.setNotificationTime(old.getNotificationTime());
			}
			if (newentry.getNotificationMessage() == null) {
				newentry.setNotificationMessage(old.getNotificationMessage());
			}
			if (newentry.getParticipationNote() == null) {
				newentry.setParticipationNote(old.getParticipationNote());
			}
			if (newentry.getParticipationStatus() == x) {
				newentry.setParticipationStatus(old.getParticipationStatus());
			}
			if (newentry.getFkEvent() == 0) {
				newentry.setFkEvent(old.getFkEvent());
			}
			/*
			 * if (newentry.getCreationDate()== null){
			 * newentry.setCreationDate(old.getCreationDate()); }
			 */
			if (newentry.getUpdateDate() == null) {
				newentry.setUpdateDate(old.getUpdateDate());
			}
 			 
			update(dbc, newentry);	
		}
	}

	public List<Integer> createResources(DBContext dbc, List<Resource> newentries)
			throws SQLException, IllegalObjectCreationException {
		
		logger.debug("calling createResources()");
		List<Integer> res = new LinkedList<Integer>();
		
		for (Resource newResource : newentries) {
			logger.debug(
					"fkEvent: '" + newResource.getFkEvent()
					+ "', fkTypes: '" + newResource.getFkTypes() 
					+ "', guid: '" + newResource.getGuid());
			
			
			if (newResource.getFkEvent() == 0)
				throw new IllegalObjectCreationException("FkEvent not set while creating  an Occurence");
				
			
			// The plain text resources 'location' and 'note' need to get an unique guid so we append the fkEvent to it.
			// They also need a valid type!
			
			if(newResource.getGuid().startsWith(Event.RESOURCE_LOCATION_0_PREFIX))
				preparePlainTextResource(newResource, "plain_location");
			else if(newResource.getGuid().startsWith(Event.RESOURCE_NOTE_0_PREFIX))
				preparePlainTextResource(newResource, "plain_note");
			
			insert(dbc, newResource);
			res.add(newResource.getPk());
		}
		
		return res;
	}
	
	private void preparePlainTextResource(Resource newResource, String typeID) throws SQLException {
		newResource.setGuid(newResource.getGuid().concat(String.valueOf(newResource.getFkEvent())));
		
		Type type = TypeDAO.getInstance().getTypeByUrn(CalendarDBAccess.getManagedContext(), typeID);
		
		if(type == null)
			throw new RuntimeException("Cannot at resource "+ newResource.getText()+": The type with ID "+typeID+" is not configured in database!");
			
		newResource.setFkTypes(type.getPk());
	}

	private Resource getResourceByPk(DBContext dbc, int pk) throws SQLException {
	    return (Resource)getEntityByIdFilter(dbc, ResourceDBMapping.STMT_SELECT_RESOURCE_BY_PK,Resource.PROPERTY_PK,pk);
		
	}
	
	/** Deletes an resource by setting the deleted flag.
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param resourcePk primary key that identifies the resource to delete
	 * @throws SQLException
	 */
	
	public void deleteResource(DBContext dbc, int resourcePk) throws SQLException{
		SQL.Update(dbc)
				.table(DBCEvent_resource.TABLENAME)
				.update(DBCEvent_resource.DELETED, "true")
				.whereAndEq(DBCEvent_resource.PK_RESOURCE, resourcePk)
				.executeUpdate(dbc);
	}
	
	/** Checks wether resources are used in that time interval somebody is going to use it 
	 * 
	 * @author Andre´ Biegel ,tarent GbmH
	 * @param dbc Database Context
	 * @param resourceGuid resource Guid which has to be checked wehter its available
	 * @param startdate Occurence startdate, which specifies the beginning of the time interval 
	 * @param enddate Occuernce enddate, which specifies the end  of the time interval
	 * @return Boolean true if it can be added, false if the resource is planned for an event somwhere else
	 * @throws SQLException 
	 * 
	 */
	public boolean checkResourceUsedInTimeOccurence(DBContext dbc,String resourceGuid, Date startdate, Date enddate) throws SQLException{
		ParamList param  = new ParamList()
				.add(Resource.PROPERTY_GUID, resourceGuid)
				.add(Occurence.PROPERTY_STARTDATE, startdate )
				.add(Occurence.PROPERTY_ENDDATE, enddate );
		List<Resource> dbres =  getEntityList(dbc, ResourceDBMapping.STMT_SELECT_RESUORCE_PKS_BY_STARTDATE_AND_ENDDATE, param);
        if (dbres.size() > 0)return false;
        else return true;
	}
	/**Method checks resource availbilities und returning colliding resource guids
	 * @author Andre´ Biegel ,tarent GmbH
	 * @param dbc DataBase Context
	 * @param newentries  List resource entries which should be added (Event fk have to be set )
	 * @return List of Resource Guids which aren´t available the the specified time interval or if there is no collisionn then am emtpy list 
	 * @throws SQLException
	 */
	public List<String> checkResourceAvailibility(DBContext dbc,ResourcesList newentries) throws SQLException {
		
		List<String> guids  = new LinkedList<String>();
		List<Integer> pks = new LinkedList<Integer>();
		Boolean check = true, checking = true;
		// filling pks
		pks.add(new Integer(newentries.get(0).getFkEvent()));
		for (int i = 0; i < newentries.size(); i++) {
			if (!isIn(newentries.get(i).getFkEvent(),pks)) {
			pks.add(new Integer(newentries.get(i).getFkEvent()));
			}
		}
		List<Event> evs = EventDAO.getInstance().getEventsList(dbc, pks);
		Event tmp ;
		for (int i = 0; i < newentries.size(); i++) {
			//get event
			tmp = searchEventbyEventPk(newentries.get(i).getFkEvent(), evs);
			//check resource for earch occurence
			for (int j = 0; j < tmp.getOccurencesList().size(); j++) {
				//check resource
				checking = checkResourceUsedInTimeOccurence(dbc, newentries.get(i).getGuid(), tmp.getOccurencesList().get(i).getStartDate(),tmp.getOccurencesList().get(i).getEndDate());
				//gathering guids if false and testing results
				if (checking == false) guids.add(newentries.get(i).getGuid());
				check  = check && checking;
			} 	
			
		}	
		return guids;
	}

	private Event searchEventbyEventPk(int pk, List<Event> evs) {
		for (int i = 0; i < evs.size(); i++) {
			if(evs.get(i).getPk()== pk){
				return evs.get(i);
			}
		}
		return null;
	}
	
	private boolean isIn(int Pk, List<Integer>  list ) {
		for (int i = 0; i < list.size(); i++)
			if(list.get(i).intValue() == Pk)return true;	
		return false;
	}
	/** Deletes all resources of one event.
	 * Resources of one event are identified by its fk_event
	 * 
	 * @param dbc
	 * @param fkEvent the foreign key that identifies the resources to delete
	 */
	public void deleteAllResourcesForGivenEvent(DBContext dbc, int fkEvent) {
		// TODO
	}

}
