/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.factory;

import de.tarent.commons.datahandling.entity.AttributeSource;
import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.LookupContext;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.impl.CategoryImpl;
/**
*  @author Andre´ Biegel, tarent GmbH
*/
public class CategoryEntityFactory extends DefaultEntityFactory {

	private static CategoryEntityFactory instance;
	
	private CategoryEntityFactory() {
		 super(CategoryImpl.class);
	}
	
	public static synchronized CategoryEntityFactory getInstance(){
		
		if (instance == null) {
			instance = new CategoryEntityFactory();
		}
		return instance;
	}
	/**
     * Template method for returning an Entity from the LookupContext. Default implementation allways returns null.
     */
    public Object getEntityFromLookupContext(AttributeSource as, LookupContext lc) {
    	return lc.getEntity(as.getAttribute(Category.PROPERTY_GUID), CategoryImpl.class.getName());
    }
    /**
     * Default implementation for template method. This implementaoin does not store the entity.
     */
    public void storeEntityInLookupContext(Object entity, LookupContext lc) {
        lc.registerEntity(((CategoryImpl)entity).getPk(), CategoryImpl.class.getName(), entity);
    }

}
