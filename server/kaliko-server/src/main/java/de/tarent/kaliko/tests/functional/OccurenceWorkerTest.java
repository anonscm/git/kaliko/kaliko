///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Date;
//import java.util.LinkedList;
//import java.util.List;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Occurence;
//import de.tarent.kaliko.objects.impl.OccurenceImpl;
//import de.tarent.kaliko.objects.impl.OccurencesList;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusCallException;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
///**
// * @author abiege
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class OccurenceWorkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//	
//	
//	@Test
//	public void testCreateOccurence(){
//		// create an OccurencesList and add one new Occurence
//		OccurencesList occurences = new OccurencesList();
//		Occurence occurence1 = new OccurenceImpl();
//		Date now = new Date();
//		
//		occurence1.setDeleted(false);
//		occurence1.setEndDate(now);
//		occurence1.setEndTime(now);
//		occurence1.setFkEvent(1);
//		occurence1.setFrequency(2);
//		occurence1.setIntervalType(4711);
//		occurence1.setStartDate(now);
//		occurence1.setStartTime(now);
//		occurence1.setType(1);
//		occurence1.setUpDown(0);
//				
//		occurences.add(occurence1);
//		
//		// call task "createOccurence"
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("addOccurences").add("newOccurences", occurences).invoke();
//		List<Integer> pks = (List<Integer>) res.getData("pks");
//		
//		// asserts
//		try {
//			ResultSet rs = new DBAccess().getOccurenceByIntervalType(4711);
//			while (rs.next()) {
//				assertTrue(rs.getInt("frequency") == 2);
//				assertTrue(rs.getInt("updown") == 0);
//				assertTrue(rs.getInt("fk_event") == 1);
//				assertTrue(rs.getInt("pk_occurence") == pks.get(0));
//			}
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//	
//	
//	@Test
//	public void testDeleteOccurence() {
//		// delete occurence with pk 1
//		List<Integer> pks = new LinkedList<Integer>();
//		pks.add(new Integer(1));
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		con.getTask("deleteOccurences").add("occurencePk", pks).invoke();
//		
//		// asserts: get occurence by pk from db (with pk) and check that the deleted flag is set
//		ResultSet rs;
//		try {
//			rs = new DBAccess().getOccurenceByPk(1);
//			
//			while (rs.next()) {
//				assertTrue(rs.getBoolean("deleted"));
//			}
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//	
//	
//	@Test
//	public void testUpdateOccurences(){
//		// fetch occurence with pk 1 from database, then update it and check if it was updated
//		try {
//			ResultSet rs = new DBAccess().getOccurenceByPk(1);
//			rs.next();
//			Occurence occurence = new OccurenceImpl();
//			occurence.setPk(1);
//			occurence.setDeleted(rs.getBoolean("deleted"));
//			occurence.setEndDate(rs.getDate("enddate"));
//			occurence.setEndTime(rs.getDate("endtime"));
//			occurence.setFkEvent(rs.getInt("fk_event"));
//			occurence.setFrequency(rs.getInt("frequency"));
//			occurence.setIntervalType(rs.getInt("intervaltype"));
//			occurence.setStartDate(rs.getDate("startdate"));
//			occurence.setStartTime(rs.getDate("starttime"));
//			occurence.setType(rs.getInt("type"));
//			occurence.setUpDown(rs.getInt("updown"));
//			
//			// change something
//			occurence.setFrequency(42);
//			occurence.setUpdateDate(new Date());
//			
//			// set something to 0 so that it will not be updated
//			occurence.setType(0);
//			
//			OccurencesList occurences = new OccurencesList();
//			occurences.add(occurence);
//			
////			 update via octopus task
//			OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//			con.getTask("updateOccurences").add("renewedentrys", occurences).invoke();
//			
//			// assert: get occurence form db again and check frequency and type
//			ResultSet rs2 = new DBAccess().getOccurenceByPk(1);
//			rs2.next();
//			assertTrue(rs2.getInt("frequency") == 42);
//			assertEquals(rs2.getInt("type"),  rs.getInt("type"));
//			
//		} catch (SQLException e) {
//			assertTrue(false);
//		} catch (OctopusCallException e) {
//			assertEquals(false,e.getLocalizedMessage());
//		}
//		
//		
//		//following scenario 
//		// while updating  an occurence server realsizes an unsynchronized occurence!
//		Occurence o;
//		try {
//			//gettting an occurence
//			ResultSet set = new DBAccess().getOccurenceByPk(1);
//			//getting occurence object 
//			o = ResultSetToOccurence(set);
////			modifying at the first time
//			o.setFrequency(5454);
//			OccurencesList occurences = new OccurencesList();
//			occurences.add(o);
////			 update via octopus task
//			OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//			con.getTask("updateOccurences").add("renewedentrys", occurences).invoke();
//			//modifieing 2. time
//			o.setFrequency(5555);
//			occurences.clear();
//			occurences.add(o);
//			//updating.. expecting exception ObejectNotSynchronized
//			con.getTask("updateOccurences").add("renewedentrys", occurences).invoke();
//		} catch (SQLException e) {
//			// error while getting Occurence with DBAccess().get....
//			assertEquals(false, e.getMessage());
//		} catch (OctopusCallException e) {
//		System.out.println(e.getErrorCode());
//			if(GuiException.isErrorCodeDefined(e.getErrorCode())){
//				// test wether it is the error we wanted to test 
//				assertEquals("Client.ObjectNotSynchronized",e.getErrorCode());
//				assertEquals(e.getCauseMessage(),"Object is older than the version on the Server");
//			}	
//			//other exceptions should´t be thrown
//			else assertEquals(false, e.getMessage());
//		}
//	}
//	private Occurence ResultSetToOccurence(ResultSet set) throws SQLException{
//		Occurence res = new OccurenceImpl();
//		while (set.next()) {
//			res.setEndDate(set.getDate("enddate"));
//			res.setEndTime(set.getDate("endtime"));
//			res.setFkEvent(set.getInt("fk_event"));
//			res.setFrequency(set.getInt("frequency"));
//			res.setIntervalType(set.getInt("intervaltype"));
//			res.setPk(set.getInt("pk_occurence"));
//			res.setStartDate(set.getDate("startdate"));
//		//rest will be merged in update method...
//			res.setUpdateDate(set.getDate("updatedate"));
//		}
//		return res;
//	}
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
