/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.kaliko.db.constants.DBCTypes;
import de.tarent.kaliko.objects.Type;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class TypeDBMapping extends AbstractDBMapping {
	
	public static final String STMT_SELECT_TYPE_BY_PK = "stmtselecttypebypk";
//	public static final String STMT_SELECT_TYPE_BY_NAME = "stmtselecttypebyname";
	public static final String STMT_SELECT_TYPE_BY_URN = "stmtselecttypebyurn";
	
	// XXX: what is the function of PARAM_TYPE_NAME? is it still needed? (pneuha 2009-06-27)
	public static final String PARAM_TYPE_NAME = "typeName";

	public TypeDBMapping(DBContext dbc) {
		super(dbc);
	}
	
	
	@Override
	public void configureMapping() {
		addField(DBCTypes.PK_TYPES, Type.PROPERTY_PK, PRIMARY_KEY_FIELDS | MINIMAL_FIELDS | COMMON_FIELDS);
//		addField(DBCTypes.NAME, Type.PROPERTY_NAME, COMMON_FIELDS | WRITEABLE_FIELDS);
		addField(DBCTypes.URN, Type.PROPERTY_URN, COMMON_FIELDS | WRITEABLE_FIELDS);
		addField(DBCTypes.UPDATEDATE,Type.PROPERTY_UPDATEDATE,COMMON_FIELDS | WRITEABLE_FIELDS);
		
		addQuery(STMT_SELECT_TYPE_BY_PK, createBasicSelectAll()
				.whereAndEq(DBCTypes.PK_TYPES, new ParamValue(Type.PROPERTY_PK))
				, COMMON_FIELDS);
		
//		addQuery(STMT_SELECT_TYPE_BY_NAME, createBasicSelectAll()
//				.whereAndEq(DBCTypes.NAME, new ParamValue(Type.PROPERTY_NAME))
//				, COMMON_FIELDS);
		
		addQuery(STMT_SELECT_TYPE_BY_URN, createBasicSelectAll()
				.whereAndEq(DBCTypes.URN, new ParamValue(Type.PROPERTY_URN))
				, COMMON_FIELDS);
	}

	@Override
	public String getTargetTable() {
		return DBCTypes.TABLENAME;
	}
	
}
