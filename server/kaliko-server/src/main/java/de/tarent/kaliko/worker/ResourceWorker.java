/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;

import de.tarent.kaliko.dao.ResourceDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Resource;
import de.tarent.kaliko.objects.impl.ResourcesList;
import de.tarent.kaliko.utils.GuiException;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.security.OctopusSecurityException;

/**
* @author Andre´ Biegel, tarent GmbH
*/

public class ResourceWorker {

	/** deletes an resource identified by its primary key
	 * 
	 * @author Andre´ Biegel
	 * @param  resourcePk the primary key of the Object which should be deleted
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result()
	public void deleteResource(@Name("resourcePks")List<Integer> pks) throws OctopusSecurityException{
		try {
			ResourceDAO dao = ResourceDAO.getInstance(); 
			for (int i = 0; i < pks.size(); i++) {
				dao.deleteResource(CalendarDBAccess.getManagedContext(), pks.get(i));
			}	
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}
	}
	/** updates an renewed attribute
	 * 
	 * @author Andre´ Biegel
	 * @param renewedentry resource object, which  contains new detail and Not_Updated_Flags like null, -1
	 * @param old old data
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result()
	public void updateResource(@Name("renewedResources")ResourcesList renewedentries) throws OctopusSecurityException {
		try {
			ResourceDAO dao = ResourceDAO.getInstance(); 
			for (int i = 0; i < renewedentries.size(); i++) {
				dao.updateEntity(CalendarDBAccess.getManagedContext(), renewedentries.get(i));
			}	
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		} catch (ObjectSynchronizationException e){
			//e.printStackTrace();
			throw new GuiException(GuiException.ERROR_OBJECT_NOT_SYNCHRONIZED);
		}
	}

	/** inserts an new resource entry in the DB 
	 * 
	 * @author Andre´ Biegel
	 * @param  newentry an new Resource Object 
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 * @throws  
	 */
	@WebMethod()
	@Result("pks")
	public List<Integer> addResource(@Name("newentries")ResourcesList newentries ) throws OctopusSecurityException{
		
		ResourceDAO dao = ResourceDAO.getInstance();
		List<Integer> res = new LinkedList<Integer>();
		try {
			List<String> list = null;
			if ( (list = dao.checkResourceAvailibility(CalendarDBAccess.getManagedContext(), newentries) ).size() > 0 ){
				GuiException e = new GuiException(GuiException.ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL);
				e.setMessage(list.toString());
				//e.setGuids(list);
				throw e ;
			}
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}
		try {
			for (int i = 0; i < newentries.size(); i++) {
				if (newentries.get(i).getFkEvent() == 0)
					throw new IllegalObjectCreationException("Fk event in resource not set");
				dao.insert(CalendarDBAccess.getManagedContext(), newentries.get(i));
				res.add(new Integer(newentries.get(i).getPk()));
			}	
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		} catch (IllegalObjectCreationException e) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG);
		}
		return res;
	}

	/** returns an list of resources, wich are associated with the EventPk 
	 * 
	 * @author Andre´ Biegel
	 * @param  EventPk primary key of an event
	 * @return List list of resources
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result("resources")
	public ResourcesList getResourcesByEventPk(@Name("eventPk")int EventPk) throws OctopusSecurityException{
		
		List<Resource> list;
		try {
			list = (List<Resource>) ResourceDAO.getInstance().getResourcesByEventPk(CalendarDBAccess.getManagedContext(), EventPk);	
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}				
		ResourcesList resList = new ResourcesList();
		resList.addAll(list);
		
		return resList;
	}
}
