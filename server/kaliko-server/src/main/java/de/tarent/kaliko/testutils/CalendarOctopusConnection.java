///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.testutils;
//
//
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusConnectionFactory;
//
//
///** This class holds an internal connection to the octopus.
// * 
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class CalendarOctopusConnection {
//
//	private static CalendarOctopusConnection instance = null;
//	
//	private OctopusConnection octopusConnection;
//	
//	
//	protected CalendarOctopusConnection() {
//		this.octopusConnection = OctopusConnectionFactory.getInstance().getConnection("CalendarOctopusConnection");	
//		this.octopusConnection.setUsername(TestDummy.getInstance().getUsername());
//		this.octopusConnection.setPassword(TestDummy.getInstance().getPwd());
//		
//	
//		//this.octopusConnection.getTask("login_SOAP").add("username", TestDummy.getInstance().getUsername()).add("password", TestDummy.getInstance().getPwd()).invoke();
//	}
//	
//	
//	public static synchronized CalendarOctopusConnection getInstance() {
//        if (instance == null) {
//            instance = new CalendarOctopusConnection();
//        }
//        return instance;
//    }
//	
//	
//	public OctopusConnection getOctopusConnection() {
//		return this.octopusConnection;
//	}
//	
//}
