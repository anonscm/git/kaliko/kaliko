///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.component;
//
//import static org.junit.Assert.assertNotNull;
//
//import java.util.Iterator;
//import java.util.List;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Event;
//import de.tarent.kaliko.objects.Occurence;
//import de.tarent.kaliko.objects.Resource;
//import de.tarent.kaliko.objects.impl.OccurencesList;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
//public class SOAPTest {
//
//	@Before
//	public void setUp() throws Exception {
//	}
//	
//	
//	@Test
//	public void gettingComplexObjects() {
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getTestEvent").add("pkEvent", 12).invoke();
//
//		Event event = (Event) res.getData("event");
//		
//		System.out.println("event.location: " + event.getLocation());
//		System.out.println("event.title: " + event.getTitle());
//		
//		List resources = event.getResourcesList();
//		System.out.println("#event.resources: " + resources.size());
//		Iterator iter = resources.iterator();
//		while (iter.hasNext()) {
//			Resource r = (Resource) iter.next();
//			System.out.println("notification message: " + r.getNotificationMessage());
//			System.out.println("text: " + r.getText());
//		}
//		
//		/*List occurences = event.getOccurencesList();
//		System.out.println(occurences.size());
//		Iterator iter2 = occurences.iterator();
//		while (iter2.hasNext()) {
//			Occurence r = (Occurence) iter2.next();
//			System.out.println(r.getType());
//			System.out.println(r.getUpDown());
//		}*/
//		
//		//assertNotNull(event.getOccurencesList());
//		assertNotNull(event.getResourcesList());
//	}
//	
//	
//	@Test
//	public void getOccurenceList() {
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getOccurences").add("pkEvent", 1).invoke();
//
//		OccurencesList occurences = (OccurencesList) res.getData("occurencesList");
//		
//		System.out.println(occurences.size());
//		Iterator iter2 = occurences.iterator();
//		while (iter2.hasNext()) {
//			Occurence r = (Occurence) iter2.next();
//			System.out.println(r.getType());
//			System.out.println(r.getUpDown());
//			System.out.println(r.getEndTime().toString());
//		}
//		
//		assertNotNull(occurences);
//	}
//	
//
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
