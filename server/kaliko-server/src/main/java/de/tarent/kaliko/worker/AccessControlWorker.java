/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;

import org.apache.log4j.Logger;

import de.tarent.kaliko.categories.CategoryManager;
import de.tarent.kaliko.dao.CategoryDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;
import de.tarent.kaliko.objects.impl.AccessImpl;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.EventList;
import de.tarent.kaliko.objects.impl.OccurencesList;
import de.tarent.kaliko.objects.impl.ResourcesList;
import de.tarent.kaliko.utils.GuiException;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.OctopusContext;

/**Worker  handles login and logout
 * ame instance 
 * @author Andre´ BIegel , tarent GmbH
 *
 */
public class AccessControlWorker {
	
	protected final static Logger logger = Logger.getLogger(AccessControlWorker.class);

	@WebMethod()
	@Result()
	public void throwAccessDenied() throws GuiException{
		throw new GuiException(GuiException.ERROR_ACCESS_DENIED);
	}
	
	/** calls controllPermission by an list of resource pks
	 * 
	 * @author Andre` Biegel,tarent GmbH
	 * @param resourcepks
	 * @param userCategories
	 * @throws GuiException
	 */
	private void controlPermissionByResourcePks(List <Integer> resourcepks, CategoriesList userCategories)
			throws GuiException{
		
		List<String> wantedCategoryGuids = null;
		CategoryDAO dao = (CategoryDAO) CategoryDAO.getInstance();
		try {
			wantedCategoryGuids = dao.getCategoriesByResourcePks(CalendarDBAccess.getManagedContext(), resourcepks);
		} catch (Exception e) {
			String message = e.getMessage();
			GuiException exce = new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
			exce.setMessage(message);
			throw exce;
		}
//		check permissions with categories from the request and the categories from the
		controlPermission(userCategories, wantedCategoryGuids);
	}
	
	/** calls controllPermission by an list of occurence pks
	 * 
	 * @author Andre` Biegel,tarent GmbH
	 * @param occurencepks
	 * @param userCategories
	 * @throws GuiException
	 */
	private void controllPermissionByOccurencePks(List <Integer> occurencepks, CategoriesList userCategories)
			throws GuiException {
		
		List<String> wantedCategoryGuids = null;
		CategoryDAO dao = (CategoryDAO) CategoryDAO.getInstance();
		try {
			wantedCategoryGuids = dao.getCategoriesByOccurencePks(CalendarDBAccess.getManagedContext(), occurencepks);
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}
//		check permissions with categories from the request and the categories from the
		controlPermission(userCategories, wantedCategoryGuids);
	}
	
	/**calls controllPermission by an list of event of Pk´s
	 * 
	 * @author Andre` Biegel,tarent GmbH
	 * @param eventfks
	 * @param userCategories
	 * @throws GuiException
	 */
	private void controlPermissionByEventPks(List <Integer> eventpks, CategoriesList userCategories) throws GuiException {
		List<String> wantedCategoryGuids = null;
		CategoryDAO dao = (CategoryDAO) CategoryDAO.getInstance();
		try {
			wantedCategoryGuids = dao.getCategoriesByPkEvents(CalendarDBAccess.getManagedContext(), eventpks);
		} catch (SQLException e) {
			String message = e.getMessage();
			GuiException exce = new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
			exce.setMessage(message);
			throw exce;
		}
//		check permissions with categories from the request and the categories from the
		controlPermission(userCategories, wantedCategoryGuids);
	}
	
	/**compares the two sets and signals an access denied Exception when the user is not allowed of getting access
	 *  
	 * @author Peter Neuhaus
	 * @param userCategories categories the user has the right to access
	 * @param wantedCategories categories the user wants to access
	 * @throws GuiException ACCESS denied
	 */
	private void controlPermission(CategoriesList userCategories, CategoriesList wantedCategories) throws GuiException {
		for (Category category : wantedCategories)
			if (!userCategories.contains(category))
				throw new GuiException(GuiException.ERROR_ACCESS_DENIED);
	}
	
	/**
	 * @author pneuha
	 * @param userCategories
	 * @param wantedCategoryGuids
	 * @throws GuiException
	 */
	private void controlPermission(CategoriesList userCategories, List<String> wantedCategoryGuids) throws GuiException {
//		logger.debug("wantedCategoryGuids:");
//		for (String guid : wantedCategoryGuids)
//			logger.debug("'" + guid + "'");
//		logger.debug("userCategories:");
//		for (Category cat : userCategories)
//			logger.debug("'" + cat.getGuid() + "'");
		
		WANTED: for (String guid : wantedCategoryGuids) {	
			for (Category category : userCategories)
				if (guid.equals(category.getGuid()))
					continue WANTED;

			throw new GuiException(GuiException.ERROR_ACCESS_DENIED);
		}
	}
	
	/**
	 * @author pneuha
	 * @param userCategories
	 * @param wantedCategoryGuid
	 * @throws GuiException
	 */
	private void controlPermission(CategoriesList userCategories, String wantedCategoryGuid) throws GuiException {
		if (!userCategories.contains(wantedCategoryGuid))
			throw new GuiException(GuiException.ERROR_ACCESS_DENIED);
	}

	
	/**creates an access object
	 * 
	 * @author Andre´Biegel
	 * @param right 
	 * @return
	 */
	private Access getAccess (int right) {
		
		Access access = new AccessImpl();
		switch (right) {
		case 1:
			access.setReadable(true);
			break;
		case 2:
			access.setWritable(true);
			break;
		case 3:
			access.setReadable(true);
			access.setWritable(true);
			break;
		default:
			break;
		}
		return access;
		
	}
	
	private CategoriesList getUserCategories(OctopusContext oc,int right) {
		//get categories associated to user
		// get user´s name
		return CategoryManager.getInstance().getCategories( oc.getConfigObject().getLoginname(),getAccess(right));
	}
	
	@WebMethod
	public void setCallTyp(OctopusContext oc,@Name("categories")@Optional List<String> categories){
		if (categories != null) {
			oc.getContentObject().setStatus("a");
		}
	}
	
	/** Checks the permission of the user ,which is calling an task by comparing the set of
	 * categories,for which the access is granted, with the categorie the user wants to access
	 * 
	 *@author Andre´ Biegel ,tarent GmbH 
	 * @param oc
	 * @param eventPk
	 * @param right
	 * @throws OctopusSecurityException
	 */
	@WebMethod()
	public void checkPermissionGetResourcesByEventPk(
			OctopusContext oc,
			@Name("eventPk") int eventPk,
			@Name("right") int right) throws OctopusSecurityException {
		List<Integer> eventpks = new ArrayList<Integer>();
		eventpks.add(new Integer(eventPk));
		controlPermissionByEventPks(eventpks, getUserCategories(oc, right));
	}
	
	@WebMethod()
	public void checkPermissionDetagCategoryToEvent(
			OctopusContext oc,
			@Name("eventPk") int eventPk,
			@Name("categoryGuid") String categoryGuid,
			@Name("right")int right) throws OctopusSecurityException {
		//checking event
		List<Integer> pks = new LinkedList<Integer>();
		pks.add(new Integer(eventPk));
		controlPermissionByEventPks(pks, getUserCategories(oc, right));
		//checking catgories
		controlPermission(getUserCategories(oc, right), categoryGuid);
	}
	
	@WebMethod()
	public void checkPermissionTagCategoryToEvent(
			OctopusContext oc,
			@Name("eventPk")int eventPk,
			@Name("category")Category category,
			@Name("right")int right) throws OctopusSecurityException {
//		checking event
		List<Integer> pks = new LinkedList<Integer>();
		pks.add(new Integer(eventPk));
		controlPermissionByEventPks(pks, getUserCategories(oc, right));
//		checking catgories
//		CategoriesList wantedCats = new CategoriesList();
//		wantedCats.add(category);
//		controllPermission(getUserCategories(oc, right), wantedCats);
		controlPermission(getUserCategories(oc, right), category.getGuid());
	}
	
	@WebMethod()
	public void checkPermissionUpdateResource(
			OctopusContext oc,
			@Name("renewedResources") ResourcesList renewedentries,
			@Name("right") int right) throws OctopusSecurityException{
		List<Integer> resourcePks = new ArrayList<Integer>();
		for (Iterator iter = renewedentries.iterator(); iter.hasNext();) {
			Resource element = (Resource) iter.next();
			resourcePks.add(element.getPk());
		}
		controlPermissionByResourcePks(resourcePks, getUserCategories(oc,right));
	}
	
	@WebMethod()
	public void checkPermissionAddResource(
			OctopusContext oc,
			@Name("newentries") ResourcesList newentries,
			@Name("right") int right) throws OctopusSecurityException {
		List<String> wantedCategoryGuids;
		List<Integer> eventfks = new ArrayList<Integer>();
		
		for (int i = 0; i < newentries.size(); i++)
			eventfks.add(new Integer(newentries.get(i).getFkEvent()));

		try {	
			wantedCategoryGuids = (
					(CategoryDAO) CategoryDAO.getInstance())
					.getCategoriesByFkEventFromResources(CalendarDBAccess.getManagedContext(), eventfks);
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}
//	check permissions with categories from the request and the categories from the
		controlPermission(getUserCategories(oc,right), wantedCategoryGuids);
	}
	
	@WebMethod()
	public void checkPermissionDeleteResource(
			OctopusContext oc,
			@Name("resourcePks") List<Integer> pks,
			@Name("right") int right) throws OctopusSecurityException {
		controlPermissionByResourcePks(pks, getUserCategories(oc, right));
	}
	
	@WebMethod()
	public void checkPermissionGetEvents(
			OctopusContext oc,
			@Name("categories") List<String> categoryGuids,
			@Name("right") int right) throws OctopusSecurityException {
		//check permissions with categories from the request and the categories from the user´s
		controlPermission(getUserCategories(oc, right), categoryGuids);
	}
	
	@WebMethod()
	public void checkPermissionDeleteEvents(
			OctopusContext oc,
			@Name("pks") List<Integer> pks,
			@Name("right") int right) throws OctopusSecurityException {
		controlPermissionByEventPks(pks,getUserCategories(oc, right));
	}
	
	@WebMethod()
	public void checkPermissionUpdateEvents(
			OctopusContext oc,
			@Name("events") EventList events,
			@Name("right") int right) throws OctopusSecurityException {
		//´cause in update methods categories aren´t set, but the pk has to be set !!! 
		List<Integer> pklist = new LinkedList<Integer>();
		for (int i = 0; i < events.size(); i++) {
			pklist.add(new Integer(events.get(i).getPk()));
		}
		controlPermissionByEventPks(pklist, getUserCategories(oc, right));
	}
	
	@WebMethod()
	public void checkPermissionCreateEvents(
			OctopusContext oc,
			@Name("events") EventList events,
			@Name("right") int right) throws OctopusSecurityException {
//		for create methods where the categories have to be set
		CategoriesList wantedCategories = new CategoriesList(); 
		for (int i = 0; i < events.size(); i++)
			wantedCategories.addAll(events.get(i).getCategoriesList());

//	check permissions with categories from the request and the categories from the
		controlPermission(getUserCategories(oc, right), wantedCategories);
	}
	
	@WebMethod()
	public void checkPermissionCreateOccurences(
			OctopusContext oc,
			@Name("newOccurences") OccurencesList newentries,
			@Name("right") int right) throws OctopusSecurityException {
		List<String> wantedCategoryGuids = null;
		List<Integer> eventfks= new ArrayList<Integer>();
		
		for (int i = 0; i < newentries.size(); i++)
			eventfks.add(newentries.get(i).getFkEvent());

		try {
			wantedCategoryGuids = (
					(CategoryDAO) CategoryDAO.getInstance())
					.getCategoriesByFkEventFromOccurences(CalendarDBAccess.getManagedContext(), eventfks);
		} catch (SQLException e) {
			String message = e.getMessage();
			GuiException exce = new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
			exce.setMessage(message);
			throw exce;
		}
//	check permissions with categories from the request and the categories from the 
		controlPermission(getUserCategories(oc, right), wantedCategoryGuids);

	}
	
	@WebMethod()
	public void checkPermissionUpdateOccurences(
			OctopusContext oc,
			@Name("renewedentrys") OccurencesList renewedentrys,
			@Name("right") int right) throws OctopusSecurityException {
		List<Integer> occurencePk = new ArrayList<Integer>();
		
		for (Occurence occurence : renewedentrys)
			occurencePk.add(occurence.getPk());

		controllPermissionByOccurencePks(occurencePk, getUserCategories(oc, right));
	}
	
	@WebMethod()
	public void checkPermissionDeleteOccurences(
			OctopusContext oc,
			@Name("occurencePk") List<Integer> occurencePks,
			@Name("right") int right) throws OctopusSecurityException{
		controllPermissionByOccurencePks(occurencePks, getUserCategories(oc, right));	
	}
}
