/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;

import de.tarent.kaliko.dao.OccurenceDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.utils.GuiException;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.security.OctopusSecurityException;

/**
* @author Andre´ Biegel, tarent GmbH
*/

public class OccurenceWorker {

	
	/** inserts an new Occurence
	 * 
	 * @author Andre´ Biegel
	 * @param newentry an new Occurence Object 
	 * @return List of created occurence pk´s
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result("pks")
	public List<Integer> createOccurence(@Name("newOccurences")List<Occurence> newentries ) throws OctopusSecurityException {
		
		List<Integer> result = new LinkedList<Integer>();
		try {
			result =  OccurenceDAO.getInstance().createOccurences(CalendarDBAccess.getManagedContext(), newentries);
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);			
		} catch (IllegalObjectCreationException ex) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG);
		}
		return result ;
		
		
	}
	
	/** deletes an occurence identified by its primary key
	 * 
	 * @author Andre´ Biegel
	 * @param occurencePk the primary key of the Object which should be deleted
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result()
	public void deleteOccurence(@Name("occurencePk")List<Integer> occurencePks) throws OctopusSecurityException{
		try {
			OccurenceDAO dao = OccurenceDAO.getInstance();
			dao.deleteOccurence(CalendarDBAccess.getManagedContext(),occurencePks); 
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		}
	}
	
	/** updates an list occurences
	 * 
	 * @author Andre´ Biegel
	 * @param renewedentrys a list of updated Occurences,these Occurences contain ontains new details and Not_Updated_Flags like null, -1
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result()
	public void updateOccurences(@Name("renewedentrys")List<Occurence> renewedentrys) throws OctopusSecurityException {
		try {
			OccurenceDAO dao = OccurenceDAO.getInstance(); 
			for (int i = 0; i < renewedentrys.size(); i++) {
				dao.updateOccurence(CalendarDBAccess.getManagedContext(),renewedentrys.get(i));
			}		
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR);
		} catch (ObjectSynchronizationException ex) {
			throw new GuiException(GuiException.ERROR_OBJECT_NOT_SYNCHRONIZED);
		} catch (IllegalObjectCreationException exc) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG);
		}
	
	}

}
