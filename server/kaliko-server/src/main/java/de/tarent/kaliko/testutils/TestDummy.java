///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.testutils;
//
///**Class interacts as an LDAP-Test-Dummy object 
// * 
// * @author Andre´ Biegel , tarent GmbH
// *
// */
//
//public class TestDummy {
//	private String username = "hwurst";
//	private String pwd = "hwurst";
//	private String mail = "h.wurst@tarent.de";
//	private String name ="Hans Wurst";
//	private static TestDummy instance;
//	
//	public static synchronized TestDummy getInstance(){
//		
//		if (instance == null) {
//			instance = new TestDummy();
//		}
//		return instance;
//	}	
//	/**returns Username 
//	 * 
//	 * @return Username: "hwurst"
//	 */
//	public String getUsername(){
//		return this.username;
//	}
//	/**returns password
//	 * 
//	 * @return password: "hwurst"
//	 */
//	public String getPwd(){
//		return this.pwd;
//	}
//	/**returns Email Address 
//	 * 
//	 * @return Mailaddress: "h.wurst@tarent.de"
//	 */
//	public String getMail(){
//		return this.mail;
//	}
//	/**returns full name of the ldap dummy 
//	 * 
//	 * @return name: "Hans Wurst"
//	 */
//	public String getName(){
//		return this.name;
//	}
//
// 
//
//}
