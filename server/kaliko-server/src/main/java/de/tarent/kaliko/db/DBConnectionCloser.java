/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.db;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.dblayer.engine.DB;
import de.tarent.octopus.server.Closeable;


/**
 * Implementation for the octopus closeable interface,
 * which closes a JDBC connection.
 */
public class DBConnectionCloser implements Closeable {

	private static final Logger logger = Logger.getLogger(DBConnectionCloser.class.getName());
	private Connection con;
	
	protected DBConnectionCloser(Connection con){
		this.con = con;
	}
	
	public void close() {
		try {
			if(con != null && !con.isClosed()){
				DB.close(con);
			}
		} catch (SQLException e) {
			logger.log(Level.WARNING, e.getLocalizedMessage(), e);
		}
	}
}
