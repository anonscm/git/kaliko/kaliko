/**
 * 
 */
package de.tarent.kaliko.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SimpleCalendarLoginManager extends AbstractCalendarLoginManager {

	protected Map<String, String> users;
	
	public SimpleCalendarLoginManager() {
		super();
		users = new HashMap<String, String>();
		users.put("fkoest", "test");
	}
	
	/**
	 * @see de.tarent.kaliko.utils.AbstractCalendarLoginManager#isValidLogin(java.lang.String, java.lang.String)
	 */
	@Override
	protected boolean isValidLogin(String username, String password) {
		String storedPassword = users.get(username);
		return (storedPassword != null && storedPassword.equals(password));
	}
}