///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.xml.namespace.QName;
//
//import de.tarent.octopus.security.OctopusSecurityException;
//
///**  ClientVersion of the GuiException 
// * @author Andre´ Biegel, tarent GmbH
// *
// */
//public class GuiException extends OctopusSecurityException {
//
//	/**Default  serial
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	//client error codes
//	
//    public static final int ERROR_OBJECT_NOT_SYNCHRONIZED = 101;
//    public static final int ERROR_PARAMETER_MISSING_OR_WRONG = 102;
//	public static final int ERROR_INTERNAL_SERVER_ERROR = 103;
//	public static final int ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL = 104;
//	public static final int ERROR_ACCESS_DENIED = 105;
//	//readable messages
//	private static final String STRING_ERROR_OBJECT_NOT_SYNCHRONIZED ="Client.ObjectNotSynchronized";
//	private static final String STRING_ERROR_PARAMETER_MISSING_OR_WRONG = "Client.ParameterMissingOrWrong";
//	private static final String STRING_ERROR_INTERNAL_SERVER_ERROR ="Client.InternalServerError";
//	private static final String STRING_ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL="Client.ResourcesUsedInRequestedTimeInterval";
//	private static final String STRING_ERROR_ACCESS_DENIED="Client.AccessDenied";
//	
//	private static Map<Integer, QName> soapFaultCodes = new HashMap<Integer, QName>();
//	private static Map<Integer, String> messages = new HashMap<Integer, String>();
//	private static Map<String, Integer> mapping = new HashMap<String, Integer>();
//	//TODO enter real url from the server here
//	private static final String uri="127.0.0.1";
//	protected int errorCode;
// 
//	
//	
//   static{
//	   soapFaultCodes.put(new Integer(ERROR_OBJECT_NOT_SYNCHRONIZED),new QName(uri,STRING_ERROR_OBJECT_NOT_SYNCHRONIZED));
//	   soapFaultCodes.put(new Integer(ERROR_PARAMETER_MISSING_OR_WRONG),new QName(uri,STRING_ERROR_PARAMETER_MISSING_OR_WRONG));
//	   soapFaultCodes.put(new Integer(ERROR_INTERNAL_SERVER_ERROR),new QName(uri,STRING_ERROR_INTERNAL_SERVER_ERROR));
//	   soapFaultCodes.put(new Integer(ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL),new QName(uri,STRING_ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL));
//	   soapFaultCodes.put(new Integer(ERROR_ACCESS_DENIED), new QName(uri, STRING_ERROR_ACCESS_DENIED));
//	   
//	   messages.put(new Integer(ERROR_OBJECT_NOT_SYNCHRONIZED),"Object is older than the version on the Server");
//	   messages.put(new Integer(ERROR_PARAMETER_MISSING_OR_WRONG),"Parameter is not set ");
//	   messages.put(new Integer(ERROR_INTERNAL_SERVER_ERROR),"Failure");
//	   messages.put(new Integer(ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL), "Resources are used by another event in the requested time interval");
//	   messages.put(new Integer(ERROR_ACCESS_DENIED), "You have no rights to do this!!");
//	   
//	   mapping.put(STRING_ERROR_OBJECT_NOT_SYNCHRONIZED, new Integer(ERROR_OBJECT_NOT_SYNCHRONIZED));
//	   mapping.put(STRING_ERROR_PARAMETER_MISSING_OR_WRONG, new Integer(ERROR_PARAMETER_MISSING_OR_WRONG));
//	   mapping.put(STRING_ERROR_INTERNAL_SERVER_ERROR, new Integer(ERROR_INTERNAL_SERVER_ERROR));
//	   // actuall the message is an  list of resource guids which are not available in the requesed time intervall 
//	   mapping.put(STRING_ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL,new Integer(ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL));
//	   mapping.put(STRING_ERROR_ACCESS_DENIED, new Integer(ERROR_ACCESS_DENIED));
//   }
//   
//	public QName getSoapFaultCode() {
//        if (errorCode >= 100)
//            return (QName)soapFaultCodes.get(new Integer(errorCode));
//        else
//            return super.getSoapFaultCode();
//    }
//	protected String getMessageByErrorCode(int errorCode) {
//		if(errorCode<100)
//			return super.getMessageByErrorCode(errorCode);
//		else if(messages.containsKey(new Integer(errorCode)))
//			return (String)messages.get(new Integer(errorCode));
//		else
//			return "Unknown Errorcode";
//	}
//		
//	/**Construktors
//	 * @param arg0 errorcode
//	 */
//	public GuiException(int errorCode) {
//		super(errorCode);
//		this.errorCode=errorCode;
//	}
//
//	/**
//	 * @param arg0 errorcode
//	 */
//	public GuiException(String arg0) {
//		super(arg0);
//	
//	}
//
//	/**
//	 * @param arg0 errorcode
//	 * @param arg1 
//	 */
//	public GuiException(String arg0, Throwable arg1) {
//		super(arg0, arg1);
//	
//	}
//
//	/**
//	 * @param arg0
//	 * @param arg1
//	 */
//	public GuiException(int arg0, Throwable arg1) {
//		super(arg0, arg1);
//	this.errorCode = arg0;
//	}
//	/**
//	 * @param arg0
//	 * @param arg1
//	 */
//	public GuiException(int arg0, String arg1) {
//		super(arg0, arg1);
//		this.errorCode = arg0;
//	}
//	
//	public static boolean isErrorCodeDefined(String errorCodeString){
//        return mapping.containsKey(errorCodeString);
//	}
//	
//	public static int getErrorCode(String errorCodeString){
//		return ((Integer)mapping.get(errorCodeString)).intValue();
//	}
//	
//}
