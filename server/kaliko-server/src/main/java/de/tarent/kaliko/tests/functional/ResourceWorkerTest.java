///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
///**
// * 
// */
//package de.tarent.kaliko.tests.functional;
//
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotSame;
//import static org.junit.Assert.assertTrue;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.LinkedList;
//import java.util.List;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Resource;
//import de.tarent.kaliko.objects.impl.ResourceImpl;
//import de.tarent.kaliko.objects.impl.ResourcesList;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusCallException;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
///**
// * @author Andre´ Biegel, tarent GmbH
// *
// */
//public class ResourceWorkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//	
//	
//	@Test
//	public void testGetResourcesByEventPk(){
//		int x = 1;
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getResourcesByEventPk").add("eventPk", x).invoke();
//		ResourcesList list = (ResourcesList) res.getData("resources");
//		
//		assertTrue(list.size() > 0);
//		for (int i = 0; i < list.size(); i++) {
//			assertEquals(x,list.get(i).getFkEvent());
//		}
//	}
//	
//	
//	@Test
//	public void testAddResource(){
//		int x = 4 ; 
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		Resource test = new ResourceImpl();
//		test.setDeleted(false);
//		test.setFkEvent(x);
//		test.setType(1);
//		test.setGuid("blubbdibadu");
//		test.setNotificationMessage("bla");
//		test.setNotificationTime(Calendar.getInstance().getTime());
//		test.setParticipationNote("bla");
//		test.setParticipationStatus(1);
//		
//		ResourcesList test1 = new ResourcesList();
//		test1.add(test);
//		
//		OctopusResult res1 = con.getTask("addResources").add("newentries", test1).invoke();
//		List<Integer> ints=  (List<Integer>) res1.getData("pks");
//		
//		assertNotSame(ints.get(0),null );
//		assertEquals(ints.get(0), 6);
//		try {
//			ResultSet rs = new DBAccess().getResourcesByNotMessage("bla");
//			while (rs.next()) {
//				//assertTrue(rs.getString("guid").equals("blubbdibadu"));
//				assertTrue(rs.getString("participationnote").equals("bla"));
//				assertTrue(rs.getInt("participationstatus")== 1);
//				assertTrue(rs.getInt("fk_event")== x );
//				assertTrue(rs.getInt("type")== 1);
//			}	
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//// adding the resource for the second time , but now it should be not available!!
//		
//		try {
//			con.getTask("addResources").add("newentries", test1).invoke();
//		} catch (OctopusCallException e) {
//			if(GuiException.isErrorCodeDefined(e.getErrorCode())){
//				// test wether it is the error we wanted to test 
//				assertEquals("Client.ResourcesUsedInRequestedTimeInterval",e.getErrorCode());
//				assertEquals(e.getCauseMessage(),"["+test.getGuid()+"]");
//			}	
//			// the assertequals is here more pragmatic for getting error infomation
//			else assertEquals(false, e.getMessage());
//		}
//		
//	}
//	
//	
//	@Test
//	public void testUpdateResource(){
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		
//		// create a resource to update
//		Resource test = new ResourceImpl();
//		test.setPk(1);
//		test.setFkEvent(3);
//		test.setGuid("xyz");
//		test.setType(1);
//		test.setUpdateDate(new Date());
//
//		ResourcesList test1 = new ResourcesList();
//		test1.add(test);
//		
//		// update resource
//		con.getTask("updateResources").add("renewedResources", test1).invoke();
//		
//		// get same resource and check if it was updated
//		OctopusResult result = con.getTask("getResourcesByEventPk").add("eventPk", 3).invoke();
//		ResourcesList list =  (ResourcesList) result.getData("resources");
//		
//		assertEquals("xyz",list.get(0).getGuid());
//		assertEquals(1,list.get(0).getType());
//		assertEquals(3,list.get(0).getFkEvent());
//		try {
//			//fetch db entity
//			ResultSet set = new DBAccess().getResourcesByPk(1);
//			//transforme to resource
//			Resource a = resultSetToResource(set);
//			//change NotificationMessage
//			a.setNotificationMessage("xxxxxy");
//			test1.clear();
//			test1.add(a);
//			//update for the first time 
//			con.getTask("updateResources").add("renewedResources", test1).invoke();
//			//modifieing
//			a.setNotificationMessage("xxxxxyz123");
//			test1.clear();
//			test1.add(a);
//			//updating second time there should be thrown an exception 
//			con.getTask("updateResources").add("renewedResources", test1).invoke();
//		} catch (SQLException e) {
//			assertTrue(false);
//		} catch (OctopusCallException e) {
//			if(GuiException.isErrorCodeDefined(e.getErrorCode())){
//				// test wether it is the error we wanted to test 
//				assertEquals("Client.ObjectNotSynchronized",e.getErrorCode());
//				assertEquals(e.getCauseMessage(),"Object is older than the version on the Server");
//			}	
//			else assertEquals(false, e.getMessage());
//		}
//		
//		
//		
//	}
//	/**method transformes an Resultset of the resource tabel into an resource objkect !!
//	 * Important: Not Object attributes are set in this method just use this when testing the UpdateResource Task 
//	 * 
//	 * @param set resultset of an DBAccess
//	 * @return result an occurenceObject
//	 * @throws SQLException
//	 */
//	private Resource resultSetToResource(ResultSet set ) throws SQLException {
//		Resource result = new ResourceImpl();
//		while (set.next()){
//			result.setUpdateDate(set.getDate("updatedate"));
//			result.setPk(set.getInt("pk_resource"));
//			result.setFkEvent(set.getInt("fk_event"));
//			result.setType(set.getInt("type"));
//			result.setText(set.getString("text"));
//			//rest will be automatically merged by updateResource task 
//		}
//		return result; 
//	}
//	
//
//	
//	@Test
//	public void testDeleteResource(){
//		int x = 1;
//		List<Integer> list= new LinkedList<Integer>();
//		list.add(new Integer(x));
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		
//		con.getTask("deleteResources").add("resourcePks", list).invoke();
//		OctopusResult result = con.getTask("getResourcesByEventPk").add("eventPk",1).invoke();
//		ResourcesList list1 =  (ResourcesList) result.getData("resources");
//		assertEquals(4,list1.size());
//	}
//
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
