package de.tarent.kaliko.demo;

import de.tarent.commons.datahandling.entity.AttributeSource;
import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.EntityFactory;
import de.tarent.commons.datahandling.entity.LookupContext;
import de.tarent.kaliko.factory.CategoryEntityFactory;
import de.tarent.kaliko.factory.OccurenceEntityFactory;
import de.tarent.kaliko.factory.ResourceEntityFactory;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

public class TestEntityFactory extends DefaultEntityFactory {
	
	private static TestEntityFactory instance = null;
	
	private TestEntityFactory() {
		super(EventImpl.class);
	}
	
	public static synchronized TestEntityFactory getInstance() {
		
		if (instance == null)
			instance = new TestEntityFactory();
		
		return instance;
	}
	
	/**
     * Template method for returning an Entity from the LookupContext. Default implementation allways returns null.
     */
	@Override
    public Object getEntityFromLookupContext(AttributeSource as, LookupContext lc) {
    	return lc.getEntity(as.getAttribute(Event.PROPERTY_PK), EventImpl.class.getName());
    }

    /**
     * Default implementation for template method. This implementation does not store the entity.
     */
	@Override
    public void storeEntityInLookupContext(Object entity, LookupContext lc) {
        lc.registerEntity(((EventImpl)entity).getPk(), EventImpl.class.getName(), entity);
    }
    
	@Override
    protected EntityFactory getFactoryFor(String attributeName) {
        if (Event.PROPERTY_OCCURENCES.equals(attributeName))
            return OccurenceEntityFactory.getInstance();
        if (Event.PROPERTY_RESOURCES.equals(attributeName))
            return ResourceEntityFactory.getInstance();
        if (Event.PROPERTY_CATEGORIES.equals(attributeName))
        	return CategoryEntityFactory.getInstance();
        return null;
    }
}
