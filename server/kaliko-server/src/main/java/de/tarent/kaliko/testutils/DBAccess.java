///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.testutils;
//
//import java.io.BufferedInputStream;
//import java.io.ByteArrayOutputStream;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//
///**
// * 
// * @author Jan Meyer, tarent GmbH
// * @author Martin Pelzer, tarent GmbH
// * 
// * This class provides different methods to access the database that is used in
// * this project. It includes methods that are used to verify the funcionality of
// * database requesting or database writing parts of the project.
// * 
// */
//public class DBAccess {
//  
//	private String DB_USER = "postgres";
//    private String DB_PASSWORD = "postgres";
//    private String DB_URL = "jdbc:postgresql://localhost:5432/test";
//
//    
//    /**
//      * A constructor of the DBAcess object
//      * 
//      * @param user
//      * @param pwd
//      * @param url
//      */
//    public DBAccess(String user, String pwd, String url) {
//    	DB_USER = user;
//    	DB_PASSWORD = pwd;
//    	DB_URL = url;
//    }
//    
//    
//    /**
//     * A constructor of the DBAcess object
//     * 
//     * @param user
//     * @param pwd
//     * @param url
//     */
//   public DBAccess() {
//
//   }
//
//    
//    /**
//      * This method runs a given sql-script.
//      * 
//      * @param Filename
//      * @throws Exception
//      */
//    public void runSQLScriptFromResouces(String Filename) throws Exception {
//		String sqlscript = null;
//		BufferedInputStream inputstream = null;
//		ByteArrayOutputStream outstream = null;
//		try {
//		    inputstream = new BufferedInputStream(DBAccess.class
//			    .getResourceAsStream("/" + Filename));
//		    outstream = new ByteArrayOutputStream(1024);
//		    byte[] buffer = new byte[4096];
//		    int len;
//	
//		    if (inputstream == null)
//			throw new Exception("Could not open file!");
//	
//		    while ((len = inputstream.read(buffer)) > 0) {
//			outstream.write(buffer, 0, len);
//		    }
//		    outstream.close();
//		    inputstream.close();
//		    sqlscript = outstream.toString();
//		} finally {
//		    try {
//			outstream.close();
//		    } catch (Exception e) {
//		    }
//		    try {
//			inputstream.close();
//		    } catch (Exception e) {
//		    }
//		}
//		String[] updatequerys = sqlscript.split(";");
//		Connection con = getConnection();
//		Statement statement = con.createStatement();
//	
//		for (int i = 0; i < updatequerys.length; i++) {
//		    if (!updatequerys[i].trim().equals("")) {
//			try {
//			    statement.executeUpdate(updatequerys[i].trim());
//			} catch (Exception e) {
//			    System.err.println(e);
//			}
//		    }
//		}
//		con.close();
//    }
//
//    
//    /**
//      * 
//      * @return
//     * @throws SQLException 
//      * @throws Exception
//      */
//    public Connection getConnection() throws SQLException {
//		Connection connection = null;
//		try {
//		    Class.forName("org.postgresql.Driver");
//		} catch (ClassNotFoundException e) {
//		    System.err.println(e);
//		}
//
//		connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
//
//		return connection;
//    }
//
//    
//    /**
//      * This method runs the resetDB und setuptestdata script.
//      * 
//      * @throws Exception
//      */
//    public void resetTestDataDB() throws Exception {
//    	runSQLScriptFromResouces("resetDB.sql");
//    }
//
//    
//    // --- project specific methods ---
//    
//
//    /** fetches all occurences with a given intervalType from the database
//      * 
//      * @param orgaId
//      * @return
//     * @throws SQLException 
//     * @throws  
//      * @throws Exception
//      */
//    public ResultSet getOccurenceByIntervalType(int intervalType) throws SQLException {
//		String query = "select * from occurence where intervaltype ='" + intervalType + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//    }
//
//
//	public ResultSet getOccurenceByPk(int pk) throws SQLException {
//		String query = "select * from occurence where pk_occurence ='" + pk + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//
//
//	public ResultSet getEventByPk(int pk) throws SQLException {
//		String query = "select * from event where pk_event ='" + pk + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//
//
//	public ResultSet getEventCategoryByEventPkAndCategoryGuid(int eventPk, String guid) throws SQLException {
//		String query = "select * from event_category where fk_event ='" + eventPk + "' and guid = '" + guid + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//	
//	
//	public ResultSet getEventByNote(String note) throws SQLException {
//		String query = "select * from event where note ='" + note + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//	
//	
//	public ResultSet getResourcesByNotMessage(String message) throws SQLException {
//		String query = "select * from event_resource where notificationmessage ='" + message + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//
//
//	public void changeUpdateDate(int i) throws SQLException {
//		
//		String query ="update event set updatedate = current_timestamp where pk_event = "+i+";";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//	    statement.executeUpdate(query);
//		
//	}
//
//
//	public ResultSet getResourcesByPk(int i) throws SQLException {
//		String query = "select * from event_resource where pk_resource ='" + i + "';";
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		ResultSet rs = statement.executeQuery(query);
//	
//		return rs;
//	}
//
//}
