/**
 * 
 */
package de.tarent.kaliko.worker;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;

import de.tarent.commons.utils.VersionTool;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;

/**
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 * @author Nils Neumaier (n.neumaier@tarent.de) tarent GmbH Bonn
 */
public class StatusWorker {
	 
//	/**
//	 * Helper class to provide a simple interface to get a sorted key list
//	 * for velocity 
//	 * 
//	 * @author pneuha
//	 *
//	 */
//	public class VersionMap extends HashMap<String,String> {
//		
//		private static final long serialVersionUID = 1L;
//		
//		public List<String> sortedKeySet() {
//			
//			List<String> sorted = new ArrayList<String>(this.keySet());
//			Collections.sort(sorted);
//			
//			return sorted;
//		}
//	}
	
	
	/** 
	 * 
	 * Reads the build-versions from the jarfiles and returns it as list
	 * 
	 */
	@WebMethod()
	@Result("versions")
    
    public Map getAllVersions(OctopusContext oc){
    	VersionTool vt = new VersionTool();
    	
    	vt.search(new String[]{
                new File(new File(oc.moduleRootPath().getParentFile(), "WEB-INF"),"lib").getAbsolutePath()
            });
    	
    	List list = vt.getVersionInfos();
    	Map<String, String> versions = new HashMap<String, String>();
//    	VersionMap versions = new VersionMap();
    	
    	// to submit the version list via xml, we have to tranlate everything to strings
    	// during this, we extract the valid information from the entries and put it into a map
    	
    	for (Iterator iter = list.iterator(); iter.hasNext();){
    		String entry = iter.next().toString();
    		int begin = entry.indexOf(".jar:") + 6;
    		int end = entry.indexOf(',');
    		String key = entry.substring(begin, end);
    		String value = entry.substring(end + 2);
    		
    		if (!value.equals("null"))
    			versions.put(key, value);
    		else
    			versions.put(key, "Not available");
    	}
    	
    	return versions;
    }
    
    
	/** 
	 * 
	 */
	@WebMethod()
	@Result("version")
    
    public String getServerVersion(OctopusContext oc){
    	Map allversions = getAllVersions(oc);
    	
    	if (allversions.containsKey("kaliko-server"))
    		return allversions.get("kaliko-server").toString();
    	
    	return "No version for kaliko-server found";
    }
	
	public String getServerHostname(OctopusContext oc) {
		
		oc.getConfigObject();
		
		return null;
	}
}
