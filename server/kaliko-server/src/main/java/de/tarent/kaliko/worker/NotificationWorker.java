/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import javax.jws.WebMethod;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;

import de.tarent.kaliko.utils.CalendarProperties;
import de.tarent.kaliko.utils.SMTPTextMessage;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;

/**
 * worker to handle email-notification of participants of events.<br/><br/>
 * 
 * Note: the existence of  email-addresses before sending is not verified
 * as SMTP VRFY is mostly disabled on mailservers due to spammer abuse
 * 
 * @author Peter Neuhaus <p.neuhaus@tarent.de> 2008-06-06
 *
 */
public class NotificationWorker {
	
	protected final static Logger logger = Logger.getLogger(NotificationWorker.class);
	
//	private String from = ""; // daemon or owner of event???
	
	// TODO: template for subject and mail body
	
	/**
	 * Send event notification per email to specified recipient ('to')
	 * 
	 * @param oc
	 * @param sender
	 * @param to
	 * @param subject
	 * @param text
	 */
	@WebMethod()
	@Result()
	public void sendNotificationByEmail(
			OctopusContext oc,
			@Name("sender") String sender,
			@Name("to") String to,
			@Name("subject") String subject,
			@Name("text") String text) {
		
		String smtpHost = CalendarProperties.getInstance().getSMTPHost();
		boolean debug = CalendarProperties.getInstance().getSMTPDebug();
		
		SMTPTextMessage mail = null;
		
		// TODO: how to handle exceptions in octopus??? client notification? GuiException??
		try {
			mail = new SMTPTextMessage(smtpHost, sender, sender, to, subject, text);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mail.setDebug(debug);
		
		try {
			mail.send();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
