///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.component;
//
//import static org.junit.Assert.assertEquals;
//
//import java.util.List;
//import java.util.Map;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Resource;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
//
//public class DatabaseTest {
//
//	@Before
//	public void setUp() throws Exception {
//		
//	}
//	
//	
//	@Test
//	public void testExample() {
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getAvailableResources").invoke();
//		
//		//List<Resource> list = (List<Resource>) res.getData("resources");
//		//Resource resource = list.get(0);
//		
//		Map<String, String> resources = (Map<String, String>) res.getData("resources");
//		
//		assertEquals(resources.get("resource 1"), "hallo");
//	}
//	
//	
//	@Test
//	public void testSerialization() {
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getRandomResource").invoke();
//		
//		Resource resource = (Resource) res.getData("resource");
//		
//		assertEquals(resource.getText(), "Martin Pelzer");
//	}
//	
//	
//	
//	@Test
//	public void testSerializationOfLists() {
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getAvailableResourcesAsList").invoke();
//		
//		List<Resource> list = (List<Resource>) res.getData("resources");
//		Resource resource = list.get(0);
//		
//		assertEquals(resource.getText(), "Resource 1");
//	}
//
//	
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
