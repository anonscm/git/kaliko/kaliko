/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.DBContextImpl;
import de.tarent.kaliko.utils.CalendarProperties;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.OctopusContext;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CalendarDBAccess {

	public static final String KEY_MANAGED_DB_CONTEXT = "managedDBContext";
    /** 
     * Static reference for holding the single DBContext, 
     * if we are in a test environment
     */
    private static DBContext testDBContext = null;

    /**
     * Return a managed Context, which will be closed from the framework after request processing.
     * This works in an Octopus Environment, where a OctopusContext is available.
     * DBContext
     * <br>If called in a test environment, the same context is returned on every call.
     */
    public static DBContext getManagedContext() {
		OctopusContext oc = Context.getActive();
        if (oc == null) {
            if (CalendarProperties.getInstance().isInTestEnvironment()) {
                return getTestContext();
            } else {
                throw new RuntimeException("No managed Database Context available.");
            }
        }
		if (!oc.contentContains(KEY_MANAGED_DB_CONTEXT)) {
			oc.setContent(KEY_MANAGED_DB_CONTEXT, new CalendarManagedDBContext());
        }
        return (DBContext) Context.getActive().contentAsObject(KEY_MANAGED_DB_CONTEXT);
    }
    
    /**
     * Returns an unmanaged Context. The caller is resposible for closing this contextz.
     */
    public static DBContext getUnmanagedContext() {
        return DB.getDefaultContext(getPoolName());
    }

    public static DBContext getContextWithoutConnection() {
        DBContextImpl dbc = new DBContextImpl() {
                public Connection getDefaultConnection() throws SQLException {
                    throw new RuntimeException("this context is not intended for connections to the database");
                };
            };
        dbc.setPoolName(getPoolName());
		return dbc;
    }

    /** 
     * Returns a DBLayer DBcontext, for use in a testenvironment
     */
    private static DBContext getTestContext() {
        try {
            if (testDBContext == null || testDBContext.getDefaultConnection().isClosed())
                testDBContext = DB.getDefaultContext(getPoolName());
            return testDBContext;
        } catch (SQLException sqle) {
            throw new RuntimeException(sqle);
        }
    }
    
    /**
     * Returns the PoolName from the configuration
     *
     * @return poolName
     */
    public static String getPoolName(){
        return CalendarProperties.getInstance().getDBPoolName();
    }

    public static String[] INPUT_openCalendarPool = {};
    public static void openCalendarPool(){
        if (!DB.hasPool(getPoolName()))
        {
            Map config = CalendarProperties.getInstance().getDBConfig();
            DB.openPool(getPoolName(), config);
        }
    }

    /**
     * The pool with the name {@link #poolName} will be closed.
     */
    public static void closeCalendarPool(){
    	if (DB.hasPool(getPoolName()))
    		DB.closePool(getPoolName());
    }
	
}
