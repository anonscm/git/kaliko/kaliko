///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Category;
//import de.tarent.kaliko.objects.impl.CategoryImpl;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusConnection;
//
//
///**
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class EventCategoryworkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//	
//	
//	@Test
//	public void testtagCategoryToEvent(){
//		int eventPk = 1;
//		Category category = new CategoryImpl();
//		category.setGuid("guid3");
//		
//		// call task tagCategoryToEvent
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		con.getTask("tagCategoryToEvent").add("eventPk", eventPk).add("category", category).invoke();
//		
//		// get entry from table event_category in database
//		try {
//			ResultSet rs = new DBAccess().getEventCategoryByEventPkAndCategoryGuid(eventPk, category.getGuid());
//			
//			// asserts: check that there is an entry for eventPk and guid in the table event_category
//			rs.next();
//			assertTrue(rs.getInt("fk_event") == eventPk);
//			assertTrue(rs.getString("guid").equals(category.getGuid()));
//		} catch (SQLException e) {
//			assertEquals(false,e.getLocalizedMessage());
//		}
//	}
//	
//	
//	@Test
//	public void testdetagCategoryFromEvent(){
//		int eventPk = 1;
//		String guid = "guid0";
//		
//		// call task to detag category from event
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		con.getTask("detagCategoryFromEvent").add("eventPk", eventPk).add("categoryGuid", guid).invoke();
//		
//		// get database entry from table event_category with pkEvent and guid
//		try {
//			ResultSet rs = new DBAccess().getEventCategoryByEventPkAndCategoryGuid(eventPk, guid);
//			
//			// asserts: check that there is an entry for eventPk and guid in the table event_category
//			assertFalse(rs.next());
//		} catch (SQLException e) {
//			assertTrue(false);
//		}	
//	}
//	
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//
//}
