/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.objects.Occurence;
/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class OccurenceDBMapping extends AbstractDBMapping {
	
	public static final String STMT_SELECT_OCCURENCE_BY_PK = "stmtselectoccurencebypk";
	
	public OccurenceDBMapping(DBContext dbc) {
		super(dbc);
	}
	
	@Override
	public void configureMapping() {
		// add all fields of table "occurence" to this class
		addField(DBCOccurence.PK_OCCURENCE, Occurence.PROPERTY_PK, PRIMARY_KEY_FIELDS | MINIMAL_FIELDS | COMMON_FIELDS);
		addField(DBCOccurence.FK_EVENT, Occurence.PROPERTY_FK_EVENT);
		addField(DBCOccurence.INTERVALTYPE, Occurence.PROPERTY_INTERVAL_TYPE);
		addField(DBCOccurence.FREQUENCY, Occurence.PROPERTY_FREQUENCY);
		addField(DBCOccurence.TYPE, Occurence.PROPERTY_TYPE);
		addField(DBCOccurence.UPDOWN, Occurence.PROPERTY_UPDOWN);
		addField(DBCOccurence.STARTDATE, Occurence.PROPERTY_STARTDATE);
		addField(DBCOccurence.ENDDATE, Occurence.PROPERTY_ENDDATE);
		addField(DBCOccurence.STARTTIME, Occurence.PROPERTY_STARTTIME);
		addField(DBCOccurence.ENDTIME, Occurence.PROPERTY_ENDTIME);
		addField(DBCOccurence.UPDATEDATE, Occurence.PROPERTY_UPDATEDATE);
		
		//quey gets an occurence  for a givwn Primary Key
		addQuery(STMT_SELECT_OCCURENCE_BY_PK, createBasicSelectAll()
				.whereAndEq(DBCOccurence.PK_OCCURENCE, new ParamValue(Occurence.PROPERTY_PK))
				.whereAndEq(DBCOccurence.DELETED, "false")
				,COMMON_FIELDS );
	
	}
	
	@Override
	public String getTargetTable() {
		return DBCOccurence.TABLENAME;
	}
}
