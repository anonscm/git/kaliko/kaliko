/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.utils;

import org.apache.log4j.Logger;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.server.OctopusContext;

/**
 * @author Alex Maier, tarent GmbH
 *
 */
public class CalendarPropertiesOctopusImpl extends CalendarProperties {
    
    private ModuleConfig calendarModuleConfig;
    
    public static String[] INPUT_initConfiguration = {};
    
    protected final static Logger logger = Logger.getLogger(CalendarPropertiesOctopusImpl.class);
    
    /**
     * Loads the configuration and register the instance and the implementaion for BZKProperties.
     */
    public void initConfiguration(OctopusContext oc) {
        // We can savely hold the reference to this module-config,
        // since it is the same object for each OctopusContext
    	calendarModuleConfig = oc.getConfigObject().getModuleConfig();
    	
    	logger.debug("initConfiguration() called."); 
    	
        setInstance(this);
    }
    
    
	/* (non-Javadoc)
	 * @see de.tarent.hdw.bzk.utils.BZKProperties#getAttribute(java.lang.String)
	 */
	@Override
	protected Object getAttribute(String configKey) {
        Object value = calendarModuleConfig.getParamAsObject(configKey);
        if (value != null)
            return value;
        return calendarModuleConfig.getParamAsObject(configKey);
	}

}
