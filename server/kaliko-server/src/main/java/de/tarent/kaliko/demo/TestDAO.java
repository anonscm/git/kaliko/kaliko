package de.tarent.kaliko.demo;

import java.sql.SQLException;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

public class TestDAO extends AbstractDAO {

	private static TestDAO instance;
	
	TestDBMapping mapping = new TestDBMapping(CalendarDBAccess.getContextWithoutConnection());
	TestEntityFactory entityFactory = TestEntityFactory.getInstance();
	
	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((EventImpl)entity).setPk(keys.getPk());
	}

	private TestDAO() {
		super();
		setDbMapping(mapping);
		setEntityFactory(entityFactory);
	}
	
	public static synchronized TestDAO getInstance() {
		
		if (instance == null)
			instance = new TestDAO();
		
		return instance;
	}
	
	public Event getEventByPk(DBContext dbc, int pk) throws SQLException {
		return (Event) getEntityByIdFilter(
				dbc, TestDBMapping.STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK, Event.PROPERTY_PK, pk);
	}
}
