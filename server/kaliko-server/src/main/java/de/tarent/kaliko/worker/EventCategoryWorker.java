/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;

import javax.jws.WebMethod;

import org.apache.log4j.Logger;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.kaliko.dao.CategoryDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCEvent_category;
import de.tarent.kaliko.objects.Category;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;


/**
* @author Andre´ Biegel, tarent GmbH
* @author Martin Pelzer, tarent GmbH
* 
*/
public class EventCategoryWorker {
	
	protected final static Logger logger = Logger.getLogger(EventCategoryWorker.class);
	
	/**tagging an event to a category
	 * 
	 * @author Andre´ Biegel
	 * @param category the category to tag with 
	 * @param eventPk the event
	 * @throws SQLException 
	 * 
	 */
	@WebMethod()
	@Result()
	public void tagCategoryToEvent(
			@Name("eventPk")int eventPk,
			@Name("category")Category category) throws SQLException {
		
		logger.debug("executing tagCategoryToEvent(int eventPk, Category category)");
		logger.debug("tagCategoryToEvent() eventPk: '" + eventPk
				+ "' category.getName(): '" + category.getName()
				+ "' category.getGuid(): '" + category.getGuid() + "'" );
		
		CategoryDAO.getInstance().tagCategoryToEvent(CalendarDBAccess.getManagedContext(), eventPk, category);
		
//		EventCategory eventCategory = new EventCategoryImpl();
//		eventCategory.setFkEvent(eventPk);
//		eventCategory.setGuid(category.getGuid());
//		eventCategory.setName(category.getName());
//		
//		// FIXME: insert or update? (pneuha)
//		EventCategoryDAO.getInstance().insert(CalendarDBAccess.getManagedContext(), eventCategory);
//		EventCategoryDAO.getInstance().update(CalendarDBAccess.getManagedContext(), eventCategory);
	}
 
	
	/**detagging an event from a category
	 * 
	 * @author Andre´ Biegel
	 * @param category the category to tag with 
	 * @param eventPk the event
	 * @throws SQLException 
	 * 
	 */
	@WebMethod()
	@Result()
	// FIXME: compare with EventCategoryDAO.detagCategoryFromEvent()
	public void detagCategoryFromEvent(
			@Name("eventPk")int eventPk,
			@Name("categoryGuid") String categoryGuid) throws SQLException{
		
		logger.debug("executing createEvent(detagCategoryFromEvent(int eventPk, String categoryGuid)");
		
		DBContext dbc = CalendarDBAccess.getManagedContext();
		
		SQL.Delete(dbc).from(DBCEvent_category.TABLENAME)
			.whereAndEq(DBCEvent_category.FK_EVENT, eventPk)
			.whereAndEq(DBCEvent_category.GUID, categoryGuid)
			.executeDelete(dbc);
	}
		
}
