/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Simple class to send "text/plain; charset=ISO-8859-1" mail via SMTP
 * 
 * @author Peter Neuhaus <p.neuhaus@tarent.de> 2008-06-06
 * 
 *
 */
public class SMTPTextMessage {

	/**
	 * for a description of JavaMail SMTP properties see
	 * http://java.sun.com/products/javamail/javadocs/com/sun/mail/pop3/package-summary.html
	 */
	private final Properties props = new Properties();
	private final String contentType = "text/plain; charset=ISO-8859-1";
	private Session session;
	private boolean debug = false;
	
//	private String user; // TODO: SMTP Authentication -> Authenticator
//	private String passw;
	private InternetAddress from;
	private InternetAddress replyTo;
	private List<InternetAddress> to;
	private List<InternetAddress> cc;
	private List<InternetAddress> bcc;
	private String subject;
	private String messageText;

	/**
	 * Default constructor
	 */
	public SMTPTextMessage() { }
	
	/**
	 * Convenience constructor.<br/>
	 * 
	 * If "<code>to</code>, <code>cc</code> or <code>bcc</code> are <code>null</code>
	 * or have zero elements, they will be ignored.
	 * 
	 * @param hostName
	 * @param from
	 * @param replyTo
	 * @param to
	 * @param cc
	 * @param bcc
	 * @param subject
	 * @param messageText
	 * @throws AddressException
	 */
	public SMTPTextMessage(
			String hostName,
			String from,
			String replyTo,
			String to,
			String subject,
			String messageText
			) throws AddressException {
		
		setSMTPHostName(hostName);
		setFrom(from);
		setReplyTo(replyTo);
		if (to != null)
			addRecipientTo(to);
		setSubject(subject);
		setMessageText(messageText);
	}
	
	/**
	 * Convenience constructor.<br/>
	 * 
	 * If "<code>to</code>, <code>cc</code> or <code>bcc</code> are <code>null</code>
	 * or have zero elements, they will be ignored.
	 * 
	 * @param hostName
	 * @param from
	 * @param replyTo
	 * @param to
	 * @param cc
	 * @param bcc
	 * @param subject
	 * @param messageText
	 * @throws AddressException
	 */
	public SMTPTextMessage(
			String hostName,
			String from,
			String replyTo,
			String [] to,
			String [] cc,
			String [] bcc,
			String subject,
			String messageText
			) throws AddressException {
		
		setSMTPHostName(hostName);
		setFrom(from);
		setReplyTo(replyTo);
		if (to != null)
			for (String addr : to)
				addRecipientTo(addr);
		if (cc != null)
			for (String addr : cc)
				addRecipientCc(addr);
		if (bcc != null)
			for (String addr : bcc)
				addRecipientBcc(addr);
		setSubject(subject);
		setMessageText(messageText);
	}
	
	/**
	 * Set hostname for SMTP session
	 */
	public void setSMTPHostName(String hostName) {
		props.setProperty("mail.smtp.host", hostName);
	}
	
	/**
	 * Set port for SMTP session.<br/>
	 * 
	 * Defaults to 25
	 * 
	 * @param port
	 */
	public void setSMTPPort(String port) {
		props.setProperty("mail.smtp.port", port);
	}
	
	/**
	 * Set port for SMTP session.<br/>
	 * 
	 * Defaults to 25
	 * 
	 * @param port
	 */
	public void setSMTPPort(int port) {
		setSMTPPort(Integer.toString(port));
	}
	
//	public void setSMTPUser(String user) {
//		this.user = user;
//	}
	
//	public void setSMTPPassword(String passw) {
//		this.passw = passw;
//	}
	
	/**
	 * Set the "From" field of mail header.
	 */
	public void setFrom(String from) throws AddressException {
		this.from = new InternetAddress(from);
	}
	
	/**
	 * Set the "Reply-To" field of mail header. Simple consistency-checking
	 * of email address is performed by <code>InternetAdress</code>.<br/>
	 * 
	 * Possible forms of email address:<br/>
	 *   'joe.doe@someserver.foo'<br/>
	 *   'Joe Doe <joe.doe@someserver.foo>'
	 * 
	 * @param replyTo
	 * @throws AddressException
	 */
	public void setReplyTo(String replyTo) throws AddressException {
		this.replyTo = new InternetAddress(replyTo);
	}
	
	/**
	 * Adds a recipient ("To" field of mail header). Simple consistency-checking
	 * of email address is performed by <code>InternetAdress</code>.<br/>
	 * 
	 * Possible forms of email address:<br/>
	 *   'joe.doe@someserver.foo'<br/>
	 *   'Joe Doe <joe.doe@someserver.foo>'
	 * 
	 * @param to
	 * @throws AddressException
	 */
	public void addRecipientTo(String to) throws AddressException {
		if (this.to == null)
			this.to = new ArrayList<InternetAddress>();
		this.to.add(new InternetAddress(to));
	}
	
	/**
	 * Adds a carbon-copy recipient ("Cc" field of mail header). Simple consistency-checking
	 * of email address is performed by <code>InternetAdress</code>.<br/>
	 * 
	 * Possible forms of email address:<br/>
	 *   'joe.doe@someserver.foo'<br/>
	 *   'Joe Doe <joe.doe@someserver.foo>'
	 * 
	 * @param cc
	 * @throws AddressException
	 */
	public void addRecipientCc(String cc) throws AddressException {
		if (this.cc == null)
			this.cc = new ArrayList<InternetAddress>();
		this.cc.add(new InternetAddress(cc));
	}
	
	/**
	 * Adds a blind carbon-copy recipient ("Bcc" field of mail header). Simple
	 * consistency-checking of email address is performed by <code>InternetAdress</code>.<br/>
	 * 
	 * Possible forms of email address:<br/>
	 *   'joe.doe@someserver.foo'<br/>
	 *   'Joe Doe <joe.doe@someserver.foo>'
	 *   
	 * @param bcc
	 * @throws AddressException
	 */
	public void addRecipientBcc(String bcc) throws AddressException {
		if (this.bcc == null)
			this.bcc = new ArrayList<InternetAddress>();
		this.bcc.add(new InternetAddress(bcc));
	}
	
	/**
	 * Set the "Subject" field of mail header.
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * Sets the body/text of the email.
	 * 
	 * @param messageText
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	
	/**
	 * Set the debug setting for the javax.mail.Session object.<br/>
	 * 
	 * Default=true
	 * 
	 * @param debug
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	
	/**
	 * initialise SMTP session, construct and send message.
	 *  
	 * @throws MessagingException
	 */
	public void send() throws MessagingException {
		
		session = Session.getDefaultInstance(props);
		session.setDebug(debug);
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(from);
		message.setReplyTo(new InternetAddress [] { replyTo });
		if (to != null)
			message.addRecipients(RecipientType.TO, to.toArray(new InternetAddress [to.size()]));
		if (cc != null)
			message.addRecipients(RecipientType.CC, cc.toArray(new InternetAddress [cc.size()]));
		message.setSubject(subject);
		message.setContent(messageText, contentType);
		
		Transport.send(message);
	}
	
//	/**
//	 * Simple check for RFC 822 compliant email address.<br/>
//	 * 
//	 * This check is by far not complete --- it handles the common type of
//	 * email addresses in the form of 'joe.doe@someserver.foo', not more.
//	 * 
//	 * @param addr
//	 * @return
//	 */
//	private boolean isValidEmailAddress(String addr) {
//		String regex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[_A-Za-z0-9-]+)$";
//		return addr.matches(regex);
//	}
}
