/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.categories;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.tarent.commons.utils.Pojo;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.kaliko.dao.CategoryProviderDBEntryDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCCategoryprovider;
import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.CategoryProvider;
import de.tarent.kaliko.objects.CategoryProviderDBEntry;
import de.tarent.kaliko.objects.impl.CategoriesList;


/** Class for managing categories.
 *  Each event can be assigned to several categories.
 *  A category is a view on some events. A view in a calendar
 *  frontend could consist of several categories.
 *  
 * @author Martin Pelzer, tarent GmbH
 * @author Andre´ Biegel, tarent GmbH
 */
public class CategoryManager implements CategoryProvider{

	private static CategoryManager instance = null;

	private CategoryManager() {
		
	}
	/** singleton method
	 * 
	 * @return
	 */
	public static CategoryManager getInstance() {
		if (instance == null)
			instance = new CategoryManager();
		return instance;
	}
	/**methods add an category provider to the database
	 * @author Andre´ Biegel ,tarent GmbH
	 * @param catProv
	 * @throws SQLException
	 */	
	public void addCategoryProvider(CategoryProvider catProv) throws SQLException {
		
		CategoryProviderDBEntryDAO.getInstance().insert(CalendarDBAccess.getManagedContext(), catProv);
	}
	
	/**methods deletes an category provider defined by pk
	 * @author Andre´ Biegel ,tarent GmbH
	 * @param pk
	 * @throws SQLException
	 */
	public void deleteCategoryProvider(int pk) throws SQLException {
		SQL.Delete(CalendarDBAccess.getManagedContext()).from(DBCCategoryprovider.TABLENAME)		
		.whereAndEq(DBCCategoryprovider.PK_CATEGORYPROVIDER, pk)
		.executeDelete(CalendarDBAccess.getManagedContext());
	}
	
	/** returns a list of all available categories for the given
	 * user name with the given access rights
	 * 
	 * @return
	 */
	public CategoriesList getCategories(String username, Access right) {
		
		// declare the result list
		CategoriesList userCategories = null;
//		 fetch available categories from database
		List<CategoryProviderDBEntry> list = null;
		try {
			DBContext dbc = CalendarDBAccess.getUnmanagedContext();
			list = CategoryProviderDBEntryDAO.getInstance().getAll(dbc);
			dbc.getDefaultConnection().close();
		} catch (SQLException e) {
			// create an empty list (so that no error occurs in the next step)
			list = new LinkedList<CategoryProviderDBEntry>();
		}
		/* 
		 * Fetch all available categories from all category providers
		 * stored in the server database.
		 * CategoryProviders return a list of categories. Each category
		 * includes the rights the requested user (username) has.
		 */ 
		Iterator<CategoryProviderDBEntry> iter = list.iterator();
		while (iter.hasNext()) {
			CategoryProviderDBEntry catProvDB = iter.next();
			Class cls = null;
			
			if(catProvDB == null)
				throw new RuntimeException("Empty Category Provider entry. Please check configuration in database-table 'categoryprovider'");
			
			if(catProvDB.getClassname() == null)
				throw new RuntimeException("No classname configured for category-provider with PK "+catProvDB.getPk());
			
			try {
				cls = Class.forName(catProvDB.getClassname().trim());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			CategoryProvider catProv = null;
			try {
				catProv = (CategoryProvider) cls.newInstance();
				Pojo.set(catProv, "storageInformation", catProvDB.getStorageInformation());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			// Call the getCategories-Service defined by the Classname in the CategoryProvider object.
			// This service should return a list of categories.
			if (userCategories == null) {
				userCategories = catProv.getCategories(username, right); 
			}else {
				userCategories.addAll(catProv.getCategories(username, right));
			}
		} 
		return userCategories;
	}


}
