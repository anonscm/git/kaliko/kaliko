/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.kaliko.dao.CategoryDAO;
import de.tarent.kaliko.dao.OccurenceDAO;
import de.tarent.kaliko.dao.ResourceDAO;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_category;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.objects.Event;
/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/
public class EventDBMapping extends AbstractDBMapping{
	
	public static final String STMT_SELECT_EVENT_AND_OCCURENCES_BY_DATE_INTERVAL = "stmtselecteventandoccurencebydateinterval";
	public static final String STMT_SELECT_EVENT_AND_RESOURCE_AND_OCCURENCES_BY_DATE_INTERVAL="stmtselecteventandresourceandoccurencebydateinterval";
	public static final String STMT_SELECT_EVENT_AND_RESOURCES_BY_GUIDS = "stmtselecteeventandresourcesbyguids";
	
	public static final String STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK = "stmtselecteventandresourcebyeventpk";
	public static final String STMT_SELECT_EVENT_WITH_OCCURENCES = "stmtselecteventwithoccurences";
	public static final String STMT_SELECT_EVENT_WITH_OCCURENCES_AND_RESOURCES = "stmtselecteventwithoccurencesAndResources";

	public static final String STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER = "stmtselecteventsbyintervalguidsfilter";
	public static final String STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER_DISTINCT = "stmtselecteventsbyintervalguidsfilterdistinct";
	
	private static final int OCCURENCE_FIELDS = 128;
	private static final int RESOURCE_FIELDS  = 256;
	private static final int CATEGORY_FIELDS  = 512;
		
	public EventDBMapping(DBContext dbc) {
		super(dbc);
	}

	@Override
	public void configureMapping() {

		addField(DBCEvent.PK_EVENT,Event.PROPERTY_PK,PRIMARY_KEY_FIELDS | COMMON_FIELDS | MINIMAL_FIELDS  );
		addField(DBCEvent.TYPE,Event.PROPERTY_TYPE);
		addField(DBCEvent.TITLE, Event.PROPERTY_TITLE);		
//		addField(DBCEvent.CATEGORY,Event.PROPERTY_CATEGORY);
//		addField(DBCEvent.LOCATION,Event.PROPERTY_LOCATION);
//		addField(DBCEvent.NOTE,Event.PROPERTY_NOTE);
		addField(DBCEvent.UPDATEDATE, Event.PROPERTY_UPDATEDATE);
		
//		Mapping all Fields as Lists
		addFields((CategoryDBMapping)CategoryDAO.getInstance().getDbMapping(),Event.PROPERTY_CATEGORIES,CategoryDBMapping.GUID_FIELD, CATEGORY_FIELDS);
		addFields((OccurenceDBMapping)OccurenceDAO.getInstance().getDbMapping()        ,Event.PROPERTY_OCCURENCES,OccurenceDBMapping.COMMON_FIELDS, OCCURENCE_FIELDS);
		addFields((ResourceDBMapping)ResourceDAO.getInstance().getDbMapping()          ,Event.PROPERTY_RESOURCES ,ResourceDBMapping.COMMON_FIELDS, RESOURCE_FIELDS);

	
	//	query getting details of an List of events in an interval with the associated occurences
	addQuery(STMT_SELECT_EVENT_AND_RESOURCES_BY_GUIDS,createBasicSelectAll()
			.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
			.joinLeftOuter(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT,DBCEvent_resource.FK_EVENT)
    		,COMMON_FIELDS | RESOURCE_FIELDS | CATEGORY_FIELDS);
	
//		query getting details of an List of events in an interval with the associated occurences
		addQuery(STMT_SELECT_EVENT_AND_OCCURENCES_BY_DATE_INTERVAL,createBasicSelectAll()
				.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
				.joinLeftOuter(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				,COMMON_FIELDS | OCCURENCE_FIELDS | CATEGORY_FIELDS);
// query getting  an List of events in an interval with the associated occurences and resources an specified categories
		addQuery(STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER, createBasicSelectAll()
				.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
				.joinLeftOuter(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				.joinLeftOuter(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT,DBCEvent_resource.FK_EVENT)
				,COMMON_FIELDS | RESOURCE_FIELDS | OCCURENCE_FIELDS | CATEGORY_FIELDS);
		
		addQuery(STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER_DISTINCT, new Select(true).from(DBCEvent.TABLENAME)
				.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
				.joinLeftOuter(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				.joinLeftOuter(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT,DBCEvent_resource.FK_EVENT)
				,COMMON_FIELDS | RESOURCE_FIELDS | OCCURENCE_FIELDS | CATEGORY_FIELDS);
		
//				Test queries		
		addQuery(STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK, createBasicSelectAll()
				.joinLeftOuter(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT, DBCEvent_resource.FK_EVENT)
				.whereAndEq(DBCEvent.PK_EVENT, new ParamValue(Event.PROPERTY_PK))
				,COMMON_FIELDS | RESOURCE_FIELDS );
				
		addQuery(STMT_SELECT_EVENT_WITH_OCCURENCES, createBasicSelectOne()
				.joinLeftOuter(DBCOccurence.TABLENAME, DBCEvent.PK_EVENT, DBCOccurence.FK_EVENT), COMMON_FIELDS | OCCURENCE_FIELDS);
		
		addQuery(STMT_SELECT_EVENT_WITH_OCCURENCES_AND_RESOURCES, createBasicSelectOne()
				.joinLeftOuter(DBCOccurence.TABLENAME, DBCEvent.PK_EVENT, DBCOccurence.FK_EVENT)
				.joinLeftOuter(DBCEvent_resource.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_resource.FK_EVENT), COMMON_FIELDS | OCCURENCE_FIELDS | RESOURCE_FIELDS);
	}
 
	@Override
	public String getTargetTable() {
		return DBCEvent.TABLENAME;
	}
}
