/**
 * 
 */
package de.tarent.kaliko.utils;


import java.util.Arrays;

import org.apache.log4j.Logger;

import de.tarent.ldap.LdapProxy;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LDAPCalendarLoginManager extends AbstractCalendarLoginManager {

	protected LdapProxy ldapProxy;

	protected final static Logger logger = Logger.getLogger(LDAPCalendarLoginManager.class);

	public LDAPCalendarLoginManager() {

		// get ldap-config
		if(!CalendarProperties.isInitialized())
			throw new RuntimeException("CalendarProperties is not initialized!");

		String host =  (String) CalendarProperties.getInstance().getLDAPHost();
		String baseDN = (String) CalendarProperties.getInstance().getLDAPBaseDN();
		String filterExpressions = (String) CalendarProperties.getInstance().getLDAPFilterExpressions();

		if(host == null)
			throw new RuntimeException("Property 'ldap-host' is not defined in LDAP-configuration!");
		if(baseDN == null)
			throw new RuntimeException("Property 'ldap-baseDN' is not defined in LDAP-configuration!");
		
		logger.info("Configured LDAP-host is \""+host+"\" with baseDN \""+baseDN+"\"" + (filterExpressions == null ? "" : "and filter-expressions "+filterExpressions));

		// get ldap-proxy instance
		ldapProxy = LdapProxy.getInstance(host.trim(), Arrays.asList(new String[] { baseDN.trim() }));

	}

	/**
	 * @see de.tarent.kaliko.utils.AbstractCalendarLoginManager#isValidLogin(java.lang.String, java.lang.String)
	 */
	@Override
	protected boolean isValidLogin(String username, String password) {
		if(ldapProxy == null)
			throw new RuntimeException("LDAP-proxy is not configured!");

		//authenticate user by username and password
		return ldapProxy.authenticateByUid(username, password, "simple");
	}
}
