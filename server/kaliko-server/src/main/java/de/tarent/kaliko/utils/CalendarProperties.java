/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.utils;

import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author Martin Pelzer, tarent GmbH
 *
 */
public abstract class CalendarProperties {

	private static CalendarProperties instance;
	
	public static final String DB_CONFIG = "dbConfig";
	public static final String DB_POOL_NAME = "dbPoolName";
	public static final String IS_IN_TEST_ENVIRONMENT = "isInTestEnvironment";
	private static final String SMTP_CONFIG = "smtpConfig";
	private static final String SMTP_HOST = "smtpHost";
	private static final String SMTP_DEBUG = "debug";
	private static final String LDAP_BASE_DN = "ldap-baseDN";
	private static final String LDAP_HOST = "ldap-host";
	private static final String LDAP_FILTER_EXPRESSIONS = "ldap-filter-expressions";
	
	public static final String defaultPoolName = "kaliko";
	
	protected final static Logger logger = Logger.getLogger(CalendarProperties.class);
	
	/**
     * Obtain the global UPMProperties instance
     */
    public static CalendarProperties getInstance() {
    	
    	logger.debug("getInstace() called. instance="+instance);
    	
        if (instance == null)
            throw new RuntimeException("no configuration instance set");
        return instance;
    }

    public static boolean isInitialized() {
        return instance != null;
    }

    
    /**
     * Set the global UPMProperties instance
     */
    protected static void setInstance(CalendarProperties newCalendarConfiguration) {
        instance = newCalendarConfiguration;
    }

    
    /**
     * This method should be implemented by the concrete subclassed to
     * return the configuration date for a specific key. It is used by the predefined getXXX() method this abstract superclass.
     */
    protected abstract Object getAttribute(String configKey);
    
    protected String getAttributeAsString(String configKey) {
    	return (String) getAttribute(configKey);
    }
    
    protected boolean getAttributeAsBoolean(String configKey) {
    	return Boolean.getBoolean(getAttributeAsString(configKey));
    }

    
    /**
     * Returns true, if the system is in test mode e.g. JUnit Tests.
     * If the system is running as octopus module, this method returns false.
     */
    public boolean isInTestEnvironment() {
        return getAttributeAsBoolean(IS_IN_TEST_ENVIRONMENT);
    }
    
    
    /**
     * Returns the DBLayer pool name for the UPM Database
     */
    public String getDBPoolName() {
        return (String)returnWithDefault(getAttribute(DB_POOL_NAME), defaultPoolName);
    }
    
    
    /**
     * Returns a Map with the Database configuration suiteable for 
     * pool creation with tarent-database'
     */
    public Map getDBConfig(){
        return (Map)getAttribute(DB_CONFIG);
    }
    
    protected Object returnWithDefault(Object value1, Object value2) {
        if (value1 != null)
            return value1;
        return value2;
    }

    private Map getSMTPConfig() {
    	return (Map) getAttribute(SMTP_CONFIG);
    }
    
    public String getSMTPHost() {
    	return (String) getSMTPConfig().get(SMTP_HOST);
    }
    
    public boolean getSMTPDebug() {
    	return Boolean.getBoolean((String) getSMTPConfig().get(SMTP_DEBUG));
    }
    
    public String getLDAPBaseDN() {
    	return getAttributeAsString(LDAP_BASE_DN);
    }
    
    public String getLDAPHost() {
    	return getAttributeAsString(LDAP_HOST);
    }
    
    public String getLDAPFilterExpressions() {
    	return getAttributeAsString(LDAP_FILTER_EXPRESSIONS);
    }
}
