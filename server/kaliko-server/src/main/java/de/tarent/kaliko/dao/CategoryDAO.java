/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.ResultProcessor;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.factory.CategoryEntityFactory;
import de.tarent.kaliko.mapping.CategoryDBMapping;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.CategoryImpl;

/**
* @author Andre´ Biegel, tarent GmbH
*/
public class CategoryDAO extends AbstractDAO {
	
	private static CategoryDAO instance = null;
	
	CategoryDBMapping mapping = new CategoryDBMapping(CalendarDBAccess.getContextWithoutConnection());
	CategoryEntityFactory entityFactory = CategoryEntityFactory.getInstance();

	private  CategoryDAO() {
		super();
		setDbMapping(mapping);
		setEntityFactory(entityFactory);
	}
	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((CategoryImpl)entity).setPk(keys.getPk());

	}
	
//	public static AbstractDAO getInstance() {
//		if ( instance == null ){
//			 instance = new EventCategoryDAO();
//		}
//		return instance;	
//	}
	
	public static synchronized CategoryDAO getInstance() {
		if ( instance == null ){
			 instance = new CategoryDAO();
		}
		return instance;	
	}
	
	/**return an Category by an given primary key
	 * 
	 * @author Andre´ Biegel 
	 * @param dbc  Database context 
	 * @param categoryPk	primary Key of the Category
	 * @return Category which has the input primary Key
	 * @throws SQLException
	 */
	public Category getCategoryByPk(DBContext dbc, int categoryPk) throws SQLException {
		return (Category) getEntityByIdFilter(
				dbc,
				CategoryDBMapping.STMT_SELECT_CATEGORY_BY_PK,
				Category.PROPERTY_PK,
				categoryPk);
		
	}
	
	/** detags an Category from Event by deleting the guid out of the CategoryList of the Event and setting the foreign key for teh Event to -1 
	 ** {@link Category}
	 * 
	 * @author Andre´ Biegel 
	 * @param dbc  Database context 
	 * @param eventPk primary Key of the {@link Event}
	 * @param categoryPk primary Key of the {@link Category}
	 * @throws SQLException
	 */
	public void detagCategoryFromEvent(DBContext dbc, int eventPk, int categoryPk) throws SQLException {
		CategoryDAO dao = (CategoryDAO) CategoryDAO.getInstance();
		Category res    = 	dao.getCategoryByPk(dbc,categoryPk);
		Event ev = EventDAO.getInstance().getEventByPk(dbc, eventPk);
		for (int i = 0; i < ev.getCategoriesList().size(); i++)
			if (ev.getCategoriesList().get(i).equals( res.getGuid()))
				ev.getCategoriesList().remove(i--);
		
		res.setFkEvent(-1);	
		dao.update(CalendarDBAccess.getManagedContext(), res);
	}
	
	/**
	 * @author pneuha
	 * @param dbc
	 * @param event
	 * @param category
	 * @throws SQLException
	 */
	public void tagCategoryToEvent(DBContext dbc, Event event, Category category) throws SQLException {
		CategoryDAO.getInstance().tagCategoryToEvent(dbc, event.getPk(), category);
	}
	
	/**
	 * tags category to an event
	 * 
	 * @author pneuha
	 * @param dbc
	 * @param eventPk
	 * @param categoryPk
	 * @throws SQLException
	 */
	public void tagCategoryToEvent(DBContext dbc, int eventPk, Category category) throws SQLException {
		
		category.setFkEvent(eventPk);
		CategoryDAO.getInstance().insert(CalendarDBAccess.getManagedContext(), category);
	}
//	public void tagCategoryToEvent(DBContext dbc, int eventPk, Category category) throws SQLException {
//		
//		Category newCategory = new CategoryImpl();
//		newCategory.setFkEvent(eventPk);
//		newCategory.setGuid(category.getGuid());
//		newCategory.setName(category.getName());
//		
//		CategoryDAO.getInstance().insert(CalendarDBAccess.getManagedContext(), newCategory);
//	}
	
	/**return an Category by an given guid
	 * 
	 * @author Andre´ Biegel ,tarent Gmbh
	 * @param dbc  Database context 
	 * @param guid	guid of the Category
	 * @return Category which has the input guid
	 * @throws SQLException
	 */
	public Category getCategorybyGuid(DBContext dbc, String guid) throws SQLException {
		return (Category) getEntityByIdFilter(dbc, CategoryDBMapping.STMT_SELECT_CATEGORY_PK_BY_GUID, Category.PROPERTY_GUID, guid);
	}	
	/**collects categoies by given event FK´s, starting from occurences
	 * 
	 * @author Andre´ Biegel ,tarent Gmbh
	 * @param dbc database context
	 * @param eventfks list of events FK`s
	 * @return received categories
	 * @throws SQLException
	 */
	public List<String> getCategoriesByFkEventFromOccurences(DBContext dbc, List<Integer> eventfks) throws SQLException {
				
		Select query = (Select)getDbMapping().getQuery(CategoryDBMapping.STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_OCCURENCES).clone();
		query.whereAnd(Expr.in(DBCOccurence.FK_EVENT, eventfks));
		
		final List<String> list = new ArrayList<String>();
		query.iterate(dbc, new ResultProcessor() {
			public void process(ResultSet rs) throws SQLException {
				list.add(rs.getString(Category.PROPERTY_GUID));
			}			
		});
		return list;	
	
	}
	/**collects categories by given event FK´s, startting from resources
	 * 
	 * @author Andre´ Biegel,tarent GmbH
	 * @param dbc database context
	 * @param eventfks
	 * @return received categories guids
	 * @throws SQLException
	 */
	public List<String> getCategoriesByFkEventFromResources(DBContext dbc, List<Integer> eventfks) throws SQLException{
		
		Select query = (Select)getDbMapping().getQuery(CategoryDBMapping.STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_RESOURCES).clone();
		query.whereAnd(Expr.in(DBCEvent_resource.FK_EVENT, eventfks));
		
		final List<String> list = new ArrayList<String>();
		query.iterate(dbc, new ResultProcessor() {
			public void process(ResultSet rs) throws SQLException {
				list.add(rs.getString(Category.PROPERTY_GUID));
			}			
		});
		return list; 
	}
	/**collects categories by given event Pk´s
	 * @author Andre´ Biegel 
	 * @param dbc database context
	 * @param eventpks
	 * @return received categories guids
	 * @throws SQLException
	 */
	public List<String> getCategoriesByPkEvents(DBContext dbc, List<Integer> eventpks) throws SQLException{
		
		Select query = (Select)getDbMapping().getQuery(CategoryDBMapping.STMT_SELECT_CATEGORY_GUID_BY_EVENT_PKS).clone();
		query.whereAnd(Expr.in(DBCEvent.PK_EVENT, eventpks));
					
		final List<String> list = new ArrayList<String>();
		query.iterate(dbc, new ResultProcessor() {
			public void process(ResultSet rs) throws SQLException {
				list.add(rs.getString(Category.PROPERTY_GUID));
			}			
		});
		return list; 
	}
	/**collects categories by given occurence Pk´s
	 *
	 * @author Andre´ Biegel,tarent GmbH
	 * @param dbc database context
	 * @param occurencePks
	 * @return received categories guids
	 * @throws SQLException
	 */
	public List<String> getCategoriesByOccurencePks(DBContext dbc, List<Integer> occurencePks) throws SQLException{
			
		Select query = (Select)getDbMapping().getQuery(CategoryDBMapping.STMT_SELECT_CATEGORY_GUID_BY_OCCURENCEPKS).clone();
		query.whereAnd(Expr.in(DBCOccurence.PK_OCCURENCE, occurencePks));
			
		final List<String> list = new ArrayList<String>();
		query.iterate(dbc, new ResultProcessor() {
			public void process(ResultSet rs) throws SQLException {
				list.add(rs.getString(Category.PROPERTY_GUID));
			}			
		});
		return list; 
	}
	/**collects categories by given resource Pk´s
	 * 
	 * @author Andre´ Biegel,tarent GmbH
	 * @param managedContext
	 * @param resourcepks
	 * @return
	 * @throws SQLException
	 */
	public List<String> getCategoriesByResourcePks(DBContext managedContext, List<Integer> resourcepks) throws SQLException {
			
		Select query = (Select)getDbMapping().getQuery(CategoryDBMapping.STMT_SELECT_CATEGORY_GUID_BY_RESOURCEPKS).clone();
		query.whereAnd(Expr.in(DBCEvent_resource.PK_RESOURCE, resourcepks));
			
		final List<String> list = new ArrayList<String>();
		query.iterate(managedContext, new ResultProcessor() {
			public void process(ResultSet rs) throws SQLException {
				list.add(rs.getString(Category.PROPERTY_GUID));
			}			
		});
		return list;
	}
}
