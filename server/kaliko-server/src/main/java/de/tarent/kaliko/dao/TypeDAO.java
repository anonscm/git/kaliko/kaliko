/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.dao;

import java.sql.SQLException;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.factory.TypeEntityFactory;
import de.tarent.kaliko.mapping.TypeDBMapping;
import de.tarent.kaliko.objects.Type;
import de.tarent.kaliko.objects.impl.TypeImpl;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class TypeDAO extends AbstractDAO {

	private static TypeDAO instance = null;
	
	private TypeDBMapping dbMapping = new TypeDBMapping(CalendarDBAccess.getContextWithoutConnection());
	private TypeEntityFactory factory = TypeEntityFactory.getInstance();
	
	
	private TypeDAO () {
		super();
		setDbMapping(dbMapping);
		setEntityFactory(factory);
	}
	
	
	public static TypeDAO getInstance() {
		if (instance == null)
			instance = new TypeDAO();
		return instance;
	}
	
	public Type getTypeByPk(DBContext dbc, Integer pkType) throws SQLException {
		return (Type) getEntityByIdFilter(dbc, TypeDBMapping.STMT_SELECT_TYPE_BY_PK, Type.PROPERTY_PK, pkType);
	}
	
	public Type getTypeByUrn(DBContext dbc, String urn) throws SQLException {
		return (Type) getEntityByIdFilter(dbc, TypeDBMapping.STMT_SELECT_TYPE_BY_URN, Type.PROPERTY_URN, urn);
	}
	
//	public List<Type> getTypesByName(DBContext dbc, String name) throws SQLException {
//		return (List<Type>) getEntityList(dbc, TypeDBMapping.STMT_SELECT_TYPE_BY_NAME);
//	}
	
	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((TypeImpl)entity).setPk(keys.getPk());
	}
}
