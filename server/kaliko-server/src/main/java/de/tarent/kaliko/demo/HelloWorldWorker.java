package de.tarent.kaliko.demo;

import javax.jws.WebMethod;

import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;

public class HelloWorldWorker {
	
	@WebMethod()
	@Result("helloWorld")
	public String getHelloWorld(OctopusContext oc) {
		return "Hello World!";
	}
	
	@WebMethod()
	@Result("helloWorld")
	public String getGreeting(OctopusContext oc, @Name("name")@Optional String name) {
		if (name == null)
			return "Hello nobody!";
		else
			return "Hello '" + name + "'!";
	}
}
