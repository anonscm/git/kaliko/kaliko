package de.tarent.kaliko.demo;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.kaliko.dao.CategoryDAO;
import de.tarent.kaliko.dao.OccurenceDAO;
import de.tarent.kaliko.dao.ResourceDAO;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.mapping.CategoryDBMapping;
import de.tarent.kaliko.mapping.OccurenceDBMapping;
import de.tarent.kaliko.mapping.ResourceDBMapping;
import de.tarent.kaliko.objects.Event;

public class TestDBMapping extends AbstractDBMapping {
	
	public static final String STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK = "stmtselecteventandresourcebypk";
	
	private static final int OCCURENCE_FIELDS = 128;
	private static final int RESOURCE_FIELDS  = 256;
	private static final int CATEGORY_FIELDS  = 512;
	
	public TestDBMapping(DBContext dbc) {
		super(dbc);
	}

	@Override
	public void configureMapping() {
		
		addField(DBCEvent.PK_EVENT, Event.PROPERTY_PK,PRIMARY_KEY_FIELDS | COMMON_FIELDS | MINIMAL_FIELDS  );
		addField(DBCEvent.TYPE, Event.PROPERTY_TYPE);
		addField(DBCEvent.TITLE, Event.PROPERTY_TITLE);		
//		addField(DBCEvent.CATEGORY, Event.PROPERTY_CATEGORY);
//		addField(DBCEvent.LOCATION, Event.PROPERTY_LOCATION);
//		addField(DBCEvent.NOTE, Event.PROPERTY_NOTE);
		addField(DBCEvent.UPDATEDATE, Event.PROPERTY_UPDATEDATE);
		
		// Mapping all Fields as Lists
		addFields((CategoryDBMapping)CategoryDAO.getInstance().getDbMapping(),
				Event.PROPERTY_CATEGORIES,CategoryDBMapping.GUID_FIELD, CATEGORY_FIELDS);
		addFields((OccurenceDBMapping)OccurenceDAO.getInstance().getDbMapping(),
				Event.PROPERTY_OCCURENCES,OccurenceDBMapping.COMMON_FIELDS, OCCURENCE_FIELDS);
		addFields((ResourceDBMapping)ResourceDAO.getInstance().getDbMapping(),
				Event.PROPERTY_RESOURCES ,ResourceDBMapping.COMMON_FIELDS, RESOURCE_FIELDS);
		
		addQuery(STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK, createBasicSelectAll()
				.joinLeftOuter(DBCEvent_resource.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_resource.FK_EVENT)
				.whereAndEq(DBCEvent.PK_EVENT, new ParamValue(Event.PROPERTY_PK))
				,COMMON_FIELDS | RESOURCE_FIELDS );
	}

	@Override
	public String getTargetTable() {
		return DBCEvent.TABLENAME;
	}
}
