/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.categories;

import java.util.List;
import java.util.Map;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.CategoryProvider;
import de.tarent.kaliko.objects.impl.CategoriesList;

/** This class runs as a cron job in the calendar server.
 * It periodically fetches all available categories and the
 * access rights for all available users from all category
 * providers stored in the calendar server database.
 * 
 * @author Martin Pelzer, tarent GmbH
 * @author Andre´ Biegel, tarent GmbH
 *
 */
public class CategoryFetcher implements CategoryProvider {

	
	private Map<String, List<Category>> userCategories;


	public CategoryFetcher() {
		
	}
	
/*	
	public void run() {
		// fetch available categories from database
		List<CategoryProviderDBEntry> list = null;
		try {
			DBContext dbc = CalendarDBAccess.getUnmanagedContext();
			list = CategoryProviderDBEntryDAO.getInstance().getAll(dbc);
			dbc.getDefaultConnection().close();
		} catch (SQLException e) {
			// create an empty list (so that no error occurs in the next step)
			list = new LinkedList<CategoryProviderDBEntry>();
		}
		
		/* 
		 * Fetch all available categories from all category providers
		 * stored in the server database.
		 * CategoryProviders return a list of categories. Each category
		 * includes the rights the requested user (username) has.
		 
		List<User> categories = new LinkedList<User>();
		Iterator<CategoryProviderDBEntry> iter = list.iterator();
		while (iter.hasNext()) {
			CategoryProviderDBEntry catProvDB = iter.next();
			
			Class cls = null;
			try {
				cls = Class.forName(catProvDB.getClassname());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			CategoryProvider catProv = null;
			try {
				catProv = (CategoryProvider) cls.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			// Call the service defined by the Classname in the CategoryProvider object.
			// This service should return a list of categories.
			if (userCategories == null) {
				userCategories = catProv.getCategories(catProvDB.getStorageInformation(), r); 
			}else {
				Map<String, List<Category>> tmp = new LinkedHashMap<String, List<Category>>();
				tmp = catProv.getCategories(catProvDB.getStorageInformation(), right);
				for (Iterator iterator = tmp.keySet().iterator(); iterator.hasNext();) {
					String element = (String) iterator.next();
					//TODO rechte beachten ? muss noch impl werden !!
					if (userCategories.containsKey(element)) {
						userCategories.get(element).addAll(tmp.get(element));
					} else {
						userCategories.put(element, tmp.get(element));
					}
				}
				
			}
		} 
			
			
			// If the url is "local" then we call a local method to add some categories.
			/*if (catProvDB.equals("local")){
				addLocalCategories(categories);
			}
			tmp.keySet().i.size()
			else {
				// TODO will not be done in this prototype
			}
			
		
		
		// set fetched categories into CategoryManager
		CategoryManager.setCategories(userCategories);
	}
	*/
	
//	/** mockup method that adds some test categories to
//	 * the category list
//	 * 
//	 *
//	 */
//	private void addLocalCategories(List<User> categories) {
//		// add a few categories for testing and mockup reasons
//		
//		List<Category> cats = new LinkedList<Category>();
//		
//		Access access = new AccessImpl(true, true);
//		
//		Category category1 = new CategoryImpl();
//		category1.setName("Kategorie 1");
//		category1.setGuid("guid0");
//		category1.setAccess(access);
//		cats.add(category1);
//		
//		Category category2 = new CategoryImpl();
//		category2.setName("Kategorie 2");
//		category2.setGuid("guid1");
//		category2.setAccess(access);
//		cats.add(category2);
//		
//		Category category3 = new CategoryImpl();
//		category3.setName("Kategorie 3");
//		category3.setGuid("guid2");
//		category3.setAccess(access);
//		cats.add(category3);
//
//		Category category4 = new CategoryImpl();
//		category4.setName("Kategorie 4");
//		category4.setGuid("guid3");
//		category4.setAccess(access);
//		cats.add(category4);
//
//		Category category5 = new CategoryImpl();
//		category5.setName("Kategorie 5");
//		category5.setGuid("guid4");
//		category5.setAccess(access);
//		cats.add(category5);
//
//		
//		User user = new User();
//		user.setUserName("hwurst");
//		user.setCategories(cats);
//		
//		categories.add(user);
//	}


	
	public CategoriesList getCategories(String arg0, Access arg1) {
		//whether the userCagories are null then then cronjob should run! 
		if (userCategories == null) {
			} 
		return null;//userCategories;
	}





}
