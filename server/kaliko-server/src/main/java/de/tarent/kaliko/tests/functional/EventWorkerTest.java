///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Event;
//import de.tarent.kaliko.objects.Occurence;
//import de.tarent.kaliko.objects.Resource;
//import de.tarent.kaliko.objects.impl.EventImpl;
//import de.tarent.kaliko.objects.impl.EventList;
//import de.tarent.kaliko.objects.impl.Filter;
//import de.tarent.kaliko.objects.impl.OccurenceImpl;
//import de.tarent.kaliko.objects.impl.ResourceImpl;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusCallException;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
///**
// * @author Andre´ Biegel, tarent GmbH
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class EventWorkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//
//	@Test
//	public void testCreateEvent(){
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		Event test = new EventImpl();
//		Occurence occ = new OccurenceImpl();
//		Resource res = new ResourceImpl();
//		Calendar sdate = Calendar.getInstance();
//		sdate.set(2006, Calendar.MARCH, 2);
//		occ.setFrequency(2);
//		occ.setIntervalType(234);
//		occ.setFkEvent(1);
//		occ.setStartDate(sdate.getTime());
//		sdate.set(2007, Calendar.MARCH, 2);
//		occ.setEndDate(sdate.getTime());
//		res.setNotificationMessage("SoEinTestStringDerJetztZuEndeIst");
//		res.setFkEvent(1);
//		res.setType(1);
//		res.setGuid("test");
//		test.setNote("sodermüssteeindeutigseinne?");
//		test.addOccurences(occ);
//		test.addResources(res);
//		EventList events = new EventList();
//		events.add(test);
//		OctopusResult	pks = con.getTask("createEvents").add("events",events)
//			.invoke();
//		// get event pk 
//		List<Integer> container = (List<Integer>) pks.getData("pks");
//		int pk = container.get(0).intValue();
//	//		test wether the event exists
//		try {
//			ResultSet rs = new DBAccess().getEventByNote("sodermüssteeindeutigseinne?");
//			while (rs.next()) {
//				assertTrue(rs.getString("note").equals("sodermüssteeindeutigseinne?"));
//				
//			}	
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//		 
//		//test wether the new occurence also exists
//		try {
//			ResultSet rs = new DBAccess().getOccurenceByIntervalType(234);
//			while (rs.next()) {
//				assertTrue(rs.getInt("intervalType")== 234);	
//				assertEquals(rs.getInt("fk_event"),pk);
//			}	
//		} catch (SQLException ex) {
//			assertTrue(false);
//		}
//		//test wether the new resource also exists
//		try {
//			ResultSet rs = new DBAccess().getResourcesByNotMessage("SoEinTestStringDerJetztZuEndeIst");
//			while (rs.next()) {
//				assertTrue(rs.getString("notificationmessage").equals("SoEinTestStringDerJetztZuEndeIst"));	
//				assertEquals(pk, rs.getInt("fk_event"));
//				assertTrue(rs.getInt("type")== 1);
//			}
//		}	
//		catch (SQLException e) {
//			assertTrue(false);
//		}
//		try {
//			con.getTask("createEvents").add("events",events).invoke();
//		} catch (OctopusCallException e) {
//			if(GuiException.isErrorCodeDefined(e.getErrorCode())){
//				// test wether it is the error we wanted to test 
//				assertEquals("Client.ResourcesUsedInRequestedTimeInterval",e.getErrorCode());
//				assertEquals(e.getCauseMessage(),"[test]");
//				
//			}	
//			// the assertequals is here more pragmatic for getting error infomation
//			else assertEquals(false, e.getMessage());
//			} catch (Exception e) {
//			System.out.println(": "+e.getMessage()+"\n\n\n\n");
//		}
//		
//			
//	}
//	
//	
//	@Test
//	public void testUpdateEvent(){
//		Event test = new EventImpl();
//		test.setPk(1);
//		test.setNote("tja");
//		test.setUpdateDate(new Date());
//		
//		EventList events = new EventList();
//		events.add(test);
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		con.getTask("updateEvents").add("events", events).invoke();
//		try {
//			ResultSet rs = new DBAccess().getEventByPk(1);
//				while (rs.next()) {
//					//old attribute
//					assertTrue(rs.getString("location").equals("test"));
//					// updated attribute
//					assertTrue(rs.getInt("pk_event")== 1);
//					assertTrue(rs.getString("note").equals("tja"));
//				}	
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//		//following scenario 
//		// while updating  an event server realsizes an unsynchronized event!
//		
//		Filter filter = new Filter();
//		Date begin = null, end = null;
//		
//		
//		// configuring the filter
//		// enable WithResources
//		filter.setWithResources(true);
//		filter.setWithOccurences(false);
//		//collecting an event
//		 con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", begin)
//		.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter).invoke();
//		EventList list = (EventList) res.getData("events");
//	//test wether its existent
//		assertTrue(list.size() == 1);
//		// modifying content 
//		Event test2 = list.get(0);
//		test2.setNote("soderle");
//		
//		EventList events2 = new EventList();
//		events2.add(test2);
//		
//		try {
//			// change the Update date 
//			new DBAccess().changeUpdateDate(1);
//		}
//		catch (SQLException e){
//			assertTrue("SQLException thrown", false);
//		}
// 	try {
//			//trying update ->should get exception
//			con.getTask("updateEvents").add("events", events2).invoke();
//		}	
// 	// catch exception 
//		catch (OctopusCallException e){
//			// testing wether it is an known errorCode
//			if(GuiException.isErrorCodeDefined(e.getErrorCode())){
//				// test wether it is the error we wanted to test 
//				assertEquals("Client.ObjectNotSynchronized",e.getErrorCode());
//				assertEquals(e.getCauseMessage(),"Object is older than the version on the Server");
//			}
//			else assertEquals(false, e.getMessage());
//		}
//	}
//	
//	
//	@Test
//	public void testDeleteEvent(){
//		// delete event with pk 1
//		List<Integer> pks = new LinkedList<Integer>();
//		pks.add(1);
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		
//		con.getTask("deleteEvents").add("pks", pks).invoke();
//		
//		// get event with pk 1 from database and check if deleted flag is set
//		try {
//			ResultSet rs = new DBAccess().getEventByPk(1);
//			
//			// asserts: check if deleted flag is set
//			while (rs.next()) {
//				assertTrue(rs.getBoolean("deleted"));
//			}
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//	
//	
//	@Test
//	public void testGetEventsWithOccurences(){
//		// creating Matcher...
//		Matcher m;
//		//precompiling the guid expression.. 
//		Pattern p = Pattern.compile("guid[0-4]");
//		
//		boolean included = false;
//		Filter filter = new Filter();
//		
//		// configuring the filter
//		// first testing for receiving only occurences
//		filter.setWithOccurences(true);
//		filter.setWithResources(false);
//		
//		Calendar beginCal = Calendar.getInstance();
//		beginCal.set(Calendar.MONTH, Calendar.AUGUST);
//		beginCal.set(Calendar.DAY_OF_MONTH, 19);
//		beginCal.set(Calendar.HOUR_OF_DAY, 0);
//		
//		Calendar endCal = Calendar.getInstance();
//		endCal.set(Calendar.MONTH, Calendar.AUGUST);
//		endCal.set(Calendar.DAY_OF_MONTH, 21);
//		endCal.set(Calendar.HOUR_OF_DAY, 23);
//				
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", beginCal)
//		.add("end", endCal)
//		.add("categories", getCategoryGuids()).invoke();
//		//.add("filter", filter).invoke();
//		EventList list = (EventList) res.getData("events");
//
//		assertTrue(list.size() == 1);
//		
//		for (int i = 0; i < list.size(); i++) {
//			//ocurence are initiated
//			//assertNotSame(null,list.get(i).getOccurencesList()); // occurencesList is never null
//			assertTrue(list.get(i).getOccurencesList().size() == 2);
//			
//			//resources should´t be initiated
//			assertTrue(list.get(i).getResourcesList().size() == 0);
//
//			assertTrue(list.get(i).getCategoriesList().size() == 5);
//			
//			for (int j = 0; j < list.get(i).getCategoriesList().size(); j++) {
////				the guids for the associated categories should all be "guid[0-4]"
//				m = p.matcher(list.get(i).getCategoriesList().get(j));
//     			if (m.matches()) {
//					included = true;
//				}
//			}
//			assertTrue("event is not in requested category ", included);
//		}
//	
//	}
//	
//	
//	@Test
//	public void testGetEventsWithResources(){	
//		//testing now getting events with resources
//		
//		// creating Matcher...
//		Matcher m;
//		//precompiling the guid expression.. 
//		Pattern p = Pattern.compile("guid[0-4]");
//		
//		Filter filter = new Filter();
//		Calendar begin = null, end = null;
//		boolean included = false;
//		
//		// configuring the filter
//		// enable WithResources
//		filter.setWithResources(true);
//		filter.setWithOccurences(false);
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", begin)
//		.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter).invoke();
//		EventList list = (EventList) res.getData("events");
//	
//		assertTrue(list.size() == 1);
//		
//		for (int i = 0; i < list.size(); i++) {
//			//occurences aren´t initiated
//			assertTrue(list.get(i).getOccurencesList().size() == 0);
//			
//			//resources are initiateds
//			//assertNotSame( null,list.get(i).getResourcesList()); // resourcesList is never null
//			assertEquals(list.get(i).getResourcesList().size(),5);
//			//assertTrue(list.get(i).getResourcesList().size() == 5);
//			
//			assertTrue(list.get(i).getCategoriesList().size() == 5);
//			
//			for (int j = 0; j < list.get(i).getCategoriesList().size(); j++) {
////				the guids for the associated categories should all be "guid[0-4]"
//				m = p.matcher(list.get(i).getCategoriesList().get(j));
//     			if (m.matches()) {
//					included = true;
//				}
//			}
//			assertTrue("event is not in requested category ", included);
//		}
//		
//	}
//	
//	
//	@Test
//	public void testGetEventsWithResourcesAndOccurences(){		
//		// testing : collecting events with all details (resources and occurences)
//		// creating Matcher...
//		Matcher m;
//		//precompiling the guid expression.. 
//		Pattern p = Pattern.compile("guid[0-4]");
//		
//		Filter filter = new Filter();
//		Calendar begin = null, end = null;
//		boolean included = false;
//		
//		//configurings the filter
//		filter.setWithOccurences(true);
//		filter.setWithResources(true);
//		
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", begin)
//		.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter)
//		.invoke();
//		EventList list = (EventList) res.getData("events");
//		
//		assertTrue(list.size() == 1);
//		
//		for (int i = 0; i < list.size(); i++) {
//			//occurences are initiated
//			assertTrue(list.get(i).getOccurencesList().size() == 2);
//			
//			//resources are also  initiated
//			assertTrue(list.get(i).getResourcesList().size() == 5);
//			
//			assertTrue(list.get(i).getCategoriesList().size() == 5);
//			
//			for (int j = 0; j < list.get(i).getCategoriesList().size(); j++) {
////				the guids for the associated categories should all be "guid[0-4]"
//				m = p.matcher(list.get(i).getCategoriesList().get(j));
//     			if (m.matches()) {
//					included = true;
//				}
//			}
//			assertTrue("event is not in requested category ", included);
//		}
//	}
//	
//	
//	@Test
//	public void testGetEventsWithOccurencesWithUpdated(){
//		
//		Filter filter = new Filter();
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		
////configurating the filter
// 		filter.setOnlyUpdated(true);
// 		Date dateOfToday = new Date(); 	
// 		filter.setUpdatedSince(dateOfToday);
//		filter.setWithOccurences(true);
//		filter.setWithResources(false);
//// event to update....		
//		Event update = new EventImpl();
//		update.setPk(1);
//		update.setLocation("neuerstring");
//		update.setNote("neueNote");
//		update.setUpdateDate(new Timestamp(new Date().getTime()));
//		
//		EventList events = new EventList();
//		events.add(update);
//		
////	updating an event
//		con.getTask("updateEvents").add("events", events).invoke();
//		
////calling...getEvents				
//		OctopusResult res = con.getTask("getEvents")
//		//.add("begin", begin)
//		//.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter).invoke();
////receiving results		
//		EventList list = (EventList) res.getData("events");
////only 1 element updated...		
//		assertEquals(list.size(), 1);
//		try {
//			ResultSet rs = new DBAccess().getEventByPk(list.get(0).getPk());
//			while (rs.next()) {
////				old attribute
//				assertTrue(rs.getString("location").equals("neuerstring"));
//				// updated attribute
//				assertTrue(rs.getInt("pk_event")== 1);
//				assertTrue(rs.getString("note").equals("neueNote"));
//				//compare date of today and updatdate
//				assertTrue(compareStamps(rs.getTimestamp("updatedate").getTime(),dateOfToday.getTime() ));
//			}	
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//
//	
//	@Test
//	public void testGetEventsWithResourcesWithUpdated(){	
//		//testing now getting events with resources
//		
//		Filter filter = new Filter();
//		Calendar begin = null, end = null;
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//
////		configurating the filter
// 		filter.setOnlyUpdated(true);
// 
//		filter.setWithOccurences(false);
//		filter.setWithResources(true);
//// event to update....		
//		Event update = new EventImpl();
//		update.setPk(1);
//		update.setLocation("neuerstring");
//		update.setNote("neueNote");
//		update.setUpdateDate(new Date());
//		
//		EventList events = new EventList();
//		events.add(update);
//		
////	updating an event
//		Date dateOfToday = new Date(); 	
// 		filter.setUpdatedSince(dateOfToday);
//		con.getTask("updateEvents").add("events", events).invoke();
////calling getEvents...s		
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", begin)
//		.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter).invoke();
//		EventList list = (EventList) res.getData("events");
//		assertEquals(list.size(), 1);
//		try {
//			ResultSet rs = new DBAccess().getEventByPk(list.get(0).getPk());
//			while (rs.next()) {
////				old attribute
//				assertTrue(rs.getString("location").equals("neuerstring"));
//				// updated attribute
//				assertTrue(rs.getInt("pk_event")== 1);
//				assertTrue(rs.getString("note").equals("neueNote"));
//				//compare date of today and updatdate
//				assertTrue(compareStamps(rs.getTimestamp("updatedate").getTime(),dateOfToday.getTime() ));				
//			}
//				
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//
//	
//	@Test
//	public void testGetEventsWithResourcesAndOccurencesWithUpdated(){		
//		// testing : collecting events with all details (resources and occurences)
//		
//		Filter filter = new Filter();
//		Calendar begin = null, end = null;
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//
////		configurating the filter
// 		filter.setOnlyUpdated(true);
// 		Date dateOfToday = new Date(); 	
// 		filter.setUpdatedSince(dateOfToday);
//		filter.setWithOccurences(false);
//		filter.setWithResources(true);
//// event to update....		
//		Event update = new EventImpl();
//		update.setPk(1);
//		update.setLocation("neuerstring");
//		update.setNote("neueNote");
//		update.setUpdateDate(new Date());
//		
//		EventList events = new EventList();
//		events.add(update);
//		
////	updating an event
//		con.getTask("updateEvents").add("events", events).invoke();
//		OctopusResult res = con.getTask("getEvents")
//		.add("begin", begin)
//		.add("end", end)
//		.add("categories", getCategoryGuids())
//		.add("filter", filter)
//		.invoke();
//		EventList list = (EventList) res.getData("events");
//		assertEquals(list.size(), 1);
//		try {
//			ResultSet rs = new DBAccess().getEventByPk(list.get(0).getPk());
//			while (rs.next()) {
////				old attribute
//				assertTrue(rs.getString("location").equals("neuerstring"));
//		//		updated attribute
//				assertTrue(rs.getInt("pk_event")== 1);
//				assertTrue(rs.getString("note").equals("neueNote"));
//				//compare date of today and updatdate
//				assertTrue(compareStamps(rs.getTimestamp("updatedate").getTime(),dateOfToday.getTime() ));
//			}	
//		} catch (SQLException e) {
//			assertTrue(false);
//		}
//	}
//	
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//	
//	
//	private List<String> getCategoryGuids() {
//		List<String> categoryGuids = new LinkedList<String>();
//		categoryGuids.add("guid0");
//		categoryGuids.add("guid1");
//		categoryGuids.add("guid2");
//		categoryGuids.add("guid3");
//		categoryGuids.add("guid4");
//		
//		return categoryGuids;
//	}
//	
//	
//	private boolean isInInterval(long a,long b, int intvgroeße){
//		// a is in Interval of b					
//		if ((a < b - intvgroeße)||(a > b + intvgroeße)
//		// b is in interval of a		
//			||(b < a - intvgroeße)||(b > a + intvgroeße))
//			return false;
//		return 	true;
//	}
//	
//	
//	private boolean compareStamps(long a, long b){
//		return isInInterval(a, b, 2000); 
//	}
//}
