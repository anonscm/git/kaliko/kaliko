/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.filter;

import java.util.Date;

import de.tarent.commons.datahandling.ListFilterImpl;
import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.Filter;

public class CalendarEventFilter extends ListFilterImpl {
	
	boolean withResources = false;
	boolean withOccurences= false;
	boolean onlyupdated = false ;
	Date updatedSince = null;
	
	public CalendarEventFilter() {
		// XXX category and location don't exist as event-properties anymore 
//		addFilter(Event.PROPERTY_LOCATION, ListFilterOperator.OPERATOR_LIKE);
		addFilter(Event.PROPERTY_TYPE,     ListFilterOperator.OPERATOR_LIKE);
		addFilter(Event.PROPERTY_TITLE,    ListFilterOperator.OPERATOR_LIKE);
//		addFilter(Event.PROPERTY_CATEGORY, ListFilterOperator.OPERATOR_LIKE);
	}

	public CalendarEventFilter(Filter filter) {
		this();
		
		if (filter != null) {
			this.setCategory(filter.getCategory());
			this.setLocation(filter.getLocation());
			this.setOnlyUpdated(filter.getOnlyUpdated());
			this.setTitle(filter.getTitle());
			this.setType(filter.getType());
			this.setUpdatedSince(filter.getUpdatedSince());
			this.setWithOccurences(filter.getWithOccurences());
			this.setWithResources(filter.getWithResources());
			this.setLimit(filter.getLimit());
			this.setStart(filter.getStart());
		}
	}
	
	public CalendarEventFilter(CalendarEventFilter filter) {
		this();
		
		if (filter != null) {
			this.setCategory(filter.getCategory());
			this.setLocation(filter.getLocation());
			this.setOnlyUpdated(filter.getOnlyUpdated());
			this.setTitle(filter.getTitle());
			this.setType(filter.getType());
			this.setUpdatedSince(filter.getUpdatedSince());
			this.setWithOccurences(filter.getWithOccurences());
			this.setWithResources(filter.getWithResources());
			this.setLimit(filter.getLimit());
			this.setStart(filter.getStart());
		}
	}
	
	
	public void setWithResources(boolean withResources) {
		this.withResources = withResources;
	}
	
	public boolean getWithResources() {
		return withResources;
	}
	
	public void setWithOccurences(boolean withOccurences) {
		this.withOccurences = withOccurences;
	}
	
	public boolean getWithOccurences() {
		return withOccurences;
	}
	
	public void setOnlyUpdated(boolean onlyUpdated) {
		this.onlyupdated = onlyUpdated;
	}
	
	public boolean getOnlyUpdated() {
		return onlyupdated ; 
	}
	
	public String getLocation() {
		// XXX 
		return null;
//		return (String) getFilterValue(Event.PROPERTY_LOCATION);
	}
	
	public void setLocation(String location) {
		// XXX 
//		setFilterValue(Event.PROPERTY_LOCATION,location);
	}
	
	public String getType() {
		return (String)getFilterValue(Event.PROPERTY_TYPE);
	}
	
	public void setType(String type) {
		setFilterValue(Event.PROPERTY_TYPE, type);
	}
	
	public String getTitle() {
		return (String) getFilterValue(Event.PROPERTY_TITLE);
	}
	
	public void setTitle(String title) {
		setFilterValue(Event.PROPERTY_TITLE, title);
	}
	
	// XXX
	public String getCategory(){
		return null;
//		return (String)getFilterValue(Event.PROPERTY_CATEGORY);
	}
	
	// XXX
	public void setCategory(String category){
//		setFilterValue(Event.PROPERTY_CATEGORY, category);
	}
	
	public Date getUpdatedSince() {
		return updatedSince;
	}
	
	public void setUpdatedSince(Date updatedSince) {
		this.updatedSince = updatedSince;
	}
	
	public Filter returnAsSerializableFilterObject() {
		
		Filter filter = new Filter();
		filter.setCategory(this.getCategory());
		filter.setLocation(this.getLocation());
		filter.setOnlyUpdated(this.getOnlyUpdated());
		filter.setTitle(this.getTitle());
		filter.setType(this.getType());
		filter.setUpdatedSince(this.getUpdatedSince());
		filter.setWithOccurences(this.getWithOccurences());
		filter.setWithResources(this.getWithResources());
		
		return filter;
	}
}
