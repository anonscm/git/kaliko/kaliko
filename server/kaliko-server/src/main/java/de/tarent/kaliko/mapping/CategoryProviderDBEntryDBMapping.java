/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.kaliko.db.constants.DBCCategoryprovider;
import de.tarent.kaliko.objects.CategoryProviderDBEntry;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CategoryProviderDBEntryDBMapping extends AbstractDBMapping {

	
	public CategoryProviderDBEntryDBMapping(DBContext dbc) {
		super(dbc);
	}
	
	
	@Override
	public void configureMapping() {
		addField(DBCCategoryprovider.PK_CATEGORYPROVIDER , CategoryProviderDBEntry.PROPERTY_PK, PRIMARY_KEY_FIELDS | MINIMAL_FIELDS | COMMON_FIELDS);
		addField(DBCCategoryprovider.STORAGEINFORMATION , CategoryProviderDBEntry.PROPERTY_STORAGEINFORMATION);
		addField(DBCCategoryprovider.CLASSNAME , CategoryProviderDBEntry.PROPERTY_CLASSNAME);
	}

	@Override
	public String getTargetTable() {
		return DBCCategoryprovider.TABLENAME;
	}

}
