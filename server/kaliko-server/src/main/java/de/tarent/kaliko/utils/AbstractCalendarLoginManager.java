/**
 * 
 */
package de.tarent.kaliko.utils;

import java.util.Iterator;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.security.AbstractLoginManager;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.PersonalConfig;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractCalendarLoginManager extends AbstractLoginManager {

	//calendar usergroups
	private static final String GROUP_USER = "user";
	
	/**
	 * @see de.tarent.octopus.security.AbstractLoginManager#doLogin(de.tarent.octopus.config.CommonConfig, de.tarent.octopus.server.PersonalConfig, de.tarent.octopus.request.OctopusRequest)
	 */
	@Override
	protected void doLogin(CommonConfig cConf, PersonalConfig pConf, OctopusRequest request) throws OctopusSecurityException {
		
		//authenticate user by username and password
		if(isValidLogin(request.getParamAsString("username"), request.getParamAsString("password"))) {
			String [] groups = new String [] {GROUP_USER};
			pConf.setUserGroups(groups);            
			pConf.userLoggedIn(request.getParamAsString("username"));

		}
		// if it fails throw exception 
		else {
			//TODO may be logging the hole thing ? discuss with martin...
			throw new OctopusSecurityException(OctopusSecurityException.ERROR_AUTH_ERROR);
			//exception


		}
	}
	
	protected abstract boolean isValidLogin(String username, String password);

	/**
	 * This method does the actual work when a user logs out. 
     * It sets the groups of the {@link PersonalConfig} instance to just
     * {@link PersonalConfig#GROUP_LOGGED_OUT} and marks it as logged out. 
     * 
	 * @see de.tarent.octopus.security.AbstractLoginManager#doLogout(de.tarent.octopus.config.CommonConfig, de.tarent.octopus.server.PersonalConfig, de.tarent.octopus.request.OctopusRequest)
	 */
	@Override
	protected void doLogout(CommonConfig cConf, PersonalConfig pConf, OctopusRequest request) throws OctopusSecurityException {
		pConf.setUserGroups(new String[]{PersonalConfig.GROUP_LOGGED_OUT, PersonalConfig.GROUP_ANONYMOUS});
        pConf.userLoggedOut();   
        clearAllSessionData(pConf);
	}
	
	/**
     * This method cleared all sessions data. 
     * This is necessary after executing of the logout task.
     * @param pConfig {@link de.tarent.octopus.server.PersonalConfig}
     */
    private void clearAllSessionData(PersonalConfig pConfig){
        pConfig.setUserLogin(null);
        pConfig.userLoggedIn(null);
    	Iterator iter = pConfig.getSessionKeys(null);
    	while(iter.hasNext())
    		pConfig.setSessionValue((String)iter.next(), null);
    	pConfig = null;
    }
}
