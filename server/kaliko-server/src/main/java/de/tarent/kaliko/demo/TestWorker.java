package de.tarent.kaliko.demo;

import java.sql.SQLException;

import javax.jws.WebMethod;

import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Event;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;

public class TestWorker {
	
	@WebMethod()
	@Result("event")
	public Event getEventByPk(OctopusContext oc, @Name("pk") int pk) throws SQLException {
		return TestDAO.getInstance().getEventByPk(CalendarDBAccess.getManagedContext(), pk);
	}
}
