/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */


package de.tarent.kaliko.db;

import java.sql.Connection;
import java.sql.SQLException;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.DBContextImpl;
import de.tarent.kaliko.utils.CalendarProperties;
import de.tarent.octopus.server.Context;


public class CalendarManagedDBContext extends DBContextImpl{

	private Connection defaultcon = null;

	protected CalendarManagedDBContext(){
	}

	/* Overridden methods */

	
    public String getPoolName() {
        return CalendarProperties.getInstance().getDBPoolName();
    }
	
	/**
	 * Returns the default-Connection of this DBContext.
	 * @see DBContext#getDefaultConnection()
	 */
	public Connection getDefaultConnection() throws SQLException {
		if( defaultcon==null || defaultcon.isClosed()){
			defaultcon = getPool().getConnection();
			Context.getActive().addCleanupCode(new DBConnectionCloser(defaultcon));
		}
		return defaultcon;
	}
}
