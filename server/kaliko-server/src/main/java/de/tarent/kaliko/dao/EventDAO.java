/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Limit;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_category;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.factory.EventEntityFactory;
import de.tarent.kaliko.filter.CalendarEventFilter;
import de.tarent.kaliko.mapping.EventDBMapping;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.CategoryImpl;
import de.tarent.kaliko.objects.impl.EventImpl;
import de.tarent.kaliko.objects.impl.OccurencesList;
import de.tarent.kaliko.objects.impl.ResourcesList;
import de.tarent.kaliko.utils.GuiException;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;

/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/
public class EventDAO extends AbstractDAO {
	
	private static EventDAO instance = null;
	
	EventDBMapping mapping = new EventDBMapping(CalendarDBAccess.getContextWithoutConnection());
	EventEntityFactory entityFactory = EventEntityFactory.getInstance();
	
	protected final static Logger logger = Logger.getLogger(EventDAO.class);
	
	private EventDAO() {
		super();
		setDbMapping(mapping);
		setEntityFactory(entityFactory); 
	}
	
	public static synchronized EventDAO getInstance() {
				
		if ( instance == null )
			 instance = new EventDAO();

		return instance;
	}
	
	public Event getEventByPk(DBContext dbc, int pk) throws SQLException{
		return (Event)getEntityByIdFilter(dbc, EventDBMapping.STMT_SELECT_EVENT_AND_RESOURCE_BY_EVENT_PK,Event.PROPERTY_PK,pk);
	}
    
	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((EventImpl)entity).setPk(keys.getPk()); 
	}
	
	/** Deletes an event identified by its primary key.
	 * Also all resources for this event will be deleted. 
	 * 
	 * @param dbc
	 * @param pkEvent
	 * @throws SQLException 
	 */
	public void deleteByPk(DBContext dbc, int pkEvent) throws SQLException {
		// delete event
		Event event = new EventImpl();
		event.setPk(pkEvent);
		
		// delete resources of event
		ResourceDAO.getInstance().deleteAllResourcesForGivenEvent(dbc, pkEvent);
	}
	
	/** fetches an event with all assigend occurences from the database
	 * 
	 * @param dbc
	 * @param pkEvent the primary key of the wanted event
	 * @throws SQLException 
	 */
	public Event getEventByPkWithOccurences(DBContext dbc, int pkEvent) throws SQLException {
		return (Event) getEntityByIdFilter(dbc, EventDBMapping.STMT_SELECT_EVENT_WITH_OCCURENCES, Event.PROPERTY_PK, pkEvent);
	}
	
	
	/** fetches an event with all assigend occurences and resources
	 * from the database
	 * 
	 * @param dbc
	 * @param pkEvent the primary key of the wanted event
	 * @throws SQLException 
	 */
	public Event getEventByPkWithOccurencesAndResources(DBContext dbc, int pkEvent) throws SQLException {
		return (Event) getEntityByIdFilter(dbc, EventDBMapping.STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER, Event.PROPERTY_PK, pkEvent);
	}
	
	/** deletes an event by firstly deleting all assigend complex detail (like occurences) in the database
	 * actually it sets only the DELETED Flag
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param eventPk the primary key of the event, wich shuold be deleted
	 * @throws SQLException 
	 */
	public void deleteEvent(DBContext dbc,int eventPk) throws SQLException {
		//gathering all DAO´s
		Event eventToDelete = (Event)getEntityByIdFilter(dbc, EventDBMapping.STMT_SELECT_EVENT_WITH_OCCURENCES_AND_RESOURCES, Event.PROPERTY_PK,eventPk );
		ResourceDAO  daoRe = ResourceDAO.getInstance();
		OccurenceDAO daoOc = OccurenceDAO.getInstance();
		CategoryDAO daoEvCa = CategoryDAO.getInstance();
		
		//deleting Occurences..
		for (Occurence occ : eventToDelete.getOccurencesList())
			daoOc.deleteOccurence(dbc, occ.getPk());

		//deleting Resources ....
		for (Resource res : eventToDelete.getResourcesList())
			daoRe.deleteResource(dbc, res.getPk());

		//detagging Categories
		for (Category cat : eventToDelete.getCategoriesList())
			daoEvCa.detagCategoryFromEvent(dbc, eventToDelete.getPk(), cat.getPk());
		
		
//		//deleting Occurences..
//		for (int i = 0; i < eventToDelete.getOccurencesList().size(); i++)
//			daoOc.deleteOccurence(dbc, eventToDelete.getOccurencesList().get(i).getPk());
//
//		//deleting Resources ....
//		for (int i = 0; i < eventToDelete.getResourcesList().size(); i++)
//			daoRe.deleteResource(dbc, eventToDelete.getResourcesList().get(i).getPk());
//
//		//detagging Categories
//		for (int i = 0; i < eventToDelete.getCategoriesList().size(); i++) {
//			EventCategory res = daoEvCa.getCategorybyGuid(dbc, eventToDelete.getCategoriesList().get(i).getGuid());
//			daoEvCa.detagCategoryFromEvent(dbc,eventToDelete.getPk(),res.getPk());
//		}
		
		//deleting event
		SQL.Update(dbc).table(DBCEvent.TABLENAME)
			.update(DBCEvent.DELETED, "true")
			.whereAndEq(DBCEvent.PK_EVENT, eventToDelete.getPk())
			.executeUpdate(dbc);
	}
	
	/** updates an event by firstly updating all assigend (complex) details in the database
	 * 
	 * @author Andre´ Biegel 
	 * @param dbc
	 * @param renewedentry  new event details
	 * @throws SQLException 
	 * @throws ObjectSynchronizationError updatedate is null or object not synchronized
	 * @throws IllegalArgumentException renewedentry.updatedate is null!
	 * 
	 */
	public void updateEvent(DBContext dbc, Event renewedentry)
			throws SQLException, ObjectSynchronizationException, IllegalArgumentException, IllegalObjectCreationException {
		
		Event old = EventDAO.getInstance().getEventByPk(dbc, renewedentry.getPk());
		
		if (renewedentry.getUpdateDate()== null)
			throw new IllegalArgumentException("reneentry.getUpdateDate is null");

		if (old.getUpdateDate().after(renewedentry.getUpdateDate()))
			throw new ObjectSynchronizationException("event is not synchronized");

		if (renewedentry.getType() == null)
			renewedentry.setType(old.getType());

		if (renewedentry.getTitle() == null)
			renewedentry.setTitle(old.getTitle());

		if (renewedentry.getLocation() == null )
			renewedentry.setLocation(old.getLocation());

		if (renewedentry.getNote() == null)
			renewedentry.setNote(old.getNote());
		
		// we need to check if the occurrences have changed
		Iterator<Occurence> occurences = renewedentry.getOccurencesList().iterator();
		
		while(occurences.hasNext()) {
			Occurence occurence = occurences.next();
			
			if(occurence.getPk() <= 0) {
				// set fk
				occurence.setFkEvent(renewedentry.getPk());
				
				// if occurence has no pk yet, it does not exist in database and has to be created
				OccurenceDAO.getInstance().createOccurence(dbc, occurence);
				
			} else {
				// the occurence object is already existant in old occurences-list.
				// update the occurence in db
				OccurenceDAO.getInstance().updateOccurence(dbc, occurence);
			}
			
			// TODO check if occurences were removed (unassigned) from event and delete them in db
		}
		
		EventDAO.getInstance().update(dbc, renewedentry);
	}
	
	/** creates an event with all assigend details given in the event	 
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param newentry the new event
	 * @throws SQLException 
	 * @throws GuiException 
	 * @throws IllegalEventCreationException
	 */
	public int createEvent(DBContext dbc, Event newentry)
			throws IllegalObjectCreationException, SQLException, GuiException {
		
		boolean check = true;
		boolean checking = false;
		int res = newentry.getPk();
		
		OccurencesList tmpocc = newentry.getOccurencesList();
		ResourcesList  tmpres = newentry.getResourcesList();
		CategoriesList tmpcat = newentry.getCategoriesList();
		
		//if amount of Occurences = 0 -> throw Exception
		if (newentry.getOccurencesList().size() == 0)
			throw new IllegalObjectCreationException("Event´s amount of Occurences is zero");
		
		//insert event -> getting event pk  	
		EventDAO.getInstance().insert(dbc, newentry);
		
		// XXX not yet implemented in kaliko!!
		//test wether all resources are available
		//creating resources List<String> categoryGuids
		List<String> guids = new LinkedList<String>();	
		for (int i = 0; i < newentry.getResourcesList().size(); i++) {
//			set fk´s in resources -> needed in the check
			tmpres.get(i).setFkEvent(newentry.getPk());
			 //checking wether the rsources are planned somewhere else
			for (int j = 0; j < newentry.getOccurencesList().size(); j++) {
				checking = ResourceDAO.getInstance().checkResourceUsedInTimeOccurence(dbc, newentry.getResourcesList().get(i).getGuid(), newentry.getOccurencesList().get(j).getStartDate(), newentry.getOccurencesList().get(j).getEndDate());
				if (checking == false)
					guids.add(newentry.getResourcesList().get(i).getGuid());
				check  = check && checking;
			}
			if(!check) {
				GuiException e = new GuiException(GuiException.ERROR_RESOURCES_USED_IN_REQUESTED_TIME_INTERVAL);
				e.setMessage(guids.toString());
				throw e;
			}
			checking = false;
		}
		
		//set fk and insert occurence
		for (int i = 0; i < tmpocc.size(); i++)
			tmpocc.get(i).setFkEvent(newentry.getPk());

		OccurenceDAO.getInstance().createOccurences(dbc, tmpocc);
		
		ResourceDAO.getInstance().createResources(dbc, tmpres);
		
		for (Category category : tmpcat)
			CategoryDAO.getInstance().tagCategoryToEvent(dbc, newentry.getPk(), category);
		
		// XXX: disabled to prevent double categories in database
		// XXX: check createEvent call chain -> client: OctopusDataSource.addCalendarEvent()
//		EventCategory x = new EventCategoryImpl();
////		set fk and insert categories		
//		logger.debug("CategoriesList tmpcat.size(): " + tmpcat.size());
//		for (int i = 0; i < tmpcat.size(); i++) {
//			logger.debug("CategoriesList tmpcat[" + i + "]: " + tmpcat.get(i));
//			x.setFkEvent(newentry.getPk());
//			x.setGuid(tmpcat.get(i));
//			EventCategoryDAO.getInstance().insert(dbc, x);
			
//			EventCategoryDAO.getInstance().tagCategoryToEvent(dbc, newentry.getPk(), category);
//		}
			
		return res;
	}
	
	/** collects events with all associated details defined by an date interval , an filter and categoryGuids 	 
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param begin begin date
	 * @param end end date 
	 * @param categoryGuids list of category guids
	 * @param updatedSince  
	 * @param filter CalendarEventFilte
	 * @throws SQLException 
	 */
	public List<Event> getEvents(
			DBContext dbc,
			Date begin,
			Date end,
			List<String> categoryGuids,
			CalendarEventFilter filter)
			throws SQLException {
		
		Select catQuery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids))
			.whereAndEq(DBCEvent.DELETED, "false");
		
		Select pkQuery = new Select(true)
			.from(DBCEvent.TABLENAME)
			.select(DBCEvent.PK_EVENT)
			.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
			.joinLeftOuter(DBCOccurence.TABLENAME, DBCEvent.PK_EVENT, DBCOccurence.FK_EVENT)
			.joinLeftOuter(DBCEvent_resource.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_resource.FK_EVENT)
			.where(Expr.in(DBCEvent_category.FK_EVENT, catQuery))		
			.Limit(new Limit(filter.getLimit(), filter.getStart()));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER_DISTINCT).clone();
		query.where(Expr.in(DBCEvent.PK_EVENT, pkQuery));
		
		if (begin != null)
			query.whereAnd(Expr.greaterOrEqual(DBCOccurence.ENDDATE,  dateFormat.format(begin)));
		if (end != null)
			query.whereAnd(Expr.lessOrEqual(DBCOccurence.STARTDATE,  dateFormat.format(end)));

		filter.setLimit(0);
		filter.setStart(0);
		
		return getEntityList(dbc,query,filter); 
	}
	
	/** collects events with all associated details defined by an date interval , an filter, categoryGuids
	 *  and a resource url.
	 * 
	 * @param dbc
	 * @param begin first day of date interval
	 * @param end last day of date interval
	 * @param categoryGuids list of category guids
	 * @param filter ListFilter
	 * @param url url of the resource
	 * @return
	 * @throws SQLException
	 */
	public List<Event> getEventsByResource(
			DBContext dbc, 
			Date begin,
			Date end,
			List<String> categoryGuids,
			CalendarEventFilter filter,
			String resourceUrl,
			Integer resourceType)
			throws SQLException {
		
		Select catQuery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids))
			.whereAndEq(DBCEvent.DELETED, "false");
		
		Select pkQuery = new Select(true)
			.from(DBCEvent.TABLENAME)
			.select(DBCEvent.PK_EVENT)
			.joinLeftOuter(DBCEvent_category.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_category.FK_EVENT)
			.joinLeftOuter(DBCOccurence.TABLENAME, DBCEvent.PK_EVENT, DBCOccurence.FK_EVENT)
			.joinLeftOuter(DBCEvent_resource.TABLENAME, DBCEvent.PK_EVENT, DBCEvent_resource.FK_EVENT)
			.where(Expr.in(DBCEvent_category.FK_EVENT, catQuery));
		
		if(resourceUrl != null)
			pkQuery.whereAnd(Expr.equal(DBCEvent_resource.INFOURL, resourceUrl));
		if(resourceType != null)
			pkQuery.whereAnd(Expr.equal(DBCEvent_resource.FK_TYPES, resourceType));

		pkQuery.Limit(new Limit(filter.getLimit(), filter.getStart()));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER_DISTINCT).clone();
		query.where(Expr.in(DBCEvent.PK_EVENT, pkQuery));
		
		if (begin != null)
			query.whereAnd(Expr.greaterOrEqual(DBCOccurence.ENDDATE,  dateFormat.format(begin)));
		if (end != null)
			query.whereAnd(Expr.lessOrEqual(DBCOccurence.STARTDATE,  dateFormat.format(end)));

		filter.setLimit(0);
		filter.setStart(0);

		return getEntityList(dbc,query,filter); 
	}
	
	/** collects events,Occurences And Categories defined by an date interval , an filter and categoryGuids 	 
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param begin begin date
	 * @param end end date 
	 * @param categoryGuids list of category guids
	 * @param updatedSince  
	 * @param filter CalendarEventFilte
	 * @throws SQLException 
	 */	
	public List<Event> getEventsAndOccurencesAndCategories(
			DBContext dbc,
			Date begin,
			Date end,
			List<String> categoryGuids,
			CalendarEventFilter filter)
			throws SQLException {
		
		Select subquery = new Select(false)
			.from(DBCEvent_category.TABLENAME)	
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENT_AND_OCCURENCES_BY_DATE_INTERVAL).clone();
		query
			.where(Expr.in(DBCEvent_category.FK_EVENT, subquery))
			.whereAndEq(DBCEvent.DELETED, "false");
		
		if (begin != null)
			query.whereAnd(Expr.greaterOrEqual(DBCOccurence.ENDDATE,  dateFormat.format(begin)));
		if (end != null)
			query.whereAnd(Expr.lessOrEqual(DBCOccurence.STARTDATE,  dateFormat.format(end)));
		
		return getEntityList(dbc,query, filter);
	}
	/** collects events,resources and categories defined by an date interval , an filter and categoryGuids 	 
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param categoryGuids list of category guids
	 * @param updatedSince Date  
	 * @param filter CalendarEventFilte
	 * @throws SQLException 
	 */	
	public List<Event> getEventsAndResourcesAndCategories(
			DBContext dbc,
			List<String> categoryGuids,
			CalendarEventFilter filter)
			throws SQLException {
		
		Select subquery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids));
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENT_AND_RESOURCES_BY_GUIDS).clone();
		query
			.whereAnd(Expr.in(DBCEvent_category.FK_EVENT, subquery))
			.whereAndEq(DBCEvent.DELETED, "false");
		
		return getEntityList(dbc,query, filter);
	}
	
	/** collects events, occurences and categories defined by an date interval , an filter, categoryGuids and UpdatedSince date	 
	 * 
	 * @author Andre´ Biegel
	 * @param managedContext DB Context
	 * @param begin begin date
	 * @param end end date 
	 * @param categoryGuids list of category guids
	 * @param updatedSince  
	 * @param filter CalendarEventFilter
	 * @throws SQLException 
	 */
	public List<Event> getEventsAndOccurencesAndCategoriesWithUpdated(
			DBContext managedContext,
			Date begin,
			Date end,
			List<String> categoryGuids,
			CalendarEventFilter filter,
			Date updatedSince)
			throws SQLException {
		
		Select subquery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENT_AND_OCCURENCES_BY_DATE_INTERVAL).clone();
		query
			.whereAnd(Expr.in(DBCEvent_category.FK_EVENT, subquery))
			//.whereAndEq(DBCEvent.DELETED, "false")
			.whereAnd(Expr.greaterOrEqual(DBCEvent.UPDATEDATE, updatedSince));
		
		if (begin != null)
			query.whereAnd(Expr.greaterOrEqual(DBCOccurence.ENDDATE,  dateFormat.format(begin)));
		if (end != null)
			query.whereAnd(Expr.lessOrEqual(DBCOccurence.STARTDATE,  dateFormat.format(end)));
		
		return getEntityList(managedContext,query, filter);
	}
	/** collects events with Resources and Categories defined by an date interval , an filter, categoryGuids  and UpdatedSince date 	 
	 * 
	 * @author Andre´ Biegel
	 * @param managedContext DB Context
	 * @param begin here null!
	 * @param end here null !
	 * @param categoryGuids list of category guids
	 * @param updatedSince  
	 * @param filter CalendarEventFilter
	 * @throws SQLException 
	 */
	public List<Event> getEventsAndResourcesAndCategoriesWithUpdated(DBContext managedContext, List<String> categoryGuids, CalendarEventFilter filter, Date updatedSince) throws SQLException {
		Select subquery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.whereAnd(Expr.in(DBCEvent_category.GUID, categoryGuids))
			.whereAnd(Expr.greaterOrEqual(DBCEvent.UPDATEDATE, updatedSince));
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENT_AND_RESOURCES_BY_GUIDS).clone();
		query
			.whereAnd(Expr.in(DBCEvent_category.FK_EVENT, subquery))
			//.whereAndEq(DBCEvent.DELETED, "false")
			.whereAnd(Expr.greaterOrEqual(DBCEvent.UPDATEDATE, updatedSince));
		
		return getEntityList(managedContext,query, filter);
	}
	
	/** collects events with all associated details defined by an date interval , an filter, categoryGuids and UpdatedSince data 	 
	 * 
	 * @param managedContext DB Context
	 * @param begin begin date
	 * @param end end date 
	 * @param categoryGuids list of category guids
	 * @param updatedSince  
	 * @param filter CalendarEventFilter
	 * @author Andre´ Biegel
	 * @throws SQLException 
	 */
	public List<Event> getEventsWithUpdated(
			DBContext managedContext,
			Date begin,
			Date end,
			List<String> categoryGuids,
			CalendarEventFilter filter,
			Date updatedSince)
			throws SQLException {
		
		Select subquery = new Select(false)
			.from(DBCEvent_category.TABLENAME)
			.select(DBCEvent_category.FK_EVENT)
			.where(Expr.in(DBCEvent_category.GUID, categoryGuids));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Select query = (Select)this.getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER).clone();
		query
			.whereAnd(Expr.in(DBCEvent_category.FK_EVENT, subquery))
			//.whereAndEq(DBCEvent.DELETED, "false")
			.whereAnd(Expr.greaterOrEqual(DBCEvent.UPDATEDATE, updatedSince));
		
		if (begin != null)
			query.whereAnd(Expr.greaterOrEqual(DBCOccurence.STARTDATE,  dateFormat.format(begin)));
		if (end != null)
			query.whereAnd(Expr.lessOrEqual(DBCOccurence.ENDDATE,  dateFormat.format(end)));
		
		return getEntityList(managedContext,query,filter);
	}
	
	public List<Event> getEventsList(DBContext dbc, List<Integer> pks)throws SQLException {
		Select query = (Select)EventDAO.getInstance().getDbMapping().getQuery(EventDBMapping.STMT_SELECT_EVENTS_BY_INTERVAL_GUIDS_FILTER).clone();
		query
			.whereAndEq(DBCEvent.DELETED, "false")
			.whereAnd(Expr.in(DBCEvent.PK_EVENT, pks));
		
		return  (List<Event>)getEntityList(dbc,query.prepare(dbc).getPreparedStatement());
	}
}
