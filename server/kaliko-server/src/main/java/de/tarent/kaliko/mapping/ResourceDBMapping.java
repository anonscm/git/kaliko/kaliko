/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.Resource;
/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/
public class ResourceDBMapping extends AbstractDBMapping {
	
	public static final String STMT_SELECT_RESOURCES_BY_EVENT_PK = "stmtselectresourcesbyeventpk";
	public static final String STMT_SELECT_RESOURCE_BY_PK = "stmtselectresourcebypk";
	public static final String STMT_SELECT_RESUORCE_PKS_BY_STARTDATE_AND_ENDDATE = "stmtselectresourcepksbystartdateandenddate";
	
	public ResourceDBMapping(DBContext dbc) {
		super(dbc);
	}

	@Override
	public void configureMapping() {
		addField(DBCEvent_resource.PK_RESOURCE,Resource.PROPERTY_PK,PRIMARY_KEY_FIELDS | MINIMAL_FIELDS | COMMON_FIELDS );
		addField(DBCEvent_resource.FK_TYPES, Resource.PROPERTY_FK_TYPES);
		addField(DBCEvent_resource.FK_EVENT, Resource.PROPERTY_FK_EVENT);
		addField(DBCEvent_resource.GUID, Resource.PROPERTY_GUID);
		addField(DBCEvent_resource.TEXT, Resource.PROPERTY_TEXT);
		addField(DBCEvent_resource.INFOURL, Resource.PROPERTY_INFOURL);
		addField(DBCEvent_resource.NOTIFICATIONTIME, Resource.PROPERTY_NOTIFICATIONTIME);
		addField(DBCEvent_resource.NOTIFICATIONMESSAGE, Resource.PROPERTY_NOTIFICATIONMESSAGE);
		addField(DBCEvent_resource.PARTICIPATIONSTATUS, Resource.PROPERTY_PARTICIPATIONSTATUS);
		addField(DBCEvent_resource.PARTICIPATIONNOTE, Resource.PROPERTY_PARTICIPATIONNOTE);
		addField(DBCEvent_resource.UPDATEDATE,Resource.PROPERTY_UPDATEDATE);
		
//		query gets the resources for one event pk 
		addQuery(STMT_SELECT_RESOURCES_BY_EVENT_PK, createBasicSelectAll()
				.whereAndEq(DBCEvent_resource.FK_EVENT, new ParamValue(Event.PROPERTY_PK))
				.whereAndEq(DBCEvent_resource.DELETED, "false")
				,COMMON_FIELDS);
//		query gets an resource fo an given pk 		
		addQuery(STMT_SELECT_RESOURCE_BY_PK, createBasicSelectAll()
				.whereAndEq(DBCEvent_resource.PK_RESOURCE, new ParamValue(Resource.PROPERTY_PK))
				.whereAndEq(DBCEvent_resource.DELETED, "false")
				,COMMON_FIELDS);
		//query collects resource pks which have events in [startdate..enddate]
		addQuery(STMT_SELECT_RESUORCE_PKS_BY_STARTDATE_AND_ENDDATE,createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_resource.FK_EVENT, DBCEvent.PK_EVENT)
				.join(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				.whereAndEq(DBCEvent_resource.GUID, new ParamValue(Resource.PROPERTY_GUID))
				.whereAndEq(DBCEvent_resource.DELETED, "false")
				.whereAnd(Expr.greaterOrEqual(DBCOccurence.STARTDATE,new ParamValue(Occurence.PROPERTY_STARTDATE)))
				.whereAnd(Expr.lessOrEqual(DBCOccurence.ENDDATE, new ParamValue(Occurence.PROPERTY_ENDDATE)))	
				,MINIMAL_FIELDS);
	}

	@Override
	public String getTargetTable() {
		return DBCEvent_resource.TABLENAME;
	}

}
