/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.mapping;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.kaliko.db.constants.DBCEvent;
import de.tarent.kaliko.db.constants.DBCEvent_category;
import de.tarent.kaliko.db.constants.DBCEvent_resource;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.objects.Category;

public class CategoryDBMapping extends AbstractDBMapping {

	public static final String STMT_SELECT_CATEGORY_BY_PK = "stmtselectcategorybypk";
	public static final String STMT_SELECT_CATEGORY_PK_BY_GUID = "stmtselectcategorypkbyguid";
	
	public static final String STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_OCCURENCES = "stmtselectcategryguidsbyfkeventfromoccurences";
	public static final String STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_RESOURCES = "stmtselectcategryguidsbyfkeventfromresources";
	public static final String STMT_SELECT_CATEGORY_GUID_BY_EVENT_PKS ="stmtselectcategoryguidbyeventpks";
	public static final String STMT_SELECT_CATEGORY_GUID_BY_OCCURENCEPKS = "stmtselectcategryguidsbyoccurencepks";
	public static final String STMT_SELECT_CATEGORY_GUID_BY_RESOURCEPKS = "stmtselectcategryguidsbyresourcepks";
	//Guids flag
	public static final int GUID_FIELD = 1024;
	
	
	public CategoryDBMapping(DBContext dbc) {
		super(dbc);
	}
	
	@Override
	public void configureMapping() {
		addField(DBCEvent_category.PK_CATEGORY,Category.PROPERTY_PK, PRIMARY_KEY_FIELDS | COMMON_FIELDS | MINIMAL_FIELDS);
		addField(DBCEvent_category.FK_EVENT,Category.PROPERTY_FK_EVENT);
		addField(DBCEvent_category.GUID, Category.PROPERTY_GUID , GUID_FIELD | COMMON_FIELDS | WRITEABLE_FIELDS);		
		addField(DBCEvent_category.NAME,Category.PROPERTY_NAME);

		
		addQuery(STMT_SELECT_CATEGORY_BY_PK	, createBasicSelectOne()
				.whereAndEq(DBCEvent_category.PK_CATEGORY,	new ParamValue(Category.PROPERTY_PK))
				, COMMON_FIELDS);
		
		addQuery(STMT_SELECT_CATEGORY_PK_BY_GUID	, createBasicSelectOne()
				.whereAndEq(DBCEvent_category.GUID,	new ParamValue(Category.PROPERTY_GUID))
				, MINIMAL_FIELDS);
		
		//queries for checkPermission
		
		addQuery(STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_OCCURENCES,createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_category.FK_EVENT,DBCEvent.PK_EVENT)
				.join(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				//.whereAnd(Expr.in(DBCOccurence.FK_EVENT, new ParamValue("eventFksFromOccurences")))
				,GUID_FIELD);
		
		addQuery(STMT_SELECT_CATEGORY_GUID_BY_FK_EVENT_FROM_RESOURCES, createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_category.FK_EVENT,DBCEvent.PK_EVENT)
				.join(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT , DBCEvent_resource.FK_EVENT)
				//.whereAnd(Expr.in(DBCEvent_resource.FK_EVENT, new ParamValue("eventFksFromResources")))
				, GUID_FIELD);
		
		addQuery(STMT_SELECT_CATEGORY_GUID_BY_EVENT_PKS, createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_category.FK_EVENT,DBCEvent.PK_EVENT)
				//.whereAnd(Expr.in(DBCEvent.PK_EVENT, new ParamValue("eventPks")))
				,GUID_FIELD);
		
		addQuery(STMT_SELECT_CATEGORY_GUID_BY_OCCURENCEPKS,createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_category.FK_EVENT,DBCEvent.PK_EVENT)
				.join(DBCOccurence.TABLENAME,DBCEvent.PK_EVENT,DBCOccurence.FK_EVENT)
				//.whereAnd(Expr.in(DBCOccurence.PK_OCCURENCE, new ParamValue("occurencePks")))
				,GUID_FIELD);
		
		addQuery(STMT_SELECT_CATEGORY_GUID_BY_RESOURCEPKS, createBasicSelectAll()
				.join(DBCEvent.TABLENAME, DBCEvent_category.FK_EVENT,DBCEvent.PK_EVENT)
				.join(DBCEvent_resource.TABLENAME,DBCEvent.PK_EVENT , DBCEvent_resource.FK_EVENT)
				//.whereAnd(Expr.in(DBCEvent_resource.PK_RESOURCE, new ParamValue("resourcepks")))
				, GUID_FIELD);
		
		

	}

	@Override
	public String getTargetTable() {
		return DBCEvent_category.TABLENAME;
	}

}
