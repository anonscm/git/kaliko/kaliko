/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;

import de.tarent.kaliko.categories.CategoryManager;
import de.tarent.kaliko.dao.CategoryProviderDBEntryDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.CategoryProvider;
import de.tarent.kaliko.objects.impl.AccessImpl;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CategoryWorker {

	
	/** returns a list of all categories available at the category
	 * providers stored in the server database
	 * 
	 * @param access defines if readable, writeable or all available
	 * categories will be returned. May not be given. In this case all
	 * available categories are returned.
	 * @throws SQLException
	 */
	@WebMethod()
	@Result("categoryList")
	public CategoriesList getAvailableCategories(OctopusContext oc, @Name("access") @Optional Access access) throws SQLException{
		if (access == null){
			access = new AccessImpl(true, true);
		}
		return CategoryManager.getInstance().getCategories(oc.getConfigObject().getLoginname(),access);
	}
	
	
	/** returns a list of the category providers stored in the database
	 * NOTE: this task is not available to clients at the moment
	 * 
	 * @throws SQLException
	 */
	@WebMethod()
	@Result("categoryProviders")
	public List<CategoryProvider> getCategoryProviders() throws SQLException{
		return CategoryProviderDBEntryDAO.getInstance().getAll(CalendarDBAccess.getManagedContext());
	}
	
	/** returns a category, selected by its name.
	 * 
	 * @param name name of the category
	 * @param categories list of available categories
	 * @return the category with the given name
	 * @throws SQLException
	 */
	@WebMethod()
	@Result("category")
	public Category getCategoryByName(OctopusContext oc, @Name("categoryName") String name, @Name("categoryList") List<Category> categories) throws SQLException{
		for(Category category : categories) {
			if(category.getName().equals(name))
				return category;
		}
		return null;
	}

}
