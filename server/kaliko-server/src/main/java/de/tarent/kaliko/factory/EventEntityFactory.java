/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.factory;

import de.tarent.commons.datahandling.entity.AttributeSource;
import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.EntityFactory;
import de.tarent.commons.datahandling.entity.LookupContext;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.impl.EventImpl;

/**
* 
* @author Andre´ Biegel, tarent GmbH
*
*/

public class EventEntityFactory extends DefaultEntityFactory {

	private static EventEntityFactory instance;
	
	private EventEntityFactory() {
		super(EventImpl.class);
	}
	
	public static synchronized EventEntityFactory getInstance(){
		
		if (instance == null) {
			instance = new EventEntityFactory();
		}
		return instance;
	}
	
	/**
     * Template method for returning an Entity from the LookupContext. Default implementation allways returns null.
     */
	@Override
    public Object getEntityFromLookupContext(AttributeSource as, LookupContext lc) {
    	return lc.getEntity(as.getAttribute(Event.PROPERTY_PK), EventImpl.class.getName());
    }

    
    /**
     * Default implementation for template method. This implementation does not store the entity.
     */
	@Override
    public void storeEntityInLookupContext(Object entity, LookupContext lc) {
        lc.registerEntity(((EventImpl)entity).getPk(), EventImpl.class.getName(), entity);
    }
    
	@Override
    protected EntityFactory getFactoryFor(String attributeName) {
        if (Event.PROPERTY_OCCURENCES.equals(attributeName))
            return OccurenceEntityFactory.getInstance();
        if (Event.PROPERTY_RESOURCES.equals(attributeName))
            return ResourceEntityFactory.getInstance();
        if (Event.PROPERTY_CATEGORIES.equals(attributeName))
        	return CategoryEntityFactory.getInstance();
        return null;
    }
	
}
