/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.utils;

/** The Exception will be thrown ,when realizing that an CalendarEventFilter
 * has an UpdateFlag set , but has no updatedTill Date !
 * @author Andre´ Biegel ,tarent GmbHS
 *
 */
public class IllegalCalendarEventFilterConfigurationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public IllegalCalendarEventFilterConfigurationException() {
		super();
	}

	/**
	 * @param message
	 */
	public IllegalCalendarEventFilterConfigurationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public IllegalCalendarEventFilterConfigurationException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public IllegalCalendarEventFilterConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

}
