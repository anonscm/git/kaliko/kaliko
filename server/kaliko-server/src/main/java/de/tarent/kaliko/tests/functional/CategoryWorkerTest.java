///*
// * Kaliko Server,
// * Implementation of the webservice based kaliko-calender-server
// * Copyright (C) 2000-2007 tarent GmbH
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License,version 2
// * as published by the Free Software Foundation.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// *
// * tarent GmbH., hereby disclaims all copyright
// * interest in the program 'Kaliko Server'
// * Signature of Elmar Geese, 26 September 2007
// * Elmar Geese, CEO tarent GmbH.
// */
//
//package de.tarent.kaliko.tests.functional;
//
//import static org.junit.Assert.assertEquals;
//
//import java.util.List;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import de.tarent.kaliko.objects.Access;
//import de.tarent.kaliko.objects.Category;
//import de.tarent.kaliko.objects.impl.AccessImpl;
//import de.tarent.kaliko.testutils.CalendarOctopusConnection;
//import de.tarent.kaliko.testutils.DBAccess;
//import de.tarent.octopus.client.OctopusConnection;
//import de.tarent.octopus.client.OctopusResult;
//
///**
// * 
// * @author Martin Pelzer, tarent GmbH
// *
// */
//public class CategoryWorkerTest {
//
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		new DBAccess().resetTestDataDB();
//	}
//	
//	
//	/** tests the getCategories service.
//	 * NOTE: this dependes depends heavily on the hard coded categories in
//	 * de.tarent.libraries.calendar.utils.CategoryManager! 
//	 *
//	 */
//	@Test
//	public void testGetCategories(){
//		OctopusConnection con = CalendarOctopusConnection.getInstance().getOctopusConnection();
//		Access access = new AccessImpl(true, true);
//		OctopusResult res = con.getTask("getCategories").add("access", access).invoke();
//
//		List<Category> categories = (List<Category>) res.getData("categoryList");
//		
//		// there have to be three categories
//
//		
//		// the first one should have the name "Kategorie 1" and the guid "1"
//		Category cat1 = categories.get(0);
//		assertEquals(cat1.getName(),"Kategorie 1");
//		
//		assertEquals(cat1.getGuid(),"guid0");
//		
//		// the second one should have the name "Kategorie 2" and the guid "2"
//		Category cat2 = categories.get(1);
//		assertEquals(cat2.getName(),"Kategorie 2");
//		assertEquals(cat2.getGuid(),"guid1");
//		// the third one should have the name "Kategorie 3" and the guid "3"
//		Category cat3 = categories.get(2);
//		assertEquals(cat3.getName(),"Kategorie 3");
//		assertEquals(cat3.getGuid(),"guid2");
//		int j = 0;
//		for (int i = 0; i < categories.size(); i++) {
//			if (categories.get(i).getGuid().equals("LDAPCatProv_hwurst_PRIVAT")){
//				j++;
//			}
//		}
//		assertEquals(1, j);
//	}
//	
//	
//	/**
//	 * @throws java.lang.Exception
//	 */
//	@After
//	public void tearDown() throws Exception {
//	}
//	
//}
