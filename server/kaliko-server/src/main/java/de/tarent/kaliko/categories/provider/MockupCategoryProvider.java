/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.kaliko.categories.provider;

import java.util.StringTokenizer;

import de.tarent.kaliko.objects.Access;
import de.tarent.kaliko.objects.Category;
import de.tarent.kaliko.objects.CategoryProvider;
import de.tarent.kaliko.objects.impl.AccessImpl;
import de.tarent.kaliko.objects.impl.CategoriesList;
import de.tarent.kaliko.objects.impl.CategoryImpl;

/**
 * @author Andre´ Biegel, tarent GmbH
 *
 */
public class MockupCategoryProvider implements CategoryProvider {

	private String storageInformation;
	private static int guidnr;
	/* (non-Javadoc)
	 * @see de.tarent.libraries.calendar.CategoryProvider#getCategories(java.lang.String, de.tarent.libraries.calendar.Access)
	 */
	public CategoriesList getCategories(String arg0, Access arg1) {
		return  storageInformationToCategoryList();
	}

	private CategoriesList storageInformationToCategoryList() {
		guidnr = -1;
		CategoriesList result = new CategoriesList();
		StringTokenizer tokenizer  = new StringTokenizer(storageInformation,",",false);	
		while (tokenizer.hasMoreTokens()) {
			String element = tokenizer.nextToken();
			Category tmp = new CategoryImpl();
			Access tmp2 = new AccessImpl(true, true);
			tmp.setName(element);
			tmp.setGuid(getGuid());
			tmp.setAccess(tmp2);
			result.add(tmp);
		}
		return result;
	} 
	
	private String getGuid() {
		guidnr ++;
		return "guid"+ guidnr;
	}
	public void setStorageInformation(String storageInformation){
		this.storageInformation = storageInformation;
	}

}
