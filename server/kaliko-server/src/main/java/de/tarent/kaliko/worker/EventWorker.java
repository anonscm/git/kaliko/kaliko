/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;

import org.apache.log4j.Logger;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.kaliko.common.ResourceGuidGenerator;
import de.tarent.kaliko.dao.EventDAO;
import de.tarent.kaliko.dao.ResourceDAO;
import de.tarent.kaliko.dao.TypeDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.filter.CalendarEventFilter;
import de.tarent.kaliko.objects.Event;
import de.tarent.kaliko.objects.Resource;
import de.tarent.kaliko.objects.Type;
import de.tarent.kaliko.objects.impl.EventList;
import de.tarent.kaliko.objects.impl.Filter;
import de.tarent.kaliko.objects.impl.ResourceImpl;
import de.tarent.kaliko.utils.CalendarProperties;
import de.tarent.kaliko.utils.GuiException;
import de.tarent.kaliko.utils.IllegalCalendarEventFilterConfigurationException;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.OctopusContext;

/**
 * 
 * @author ?
 *
 */
public class EventWorker {
	
	protected final static Logger logger = Logger.getLogger(EventWorker.class);
	
	/** creates an event
	 * 
	 * @param event the event to add
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result("pks")
	public List<Integer> createEvents(OctopusContext oc, @Name("events") EventList events) throws OctopusSecurityException {
		
		logger.debug("executing createEvents(EventList events)");
		
		String username = oc.getRequestObject().getParamAsString("username");
		String ldapSearchPath = CalendarProperties.getInstance().getLDAPBaseDN();
		String ldapHostUrl = CalendarProperties.getInstance().getLDAPHost();
		
		DBContext dbc = CalendarDBAccess.getManagedContext();
		List<Integer> pks = new LinkedList<Integer>();
		
		try {
			Iterator<Event> iter = events.iterator();
			while (iter.hasNext()) {
				Event event = iter.next();
				EventDAO.getInstance().createEvent(dbc, event);
				
				// ldap-user is automatically owner of the event
				Resource owner = new ResourceImpl();
				Type type = TypeDAO.getInstance().getTypeByUrn(CalendarDBAccess.getManagedContext(), "owner");
				owner.setFkEvent(event.getPk());
				owner.setFkTypes(type.getPk());
				owner.setGuid(ResourceGuidGenerator.getOwnerGuid(ldapHostUrl, ldapSearchPath, username));
				owner.setText(username); // XXX maybe full name (ldap 'displayName') here?
				
				// XXX warum nicht ResourceList??
				List<Resource> newentries = new ArrayList<Resource>();
				newentries.add(owner);
				
				ResourceDAO.getInstance().createResources(dbc, newentries);
				pks.add(event.getPk());
			}
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		} catch (IllegalObjectCreationException e) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG, e);
		} catch (IllegalArgumentException e) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG, e);
		}
		return pks;
	}
	
//	/** creates an event from the given data and returns it
//	 * 
//	 * @param oc the octopus context
//	 * @throws SQLException
//	 * @returns the created event object
//	 */
//	@WebMethod()
//	@Result("event")
//	public Event createEvent(OctopusContext oc) throws SQLException {
//		
//		logger.debug("executing createEvent(OctopusContext oc)");
//		
//		// create an attribute source and add all attributes we want to fill
//		OctopusAttributeSource oas = new OctopusAttributeSource(oc);
//
//		if (oc.getRequestObject().containsParam("pk_event"))
//			oas.addAttributeName(Event.PROPERTY_PK);
//		oas.addAttributeName(Event.PROPERTY_TYPE);
//		oas.addAttributeName(Event.PROPERTY_TITLE);
//		oas.addAttributeName(Event.PROPERTY_CATEGORY);
//		oas.addAttributeName(Event.PROPERTY_LOCATION);
//		oas.addAttributeName(Event.PROPERTY_NOTE);
//		
//		// get an entity and fill it using the attribute source
//		Event event = (Event) EventDAO.getInstance().getEntityFactory().getEntity();
//		EventDAO.getInstance().getEntityFactory().fillEntity(event, oas, null);
//		
//		return event;
//	}
	
	/** updates an event
	 * 
	 * @param event the event to update
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s
	 */
	@WebMethod()
	@Result()
	public void updateEvent(@Name("events") EventList events) throws OctopusSecurityException {
		
		logger.debug("executing updateEvent(EventList events)");
		
		DBContext dbc = CalendarDBAccess.getManagedContext();
		
		try {
			Iterator<Event> iter = events.iterator();
			while (iter.hasNext()) {
				EventDAO.getInstance().updateEvent(dbc, iter.next());
			}
		} catch (ObjectSynchronizationException e) {
			throw new GuiException(GuiException.ERROR_OBJECT_NOT_SYNCHRONIZED, e);
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		} catch (IllegalArgumentException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		} catch (IllegalObjectCreationException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		}
	}
	
	
	/** deletes an event identified by its primary key
	 * 
	 * @param pkEvent the primary key of the event to delete
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result()
	public void deleteEvent(@Name("pks") List<Integer> pks) throws OctopusSecurityException {
		
		logger.debug("executing deleteEvent(List<Integer> pks)");
		
		DBContext dbc = CalendarDBAccess.getManagedContext();
		
		try {
			Iterator<Integer> iter = pks.iterator();
			while (iter.hasNext()) {
				EventDAO.getInstance().deleteEvent(dbc, iter.next());
			}
		} catch (SQLException e) {
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		}
	}
	
	/** returns a list of events 
	 * 
	 * @author Andre´ Biegel,tarent GmbH
	 * @param begin begin of the date interval
	 * @param end end of the date interval
	 * @param categoryGuids LIst of categories,in which the receiving events have to be 
	 * @param givenFilter filter for more possibilities of limiting the return 
	 * @param resourceUrl url of a resource
	 * @param resourceType type of a resource
	 * @return EventList list of events in the specified interval
	 * @throws OctopusSecurityException Exception which will be sent to  the client, exatinfomation are hidden inside with errorCode´s 
	 */
	@WebMethod()
	@Result("events")
	public EventList getEvents(
			@Name("begin") @Optional Calendar beginCal,
			@Name("end") @Optional Calendar endCal,
			@Name("categories") @Optional List<String> categoryGuids,
			@Name("filter") @Optional Filter givenFilter,  
			@Name("resourceUrl") @Optional() String resourceUrl,
			@Name("resourceType") @Optional() Integer resourceType)
	throws OctopusSecurityException
	{
		logger.debug(
				"executing getEvents(Calendar beginCal, Calendar endCal, List<String> categoryGuids, Filter givenFilter, String resourceUrl, Integer resourceType)");
		
		for (String guid : categoryGuids)
			logger.debug("guid: '" + guid + "'");
		
		CalendarEventFilter filter = new CalendarEventFilter(givenFilter);
		if(filter.getLimit() > 0)
			filter.setUseLimit(true);
		
		List<Event> eventList;
		
		Date begin = null;
		if (beginCal != null)
			begin = beginCal.getTime();
					
		Date end = null;
		if (endCal != null)
			end = endCal.getTime();
		
		try{
			//Updated-Flag is  NOT set
			//with occurences and categories
			if (!filter.getOnlyUpdated()) {
				// both and categories
				if (filter.getWithResources() && filter.getWithOccurences()) {
					if(resourceUrl != null || resourceType != null) {
						eventList = EventDAO.getInstance().getEventsByResource(
								CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter, resourceUrl, resourceType);
					}
					else {
						eventList = EventDAO.getInstance().getEvents(
								CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter);
					}
				}
				else if (filter.getWithOccurences()) {
					eventList = EventDAO.getInstance().getEventsAndOccurencesAndCategories(
							CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter);
				}
				// with resources and categories
				else if (filter.getWithResources()) {
					eventList = EventDAO.getInstance().getEventsAndResourcesAndCategories(
							CalendarDBAccess.getManagedContext(), categoryGuids, filter);
				}	
				else {
				// only events with categories and occurences
					eventList = EventDAO.getInstance().getEventsAndOccurencesAndCategories(
							CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter);
				}
			}
			//Updated-Flag is set
			else {
				//updated Flag is set but Date object is not set ->
				if (filter.getUpdatedSince() == null) throw new IllegalCalendarEventFilterConfigurationException();	
			
				// both and categories
				if (filter.getWithResources() && filter.getWithOccurences()) {
					eventList = EventDAO.getInstance().getEventsWithUpdated(
							CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter, filter.getUpdatedSince());
				}
				else if (filter.getWithOccurences()) {
					eventList = EventDAO.getInstance().getEventsAndOccurencesAndCategoriesWithUpdated(
							CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter, filter.getUpdatedSince());
				}
				// with resources and categories
				else if (filter.getWithResources()) {
					eventList = EventDAO.getInstance().getEventsAndResourcesAndCategoriesWithUpdated(
							CalendarDBAccess.getManagedContext(), categoryGuids, filter, filter.getUpdatedSince());
				}
				else {
				// only events with categories
					eventList = EventDAO.getInstance().getEventsAndOccurencesAndCategoriesWithUpdated(
							CalendarDBAccess.getManagedContext(), begin, end, categoryGuids, filter, filter.getUpdatedSince());
				}
			}
		}
		catch (SQLException	e) {
			//e.printStackTrace();
			throw new GuiException(GuiException.ERROR_INTERNAL_SERVER_ERROR, e);
		}
		catch (IllegalCalendarEventFilterConfigurationException ex) {
			throw new GuiException(GuiException.ERROR_PARAMETER_MISSING_OR_WRONG, ex);
		}
		
		EventList list = new EventList();
		list.addAll(eventList); 
		return list;
	}
	
	@WebMethod()
	@Result("eventcount")
	public Integer getEventCount(@Name("events") EventList eventList) {
		logger.debug("executing getEventCount(EventList eventList)");
		return new Integer(eventList.size());
	}

	
	@WebMethod()
	@Result()
	public void addResourceToEvent(@Name("pkResource") int pkResource, @Name("pkEvent") int pkEvent) {
		logger.debug("executing addResourceToEvent(int pkResource, int pkEvent)");
		// TODO das ist Unsinn. die Ressource liegt ja noch gar nicht
		// in der DB. Also gibt es auch noch keinen pk.
	}
	
	
	
	/** returns an event conditiones by the given Primary Key
	 * 
	 * @author Andre´ Biegel, tarent Gmbh
	 * @return Event - the collected event
	 * @throws SQLException
	 * @param int - the primary Key(ID) of an Event
	 *
	 */
	@WebMethod()
	@Result("event")
	public Event getEventByEventPk(@Name("pk") int pk) throws SQLException {
		logger.debug("executing getEventByEventPk(int pk)");
		return EventDAO.getInstance().getEventByPk(CalendarDBAccess.getManagedContext(),pk);
	}
	
	//-----------------------------------------------------------------------------------------------------	
	//test methods	
	//-----------------------------------------------------------------------------------------------------	
		
	@WebMethod()
	@Result("resource")
	public Resource getRandomResource(OctopusContext oc) {
		
		logger.debug("executing getRandomResource(OctopusContext oc)");
		
		Resource resource = new ResourceImpl();
		resource.setNotificationMessage("Der Termin fängt gleich an!");
		resource.setParticipationStatus(2);
		resource.setText("Martin Pelzer");
//		resource.setType(1); // XXX
		
		return resource;
	}
}
