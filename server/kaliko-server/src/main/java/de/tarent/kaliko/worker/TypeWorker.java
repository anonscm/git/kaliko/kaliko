/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.worker;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;

import de.tarent.kaliko.dao.TypeDAO;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.objects.Type;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Result;


/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class TypeWorker {

	/** returns a list of the resource types stored in the database
	 * 
	 * - SQLException
	 */
	@WebMethod()
	@Result("resourceTypes")
	public List<Type> getResourceTypes() throws SQLException{
		return TypeDAO.getInstance().getAll(CalendarDBAccess.getManagedContext());
	}
	
	@WebMethod()
	@Result("resourceType")
	public Type getResourceTypeByUrn(@Name("typeUrn") String urn) throws SQLException {
		return TypeDAO.getInstance().getTypeByUrn(CalendarDBAccess.getManagedContext(), urn);
	}
	
	@WebMethod()
	@Result("resourceType")
	public Type getResourceTypeByPk(@Name("typePk") Integer pk) throws SQLException {
		return TypeDAO.getInstance().getTypeByPk(CalendarDBAccess.getManagedContext(), pk);
	}
	
}
