/*
 * Kaliko Server,
 * Implementation of the webservice based kaliko-calender-server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'Kaliko Server'
 * Signature of Elmar Geese, 26 September 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.kaliko.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.SQL;
import de.tarent.kaliko.db.CalendarDBAccess;
import de.tarent.kaliko.db.constants.DBCOccurence;
import de.tarent.kaliko.factory.OccurenceEntityFactory;
import de.tarent.kaliko.mapping.OccurenceDBMapping;
import de.tarent.kaliko.objects.Occurence;
import de.tarent.kaliko.objects.impl.OccurenceImpl;
import de.tarent.kaliko.utils.IllegalObjectCreationException;
import de.tarent.kaliko.utils.ObjectSynchronizationException;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class OccurenceDAO extends AbstractDAO {

	private static OccurenceDAO instance = null;

	
	OccurenceDBMapping mapping = new OccurenceDBMapping(CalendarDBAccess.getContextWithoutConnection());
	OccurenceEntityFactory entityFactory =  OccurenceEntityFactory.getInstance();
	
	
	private OccurenceDAO() {
        super();
        setDbMapping(mapping);
        setEntityFactory(entityFactory);
    }
	
	
	public static synchronized OccurenceDAO getInstance() {
        if (instance == null) {
            instance = new OccurenceDAO();
        }
        return instance;
    }
	
	/**These Method updates an Occurence item in the DB by firstly checking which detail is really updated. After that the newentry ist denn updated in the DB
	 * 
	 * @author Andre´ Biegel
	 * @param  newentry an new Occurence Object with maybe new details
	 * @param  old  old Occurence entry 
	 * @throws SQLException 
	 * @throws IllegalObjectCreationException indicates that the FK Event of teh newentry is not set 
	 * @throws ObjectSynchronizationError updatedate null or hole object not synchronized
	 */
	public void updateOccurence(DBContext dbc, Occurence newentry) throws SQLException, IllegalObjectCreationException, ObjectSynchronizationException{
		//x is the Not-Updated-Flag
		int x = -1;
		Occurence old = OccurenceDAO.getInstance().getOccurenceByPk(dbc,newentry.getPk());
		if (newentry.getFkEvent() == x)throw new IllegalObjectCreationException("the fk event in this Occurence´is not set");
		
		if (newentry.getUpdateDate() == null) {
			throw new ObjectSynchronizationException("updateDate in given occurence entity is null");
		}
		if (old.getUpdateDate().after(newentry.getUpdateDate()) ) {
			throw new ObjectSynchronizationException("Occurence is not synchronized");
		}		
		//check wether the pk`s are the same
		// if then error and writing the old data so that nothing is lost 
		if (newentry.getPk()!= old.getPk()) {
			insert(dbc,old);
		}else {
			//checking Occurence attributes wether they are updated or not !
			if (newentry.getFkEvent() == 0) {
				newentry.setFkEvent(old.getFkEvent());
			}
			if (newentry.getIntervalType()== x) {
				newentry.setIntervalType(old.getIntervalType());
			}
			if (newentry.getFrequency()== x) {
				newentry.setFrequency(old.getFrequency());
			}
			if (newentry.getType() == 0) {
				newentry.setType(old.getType());
			} 
			if (newentry.getUpDown()== x) {
				newentry.setUpDown(old.getUpDown());
			}
			if (newentry.getStartDate()== null ) {
				newentry.setStartDate(old.getStartDate());
			}
			if (newentry.getEndDate()== null) {
				newentry.setEndDate(old.getEndDate());
			}
//			if (newentry.getStartTime()== null) {
//				newentry.setStartTime(old.getStartTime());
//			}
//			if (newentry.getEndTime()== null ) {
//				newentry.setEndTime(old.getEndTime());
//			}
			
			// update
			this.update(dbc,newentry);	
		}
	}

	private Occurence getOccurenceByPk(DBContext dbc, int pk) throws SQLException {
	
		return (Occurence)getEntityByIdFilter(dbc, OccurenceDBMapping.STMT_SELECT_OCCURENCE_BY_PK,Occurence.PROPERTY_PK,pk);
	}


	/** Deletes an occurence by setting the deleted flag.
	 * 
	 * @author Andre´ Biegel
	 * @param dbc
	 * @param resourcePk primary key that identifies the resource to delete
	 * @throws SQLException
	 */
	
	public void deleteOccurence(DBContext dbc, List<Integer> occurencePks) throws SQLException{
		for (int i = 0; i < occurencePks.size(); i++) {
			deleteOccurence(dbc,occurencePks.get(i).intValue());
		}
	}
	protected void deleteOccurence(DBContext dbc, int occurencePk) throws SQLException{
			SQL.Update(dbc).table(DBCOccurence.TABLENAME)
			.update(DBCOccurence.DELETED, "true")
			.whereAndEq(DBCOccurence.PK_OCCURENCE, occurencePk)
			.executeUpdate(dbc);
	}

	public List<Integer> createOccurences(DBContext dbc, List<Occurence> newentries)
			throws SQLException, IllegalObjectCreationException {
		
		List<Integer> res = new LinkedList<Integer>();
		
		for (int i = 0; i < newentries.size(); i++)
			res.add(createOccurence(dbc, newentries.get(i)));
		
		return res;
	}
	
	public Integer createOccurence(DBContext dbc, Occurence newentry) throws SQLException, IllegalObjectCreationException {
		if (newentry.getFkEvent() == 0)
			throw new IllegalObjectCreationException("FkEvent not set while creating an Occurence");
		
		this.insert(dbc, newentry);
		return newentry.getPk();
	}
	
	@Override
	public void setEntityKeys(InsertKeys keys, Object entity) {
		((OccurenceImpl)entity).setPk(keys.getPk());
	}

}
