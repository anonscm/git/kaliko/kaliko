# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
#
# Author: Fabian K�ster <f.koester@tarent.de> tarent GmbH Bonn

inherit kaliko

DESCRIPTION="${DESCRIPTION} (GUI Widgets)"

SLOT="0"
KEYWORDS="x86"

S=${WORKDIR}