# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
#
# Author: Fabian K�ster <f.koester@tarent.de> tarent GmbH Bonn

inherit java-maven-2

DESCRIPTION="A Webservice-based Scheduling Framework"
HOMEPAGE="http://kaliko.evolvis.org"
SRC_URI="http://maven-repo.evolvis.org/releases/org/evolvis/kaliko/${PN}/${PV}/${P}-sources.jar

LICENSE="GPL-2"

S=${WORKDIR}